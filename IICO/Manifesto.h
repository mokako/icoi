//
//  Manifesto.h
//  ICOI
//
//  Created by moca on 2013/12/31.
//  Copyright (c) 2013年 moca. All rights reserved.
//
// manifesto.plistをマネージメントするクラスです。
#import <Foundation/Foundation.h>
#import "FileManager.h" 

@class FileManager;

@interface Manifesto : NSObject



//+(BOOL)checkManifestoConsistency:(NSDictionary *)dic service:(NSString *)serviceName;


+(BOOL)setManifestoPlist:(NSDictionary *)dic;


@end
