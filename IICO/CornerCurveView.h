//
//  CornerCurveView.h
//  ICOI
//
//  Created by mokako on 2016/03/26.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface CornerCurveView : UIView
{
    CGFloat angle;
    UIColor *color;
}


-(void)setCurveAngle:(CGFloat)_angle;
-(void)setColor:(UIColor *)_color;
-(void)setAngleWithColor:(CGFloat)_angle color:(UIColor *)_color;
@end
