//
//  PopupView.m
//  ICOI
//
//  Created by mokako on 2014/12/06.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "PopupView.h"


@implementation PopupView

- (id)init
{
    self = [super init];
    if (self) {
        self.mountHidden = NO;
        ver = Center;
        self.hidden = YES;
        self.alpha = 0.0;
        [self initializeView];
    }
    return self;
}

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:(CGRect)frame];
    if (self) {
        self.mountHidden = NO;
        ver = Center;
        self.alpha = 0.0;
        self.hidden = YES;
        [self initializeView];
    }
    return self;
}

-(void)initializeView
{
    _mount = [[UIView alloc]initWithFrame:CGRectZero];
    _mount.translatesAutoresizingMaskIntoConstraints = NO;
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hide)];
    [_mount addGestureRecognizer:tapGesture];
    _mount.hidden = self.mountHidden;
    [self addSubview:_mount];
    NSArray *mw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":_mount}];
    [self addConstraints:mw];
    
    NSArray *mh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":_mount}];
    [self addConstraints:mh];
    
    
    
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = CGRectZero;
    visualEffectView.translatesAutoresizingMaskIntoConstraints = NO;
    [_mount addSubview:visualEffectView];
    
    NSArray *vsw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":visualEffectView}];
    [_mount addConstraints:vsw];
    
    NSArray *vsh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":visualEffectView}];
    [_mount addConstraints:vsh];
    
    
    
    _blank = [[UIView alloc]initWithFrame:CGRectZero];
    _blank.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_blank];
    
    
    
    
    
    //CGRectMake(0,0,250,190)
    self.popSub = [[PopupSubview alloc]initWithFrame:CGRectZero];
    self.popSub.translatesAutoresizingMaskIntoConstraints = NO;
    self.popSub.delegate = self;
    self.popSub.hidden = YES;
    [self addSubview:self.popSub];
    
    
    NSArray *psw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]"
                                            options:0
                                            metrics:@{@"width":@250}
                                              views:@{@"view":self.popSub}];
    [self addConstraints:psw];
    
    NSArray *psh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":@190}
                                              views:@{@"view":self.popSub}];
    [self addConstraints:psh];
    NSLayoutConstraint *xConstraint = [NSLayoutConstraint constraintWithItem:self.popSub
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1
                                                                    constant:0];
    
    //垂直値の制約
    NSLayoutConstraint *yConstraint = [NSLayoutConstraint constraintWithItem:self.popSub
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1
                                                                    constant:0];
    
    [self addConstraint:xConstraint];
    [self addConstraint:yConstraint];
    
    
    
}

-(void)setView:(UIView *)object
{
    self.popSub.hidden = YES;
    _blank = [[UIView alloc]initWithFrame:CGRectZero];
    _blank.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:_blank];
    
    NSArray *psw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:object.bounds.size.width]}
                                              views:@{@"view":_blank}];
    [self addConstraints:psw];
    
    NSArray *psh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:object.bounds.size.height]}
                                              views:@{@"view":_blank}];
    [self addConstraints:psh];
    NSLayoutConstraint *xConstraint = [NSLayoutConstraint constraintWithItem:_blank
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1
                                                                    constant:0];
    
    //垂直値の制約
    NSLayoutConstraint *yConstraint = [NSLayoutConstraint constraintWithItem:_blank
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1
                                                                    constant:0];
    
    [self addConstraint:xConstraint];
    [self addConstraint:yConstraint];
    [_blank addSubview:object];
    
}

-(void)setMountColor:(UIColor *)mountColor{
    _mount.backgroundColor = mountColor;
}

-(void)popupview:(event)eventState{
    [self.delegate popupview:eventState];
}

-(CGFloat)verticalFloat:(vertical)vertical{
    CGFloat f;
    switch (vertical) {
        case Top:
            f = 0;
            break;
        case Topmiddle:
            f = 0.25;
            break;
        case Center:
            f = 0.5;
            break;
        case Centermiddle:
            f = 0.75;
            break;
        case Under:
            f = 1;
            break;
        default:
            break;
    }
    return f;
}

-(void)setVertical:(vertical)vertical{
    ver = vertical;
}

-(void)show{
    self.hidden = NO;
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.alpha = 1.0;
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

-(void)hide{
    [UIView animateWithDuration:0.3f
                     animations:^{
                         self.alpha = 0.0;
                     }
                     completion:^(BOOL finished){
                         self.hidden = YES;
                     }];
}

@end
