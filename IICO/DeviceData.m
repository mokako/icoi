//
//  DeviceData.m
//  ICOI
//
//  Created by mokako on 2015/09/21.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "DeviceData.h"

@implementation DeviceData

#pragma mark -orientation
+(BOOL)deviceOrientation{
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    switch (orientation) {
        case UIInterfaceOrientationPortrait:
        {
            return YES;
        }
            break;
        case UIInterfaceOrientationPortraitUpsideDown:
        {
            return YES;
            
        }
            break;
        case UIInterfaceOrientationLandscapeLeft:
        {
            return NO;
        }
            break;
        case UIInterfaceOrientationLandscapeRight:
        {
            return NO;
        }
            break;
        default:
            return NO;
            break;
    }
}


+(CGRect)deviceMainScreen{
    return [[UIScreen mainScreen] bounds];
}



@end
