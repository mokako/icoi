//
//  ServiceListTableCell.m
//  ICOI
//
//  Created by mokako on 2016/03/19.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ServiceListTableCell.h"

@implementation ServiceListTableCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = [UIColor colorWithHex:@"FFFFFF" alpha:0.0];
}


-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted){
        self.base.backgroundColor = [UIColor colorWithHex:@"34383B" alpha:0.5];
    }else{
        self.base.backgroundColor = [UIColor colorWithHex:@"34383B" alpha:0.3];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected){
        self.base.backgroundColor = [UIColor colorWithHex:@"34383B" alpha:0.5];
    }else{
        self.base.backgroundColor = [UIColor colorWithHex:@"34383B" alpha:0.3];
    }
    // Configure the view for the selected state
}

@end
