//
//  PickupCell.m
//  ICOI
//
//  Created by mokako on 2016/03/11.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "PickUpCell.h"

@implementation PickUpCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.backgroundColor = [UIColor colorWithHex:@"FFFFFF" alpha:0.0];
    // Initialization code
    //[self.spot addTarget:self action:@selector(touch:) forControlEvents:UIControlEventTouchUpInside];
    
    self.stateIcon.textColor = [UIColor whiteColor];
    self.stateIcon.layer.cornerRadius = 12.0;
    self.title.layer.cornerRadius = 5;
    
    fontSize = 15.0;
    
    colorList = @[
                  RGBA(145,145,145,0.5),
                  RGBA(88,179,49,0.5),
                  RGBA(0,159,232,0.5),
                  RGBA(236,111,0,0.5),
                  RGBA(240,96,96,0.5)
                  ];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted){
        self.backgroundColor = [UIColor colorWithHex:@"bcbcbc" alpha:0.5];
    }else{
        self.backgroundColor = [UIColor colorWithHex:@"efefef" alpha:0.0];
    }
}








-(void)setIconState:(NSInteger)state{
    self.stateIcon.textColor = colorList[state];
    switch (state) {
        case 0://no comment path
        {
            self.stateIcon.text = @"4";

        }
            break;
        case 1:// comment path
        {
            self.stateIcon.text = @"4";
        }
            break;
        case 2:// title
        {
            self.stateIcon.text = @"n";
            
        }
            break;
        case 3:// sub
        {
            self.stateIcon.text = @"d";
        }
            break;
        case 4:// message
        {
            self.stateIcon.text = @"j";
        }
            break;
        default:
            break;
    }
}




-(void)setArticleText:(NSString *)str{
    self.article.text = str;
    [self.article setNeedsLayout];
}

@end
