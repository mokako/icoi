//
//  SketchListTabView.h
//  ICOI
//
//  Created by mokako on 2016/03/22.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
@interface SketchListTabView : UIView
{
    CGPoint oldPoint;
    BOOL isSlide;
}
@property (weak, nonatomic) IBOutlet UILabel *tab;

+ (instancetype)view;
@end
