//
//  DeviceData.h
//  ICOI
//
//  Created by mokako on 2015/09/21.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DeviceData : NSObject
+(BOOL)deviceOrientation;
+(CGRect)deviceMainScreen;
@end
