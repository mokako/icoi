//
//  DataListManager.h
//  ICOI
//
//  Created by mokako on 2016/03/21.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICOI.h"

@interface DataListManager : NSObject
{
    NSString *path;
    NSMutableDictionary *dataList;
}
@property (nonatomic) NSString *service;
@property (nonatomic) NSMutableDictionary *dataInfo;
@property (nonatomic) NSMutableDictionary *settingInfo;
@property (nonatomic) NSString *version;
@property (nonatomic) NSInteger versionNum;

-(void)setServiceName:(NSString *)name;
-(void)initDataList;
-(BOOL)save;
@end
