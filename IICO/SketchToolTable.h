//
//  SketchToolTable.h
//  ICOI
//
//  Created by mokako on 2014/03/11.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@protocol SketchToolTableDelegate <NSObject>
-(void)setIndexPathRow:(NSInteger)stat;
@end

@interface SketchToolTable : UIView<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>
{
    UITableView *sketchTool;
    NSArray *cellItem;
    BOOL sketchFlag;
}

-(void)review;
@property (nonatomic, weak) id <SketchToolTableDelegate> delegate;
@end
