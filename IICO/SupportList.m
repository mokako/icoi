//
//  SupportList.m
//  ICOI
//
//  Created by mokako on 2015/04/07.
//  Copyright (c) 2015年 moca. All rights reserved.
//
/*
 menuの項目数を増やした場合SupportMenuにも反映させて下さい。
 
 */
#import "SupportList.h"
#import "CustomCell.h"

@implementation SupportList
- (id)init
{
    self = [super init];
    if (self) {
        // Initialization code
        
        CGRect r = [DeviceData deviceMainScreen];
        
        self.frame = CGRectMake(r.size.width,0,r.size.width,r.size.height);
        self.backgroundColor = RGBA(255,255,255,1.0);
        self.clipsToBounds = YES;

        
        menuList = [[NSArray alloc]initWithObjects:
                      @"The first",
                      NSLocalizedString(@"Board",@""),
                      NSLocalizedString(@"Flip",@""),
                      NSLocalizedString(@"Theme",@""),
                      NSLocalizedString(@"Album",@""),
                      NSLocalizedString(@"Link",@""),
                      NSLocalizedString(@"Exit",@""),
                      @"Q&A"
                      , nil];
        
        NSBundle* bundle = [NSBundle mainBundle];
        //読み込むファイルパスを指定
        NSString* path = [bundle pathForResource:@"Config" ofType:@"plist"];
        NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
        list = [[NSArray alloc]initWithArray:[dic objectForKey:@"DescriptionList"]];
        
        [self initializeView];
        
    }
    return self;
}

-(void)initializeView{
    //toolbar setting
    tool = [[UIToolbar alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,40)];
    tool.backgroundColor = RGBA(230,230,230,0.0);
    [self addSubview:tool];
    gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0,39.5,self.frame.size.width,0.5);
    gradient.colors = @[
                        (id)[UIColor colorWithHex:@"#70e1f5"].CGColor,
                        (id)[UIColor colorWithHex:@"#ffd194"].CGColor
                        ];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1,0);
    [self.layer addSublayer:gradient];
    
    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = -15;
    
    
    UIButton *done = [[UIButton alloc]initWithFrame:CGRectMake(0,0,60,40)];
    [done addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    done.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Regular" size:14.0];
    [done setTitle:NSLocalizedString(@"Close",@"") forState:UIControlStateNormal];
    [done setTitleColor:RGBA(242,56,39,1.0) forState:UIControlStateNormal];
    UIBarButtonItem *item0 = [[UIBarButtonItem alloc]initWithCustomView:done];
    
    
    UILabel *setting = [[UILabel alloc]initWithFrame:CGRectMake(0,0,100,40)];
    setting.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:14.0];
    setting.textAlignment = NSTextAlignmentCenter;
    setting.text = NSLocalizedString(@"icoi-support",@"");
    setting.textColor = RGBA(45,45,45,1.0);
    UIBarButtonItem *item1 = [[UIBarButtonItem alloc]initWithCustomView:setting];
    
    
    tool.items = [NSArray arrayWithObjects:fixedSpace,item0,gap,item1,gap,nil];
    
    
    //tableview setting
    table = [[UITableView alloc]initWithFrame:CGRectMake(0,40,self.frame.size.width,self.frame.size.height - 40) style:UITableViewStyleGrouped];
    table.delegate = self;
    table.dataSource = self;
    table.backgroundColor = RGBA(240,240,240,1.0);
    table.rowHeight = 40.0;
    table.sectionHeaderHeight = 30;
    [self addSubview:table];
    
}
-(void)update{
    CGRect r = [DeviceData deviceMainScreen];
    if(self.frame.origin.x == 0){
        //開いている
        self.frame = CGRectMake(0,0,r.size.width,r.size.height);
    }else{
        //閉じている
        self.frame = CGRectMake(r.size.width,0,r.size.width,r.size.height);
    }
    tool.frame = CGRectMake(0,0,self.frame.size.width,40);
    table.frame = CGRectMake(0,40,self.frame.size.width,self.frame.size.height - 40);
    gradient.frame = CGRectMake(0,39.5,self.frame.size.width,0.5);
}
-(void)fadeIn{
    self.hidden = NO;
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    } completion:^(BOOL finished){
    }];
}
-(void)fadeOut{
    [UIView animateWithDuration:0.3 animations:^{
        self.frame = CGRectMake(self.frame.size.width,0,self.frame.size.width,self.frame.size.height);
    } completion:^(BOOL finished){
        self.hidden = YES;
    }];
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return menuList[section];
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 40;
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return list.count;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  ((NSArray *)list[section]).count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(56,166,3,1.0) selectColor:RGBA(105,105,105,0.9) defaultBack:RGBA(255,255,255,1.0) selectBack:RGBA(245,245,245,1.0)];
    }
    NSString *str = [NSString stringWithFormat:@"operation-description%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    
    cell.textLabel.text = NSLocalizedString(str,@"");
    cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    cell.textLabel.baselineAdjustment = UIBaselineAdjustmentAlignCenters;
    cell.textLabel.textAlignment = NSTextAlignmentCenter;

    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    //[smenu setMenuDescription:indexPath];
}




@end
