//
//  ExchageHeaderView.h
//  ICOI
//
//  Created by mokako on 2016/03/15.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ExchageHeaderView : UIView

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *cancel;
@property (weak, nonatomic) IBOutlet UIButton *undo;
@property (weak, nonatomic) IBOutlet UIButton *ok;
+(instancetype)view;
@end
