//
//  DescriptionSetter.h
//  ICOI
//
//  Created by mokako on 2015/07/11.
//  Copyright (c) 2015年 moca. All rights reserved.
//
#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "SupportList.h"
#import "SetDescHTML.h"
#import "UIColor+Hex.h"

#import "DescriptionSetterCell.h"

//#import "PlayerView.h"

typedef NS_ENUM(NSInteger, descriptionMode) {
    desc_normal = 0,
    desc_base = 1,
    desc_account = 2,
    desc_about = 3,
    desc_howto = 4,
    desc_policy = 5,
    desc_shooting = 6
};


@protocol DescriptionSetterDelegate <NSObject>
@end

@interface DescriptionSetter : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *sectionList;
    SupportList *support;
    SetDescHTML *setHTML;
    descriptionMode mode;
    
    //PlayerView *av;
}

@property (weak, nonatomic) IBOutlet UITableView *table;

@property (nonatomic, weak)id <DescriptionSetterDelegate>delegate;

+ (instancetype)view;
@end