//
//  ChatList.h
//  ICOI
//
//  Created by mokako on 2015/11/27.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "MessageCell.h"
#import "ServiceManager.h"
@interface ChatList : UIView<UITableViewDataSource,UITableViewDelegate>
{
    ChatData *cd;
    NSMutableArray *list;
    NSMutableArray *chatList;
    UITableView *table;
    ServiceManager *sb;
    NSInteger pageCount;
    NSString *colorHex;
    NSString *mainColorHex;
    NSDictionary *selectPath;
    
    UIFont *defaultFont;
}
-(void)setList:(NSArray *)pathList;
@end
