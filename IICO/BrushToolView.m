//
//  BrushToolView.m
//  ICOI
//
//  Created by mokako on 2015/10/10.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "BrushToolView.h"

@implementation BrushToolView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.hidden = YES;
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.layer.borderColor = [UIColor colorWithHex:@"cccccc"].CGColor;
        self.layer.borderWidth = 0.5;
        isToggle = NO;
        isFirst = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fadeOut) name:@"fadeOut" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.line0.layer.cornerRadius = 20;
    self.line1.layer.cornerRadius = 20;
    selectButton = self.line0;
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"Config" ofType:@"plist"];
    NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
    list = [NSArray arrayWithArray:[dic objectForKey:@"OtherLineWidth"]];
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = self.base.bounds;
    [self.base insertSubview:visualEffectView atIndex:0];
    
    
    UINib *nib = [UINib nibWithNibName:@"ToolCell" bundle:nil];
    [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];
    self.table.dataSource = self;
    self.table.estimatedRowHeight = 40.0;
    self.table.delegate = self;
    
    
    [self.fade addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
}


-(void)set{
    UIView *superView = [self superview];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:-self.frame.size.width],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:self.frame.size.height]}
                                              views:@{@"view":self}];
    [superView addConstraints:h];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:self  attribute:NSLayoutAttributeCenterY  relatedBy:NSLayoutRelationEqual  toItem:superView  attribute:NSLayoutAttributeCenterY  multiplier:1  constant:0]];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}



#pragma mark - table

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ToolCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    if(isFirst && indexPath.row == 0){
        cell.base.backgroundColor = [UIColor colorWithHex:@"1be2e5"];
        view = cell.base;
        isFirst = NO;
    }
    [cell setWidth:[list[indexPath.row] floatValue]];
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    view.backgroundColor = RGBA(255,255,255,0.5);
    ToolCell *cell = (ToolCell *)[tableView cellForRowAtIndexPath:indexPath];
    cell.base.backgroundColor = [UIColor colorWithHex:@"1be2e5"];
    view = cell.base;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@2,@"state":@(indexPath.row)}];
    
}

-(IBAction)brushPaint:(UIButton *)sender{
    selectButton.backgroundColor = [selectButton.backgroundColor colorWithAlphaComponent:0.0];
    sender.backgroundColor = [selectButton.backgroundColor colorWithAlphaComponent:0.5];
    selectButton = sender;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@3,@"state":@(sender.tag)}];
}







#pragma mark - display state change

-(void)toggle{
    if(isToggle){
        [self fadeOut];
    }else{
        isToggle = YES;
        [self fadeIn];
    }
}

-(void)fadeIn{
    self.hidden = NO;
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:80],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    }];
}


-(void)fadeOut{
    isToggle = NO;
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:-self.frame.size.width],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        self.hidden = YES;
    }];
}



#pragma mark - send chage state data






@end
