//
//  ICOI.h
//  ICOI
//
//  Created by mokako on 2015/06/19.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
@import MultipeerConnectivity;
#import "FileManager.h"
#import "DataManager.h"
#import "FMDatabaseQueue.h"
#import "FMDatabaseAdditions.h"
#import "DateFormatter.h"
#import "ICOIImage.h"
#import "ChatData.h"
#import "DeviceData.h"
#import "UIColor+Hex.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
#define CGpointSum(a,b) CGPointMake(b.x - a.x,b.y - a.y)
#define CGSizeScale(a,b) CGSizeMake(a.width * b,a.height * b)
#define requestMaxCount 2

#define MAX_LENGTH 1000
#define MIN_LENGTH 1

#ifdef DEBUG
# define LOG(...) NSLog(__VA_ARGS__)
# define LOG_METHOD NSLog(@"%s", __func__)
#else
# define LOG(...) ;
# define LOG_METHOD ;
#endif


#define PickupMinHeight 80
#define PickupMaxLists 10


//pickerView
#define pickerQuality 1.0

@interface ICOI : NSObject

@end
