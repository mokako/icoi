//
//  NewTitleHeaderView.h
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewTitleHeaderView : UIView
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
+(instancetype)view;
@end
