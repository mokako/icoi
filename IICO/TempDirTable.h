//
//  TempDirTable.h
//  ICOI
//
//  Created by mokako on 2014/07/25.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ConvertFileView.h"
#import "ClearToolBar.h"
#import "CustomCell.h"
#import "FileManager.h"


@protocol TempDirTableDelegate <NSObject>

@end

@interface TempDirTable : UIView<UITableViewDataSource, UITableViewDelegate,ConvertFileViewDelegate>
{
    NSMutableArray *fileList;
    NSMutableArray *fileAttribute;
    UITableView *table;
    UIToolbar *toolbar;
    NSArray *colorArray;
    ConvertFileView *convert;
}
@property (nonatomic) NSString *serviceName;
@property (nonatomic, weak) id<TempDirTableDelegate>delegate;

-(void)review;
-(void)loadTmp;
-(void)getFileData; //TEST
-(void)removeAllLists;
@end
