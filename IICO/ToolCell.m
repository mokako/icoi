//
//  ToolCell.m
//  ICOI
//
//  Created by mokako on 2015/10/10.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "ToolCell.h"

@implementation ToolCell

- (void)awakeFromNib {
    // Initialization code
    self.base.layer.cornerRadius = 15;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setWidth:(CGFloat)width{
    NSArray *w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:width]}
                                              views:@{@"view":self.view}];
    [self addConstraints:w];
    dispatch_async(dispatch_get_main_queue(),^{
        [self layoutIfNeeded];
    });
    
}

@end
