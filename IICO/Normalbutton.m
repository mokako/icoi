//
//  Normalbutton.m
//  ICOI
//
//  Created by mokako on 2016/03/27.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "NormalButton.h"

@implementation NormalButton

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {


    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


-(void)setButtonState:(BOOL)state{
    if(state == YES){
        self.layer.borderColor = [UIColor colorWithHex:@"FFFFFF" alpha:1.0].CGColor;
    }else{
        self.layer.borderColor = [UIColor colorWithHex:@"FFFFFF" alpha:0.0].CGColor;
    }
}

@end
