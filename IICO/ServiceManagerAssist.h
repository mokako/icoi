//
//  ServiceManagerAssist.h
//  ICOI
//
//  Created by mokako on 2016/01/16.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, editMode) {
    edit_none = -1, //例外
    edit_order  = 0, //順序変更
    edit_pre_exchange = 1,
    edit_exchange  = 2, //所属変更step1
    edit_exchange2 = 3, //所属変更step2
    edit_exchange3 = 4, //所属変更step3
    edit_rename = 5,//リネーム
    edit_exclusion  = 6, //除外
    edit_delete = 7,
    edit_display  = 8, //反映
    edit_restore = 9,//復活
    edit_normal = 99
};
/*
 titleID = @[
 NSLocalizedString(@"Board",@""),
 NSLocalizedString(@"Flip",@""),
 NSLocalizedString(@"Theme",@""),
 NSLocalizedString(@"Album",@""),
 NSLocalizedString(@"Network",@""),
 @"System"
 ];
 */
typedef NS_ENUM(NSInteger, message_type) {
    message_type_board = 0,
    message_type_flip = 1,
    message_type_thme = 2,
    message_type_album = 3,
    message_type_network = 4,
    message_type_system = 5
};

typedef NS_ENUM(NSInteger, message_state) {
    message_state_negative = 0,
    message_state_confirm = 1,
    message_state_positive = 2
};

@interface ServiceManagerAssist : NSObject{



    
}



+(NSDictionary *)adjustDictionary:(NSDictionary *)oldDic;
+(NSArray *)initIndexPath:(NSArray *)ary;
@end
