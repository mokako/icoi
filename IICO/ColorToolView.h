//
//  ColorToolView.h
//  ICOI
//
//  Created by mokako on 2015/10/10.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ColorListCell.h"

@interface ColorToolView : UIView<UICollectionViewDataSource,UICollectionViewDelegate>
{
    NSArray *w;
    NSArray *h;
    BOOL isToggle;
    NSArray *list;
    NSInteger mode;
    NSIndexPath *selectColor;
    NSInteger color1;
}


@property (weak, nonatomic) IBOutlet UIButton *fade;
@property (weak, nonatomic) IBOutlet UIView *opacityBase;
@property (weak, nonatomic) IBOutlet UILabel *glasses;
@property (weak, nonatomic) IBOutlet UICollectionView *collection;


+(instancetype)view;
-(void)set;
-(void)toggle;
-(void)fadeOut;
@end
