//
//  PickUp.m
//  ICOI
//
//  Created by mokako on 2016/01/03.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "PickUp.h"

@implementation PickUp
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        //self.clipsToBounds = YES;
        //self.alpha = 0.0;
        add = [@[] mutableCopy];
        cellHeight = [@[] mutableCopy];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"setPickUp" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPath:) name:@"addPath" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setItem) name:@"setItem" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServiceManager:) name:@"sendServiceManagerSeed" object:nil];
        
        [self performSelector:@selector(delayProc) withObject:nil afterDelay:4];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UINib *nib = [UINib nibWithNibName:@"PickUpCell2" bundle:nil];
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.rowHeight = UITableViewAutomaticDimension;
    self.table.estimatedRowHeight = 103.0;
    [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];
    dummyCell = [self.table dequeueReusableCellWithIdentifier:@"Cell"];
    fixedHeight = (dummyCell.frame.size.height) - dummyCell.article.frame.size.height;
}

-(void) layoutSubviews {
    [super layoutSubviews];
    [self delayProc];
    superView = [self superview];
    if(self.frame.size.height > (superView.frame.size.height - 50)){
        animation = YES;
        [superView removeConstraints: vartical];
        vartical =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                                options:0
                                                metrics:@{@"height":[NSNumber numberWithFloat:superView.bounds.size.height - 50]}
                                                  views:@{@"view":self}];
        [superView addConstraints:vartical];
        [UIView animateWithDuration:0.5 animations:^{
            [superView layoutIfNeeded];
        } completion:^(BOOL comp){
            animation = NO;
        }];
        
    }
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

-(void)setServiceManager:(NSNotification *)center{
    sb = [center userInfo][@"manager"];
}

-(void)delayProc{
    dummyCell.frame = CGRectMake(0,0,self.table.contentSize.width,dummyCell.frame.size.height);
    [dummyCell layoutIfNeeded];
}

-(void)addPath:(NSNotification *)center{
    if([sb.selectFile isEqualToString:[center userInfo][@"path"][@"fid"]]){
        /*
         表示可能なデータかどうかを調べる。
         親要素が除外されている場合
         
         
         */
        dispatch_async(dispatch_get_main_queue(),^{
            [self.table beginUpdates];
            if(add.count >= PickupMaxLists){
                [add removeLastObject];
                NSIndexPath *deletePath = [NSIndexPath indexPathForRow:add.count - 1 inSection:0];
                [self.table deleteRowsAtIndexPaths:@[deletePath] withRowAnimation:UITableViewRowAnimationNone];
            }
            [add insertObject:[center userInfo][@"path"] atIndex:0];
            NSIndexPath *path = [NSIndexPath indexPathForRow:0 inSection:0];
            [self.table insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
            [self.table endUpdates];
        });
    }
}


#pragma mark - table data

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.row == 0){[cellHeight removeAllObjects];}
    dummyCell.article.text = [add[indexPath.row][@"comment"] length] == 0 ? @"Add line" : add[indexPath.row][@"comment"];
    CGSize si = [dummyCell.article sizeThatFits:CGSizeMake(dummyCell.article.frame.size.width,9999)];
    CGFloat height = ceilf(si.height) + fixedHeight;
    [cellHeight addObject:[NSNumber numberWithFloat:height]];
    return height;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [cellHeight[indexPath.row] floatValue];
}







-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return add.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [self setCellData:indexPath];
}

-(PickUpCell2 *)setCellData:(NSIndexPath *)indexPath{
    PickUpCell2 *cell = [self.table dequeueReusableCellWithIdentifier:@"Cell"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.tag = indexPath.row;
    cell.name.text = add[indexPath.row][@"pname"];
    [cell setArticleText:[add[indexPath.row][@"comment"] isEqualToString:@""] ? @"Add line" : add[indexPath.row][@"comment"]];
    if(![add[indexPath.row][@"pauid"] isEqual:[NSNull null]]){
        dispatch_async(dispatch_get_main_queue(),^{
            NSDictionary *dic = [sb getItemToList:add[indexPath.row][@"pauid"]];
            cell.title.text = dic[@"comment"];
        });
    }else{
        cell.title.text = @"Top";
    }
    [cell setIconState:[add[indexPath.row][@"state"] integerValue]];
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self setHashToMessageBoard:add[indexPath.row] board:YES];
}


-(void)setHashToMessageBoard:(NSDictionary *)item board:(BOOL)open{
    NSDictionary *dic = @{@"type":@3,@"item":item ? item : [NSNull null],@"board":[NSNumber numberWithBool:open]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:dic];
}


#pragma mark - touch

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    oldPoint = currentPoint;
    isSlide = NO;
    superView = [self superview];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesMoved:touches withEvent:event];
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    CGFloat dif = currentPoint.y - oldPoint.y;
    //しきい値を設定
    if((self.bounds.size.height - dif) > 50){
        isSlide = YES;
        [superView removeConstraints: vartical];
        vartical =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                                options:0
                                                metrics:@{@"height":[NSNumber numberWithFloat:self.bounds.size.height - dif]}
                                                  views:@{@"view":self}];
        [superView addConstraints:vartical];
        [superView layoutIfNeeded];
    }else{
        [self procCloseView];
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[self layoutIfNeeded];
    if(self.bounds.size.height <= 50){
        [self procCloseView];
    }
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    //[self layoutIfNeeded];
    if(self.bounds.size.height <= 50){
        [self procCloseView];
    }
}


#pragma mark - nsconstraint
-(void)setLayout:(NSArray *)layout{
    vartical = layout;
    //[self performSelector:@selector(reConstraints) withObject:nil afterDelay:0];
}

#pragma mark - notification
-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //pickup
        {
            
        }
            break;
        case 1:
        {//clear list
            
        }
            break;
        case 10:
        {
            [self openView];
        }
            break;
        case 20:
        {
            if(!animation){
                if([[center userInfo][@"y"] floatValue] > 0 ){
                    superView = [self superview];
                    [superView removeConstraints: vartical];
                    vartical =
                    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                                            options:0
                                                            metrics:@{@"height":[NSNumber numberWithFloat:self.bounds.size.height + [[center userInfo][@"y"] floatValue]]}
                                                              views:@{@"view":self}];
                    [superView addConstraints:vartical];
                    [superView layoutIfNeeded];
                }else if((self.bounds.size.height + [[center userInfo][@"y"] floatValue]) > PickupMinHeight){
                    
                    superView = [self superview];
                    [superView removeConstraints: vartical];
                    vartical =
                    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                                            options:0
                                                            metrics:@{@"height":[NSNumber numberWithFloat:self.bounds.size.height + [[center userInfo][@"y"] floatValue]]}
                                                              views:@{@"view":self}];
                    [superView addConstraints:vartical];
                    [superView layoutIfNeeded];
                }else if((self.bounds.size.height + [[center userInfo][@"y"] floatValue]) < PickupMinHeight){
                    [self procCloseView];
                }
            }
        }
            break;
        case 21:
        {
        }
            break;
        default:
            break;
    }
}






-(void)setItem{
    [add removeAllObjects];
    [self.table reloadData];
}




#pragma mark  - window method

-(void)procCloseView{
    animation = YES;
    superView = [self superview];
    [superView removeConstraints: vartical];
    vartical =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":@0}
                                              views:@{@"view":self}];
    [superView addConstraints:vartical];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        animation = NO;
    }];
}

-(void)openView{
    animation = YES;
    superView = [self superview];
    [superView removeConstraints: vartical];
    vartical =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":@100}
                                              views:@{@"view":self}];
    [superView addConstraints:vartical];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        animation = NO;
    }];
}

@end
