//
//  PageListTable.h
//  ICOI
//
//  Created by mokako on 2014/09/05.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "DataManager.h"
#import "CustomUILabel.h"



@protocol PageListTableDelegate <NSObject>
-(void)setPage:(NSInteger)pages page:(NSInteger)page;
@end


@interface PageListTable : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *strObjects;
    NSMutableArray *viewObjects;
    UITableView *table;
    CGFloat scale;
    UIView *mview;
    CGRect baseRect;
}

@property (nonatomic) CGFloat height;
@property (nonatomic) CGFloat width;
@property (nonatomic) UIFont *font;
@property (nonatomic) NSInteger lineCount;
@property (nonatomic) NSInteger pages;







@property (nonatomic, weak) id<PageListTableDelegate>delegate;
-(void)review;
-(void)setTableState:(NSArray *)strArray;


//TEST
-(void)setLog;
@end
