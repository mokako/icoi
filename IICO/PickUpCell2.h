//
//  PickUpCell2.h
//  ICOI
//
//  Created by mokako on 2016/03/30.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface PickUpCell2 : UITableViewCell
{
    NSArray *colorList;
    NSArray *titleList;
    CGFloat fontSize;
}

@property (weak, nonatomic) IBOutlet UILabel *stateIcon;
@property (weak, nonatomic) IBOutlet UILabel *stateTitle;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *article;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UIView *fr;


-(void)setArticleText:(NSString *)str;
-(void)setIconState:(NSInteger)state;
@end
