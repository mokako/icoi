//
//  DateFormatter.h
//  ICOI
//
//  Created by mokako on 2015/06/19.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DateFormatter : NSObject

+(NSDate *)getNow;
+(NSString *)dateFormatter:(NSDate *)date;
+(NSInteger)dateFormatterWithInteger:(NSDate *)date;
+(NSString *)beforeDateTimeString:(NSString *)dateTime;
+(NSDate *)localTime:(NSString *)time;
+(NSString *)dateToString:(NSString *)time state:(NSInteger)state;
+(NSInteger)beforeDateTimeState:(NSString *)dateTime;
+(NSString *)beforeDateTimeFromDate:(NSDate *)dateTime;
+(NSTimeInterval)julianDate:(NSDate *)date;
+(NSDate *)dateFromJulian:(double)julianDays;
+(NSString *)customDataForJulian:(double)julianDays;
@end
