//
//  LogViewCell.m
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "LogViewCell.h"

@implementation LogViewCell

- (void)awakeFromNib {
    // Initialization code
    self.re.transform = CGAffineTransformMakeRotation(M_PI); 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
