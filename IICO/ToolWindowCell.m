//
//  ToolWindowCell.m
//  ICOI
//
//  Created by mokako on 2016/02/03.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ToolWindowCell.h"

@implementation ToolWindowCell

- (void)awakeFromNib {
    // Initialization code
    self.backgroundColor = RGBA(0,0,0,0);
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected){
        self.title.textColor = [UIColor colorWithHex:@"99C630"];
        self.icon.textColor = [UIColor colorWithHex:@"99C630"];
    }else{
        self.title.textColor = [UIColor colorWithHex:@"2E97D8"];
        self.icon.textColor = [UIColor colorWithHex:@"2E97D8"];
    }
    // Configure the view for the selected state
}

@end
