//
//  FooterCurveView.m
//  ICOI
//
//  Created by mokako on 2016/03/26.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "FooterCurveView.h"

@implementation FooterCurveView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.color = [UIColor colorWithHex:@"EFEFF4"];
        self.angle = 10.0;
    }
    return self;
}



- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(c, self.color.CGColor) ;
    CGContextBeginPath(c);
    
    // 点Aに移動
    CGContextMoveToPoint(c, CGRectGetMinX(rect), CGRectGetMinY(rect));
    
    CGContextAddLineToPoint(c, CGRectGetMaxX(rect), CGRectGetMinY(rect)) ;

    CGContextAddArcToPoint(c, CGRectGetMaxX(rect), CGRectGetMaxY(rect),
                           CGRectGetMidX(rect), CGRectGetMaxY( rect ),
                           self.angle);
    CGContextAddArcToPoint(c, CGRectGetMinX(rect), CGRectGetMaxY(rect), CGRectGetMinX(rect), CGRectGetMidY(rect), self.angle);
    CGContextClosePath(c);
    CGContextFillPath(c) ;
    
}

@end
