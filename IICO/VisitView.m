//
//  VisitView.m
//  ICOI
//
//  Created by mokako on 2016/03/23.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "VisitView.h"

@implementation VisitView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [[self.signin layer]setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
    [[self.signin layer]setBorderWidth:0.5];
    [self.signin setTitle:NSLocalizedString(@"view_visit_signup",@"SIGN UP") forState:UIControlStateNormal];
    
    
    [[self.login layer]setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
    [[self.login layer]setBorderWidth:0.5];
    [self.login setTitle:NSLocalizedString(@"view_visit_login",@"LOG IN") forState:UIControlStateNormal];
    
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


@end
