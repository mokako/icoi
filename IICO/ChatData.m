//
//  ChatData.m
//  ICOI
//
//  Created by mokako on 2015/09/18.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "ChatData.h"

@implementation ChatData
-(id)init{
    self = [super init];
    if(self){
        [self setup];
        
    }
    return self;
}
/*
 chat data
 
 
 displayName
 displayUUID
 comment
 hash //ハッシュ情報
 phash //親ハッシュ
 child //子の有無
 state //ステータス（未実装ですが設置のみ)
 date
 
 
 */

-(void)setup{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"chat.db"];
    NSFileManager *fm = [NSFileManager defaultManager];
    if(![fm fileExistsAtPath:path]){
        db = [FMDatabase databaseWithPath:path];
        //カラムを生成
        NSString *sql = @"create table if not exists `chat`(displayName text, displayUUID text, comment text, hash text, phash text, child bool, state integer, date real);";
        [db open];
        [db executeUpdate:sql];
        [db close];
    }else{
        db = [FMDatabase databaseWithPath:path];
    }
}



#pragma mark -chat database

-(NSArray *)getChat:(NSInteger)offset limit:(NSInteger)limit{
    NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSString *sql = @"SELECT displayName, displayUUID, comment, hash, phash, child, state, date FROM `chat` ORDER BY date ASC limit ? offset ?";
    //order by id desc
    [db open];
    FMResultSet *rs = [db executeQuery:sql,[NSNumber numberWithInteger:limit],[NSNumber numberWithInteger:offset]];
    while ([rs next]) {
        NSDictionary *dic = @{
                              @"displayName" : [rs stringForColumn:@"displayName"],
                              @"displayUUID" : [rs stringForColumn:@"displayUUID"],
                              @"comment"     : [rs stringForColumn:@"comment"],
                              @"hash"        : [rs stringForColumn:@"hash"],
                              @"phash"        : [rs stringForColumn:@"phash"] == nil ? [NSNull null] : [rs stringForColumn:@"phash"],
                              @"child"       : [NSNumber numberWithBool:[rs boolForColumn:@"child"]],
                              @"state"        : [NSNumber numberWithInt:[rs intForColumn:@"state"]],
                              @"date"        : [NSNumber numberWithDouble:[rs doubleForColumn:@"date"]],
                              };
        [ary addObject:dic];
    }
    [rs close];
    [db close];
    return ary;
}


-(NSDate *)dateToJulian:(double)julianDays{
    NSTimeInterval seconds = (julianDays - 2440587.50000) * 86400.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    return date;
}
-(void)inserChat:(NSDictionary *)dic{
    [db open];
    [db executeUpdate:@"insert into `chat`(displayName,displayUUID,comment,hash,phash,child,state,date) values(?,?,?,?,?,?,?,julianday(?))",
     dic[@"displayName"],dic[@"displayUUID"],dic[@"comment"],dic[@"hash"],dic[@"phash"],dic[@"child"],dic[@"state"],dic[@"date"]];
    [db close];

}
//参照された場合
-(void)updateToChild:(NSString *)hash{
    BOOL state = YES;
    NSString *sql = @"update `chat` set child = ? where hash = ?";
    [db open];
    [db executeUpdate:sql,[NSNumber numberWithBool:state],hash];
    [db close];
}

-(NSInteger)getChatCount{
    [db open];
    NSInteger count = [db intForQuery:@"select count(`displayName`) FROM `chat`;"];
    [db close];
    return count;
}



-(NSArray *)getHash:(NSString *)parentHash{
    NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSString *sql = @"SELECT displayName, displayUUID, comment, hash, phash, child, state, date FROM `chat` where hash = ? OR phash = ?";
    [db open];
    FMResultSet *rs = [db executeQuery:sql,parentHash,parentHash];
    
    while ([rs next]) {
        NSDictionary *dic = @{
                              @"displayName" : [rs stringForColumn:@"displayName"],
                              @"displayUUID" : [rs stringForColumn:@"displayUUID"],
                              @"comment"     : [rs stringForColumn:@"comment"],
                              @"hash"        : [rs stringForColumn:@"hash"],
                              @"phash"        : [rs stringForColumn:@"phash"] == nil ? [NSNull null] : [rs stringForColumn:@"phash"],
                              @"child"       : [NSNumber numberWithBool:[rs boolForColumn:@"child"]],
                              @"state"        : [NSNumber numberWithInt:[rs intForColumn:@"state"]],
                              @"date"        : [NSNumber numberWithDouble:[rs doubleForColumn:@"date"]],
                              };
        [ary addObject:dic];
    }
    
    [rs close];
    [db close];
    return ary;
}

-(void)deleteChat:(NSString *)hash date:(double)date{
    NSString*   sql = @"DELETE FROM `chat` WHERE hash = ? AND date = ?";
    [db open];
    [db executeUpdate:sql,hash,[NSNumber numberWithDouble:date]];
    [db close];
}


@end
