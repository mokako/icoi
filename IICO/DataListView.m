//
//  DataListView.m
//  ICOI
//
//  Created by mokako on 2014/03/21.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "DataListView.h"

@implementation DataListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        dataList = [[NSMutableDictionary alloc]init];
        dataList[@"dataInfo"] = [[NSMutableArray alloc]init];
        dataList[@"serviceName"] = @"";
        dataList[@"hostID"] = @"";
        [self initializeTableView];

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)initializeTableView
{
    dataTable = [self createTable:CGRectMake(0,0,self.frame.size.width,self.frame.size.height) tag:0];
    [self addSubview:dataTable];
    UIRefreshControl *ref = [[UIRefreshControl alloc] init];
    [ref addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    [dataTable addSubview:ref];
}

-(void)refresh:(UIRefreshControl *)ref
{
    dataList = [NSMutableDictionary dictionaryWithDictionary:[self.delegate setDataList]];
    [dataTable reloadData];
    [ref endRefreshing];
}

-(void)review
{
    dataTable.frame = self.frame;
}

-(CGFloat) tableView:(UITableView*)tableView heightForRowAtIndexPath:(NSIndexPath*)indexPath {
    if(indexPath.section == 0){
        return 40.0;
    }else{
        return 150.0;
    }
    return 60.0;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        return 2;
    }else{
        return [dataList[@"dataInfo"] count];
    }
    return 0;
}

/*
 @"title",@"exte",@"mime",@"fileName",@"lastUpdate",@"fileState",
 
 */

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleValue1 reuseIdentifier:ident defaultColor:RGBA(45,45,45,1.0) selectColor:RGBA(255,255,255,1.0) defaultBack:RGBA(255,255,255,0.0) selectBack:RGBA(82,188,174,1.0)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:11.0];
        if(indexPath.section == 0){
            if(indexPath.row == 0){
                cell.textLabel.text = @"Service Name";
                cell.detailTextLabel.text = dataList[@"serviceName"];
            }else{
                cell.textLabel.text = @"Host ID";
                cell.detailTextLabel.text = dataList[@"hostID"];
            }
        }else{
            
            
            
        }
    }
    return cell;
}
-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *table = [[UITableView alloc] initWithFrame:rect  style:UITableViewStylePlain];
    table.backgroundColor = [UIColor clearColor];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.delegate = self;
    table.dataSource = self;
    table.tag = tag;
    table.rowHeight = 60.0;
    table.showsVerticalScrollIndicator = NO;
    return table;
}


@end
