//
//  ChatData.h
//  ICOI
//
//  Created by mokako on 2015/09/18.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FMDB.h"

@interface ChatData : NSObject
{
    FMDatabase *db;
    NSString *getPhash;
}



-(NSArray *)getChat:(NSInteger)offset limit:(NSInteger)limit;

-(void)inserChat:(NSDictionary *)dic;
-(void)updateToChild:(NSString *)hash;
-(NSInteger)getChatCount;
-(NSArray *)getHash:(NSString *)parentHash;
-(void)deleteChat:(NSString *)hash date:(double)date;

-(NSDate *)dateToJulian:(double)julianDays;
@end
