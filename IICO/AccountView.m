//
//  AccountView.m
//  ICOI
//
//  Created by mokako on 2016/03/23.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "AccountView.h"

@implementation AccountView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.hidden = YES;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.title.text = NSLocalizedString(@"account_view_groupName", @"Group name");
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}





-(IBAction)touchbut:(UIButton *)but{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"appFront" object:self userInfo:@{@"type":@0,@"state":[NSNumber numberWithInteger:but.tag]}];
}

@end
