//
//  LayerView.m
//  ICOI
//
//  Created by mokako on 2014/02/11.
//  Copyright (c) 2014年 moca. All rights reserved.
//




#import "LayerView.h"





@implementation LayerView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.tag = 250;
        self.hidden = YES;
        self.backgroundColor = RGBA(245,245,245,1.0);
        layerArray = [[NSMutableArray alloc]init];
        layerImageArray = [[NSMutableArray alloc]init];
        imageDic = [[NSMutableDictionary alloc]init];

        
        [self initializeTable];
        selectedHash = @"Image";
        defaultHash = selectedHash;
        accessoryType = [[NSArray alloc]initWithObjects:@"3",@"q",@"2", nil];
        accessoryColor = [[NSArray alloc]initWithObjects:RGBA(185,26,12,1.0),RGBA(255,221,20,1.0),RGBA(0,154,24,1.0), nil];
        fontColor = RGBA(175,185,170,1.0);
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendLayer" object:nil];
        
        
    }
    return self;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initializeTable
{
    layoutWidth = 120.0;
    layoutHeight = 170.0;

    layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(layoutWidth,layoutHeight);
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;
    
    collectview = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    collectview.translatesAutoresizingMaskIntoConstraints = NO;
    [collectview setDelegate:self];
    [collectview setDataSource:self];
    [collectview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CELL"];
    [collectview setBackgroundColor:RGBA(230,230,230,0.0)];
    
    [self addSubview:collectview];

    NSArray *w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-5-[view]-5-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":collectview}];
    [self addConstraints:w];
    
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-5-[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":collectview}];
    [self addConstraints:h];
    

    //popupView
    popup = [[PopupView alloc]initWithFrame:CGRectZero];
    popup.translatesAutoresizingMaskIntoConstraints = NO;
    [popup setMountColor:RGBA(145,145,145,0.2)];
    [self addSubview:popup];
    NSArray *popw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":popup}];
    [self addConstraints:popw];
    
    NSArray *poph =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":popup}];
    [self addConstraints:poph];
    
    
    
    UIView *po = [[UIView alloc] initWithFrame:CGRectMake(0,0,300,275)];
    po.layer.cornerRadius = 5;
    po.backgroundColor = RGBA(255,255,255,1.0);
    
    ptitle = [[UILabel alloc]initWithFrame:CGRectMake(5,5,290,30)];
    ptitle.layer.cornerRadius = 5;
    ptitle.clipsToBounds = YES;
    ptitle.textAlignment = NSTextAlignmentCenter;
    ptitle.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    ptitle.textColor = RGBA(255,255,255,1.0);
    
    UIButton *can = [[UIButton alloc]initWithFrame:CGRectMake(265,5,30,30)];
    can.titleLabel.font = [UIFont fontWithName:@"point" size:24];
    [can setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [can setTitle:@"9" forState:UIControlStateNormal];
    [can addTarget:self action:@selector(hidePopup) forControlEvents:UIControlEventTouchUpInside];
    
    fileName = [[UILabel alloc]initWithFrame:CGRectMake(10,40,280,30)];
    fileName.font = [UIFont fontWithName:@"AvenirNext-Medium" size:13.0];
    fileName.textColor = RGBA(90,90,90,1.0);
    
    fileID = [[UILabel alloc]initWithFrame:CGRectMake(10,70,280,30)];
    fileID.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
    fileID.textColor = RGBA(100,100,100,1.0);
    
    donorName = [[UILabel alloc]initWithFrame:CGRectMake(10,110,280,30)];
    donorName.font = [UIFont fontWithName:@"AvenirNext-Medium" size:13.0];
    donorName.textColor = RGBA(90,90,90,1.0);
    
    donorID = [[UILabel alloc]initWithFrame:CGRectMake(10,140,280,30)];
    donorID.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
    donorID.textColor = RGBA(100,100,100,1.0);
    
    donorDate = [[UILabel alloc]initWithFrame:CGRectMake(10,180,280,30)];
    donorDate.font = [UIFont fontWithName:@"Avenir Next" size:11.0];
    donorDate.textColor = RGBA(100,100,100,1.0);
    
    rejectButton = [[UIButton alloc]initWithFrame:CGRectMake(po.frame.size.width / 2 - 35,230,70,30)];
    rejectButton.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    [rejectButton setTitle:@"OK" forState:UIControlStateNormal];
    [rejectButton setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [rejectButton addTarget:self action:@selector(set) forControlEvents:UIControlEventTouchUpInside];
    [po addSubview:ptitle];
    [po addSubview:can];
    [po addSubview:fileName];
    [po addSubview:fileID];
    [po addSubview:donorName];
    [po addSubview:donorID];
    [po addSubview:donorDate];
    [po addSubview:rejectButton];
    [popup setView:po];
    
}

-(void)setServiceManager:(ServiceManager *)servicemanager{
    sb = servicemanager;
    myUuid = [sb getUUID];
    pcon = [[PeerController alloc]init:[sb getService]];
}


-(void)update
{
    //[popup update];
}

-(void)setLayerImage:(NSDictionary *)list
{
    [popup hide];
    [self addImageDictionary:list];
    [self setArray:layerArray addData:list.allKeys];
    [self checkFile:layerArray dictionary:imageDic];
    [self createLayerImage];
    
    dispatch_async(dispatch_get_main_queue(),^{
        [collectview reloadData];
    });
}


- (BOOL)shouldInvalidateLayoutForBoundsChange:(CGRect)newBounds
{
    return YES;
}

-(void)hidePopup{
    [popup hide];
}

-(void)setPeerUUID:(NSString *)uuid{
    myUuid = uuid;
}

-(void)addLayerItem:(NSString *)item list:(NSDictionary *)list{
    [popup hide];
    [self addImageDictionary:list];
    [self setArray:layerArray addData:list.allKeys];
    [self checkFile:layerArray dictionary:imageDic];
    [self createLayerImage];
    dispatch_async(dispatch_get_main_queue(),^{
        [collectview reloadData];
    });
}
-(void)addLayerImage:(NSString *)file list:(NSDictionary *)list{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reload) object:nil];
    imageDic[file][@"imageLayer"] = (id)[sb displaythumbnailImage:file size:layoutWidth];
    NSUInteger index = [layerArray indexOfObject:file];
    // 要素があったか?
    if (index != NSNotFound) { // yes
        dispatch_async(dispatch_get_main_queue(),^{
            [collectview reloadItemsAtIndexPaths:@[[NSIndexPath indexPathForRow:index inSection:0]]];
        });
    }
}

-(void)insertImage:(NSDictionary *)dic{
    
}


-(void)reloadTable:(NSDictionary *)list{
    [popup hide];
    [self addImageDictionary:list];
    [self setArray:layerArray addData:list.allKeys];
    [self checkFile:layerArray dictionary:imageDic];
    [self createLayerImage];
    //ここで選択されているファイルと削除済みのファイルのチェックを行ってください。
    if([layerArray indexOfObject:selectedHash] == NSNotFound){
        selectedHash = defaultHash;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendConsole" object:self userInfo:@{@"type":@0}];
    }
    dispatch_async(dispatch_get_main_queue(),^{
        [collectview reloadData];
    });
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(layoutWidth, layoutHeight);
}

- (UICollectionViewLayoutAttributes *)initialLayoutAttributesForAppearingItemAtIndexPath:(NSIndexPath *)itemIndexPath {
    UICollectionViewLayoutAttributes* attributes = [collectview layoutAttributesForItemAtIndexPath:itemIndexPath];
    attributes.frame = CGRectMake(0,0,layoutWidth,layoutHeight);
    return attributes;
}
//CELL数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return layerArray.count;
}
//collection数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];

    //ここで新規作成か再利用可を調べます。
    /*
     base viewを設定しtagにindexPath.rowと同じnumbarを振ります。
     contentViewsの階層
     
     1.baseView + statusLabel - 2.imageView - 3.selectedView
     2.title
     3.donar
     
     
     */
    
    NSMutableArray *contntArray = [[NSMutableArray alloc]initWithArray:[cell.contentView subviews]];
    NSString *hash = layerArray[indexPath.row];
    if(contntArray.count > 0){
        //再利用
        NSMutableArray *baseArray = [[NSMutableArray alloc]initWithArray:[contntArray[0] subviews]];
        switch ([imageDic[hash][@"imageState"][@"fileState"] integerValue]) {
            case 0:
            {
                ((UILabel *)baseArray[0]).font = [UIFont fontWithName:@"font-icon-00" size:40];
                ((UILabel *)baseArray[0]).text = @"c";
            }
                break;
            case 1:
            {
                ((UILabel *)baseArray[0]).font = [UIFont fontWithName:@"font-icon-00" size:40];
                ((UILabel *)baseArray[0]).text = @"2";
            }
                break;
            case 2:{
                if([imageDic[layerArray[indexPath.row]][@"imageLayer"] isEqual:[NSNull null]]){
                    ((UILabel *)baseArray[0]).font = [UIFont fontWithName:@"font-icon-01" size:40];
                    ((UILabel *)baseArray[0]).text = @"8";
                }else{
                    ((UILabel *)baseArray[0]).font = [UIFont fontWithName:@"font-icon-02" size:40];
                    ((UILabel *)baseArray[0]).text = @"k";
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        ((UIImageView *)baseArray[1]).image = (UIImage *)imageDic[layerArray[indexPath.row]][@"imageLayer"];
                    });
                }
            }
                break;
            default:
                break;
        }
        
        //reject
        if([pcon checkFile:hash]){
            ((UILabel *)baseArray[3]).hidden = NO;
        }else{
            ((UILabel *)baseArray[3]).hidden = YES;
        }
        
        
        //selected
        if([selectedHash isEqualToString:hash]){
            ((UILabel *)baseArray[2]).hidden = NO;
        }else{
            ((UILabel *)baseArray[2]).hidden = YES;
        }
        ((UILabel *)contntArray[1]).text = [NSString stringWithFormat:@"%@",imageDic[hash][@"imageState"][@"title"]];//title
        ((UILabel *)contntArray[2]).text = imageDic[hash][@"imageState"][@"donor"];//donor
        
        ((UIButton *)contntArray[3]).tag = indexPath.row;

    }else{
        //新規作成
        cell.contentView.backgroundColor = RGBA(255,255,255,1.0);
        
        UIView *base = [[UIView alloc]initWithFrame:CGRectMake(0,0,layoutWidth,layoutWidth)];
        base.tag = indexPath.row;
        [cell.contentView addSubview:base];//0
        UILabel *state = [[UILabel alloc]initWithFrame:CGRectMake(0,0,layoutWidth,layoutWidth)];
        state.textAlignment = NSTextAlignmentCenter;
        state.textColor = RGBA(145,145,145,1.0);
        [base addSubview:state];//0
        UIImageView *image = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,layoutWidth,layoutWidth)];
        [base addSubview:image];//1
        UILabel *checklab = [[UILabel alloc]initWithFrame:CGRectMake(0,0,layoutWidth,layoutWidth)];
        checklab.font = [UIFont fontWithName:@"font-icon-03" size:40.0];
        checklab.text = @"d";
        checklab.textAlignment = NSTextAlignmentCenter;
        checklab.textColor = RGBA(12,232,123,1.0);
        checklab.backgroundColor = RGBA(125,125,125,0.3);
        [base addSubview:checklab];//2
        
        UILabel *rejectMark = [[UILabel alloc]initWithFrame:CGRectMake(0,0,layoutWidth,layoutWidth)];
        rejectMark.font = [UIFont fontWithName:@"font-icon-01" size:40.0];
        rejectMark.text = @"8";
        rejectMark.textAlignment = NSTextAlignmentCenter;
        rejectMark.textColor = RGBA(255,13,65,1.0);
        rejectMark.backgroundColor = RGBA(255,255,255,1.0);
        
        if([pcon checkFile:hash]){
            rejectMark.hidden = NO;
        }else{
            rejectMark.hidden = YES;
        }
        
        
        [base addSubview:rejectMark];//3
        
        if([selectedHash isEqualToString:hash]){
            checklab.hidden = NO;
        }else{
            checklab.hidden = YES;
        }
        
        switch ([imageDic[hash][@"imageState"][@"fileState"] integerValue]) {
            case 0:
            {
                state.font = [UIFont fontWithName:@"font-icon-00" size:40];
                state.text = @"c";
            }
                break;
            case 1:
            {
                state.font = [UIFont fontWithName:@"font-icon-00" size:40];
                state.text = @"2";
            }
                break;
            case 2:{
                if([imageDic[layerArray[indexPath.row]][@"imageLayer"] isEqual:[NSNull null]]){
                    state.font = [UIFont fontWithName:@"font-icon-01" size:40];
                    state.text = @"8";
                }else{

                    state.font = [UIFont fontWithName:@"font-icon-02" size:40];
                    state.text = @"k";
                    
                    dispatch_async(dispatch_get_main_queue(),^{
                        image.image = (UIImage *)imageDic[layerArray[indexPath.row]][@"imageLayer"];
                    });
                    
                }
            }
                break;
            default:
                break;
        }
        UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(5,layoutWidth + 25,layoutWidth - 10,20)];
        title.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:13.0];
        title.textColor = RGBA(50,50,50,1.0);
        title.textAlignment = NSTextAlignmentCenter;
        UILabel *donor = [[UILabel alloc]initWithFrame:CGRectMake(5,layoutWidth + 8,layoutWidth - 10,20)];
        donor.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:12.0];
        donor.textColor = RGBA(100,100,100,1.0);
        donor.textAlignment = NSTextAlignmentCenter;
        donor.text = imageDic[hash][@"imageState"][@"donor"];
        title.text = [NSString stringWithFormat:@"%@",imageDic[hash][@"imageState"][@"title"]];
        UIButton *pich = [[UIButton alloc]initWithFrame:CGRectMake(0,layoutWidth,layoutWidth,layoutHeight - layoutWidth)];
        pich.tag = indexPath.row;
        pich.backgroundColor = RGBA(255,255,255,0.0);
        [pich addTarget:self action:@selector(reject:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:title];//1
        [cell.contentView addSubview:donor];//2
        [cell.contentView addSubview:pich];//3

    }
    
    return cell;
}



- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSString *hash = layerArray[indexPath.row];
    if([imageDic[hash][@"imageState"][@"fileState"] integerValue] == 2){
        if ([pcon checkFile:hash]){
            //throw
            //除外対象の為
        }else{
            if([sb checkImageFile:hash]){
                NSUInteger index = [layerArray indexOfObject:selectedHash];
                if (index != NSNotFound) { // yes
                    NSArray *visibleArray = [collectview indexPathsForVisibleItems];
                    for(NSIndexPath *path in visibleArray){
                        if(path.row == index){
                            UICollectionViewCell *oldcell = [collectionView cellForItemAtIndexPath:path];
                            NSMutableArray *ary = [[NSMutableArray alloc]initWithArray:[oldcell.contentView subviews]];
                            NSMutableArray *base = [[NSMutableArray alloc]initWithArray:[ary[0] subviews]];
                            ((UILabel *)base[2]).hidden = YES;
                        }
                    }
                }
                selectedHash = layerArray[indexPath.row];
                UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
                NSMutableArray *newary = [[NSMutableArray alloc]initWithArray:[cell.contentView subviews]];
                NSMutableArray *newbase = [[NSMutableArray alloc]initWithArray:[newary[0] subviews]];
                ((UILabel *)newbase[2]).hidden = NO;
                [sb selectImage:layerArray[indexPath.row]];
            }
        }
    }
}

- (void)collectionView:(UICollectionView *)collectionView didEndDisplayingCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath
{

}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{

}


-(void)reject:(UIButton *)but{
    rejectionHash = layerArray[but.tag];
    if([rejectionHash isEqualToString:@"Image"]){
        //throw
    }else if ([rejectionHash isEqualToString:selectedHash]){
        //throw
    }else if ([imageDic[rejectionHash][@"imageState"][@"donorID"] isEqualToString:myUuid]){
        imageState = 2;
        ptitle.text = NSLocalizedString(@"addRejectionImage",@"");
        ptitle.backgroundColor = RGBA(46,151,216,1.0);
        [rejectButton setBackgroundColor:RGBA(46,151,216,1.0)];
        fileName.text = [NSString stringWithFormat:@"File name : %@",imageDic[rejectionHash][@"imageState"][@"title"]];
        fileID.text = [NSString stringWithFormat:@"File ID : %@",rejectionHash];
        donorName.text = [NSString stringWithFormat:@"Donor Name : %@",imageDic[rejectionHash][@"imageState"][@"donor"]];
        donorID.text = [NSString stringWithFormat:@"Donor ID : %@",imageDic[rejectionHash][@"imageState"][@"donorID"]];
        donorDate.text = [NSString stringWithFormat:@"Date : %@",[DateFormatter dateFormatter:imageDic[rejectionHash][@"imageState"][@"lastUpdate"]]];
        [popup show];
    }else{
        if([pcon checkFile:rejectionHash]){
            imageState = 1;
            ptitle.text = NSLocalizedString(@"deleteRejectionFile",@"");
            ptitle.backgroundColor = RGBA(0,232,51,1.0);
            [rejectButton setBackgroundColor:RGBA(0,232,51,1.0)];
        }else{
            imageState = 0;
            ptitle.text = NSLocalizedString(@"addRejectionFile",@"");
            ptitle.backgroundColor = RGBA(242,56,39,1.0);
            [rejectButton setBackgroundColor:RGBA(242,56,39,1.0)];
        }
        fileName.text = [NSString stringWithFormat:@"File name : %@",imageDic[rejectionHash][@"imageState"][@"title"]];
        fileID.text = [NSString stringWithFormat:@"File ID : %@",rejectionHash];
        donorName.text = [NSString stringWithFormat:@"Donor Name : %@",imageDic[rejectionHash][@"imageState"][@"donor"]];
        donorID.text = [NSString stringWithFormat:@"Donor ID : %@",imageDic[rejectionHash][@"imageState"][@"donorID"]];
        donorDate.text = [NSString stringWithFormat:@"Date : %@",[DateFormatter dateFormatter:imageDic[rejectionHash][@"imageState"][@"lastUpdate"]]];
        [popup show];
    }
}


-(void)set{
    switch (imageState) {
        case 0:
        {
            [pcon addFile:rejectionHash];
            [collectview reloadData];
        }
            break;
        case 1:
        {
            [pcon removeFile:rejectionHash];
            [collectview reloadData];
        }
            break;
        case 2:
        {
            [sb setDeleteRequest:rejectionHash];
        }
            break;
        default:
            break;
    }
    [popup hide];
}

-(NSMutableArray *)setArray:(NSMutableArray *)base addData:(NSArray *)new{
    [new enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([base indexOfObject:obj] == NSNotFound) {
            [base addObject:obj];
        }
    }];
    return base;
}
-(void)addImageDictionary:(NSDictionary *)sub{
    [sub enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSMutableDictionary *obj, BOOL *stop){
        if([imageDic objectForKey:key] != nil){
            if([sub[key][@"fileState"] integerValue] == 4 ){
                [imageDic removeObjectForKey:key];
            }else{
                imageDic[key][@"imageState"] = sub[key];
            }
        }else{
            if([sub[key][@"fileState"] integerValue] == 4 ){
                //削除依頼の出ているもしくは削除済みの為除外します。
            }else{
                NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:[NSNull null],@"imageLayer",sub[key],@"imageState", nil];
                [imageDic setObject:dic forKey:key];
            }
        }
    }];
}

-(void)checkFile:(NSMutableArray *)array dictionary:(NSMutableDictionary *)dic{
    NSMutableArray *newArray = [[NSMutableArray alloc]init];
    [array enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if([dic objectForKey:obj] != nil){
            if([dic[obj][@"imageState"][@"fileState"] integerValue] != 4){
                [newArray addObject:obj];
            }
        }
    }];
    [layerArray setArray:newArray];
}


-(void)setLayerImageDictionarys:(NSDictionary *)dic{
    //追加されるキーが既にあるかどうかを調べます。

    NSArray *key = [imageDic allKeys];
    NSArray *new = [dic allKeys];

    for(NSString *k in new){
        NSInteger hit = [key indexOfObject:k];

        if(hit  != NSNotFound){
            //既存なら無視

        }else{
            //未生成

            NSMutableDictionary *dic1 = [[NSMutableDictionary alloc]initWithObjectsAndKeys:dic[k],@"imageState",[NSNull null],@"imageLayer", nil];
            imageDic[k] = dic1;
        }
    }
}

-(void)createLayerImage{
    NSArray *dic = [imageDic allKeys];
    for(NSString *key in dic){
        if([sb checkImageFile:key]){
            //fileが有る
            if([imageDic[key][@"imageLayer"] isEqual:[NSNull null]]){
                imageDic[key][@"imageLayer"] = (id)[sb displaythumbnailImage:key size:layoutWidth];
            }else{
                //既にセットされている
            }
        }else{
            //fileが無いため無
        }
    }
}



-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //reload
        {
            [self reloadTable:[center userInfo][@"list"]];
        }
            break;
        case 1: //additem
        {
            [self addLayerItem:[center userInfo][@"item"] list:[center userInfo][@"list"]];
        }
            break;
        case 2: //addimage
        {
            [self addLayerImage:[center userInfo][@"item"] list:[center userInfo][@"list"]];
        }
            break;
        case 3:
        {
            [self addLayerItem:[center userInfo][@"item"] list:[center userInfo][@"list"]];
            [self addLayerImage:[center userInfo][@"item"] list:[center userInfo][@"list"]];
            break;
        }
        default:
            break;
    }
}





















@end
