//
//  AnimateConsoleView.m
//  ICOI
//
//  Created by mokako on 2014/06/15.
//  Copyright (c) 2014年 moca. All rights reserved.
//
//
/*
 animationのスピードや表示時の設定を表示させます。
 delegate先はConsoleViewとなります。
 */

#import "AnimateConsoleView.h"

@implementation AnimateConsoleView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.speed = 0.0f;
        self.keep = NO;
        self.reverse = NO;
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)initializeView
{
    closeBut = [[UIButton alloc]initWithFrame:CGRectMake(0,self.frame.size.height - 50,self.frame.size.width,50)];
    [[closeBut layer]setBorderColor:[RGBA(65,60,55,1.0) CGColor]];
    [[closeBut layer]setBorderWidth:0.5];
    closeBut.backgroundColor = RGBA(55,50,45,1.0);
    closeBut.titleLabel.font = [UIFont fontWithName:@"Avenir-Light" size:17.0];
    closeBut.titleLabel.textColor = RGBA(255,255,255,1.0);
    closeBut.titleLabel.textAlignment = NSTextAlignmentCenter;
    [closeBut setTitle:@"Close >>" forState:UIControlStateNormal];
    closeBut.tag = 10;
    [closeBut addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:closeBut];
    
    root = [self createTable:CGRectMake(0,0,self.frame.size.width,self.frame.size.height - 50) tag:1];
    [self addSubview:root];
    
}

-(void)review
{
    root.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height - 50);
    closeBut.frame = CGRectMake(0,self.frame.size.height - 50,self.frame.size.width,50);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}


-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *tempView=[[UIView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,30)];
    tempView.backgroundColor = RGBA(65,60,55,0.3);
    
    UILabel *tempLabel=[[UILabel alloc]initWithFrame:CGRectMake(10,0,self.frame.size.width - 40,30)];
    tempLabel.backgroundColor=[UIColor clearColor];
    tempLabel.textColor = RGBA(155,155,155,1.0);
    tempLabel.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    if(section == 0)
    {
        tempLabel.text = @"Animation speed.";
    }else if (section == 1){
        tempLabel.text = @"To display the path by placing the image.";
    }else if(section == 2){
        tempLabel.text = @"Leave the path of the previous.";
    }else if(section == 3){
        tempLabel.text = @"Reverse mode.";
    }else if(section == 4){
        tempLabel.text = @"Remove the path data to the image.";
    }
    
    [tempView addSubview:tempLabel];
    return tempView;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 5;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if(section == 0){
        //speed
        return 1;
    }else if (section == 1){
        return 1;
    }else if(section == 2){
        //leave the path of the previous
        return 1;
    }else if(section == 3){
        //reverse
        return 1;
    }else if(section == 4){
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    //NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    //NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:@"identifier" defaultColor:RGBA(82,188,174,1.0) selectColor:RGBA(55,55,55,1.0) defaultBack:RGBA(35,30,25,0.9) selectBack:RGBA(35,35,35,0.9)];
        
        
        
        switch (indexPath.section) {
            case 0:
            {
                UILabel *min = [[UILabel alloc]initWithFrame:CGRectMake(0,0,32,32)];
                min.backgroundColor = [UIColor clearColor];
                min.textColor = RGBA(82,188,174,1.0);
                min.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
                min.text = @"High";
                min.textAlignment = NSTextAlignmentCenter;
                
                UILabel *max = [[UILabel alloc]initWithFrame:CGRectMake(0,0,32,32)];
                max.textColor = RGBA(255,255,255,0.6);
                max.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
                max.text = @"0";
                max.textAlignment = NSTextAlignmentCenter;
                
                s = [[UISlider alloc]initWithFrame:CGRectMake(0,0,290,30)];
                s.center = CGPointMake(self.frame.size.width / 2,30.0);
                s.minimumTrackTintColor = RGBA(0,138,255,1.0);
                s.maximumTrackTintColor = RGBA(200,200,200,1.0);
                s.maximumValue = 10;
                s.minimumValue = 0;
                
                s.minimumValueImage = [DataManager imageFromView:min];
                s.maximumValueImage = [DataManager imageFromView:max];
                
                s.continuous = NO;
                
                [s addTarget:self action:@selector(slideValue:) forControlEvents:UIControlEventTouchUpInside];
                [cell.contentView addSubview:s];
                
            }
                break;
            case 1:
            {
                UISwitch *sw = [[UISwitch alloc] init];
                
                sw.tag = 0;
                sw.on = NO;
                // 値が変更された時にhogeメソッドを呼び出す
                [sw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
                sw.onTintColor = RGBA(255,255,255,0.7);
                cell.textLabel.text = @"Place the image";
                cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
                cell.accessoryView = sw;
            }
                break;
            case 2:
            {
                UISwitch *sw = [[UISwitch alloc] init];
                
                sw.tag = 1;
                sw.on = NO;
                // 値が変更された時にhogeメソッドを呼び出す
                [sw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
                sw.onTintColor = RGBA(255,255,255,0.7);
                cell.textLabel.text = @"Keep paths";
                cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
                cell.accessoryView = sw;
            }
                break;
            case 3:
            {
                UISwitch *sw = [[UISwitch alloc] init];
                sw.onTintColor = RGBA(255,255,255,0.7);
                sw.tag = 2;
                sw.on = NO;
                // 値が変更された時にhogeメソッドを呼び出す
                [sw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
                
                cell.textLabel.text = @"Reverse mode";
                cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
                cell.accessoryView = sw;
            }
                break;
            case 4:
            {
                UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(50,10,self.frame.size.width - 100,40)];
                [but setBackgroundColor:RGBA(255,255,255,1.0)];
                [but setTitle:@"Remove data" forState:UIControlStateNormal];
                [[but layer] setBorderColor:[RGBA(100,100,100,0.6) CGColor]];
                [[but  layer] setBorderWidth:0.5];
                but.layer.cornerRadius = 20.0;
                but.tag = 1;
                but.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
                [but setTitleColor:RGBA(45,45,45,0.9) forState:UIControlStateNormal];
                [but addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
                [but addTarget:self action:@selector(touchDown:) forControlEvents:UIControlEventTouchDown];
                [but addTarget:self action:@selector(touchCancel:) forControlEvents:UIControlEventTouchDragOutside ];
                [cell.contentView addSubview:but];
            }
                break;
            default:
                break;
        }
  
    }
    return cell;
}


-(void)slideValue:(UISlider *)slide
{
    UILabel *max = [[UILabel alloc]initWithFrame:CGRectMake(0,0,32,32)];
    max.textColor = RGBA(255,255,255,0.6);
    max.font = [UIFont fontWithName:@"Verdana" size:11.0];
    max.text = [NSString stringWithFormat:@"%0.00fs",slide.value];
    max.textAlignment = NSTextAlignmentCenter;
    s.maximumValueImage = [DataManager imageFromView:max];
    self.speed = slide.value;
}

-(void)switchValue:(UISwitch *)state
{
    switch (state.tag) {
        case 0:
        {
             [self.delegate setEditTab:24];
        }
            break;
        case 1:
        {
            self.keep = state.on;
        }
            break;
        case 2:
        {
            self.reverse = state.on;
        }
            break;
        default:
            break;
    }
}

#pragma mark -- ボタン設定
-(void)touchBut:(UIButton *)but
{
    if(but.tag == 1){
        [but setBackgroundColor:RGBA(200,200,200,1.0)];
        [but setTitleColor:RGBA(45,45,45,0.9) forState:UIControlStateNormal];
        
        //削除処理
        UIAlertView *alert =
        [[UIAlertView alloc] initWithTitle:@"Caution" message:@"Delete the path to this image."
                                  delegate:self cancelButtonTitle:@"Decline" otherButtonTitles:@"Accept", nil];
        [alert show];
    }else{
        [self.delegate setEditTab:22];
    }

}

-(void)alertView:(UIAlertView*)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex {
    switch (buttonIndex) {
        case 0:
            
            break;
        case 1:
            //データを削除します。
            [self.delegate setEditTab:23];
            break;
    }
    
}


-(void)touchDown:(UIButton *)but
{
    [but setBackgroundColor:RGBA(23,23,23,1.0)];
    [but setTitleColor:RGBA(200,200,200,1.0) forState:UIControlStateNormal];
}
-(void)touchCancel:(UIButton *)but
{
    [but setBackgroundColor:RGBA(200,200,200,1.0)];
    [but setTitleColor:RGBA(45,45,45,0.9) forState:UIControlStateNormal];
}




-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStylePlain];
    atable.backgroundColor = RGBA(35,30,25,1.0);
    atable.separatorStyle = UITableViewCellSeparatorStyleNone;
    atable.delegate = self;
    atable.dataSource = self;
    atable.allowsSelection = NO;
    atable.tag = tag;
    atable.rowHeight = 60.0;
    atable.showsVerticalScrollIndicator = NO;
    return atable;
}

@end
