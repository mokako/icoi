//
//  PopupSubview.m
//  ICOI
//
//  Created by mokako on 2015/09/05.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "PopupSubview.h"

@implementation PopupSubview
-(id)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if(self){
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = self.bounds;
        [self addSubview:visualEffectView];
        self.clipsToBounds = YES;
        self.layer.cornerRadius = 5;
        [self setup];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)setup{
    self.title = [[UILabel alloc]initWithFrame:CGRectMake(5,5,self.frame.size.width - 10,40)];
    [self addSubview:self.title];
    self.title.font = [UIFont fontWithName:@"AvenirNext-Medium" size:15];
    self.title.clipsToBounds = YES;
    self.title.textAlignment = NSTextAlignmentCenter;
    self.title.layer.cornerRadius = 5;
    
    self.textView = [[UITextView alloc]initWithFrame:CGRectMake(5,50,self.frame.size.width - 10,100)];
    self.textView.editable = NO;
    self.textView.font = [UIFont fontWithName:@"AvenirNext-Regular" size:13];
    self.textView.backgroundColor = RGBA(255,255,255,0.0);
    [self addSubview:self.textView];
    
    UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(5,155,self.frame.size.width - 10,30)];
    [but setTitleColor:RGBA(100,100,100,1.0) forState:UIControlStateNormal];
    but.titleLabel.textAlignment = NSTextAlignmentCenter;
    but.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:15];
    [but setTitle:@"OK" forState:UIControlStateNormal];
    [but addTarget:self action:@selector(clear) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:but];
    
    [self type:Warning];
}

-(void)clear{
    [self.delegate popupview:OK];
    [self.delegate hide];
}

-(void)type:(type)type{
    switch (type) {
        case Caution:
        {
            self.title.backgroundColor = RGBA(4,163,139,1.0);
            self.title.textColor = RGBA(255,255,255,1.0);
        }
            break;
        case Warning:
        {
            self.title.backgroundColor = RGBA(242,56,39,1.0);
            self.title.textColor = RGBA(255,255,255,1.0);
        }
            break;
        case Fatal:
        {
            self.title.backgroundColor = RGBA(255,255,255,1.0);
            self.title.textColor = RGBA(45,45,45,1.0);
        }
            break;
        default:
            break;
    }
}

-(void)setTitle:(NSString *)title text:(NSString *)text{
    dispatch_async(dispatch_get_main_queue(), ^{
        self.title.text = title;
        self.textView.text = text;
    });
}

@end
