//
//  BrushToolView.h
//  ICOI
//
//  Created by mokako on 2015/10/10.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ToolCell.h"

@interface BrushToolView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *w;
    NSArray *h;
    BOOL isToggle;
    NSArray *list;
    BOOL isFirst;
    UIView *view;
    UIButton *selectButton;
}
@property (weak, nonatomic) IBOutlet UIView *base;
@property (weak, nonatomic) IBOutlet UIButton *line0;
@property (weak, nonatomic) IBOutlet UIButton *line1;
@property (weak, nonatomic) IBOutlet UIButton *fade;
@property (weak, nonatomic) IBOutlet UITableView *table;

+(instancetype)view;
-(void)set;
-(void)toggle;
-(void)fadeOut;
@end
