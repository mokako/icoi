//
//  DataListView.h
//  ICOI
//
//  Created by mokako on 2014/03/21.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "FileManager.h"
#import "DataManager.h"


@protocol DataListViewDelegate<NSObject>
-(NSDictionary *)setDataList;
@end

@interface DataListView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableDictionary *dataList;
    UITableView *dataTable;
}
-(void)review;
@property (nonatomic, weak) id <DataListViewDelegate> delegate;
@end
