//
//  PeerViewCell.m
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "PeerViewCell.h"

@implementation PeerViewCell

- (void)awakeFromNib {
    // Initialization code
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"peerViewCell" object:nil];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setNetworkState:(NSInteger)state{
    switch (state) {
        case 0:
        {
            self.stateIcon.textColor = RGBA(242,56,39,1.0);
            self.state.text = @"Disconnected";
        }
            break;
        case 1:
        {
            self.stateIcon.textColor = RGBA(0,172,247,1.0);
            self.state.text = @"Connecting";
        }
            break;
        case 2:
        {
            self.stateIcon.textColor = RGBA(27,253,110,1.0);
            self.state.text = @"Connected";
        }
            break;
        default:
            break;
    }
    
}

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //reload
        {//画像情報
            if([self.uid isEqualToString:[center userInfo][@"user"]]){
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.file.text = [center userInfo][@"item"][@"title"];
                });
                
            }
        }
            break;
        case 2:
        {
            
            
        }
            break;
        default:
            break;
    }
}


@end
