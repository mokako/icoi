//
//  PickupCell.h
//  ICOI
//
//  Created by mokako on 2016/03/11.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "HeaderCurveView.h"
#import "FooterCurveView.h"
@interface PickUpCell : UITableViewCell
{
    NSArray *colorList;
    CGFloat fontSize;
}






@property (weak, nonatomic) IBOutlet UILabel *stateIcon;
@property (weak, nonatomic) IBOutlet UIButton *title;
@property (weak, nonatomic) IBOutlet UILabel *article;
@property (weak, nonatomic) IBOutlet HeaderCurveView *statusView;
@property (weak, nonatomic) IBOutlet FooterCurveView *footer;
@property (weak, nonatomic) IBOutlet UILabel *name;




-(void)setArticleText:(NSString *)str;
-(void)setIconState:(NSInteger)state;
@end
