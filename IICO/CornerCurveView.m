//
//  CornerCurveView.m
//  ICOI
//
//  Created by mokako on 2016/03/26.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "CornerCurveView.h"

@implementation CornerCurveView
-(id)init{
    self = [super init];
    if(self){
        angle = 10.0;
        color = [UIColor colorWithRed:255/255 green:255/255 blue:255/255 alpha:1.0];
    }
    return self;
}



- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (id)initWithFrameColor:(CGRect)frame color:(UIColor *)_color angle:(CGFloat)_angle
{
    self = [super initWithFrame:frame];
    if (self) {
        color = _color;
        angle = _angle;
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}



- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGFloat lx = CGRectGetMinX(rect);
    CGFloat cx = CGRectGetMidX(rect);
    CGFloat rx = CGRectGetMaxX(rect);
    CGFloat by = CGRectGetMinY(rect);
    CGFloat cy = CGRectGetMidY(rect);
    CGFloat ty = CGRectGetMaxY(rect);
    CGFloat radius = angle; // 角丸の半径

    CGContextSetFillColorWithColor(context, color.CGColor); // 色設定
    
    CGContextMoveToPoint(context, lx, cy);
    CGContextAddArcToPoint(context, lx, by, cx, by, radius);
    CGContextAddArcToPoint(context, rx, by, rx, cy, radius);
    CGContextAddArcToPoint(context, rx, ty, cx, ty, radius);
    CGContextAddArcToPoint(context, lx, ty, lx, cy, radius);
    CGContextClosePath(context);
    CGContextFillPath(context);

}

-(void)setCurveAngle:(CGFloat)_angle{
    angle = _angle;
    //[self setNeedsDisplay];
}

-(void)setColor:(UIColor *)_color{
    color = _color;
    //[self setNeedsDisplay];
}

-(void)setAngleWithColor:(CGFloat)_angle color:(UIColor *)_color{
    angle = _angle;
    color = _color;
    //[self setNeedsDisplay];
}
@end
