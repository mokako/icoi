//
//  CloudView.m
//  ICOI
//
//  Created by mokako on 2014/04/13.
//  Copyright (c) 2014年 moca. All rights reserved.
//
/*
 
 仕様変更
 
 
 cloudviewでの処理の流れ
 
 将来クラウドサービスを複数サポートする事を前提に組み込むます。
 その為に最初のinitializeの時点ではテーブルの最小限の要素しか作成されず、
 クラウドサービスを選択した時点でテーブルデータを取得する形とします。
 
 選択方法とダウンロード方法が各サービスで違いがあると思うのでそこを選んだ選択肢によって変更出来る様にサポートさせます。
 リストの拡張子イメージの表示は出来るだけ同じ表示になる様に心がけてください。
 
 
 */
#import "CloudView.h"
#import "AppDelegate.h"




#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@implementation CloudView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        folderList = [[NSMutableArray alloc]init];
        stack = [[NSMutableDictionary alloc]init];
        allPath = [[NSMutableDictionary alloc]init];
        extensionList = [[NSArray alloc]initWithObjects:@"JPG",@"jpg",@"png",@"PNG",@"gif",@"GIF",@"txt",@"log",@"XML",@"xml",@"m",@"h", @"html",@"js",@"pdf",@"PDF",@"",nil];
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)initializeView
{
    table = [[UITableView alloc]initWithFrame:CGRectMake(0,48,self.frame.size.width,self.frame.size.height - 48)];
    table.dataSource = self;
    table.delegate = self;
    table.rowHeight = 40.0;
    table.separatorInset = UIEdgeInsetsZero;
    table.separatorColor = RGBA(200,200,200,1.0);
    [self addSubview:table];
    
    dirPath = @"/";
    lastElement = @"";
    folderList = [[NSMutableArray alloc]init];
    dirList = [[NSMutableArray alloc]init];
    
    //UIRefreshControl *ref = [[UIRefreshControl alloc] init];
    //[ref addTarget:self action:@selector(refresh:) forControlEvents:UIControlEventValueChanged];
    //[table addSubview:ref];
    
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *DocumentsDirPath = [paths objectAtIndex:0];
    
    // ファイルマネージャを作成
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSError *error;
    NSArray *list = [fileManager contentsOfDirectoryAtPath:DocumentsDirPath
                                                     error:&error];
    
    // ファイルやディレクトリの一覧を表示する
    for (NSString *path in list) {
        NSLog(@"%@", path);
    }
    
#pragma mark -- ファイル操作系のボタンです
    //toolbar
    
    toolbar = [[ClearToolBar alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,48)];
    toolbar.backgroundColor = RGBA(35,30,25,1.0);
    [self addSubview:toolbar];
    //
    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //
    UIBarButtonItem *blankSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    blankSpace.width = 48;
    
    

    
    UIButton *window = [[UIButton alloc]initWithFrame:CGRectMake(0,0,150,32)];
    window.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:13];
    [window setTitle:@"Open temporary." forState:UIControlStateNormal];
    window.tag = 10;
    [window addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *c = [[UIBarButtonItem alloc]initWithCustomView:window];
    
    
    toolbar.items = [NSArray arrayWithObjects:gap,blankSpace,c,blankSpace,gap,nil];
    
    tmp = [[TempDirTable alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    tmp.hidden = YES;
    [self addSubview:tmp];
    
    
    NSBundle* bundle = [NSBundle mainBundle];
    //読み込むファイルパスを指定
    NSString* path = [bundle pathForResource:@"fileExtension" ofType:@"plist"];
    extensionDictionary = [[NSDictionary alloc]initWithDictionary:[NSDictionary dictionaryWithContentsOfFile:path][@"extensionList"]];
    
}

-(void)review
{
    table.frame = CGRectMake(0,48,self.frame.size.width,self.frame.size.height - 48);
    toolbar.frame = CGRectMake(0,0,self.frame.size.width,48);
    tmp.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    [tmp review];
}


-(void)touchBut:(UIButton *)but
{
    switch (but.tag) {
        case 1:
        {
            //ファイルの情報を取得
            [tmp getFileData];
        }
            break;
        case 2:
        {
            [tmp removeAllLists];
        }
            break;
        case 10:
        {
            tmp.hidden = NO;
            [tmp loadTmp];
            [but setTitle:@"Open temporary." forState:UIControlStateNormal];
        }
            break;
            
        default:
            break;
    }
    
    
}





-(void)setController:(UIViewController *)viewController
{
    controller = viewController;
}

-(void)setCloudSession:(NSUInteger)sessionService
{
    /*
     メニューで選択されたクラウドサービスに応じた処理を施してください。
     
     
     */
    switch (sessionService) {
        case 0:
        {//Dropbox
            [self createDropboxSession];
            [self startDropbox];
        }
            break;
            
        default:
            break;
    }
}


#pragma mark -
#pragma mark - Dropbox
-(void)createDropboxSession
{
    NSString* appKey = @"27gs4uhua8dj36t";
	NSString* appSecret = @"1wwiyo127xhzo53";
	NSString *root = kDBRootDropbox;
	dirPath = @"/";
	DBSession* session = [[DBSession alloc] initWithAppKey:appKey appSecret:appSecret root:root];
	session.delegate = self;
	[DBSession setSharedSession:session];
	[DBRequest setNetworkRequestDelegate:self];
}

-(void)startDropbox
{
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:dirPath,@"path",@"Dropbox",@"name", nil];
    dirList = [NSMutableArray arrayWithObject:dic];
    
    
    
    NSURL *url = (NSURL *)((AppDelegate *)[[UIApplication sharedApplication] delegate]).launchURL;
    lastElement = @"Dropbox";
    if(url)
    {
        if ([[DBSession sharedSession] handleOpenURL:url]) {
            if ([[DBSession sharedSession] isLinked]) {
                [[self restClient] loadMetadata:dirPath];
            }else{
                [[DBSession sharedSession] linkFromController:controller];
            }
        }
    }else{
        if (![[DBSession sharedSession] isLinked]) {
             [[DBSession sharedSession] linkFromController:controller];
        } else {
            [[self restClient] loadMetadata:dirPath];
        }
    }
}


-(void)delayDB
{
   
}


- (DBRestClient*)restClient {
    if (!restClient) {
        restClient = [[DBRestClient alloc] initWithSession:[DBSession sharedSession]];
        restClient.delegate = self;
    }
    return restClient;
}

- (void)restClient:(DBRestClient *)client loadedMetadata:(DBMetadata *)metadata {
    [folderList removeAllObjects];
    if([dirList count] > 2){
        [folderList addObject:dirList[dirList.count - 2][@"path"]];
    }else{
        [folderList addObject:@"/"];
    }
    
    if (metadata.isDirectory) {
        for (DBMetadata *file in metadata.contents) {
            [folderList addObject:file];
        }
        [table reloadData];
        table.hidden = NO;
    }
}

- (void)restClient:(DBRestClient *)client
loadMetadataFailedWithError:(NSError *)error {
    NSLog(@"Error loading metadata: %@", error);
}

#pragma mark -
#pragma mark DBSessionDelegate methods

- (void)sessionDidReceiveAuthorizationFailure:(DBSession*)session userId:(NSString *)userId {
}

#pragma mark -
#pragma mark DBNetworkRequestDelegate methods

static int outstandingRequests;

- (void)networkRequestStarted {
	outstandingRequests++;
	if (outstandingRequests == 1) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
	}
}

- (void)networkRequestStopped {
	outstandingRequests--;
	if (outstandingRequests == 0) {
		[[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
	}
}


#pragma mark -
#pragma mark -- Table

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0){
        return 60;
    }
    return 40.0;
}

// テーブル要素数を返す
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [folderList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConfigIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier: ident];
    }

    if(indexPath.row == 0){
        cell.textLabel.text = lastElement;
        cell.textLabel.font = [UIFont fontWithName:@"Avenir-Black" size:20.0];
        cell.textLabel.textColor = RGBA(125,120,115,1.0);
    }else{
        DBMetadata *file = [folderList objectAtIndex: [indexPath row]];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
        cell.textLabel.text = file.filename;
        cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
        cell.textLabel.textColor = RGBA(125,125,125,1.0);
        
        
        
        NSString *sr = [file.filename pathExtension];
        cell.detailTextLabel.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
        

        
        
        NSString *filePath = [[NSBundle mainBundle] pathForResource:file.icon ofType:@"gif"];
        
        cell.imageView.image = [[UIImage alloc]initWithContentsOfFile:filePath];
        cell.selectionStyle = UITableViewCellSelectionStyleGray;
        
        if(file.isDirectory)
        {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }else{
            if(extensionDictionary[sr]){
                cell.detailTextLabel.text = @"supported";
                cell.detailTextLabel.textColor = RGBA(0,230,0,1.0);
            }else{
                cell.detailTextLabel.text = @"not supported";
                cell.detailTextLabel.textColor = RGBA(255,0,0,1.0);
            }
        }
        
    }
    return cell;
}

// テーブルセルクリック時の処理
- (NSIndexPath *)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if(indexPath.row == 0){
        if(dirList.count > 2){
            
            [[self restClient] loadMetadata:dirList[dirList.count - 2][@"path"]];
            [dirList removeLastObject];
            lastElement = dirList[dirList.count - 1][@"name"];
            
        }else if(dirList.count == 2){
            [[self restClient] loadMetadata:dirPath];
            [dirList removeLastObject];
            lastElement = dirList[dirList.count - 1][@"name"];
            
            
        }else{
            lastElement = dirList[0][@"name"];
        }
    }else{
        DBMetadata *file = [folderList objectAtIndex: [indexPath row]];
        if ([file isDirectory]) {
            lastElement = file.filename;
            NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:file.path,@"path",file.filename,@"name", nil];
            [dirList addObject:dic];
            [[self restClient] loadMetadata:file.path];
        }else{
            
            //file選択時の処理
            
#pragma mark - 選択されたファイルがダウンロードされた後利用可能な拡張子のものか判別します。
            NSString *sr = [file.filename pathExtension];
            if(extensionDictionary[sr]){
                //NSString *ex = [file.filename pathExtension];
                //全てのファイルは一旦一時ファイル保管フォルダ(/tmp)に保管されます。
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString *tmpPath =  [[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"];
                NSString *path = [tmpPath stringByAppendingPathComponent:file.filename];
                
                
                
                
                EVCircularProgressView *progress = [[EVCircularProgressView alloc]init];
                UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
                cell.accessoryView = progress;
                if([stack[path][@"progress"] progress] == 0.0){
                    NSMutableDictionary *dic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:indexPath,@"index",progress,@"progress", nil];
                    stack[path] = dic;
                    [[self restClient] loadFile:file.path intoPath:path];
                }
            }else{
                //unsupport file type.
            }
        }
    }
    return nil;
}


- (void)restClient:(DBRestClient*)client loadProgress:(CGFloat)progress forFile:(NSString*)destPath
{
    [stack[destPath][@"progress"] setProgress:progress animated:YES];
}

- (void)restClient:(DBRestClient *)client loadedFile:(NSString *)localPath
       contentType:(NSString *)contentType metadata:(DBMetadata *)metadata {
    
    
    UITableViewCell *cell = [table cellForRowAtIndexPath:stack[localPath][@"index"]];
    [stack[localPath][@"progress"] setProgress:1.0 animated:YES];
    
    
    
    SEL selector = @selector(delayproc:path:);
    
    NSMethodSignature *signature = [[self class] instanceMethodSignatureForSelector:selector];
    if(signature)
    {
        NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
        [invocation setSelector:selector];
        [invocation setTarget:self];
        [invocation setArgument:&cell atIndex:2];
        [invocation setArgument:&localPath atIndex:3];
        [self performSelector:@selector(performInvocation:)
                   withObject:invocation afterDelay:1.0];
    }
    
}
-(void)performInvocation:(NSInvocation *)anInvocation{
	[anInvocation invokeWithTarget:self];
}

-(void)delayproc:(UITableViewCell *)cell path:(NSString *)path
{
    [cell.accessoryView removeFromSuperview];
    [table reloadData];
}

- (void)restClient:(DBRestClient *)client loadFileFailedWithError:(NSError *)error {
    NSLog(@"There was an error loading the file: %@", error);
}







@end
