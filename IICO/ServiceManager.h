//
//  ServiceManager.h
//  ICOI
//
//  Created by mokako on 2015/10/27.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICOI.h"
#import "ServiceManagerAssist.h"
#import "SessionController.h"
#import "FMDatabaseQueue.h"
#import "FMDatabaseAdditions.h"
#import "PeerController.h"
#import "DataListManager.h"









@interface ServiceManager : NSObject<SessionControllerDelegate>
{
    
    //service account
    NSString *service;
    NSString *display;
    NSString *dspUUID;
    NSString *serviceAccount;
    
    SessionController *session;
    FMDatabaseQueue *db;
    PeerController *pcon;
    DataListManager *DM;
    
    
    NSMutableDictionary *tempList;
    
    NSMutableDictionary *fileList;
    
}

@property (nonatomic) NSString *selectFile;

#pragma mark - initialize service
-(void)initService:(NSString *)serviceName display:(NSString *)display uuid:(NSString *)uuid;
-(BOOL)setFileManager;

#pragma mark - peer controller

-(void)appealPeerToConsole:(NSString *)peerName;
-(void)loadPeerController;


#pragma mark - service state
-(void)startService;
-(void)stopService;
-(void)deleteService;
-(void)endService;


-(void)ignition;



#pragma mark - sketch data
-(void)setSendData:(NSDictionary *)dic;
-(void)setRequestForList:(NSString *)uid;

#pragma mark - sketch path list
@property (nonatomic) NSString *titleID;
@property (nonatomic) NSInteger mode;


#pragma mark - sketch dara list edit mode
@property (nonatomic) editMode edit_mode;
@property (nonatomic) NSArray *pathOrder;

#pragma mark - layer method
-(UIImage *)displaythumbnailImage:(NSString *)fileName size:(CGFloat)size;
-(BOOL)checkImageFile:(NSString *)fileName;
-(void)setDeleteRequest:(NSString *)imageID;




#pragma mark - photo manager
-(void)setNewFileData:(NSDictionary *)fileData;

#pragma mark - log message
-(void)setMessageform:(message_type)type message:(NSString *)string state:(message_state)state;


#pragma mark - chage image to sketch
-(void)selectImage:(NSString *)fileName;

#pragma mark - get database data

-(NSArray *)getListManager:(NSString *)uid root:(BOOL)root;

-(NSArray *)getTreeList;
-(NSArray *)getTreeBranch:(NSString *)uid;
-(NSArray *)getTreeAll:(NSString *)uid;
-(NSInteger)getTreeNumbar:(NSString *)uid;
-(NSArray *)getTreeMessage:(NSString *)uid;
-(NSInteger)getTreeMessageNumbar:(NSString *)uid;
-(NSArray *)getTreeBranchMessage:(NSString *)uid;
-(NSInteger)getTreeBranchMessageNumbar:(NSString *)uid;
-(NSDictionary *)getItemToList:(NSString *)uid;
-(NSArray *)getListWithoutException:(NSString *)uid;
-(BOOL)setUpdateNumber:(NSArray *)list;
-(BOOL)setUpdateList:(NSArray *)list;
-(BOOL)setUpdateComment:(NSDictionary *)dic;


-(BOOL)deleteUidFromList:(NSArray *)list;
-(NSArray *)getRestoreList;

#pragma mark - get method
-(NSString *)getService;
-(NSString *)getUUID;
-(NSString *)getDisplay;


-(double)getJulianDay:(NSDate *)date;



@end
