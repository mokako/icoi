//
//  ExchageHeaderView.m
//  ICOI
//
//  Created by mokako on 2016/03/15.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ExchageHeaderView.h"

@implementation ExchageHeaderView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) { 
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    // 初期化
    //self.label.text = NSStringFromClass([self class]);
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}
@end
