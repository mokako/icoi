//
//  NormalCell.h
//  ICOI
//
//  Created by mokako on 2016/02/05.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ClearToolBar.h"
#import "CustomBarButton.h"


#import "HeaderCurveView.h"
#import "FooterCurveView.h"

#define slideMinWidth 10

@interface NormalCell : UITableViewCell
{
    CustomBarButton *a,*b,*c,*d,*e,*f,*g;
    UIBarButtonItem *gap,*flex,*flex2;
    
    CGPoint oldPoint;
    BOOL isSlide;
    BOOL isOpen;
    
    NSArray *cellHeight;
    
    NSArray *constraits;
    NSArray *colorList;
    CGFloat fontSize;
    
}


@property (weak, nonatomic) IBOutlet UIView *forView;
@property (weak, nonatomic) IBOutlet HeaderCurveView *header;
@property (weak, nonatomic) IBOutlet FooterCurveView *footer;
@property (weak, nonatomic) IBOutlet UIView *statusView;
@property (weak, nonatomic) IBOutlet UILabel *stateIcon;
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UILabel *dateType;
@property (weak, nonatomic) IBOutlet UIButton *spot;
@property (weak, nonatomic) IBOutlet UIView *textBase;
@property (weak, nonatomic) IBOutlet UILabel *article;
@property (weak, nonatomic) IBOutlet ClearToolBar *ct;


-(IBAction)touch:(UIButton *)but;
//element
@property (weak, nonatomic) NSString *uid;
@property (nonatomic) NSIndexPath *indexPath;

-(void)setArticleText:(NSString *)str;
-(void)setButton:(NSInteger)state holder:(BOOL)holder;

-(void)drawCorner;
@end
