//
//  ManifestTable.m
//  ICOI
//
//  Created by mokako on 2014/02/12.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "ManifestoTable.h"

@implementation ManifestoTable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        manifesto = [[NSMutableDictionary alloc]init];
        dataList = [[NSMutableDictionary alloc]init];
        peers = [[NSMutableArray alloc]init];
        AdvertiserFlag = NO;
        [self initializeTable];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
-(void)initializeTable
{
    table = [self createTable:self.frame tag:1];
    table.backgroundColor = RGBA(240, 240, 240, 1.0);
    [self addSubview:table];
}

-(void)review
{
    table.frame = self.frame;
}




-(NSString *)tableView:
(UITableView *)tableView
titleForHeaderInSection:(NSInteger)section
{
    if(section == 0)
    {
        return @"Manifesto settings.";
    }
    return @"";
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    serviceName = [self.delegate setServiceName];
    if(section == 0){
        return 1;
    }else if(section == 1){
        return 4;
    }else if(section == 2){
        return 4;
    }else if(section == 3){
        return 1;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
        cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1 reuseIdentifier:ident];
            cell.backgroundColor = RGBA(255, 255, 255, 0.7);
            cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
            cell.textLabel.textColor = [UIColor blackColor];
            cell.detailTextLabel.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
            cell.detailTextLabel.textColor = [UIColor darkGrayColor];
            if(indexPath.section == 0)
            {
                cell.textLabel.text = @"Host";
                UISwitch *sw = [[UISwitch alloc] init];
                if([manifesto[@"hostUser"]boolValue] == YES)
                {
                    sw.enabled = NO;
                }
                sw.on = [manifesto[@"host"]boolValue];
                sw.tag = 0;
                [sw addTarget:self action:@selector(selectedManifestoTable:) forControlEvents:UIControlEventValueChanged];
                cell.accessoryView = sw;
            }else if(indexPath.section == 1){
                cell.detailTextLabel.textAlignment = NSTextAlignmentCenter;
                if(indexPath.row == 0)
                {
                    cell.textLabel.text = @"HostUser";
                    cell.detailTextLabel.text = [manifesto[@"hostUser"]boolValue] ? @"YES" : @"NO";
                }else if(indexPath.row == 1){
                    cell.textLabel.text = @"HostID";
                    cell.detailTextLabel.text = manifesto[@"hostID"];
                }else if(indexPath.row == 2){
                    cell.textLabel.text = @"Connect Peers";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)[peers count]];
                }else{
                    cell.textLabel.text = @"LastUpdate";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%@",manifesto[@"lastUpdate"]];
                    
                }
            }else if(indexPath.section == 2){
                if(indexPath.row == 0){
                    cell.textLabel.text = @"AddData";
                    UISwitch *sw = [[UISwitch alloc] init];
                    if([manifesto[@"host"]boolValue] == NO)
                    {
                        sw.enabled = NO;
                    }
                    sw.on = [manifesto[@"addData"]boolValue];
                    // 値が変更された時にhogeメソッドを呼び出す
                    sw.tag = 1;
                    [sw addTarget:self action:@selector(selectedManifestoTable:) forControlEvents:UIControlEventValueChanged];
                    cell.accessoryView = sw;
                }else if(indexPath.row == 1){
                    cell.accessoryView = nil;
                    cell.textLabel.text = @"AddCountButton";
                    if([manifesto[@"addData"]boolValue] == YES && [manifesto[@"host"]boolValue] == YES){
                        UIStepper *step = [[UIStepper alloc]init];
                        step.value = [manifesto[@"addCount"]integerValue];
                        step.minimumValue = 0;
                        step.maximumValue = 10;
                        step.stepValue = 1;
                        step.autorepeat = NO;
                        [step addTarget:self action:@selector(stepAddCount:) forControlEvents:UIControlEventTouchUpInside];
                        cell.accessoryView = step;
                    }else{
                        cell.detailTextLabel.text = @"Can not be press";
                    }
                }else if(indexPath.row == 2){
                    cell.textLabel.text = @"AddCount";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)[manifesto[@"addCount"]integerValue]];
                    
                }else if(indexPath.row == 3){
                    cell.textLabel.text = @"Possible number that can be sent by you.";
                    cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld",(long)[manifesto[@"itemCount"]integerValue]];
                }
                
            }else if(indexPath.section == 3){
                //dismiss search mode
                cell.textLabel.text = @"Dismiss search mode";
                UISwitch *sw = [[UISwitch alloc] init];
                sw.on = NO;
                sw.tag = 2;
                sw.on = NO;
                [sw addTarget:self action:@selector(selectedManifestoTable:) forControlEvents:UIControlEventValueChanged];
                cell.accessoryView = sw;
            }
        }
    return cell;
}
-(void)setMember:(SessionController *)session peers:(NSArray *)peerIDs dataList:(NSDictionary *)data
{
    sessionController = session;
    peers = [NSMutableArray arrayWithArray:peerIDs];
    dataList = [NSMutableDictionary dictionaryWithDictionary:data];
    [self setManifesto];
    [table reloadData];
}


-(void)stepAddCount:(UIStepper *)sender
{
    //addCountの変更、変更が適用されるのはクライアントのみでhostには適用されない。クライアントへの変更要請です。
    
    manifesto[@"addCount"] = [NSNumber numberWithInteger:sender.value];
    manifesto[@"lastUpdate"] = [NSDate date];
    if([peers count] > 0)
    {
        [sessionController dataSender:peers data:[SessionController sendDataEncodeToArray:103 data:manifesto]];
    }
    [self changeManfestoTable];
}


-(void)selectedManifestoTable:(UISwitch *)sender
{
    if(sender.tag == 0){
        if(sender.on == YES){
            //manifestoの変更が反映されていない場合を考慮します
            if([manifesto[@"hostUser"]boolValue] == YES)
            {
                [DataManager alert:@"UNSUCCESS" text:@"Host already exists."];
            }else{
                manifesto[@"host"] = @1;
                manifesto[@"hostID"] = [DataManager getHash];
                manifesto[@"lastUpdate"] = [NSDate date];
                manifesto[@"addData"] = @0;
                manifesto[@"itemCount"] = @0;
                //そしてもしネットワークに繋がっているならそれを伝える必要が有ります。
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                    [FileManager saveManifestoPlist:manifesto service:serviceName];
                });
                //data.plist側の処理
                //hostに成った時にdata.plistのhostIDを設定します。逆にhostから降りた時はこの値を放棄する処理を施します。
                dataList[@"hostID"] = manifesto[@"hostID"];
                
                if([peers count] > 0)
                {
                    [sessionController dataSender:peers data:[SessionController sendDataEncodeToArray:103 data:manifesto]];
                }
                [self changeManfestoTable];
            }
        }else{
            [self.delegate resetManifesto];
            [self setManifesto];
            if([peers count] > 0)
            {
                [sessionController dataSender:peers data:[SessionController sendDataEncodeToArray:103 data:manifesto]];
            }
            dataList[@"hostID"] = @"";
            
            
            [self changeManfestoTable];
        }
    }else if(sender.tag == 1){
        //自由モードだとデフォルトが書き込みokなのでhostが決まってる時のみ選択出来ます。
        manifesto[@"lastUpdate"] = [NSDate date];
        manifesto[@"addData"] =  sender.on ? @1 : @0;
        [self changeManfestoTable];
    }else if (sender.tag == 2){
        if(sender.on == YES){
#pragma mark - アドバタイザーの終了設定
            AdvertiserFlag = NO;
        }else{
            AdvertiserFlag = YES;
        }
    }
}
-(void)setManifesto
{
    manifesto = [NSMutableDictionary dictionaryWithDictionary:[self.delegate setManifesto]];
}

-(void)changeManfestoTable
{
    [self.delegate changeManifesto:manifesto];
    [self.delegate changeDataList:dataList];
    dispatch_async(dispatch_get_main_queue(), ^{
        [table reloadData];
    });
}

-(void)reloadManifestoTable{
    manifesto = [NSMutableDictionary dictionaryWithDictionary:[self.delegate setManifesto]];
    dataList = [NSMutableDictionary dictionaryWithDictionary:dataList];
    dispatch_async(dispatch_get_main_queue(), ^{
        [table reloadData];
    });
}

-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStyleGrouped];
    atable.backgroundColor = [UIColor clearColor];
    atable.separatorStyle = UITableViewCellSeparatorStyleNone;
    atable.delegate = self;
    atable.dataSource = self;
    atable.tag = tag;
    atable.rowHeight = 40.0;
    atable.showsVerticalScrollIndicator = NO;
    return atable;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}
@end
