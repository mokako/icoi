//
//  SliderCell.h
//  ICOI
//
//  Created by mokako on 2016/02/03.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClearToolBar.h"
#import "CustomBarButton.h"

#define slideMinWidth 10

@interface SliderCell : UITableViewCell
{
    CustomBarButton *a,*b,*c,*d,*e,*f,*g;
    UIBarButtonItem *gap,*flex,*flex2;
    
    
    //
    CGPoint oldPoint;
    BOOL isSlide;
    BOOL isOpen;
}
@property (weak, nonatomic) IBOutlet ClearToolBar *ct;
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *mess;
@property (weak, nonatomic) IBOutlet UILabel *arow;
@property (weak, nonatomic) IBOutlet UIView *forView;

@property (nonatomic) NSIndexPath *indexPath;
-(void)setButton:(NSInteger)state;

@end
