//
//  UIFlexibleLabel.h
//  ICOI
//
//  Created by picobit.info on 2015/07/18.
//  Copyright (c) 2015年 picobit.info. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AudioToolbox/AudioToolbox.h>

typedef NS_ENUM(NSInteger, soundType){
    type0,
    type1,
    type2
};



@interface UIFlexibleLabel : UILabel
{
    SystemSoundID popupSound;
}
@property (nonatomic)NSString *trigger;
@property (nonatomic)CGFloat radius;
@property (nonatomic)CGFloat minWidth;
@property (nonatomic)CGFloat padding;
@property (nonatomic)BOOL animatable;
-(void)setValue:(NSString *)text;
-(void)popupSoundType:(soundType)type;

@end
