//
//  NormalList.m
//  ICOI
//
//  Created by mokako on 2015/10/29.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "NormalList.h"

@implementation NormalList

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        cellHeight = [@[] mutableCopy];
        list = [@[] mutableCopy];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServiceManager:) name:@"sendServiceManagerSeed" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendNormalList" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setItem) name:@"setItem" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPath:) name:@"addPath" object:nil];
    }
    return self;
}



- (void)awakeFromNib
{
    [super awakeFromNib];

    visitList = [NSMutableDictionary dictionary];
    selectPath = @{@"uid":[NSNull null],@"path":@""};
    selectCell = -1;
    hash = @"";topHash = @"";
    pageCount = 1;
    editUIDs = [@[] mutableCopy];
    fix = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fix.width = -12.5;
    
    selectUID = NULL;
    root = YES;
    /*
     
     mode
     0 normal
     1 normal -> list
     2 list
     
     */
    self.close.transform = CGAffineTransformMakeRotation(M_PI);
    [self.close addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    [self.menu addTarget:self action:@selector(toolToggle) forControlEvents:UIControlEventTouchUpInside];
    [self.update addTarget:self action:@selector(updateListState) forControlEvents:UIControlEventTouchUpInside];
    
    list = [NSMutableArray array];
    //self.backgroundColor = [UIColor clearColor];
    //self.layer.shadowColor = [UIColor colorWithHex:@"999999"].CGColor;
    //self.layer.shadowOffset = CGSizeMake(-1,0);
    //self.layer.shadowOpacity = 0.5;
    //self.layer.shadowRadius = 6;
    
    UINib *nib = [UINib nibWithNibName:@"NormalCell" bundle:nil];
    UINib *nib1 = [UINib nibWithNibName:@"DecoCell" bundle:nil];
    UINib *nib3 = [UINib nibWithNibName:@"UnDecoCell" bundle:nil];
    UINib *nib4 = [UINib nibWithNibName:@"TreeListCell" bundle:nil];
    
    UINib *nib5 = [UINib nibWithNibName:@"SliderCell" bundle:nil];
    //self.table.backgroundColor = [UIColor colorWithHex:@"F5F5F0"];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.rowHeight = UITableViewAutomaticDimension;
    self.table.estimatedRowHeight = 54.0;
    self.table.translatesAutoresizingMaskIntoConstraints = NO;
    [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];
    [self.table registerNib:nib1 forCellReuseIdentifier:@"Cell1"];
    [self.table registerNib:nib3 forCellReuseIdentifier:@"Cell3"];
    [self.table registerNib:nib4 forCellReuseIdentifier:@"Tree"];
    [self.table registerNib:nib5 forCellReuseIdentifier:@"Slide"];
    
    
    dummyCell = [self.table dequeueReusableCellWithIdentifier:@"Cell"];
    fixedHeight = (dummyCell.frame.size.height) - dummyCell.article.frame.size.height;
    
    [self setup];
    
    [self performSelector:@selector(delayProc) withObject:nil afterDelay:4];
}



-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

-(void)delayProc{
    dummyCell.frame = CGRectMake(0,0,self.table.contentSize.width,dummyCell.frame.size.height);
    [dummyCell layoutIfNeeded];
}


-(void)setup{
    tw = [ToolWindow view];
    tw.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:tw];
    
    
    
    NSArray *vw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":tw}];
    [self addConstraints:vw];
    NSArray *mw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-height-[view]|"
                                            options:0
                                            metrics:@{@"height":@40}
                                              views:@{@"view":tw}];
    [self addConstraints:mw];
}



-(void)setServiceManager:(NSNotification *)center{
    sb = [center userInfo][@"manager"];
}



/*
 state 
 0 : line 
 1 : line and message
 2 : title
 3 : sub title
 4 : message
 */
/*
 mode (-> state)
 0 : title -> 2
 1 : sub title -> 3
 2 : message -> 0,1,4
 3 : no title  message -> 0,1,4
 4 : no subtitle message -> 0,1,4
 5 : edit mode
 */
-(void)addPath:(NSNotification *)center{
    NSString *uid = [center userInfo][@"path"][@"pauid"] == [NSNull null] ? @"" : [center userInfo][@"path"][@"pauid"];
    if(selectUID){
        if([selectUID[@"uid"] isEqualToString:uid]){
            if([[center userInfo][@"path"][@"state"] integerValue] == 3 && [selectUID[@"state"] integerValue] == 2){
                    [self drawPath:[center userInfo][@"path"]];
            }else if([selectUID[@"state"] integerValue] == 3){
                [self drawPath:[center userInfo][@"path"]];
            }else if(root == NO){
                [self drawPath:[center userInfo][@"path"]];
            }
        }
    }else if(uid.length == 0){
        //どちらも0
        //しかしここがtitleの階層の為追加するかどうかはrootを調べて下さい。
        if([[center userInfo][@"path"][@"state"] integerValue] == 2 && root == YES){
            [self drawPath:[center userInfo][@"path"]];
        }else if([[center userInfo][@"path"][@"state"] integerValue] != 2 && root == NO){
            [self drawPath:[center userInfo][@"path"]];
        }
    }
    //データはdatabaseに入れられるのでそれ以外はスルー
}


-(void)drawPath:(NSDictionary *)dic{
    dispatch_async(dispatch_get_main_queue(),^{
        [self.table beginUpdates];
        [list addObject:dic];
        NSIndexPath *path = [NSIndexPath indexPathForRow:list.count - 1 inSection:0];
        [self.table insertRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationTop];
        [self.table endUpdates];
        [self.table reloadData];
    });
}

#pragma mark - Update Table
//現在表示されてるリストの更新部分が無いか他の接続者に要求します
-(void)updateListState{
    [sb setRequestForList:selectUID ? selectUID[@"uid"] : @""];
}

#pragma mark - table methods

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if(section == 0){
        if(sb.edit_mode == edit_exchange){
            return 80.0;
        }else{
            return 50.0;
        }
        
    }else{
        return 10.0;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *colorHex = @"6699A6";
    NSString *backMess = @"FEFEFE";
    UIView *view;
    if(section == 0){
        if(sb.edit_mode == edit_normal){
            if(selectUID){
                NormalHeaderView *nv = [NormalHeaderView view];
                [nv.undo addTarget:self action:@selector(backHash) forControlEvents:UIControlEventTouchUpInside];
                [nv setButtonColor:[selectUID[@"state"] integerValue] == 2 ? YES : NO];
                [nv.titleButton setTitle:[NSString stringWithFormat:@"+ %@",selectUID[@"comment"]] forState:UIControlStateNormal];
                [nv.titleButton addTarget:self action:@selector(setTextBoard:) forControlEvents:UIControlEventTouchUpInside];
                return nv;
            }else{
                if(root){
                    NewTitleHeaderView *nv = [NewTitleHeaderView view];
                    [nv.titleButton addTarget:self action:@selector(setTextBoard:) forControlEvents:UIControlEventTouchUpInside];
                    return nv;

                }else{
                    view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width,40)];
                    UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,30,30)];
                    but.backgroundColor = [UIColor colorWithHex:colorHex];
                    [but setTitle:@"v" forState:UIControlStateNormal];
                    but.layer.cornerRadius = 5;
                    but.titleLabel.textAlignment = NSTextAlignmentCenter;
                    but.titleLabel.font = [UIFont fontWithName:@"select" size:17];
                    [but setTitleColor:[UIColor colorWithHex:backMess] forState:UIControlStateNormal];
                    [but addTarget:self action:@selector(backHash) forControlEvents:UIControlEventTouchUpInside];
                    UIBarButtonItem *a = [[UIBarButtonItem alloc]initWithCustomView:but];
                    ClearToolBar *ct = [[ClearToolBar alloc]initWithFrame:view.bounds];
                    [view addSubview:ct];
                    
                    UIBarButtonItem *ga = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
                    
                    ct.items = @[fix,a,ga,fix];
                    
                }
            }
            
        }else if(sb.edit_mode == edit_exchange){
            ExchageHeaderView *ev = [ExchageHeaderView view];
            
            ev.title.text = selectUID ? selectUID[@"comment"] : @"";
            ev.undo.hidden = selectUID ? NO : YES;
            [ev.cancel addTarget:self action:@selector(resetMode) forControlEvents:UIControlEventTouchUpInside];
            [ev.undo addTarget:self action:@selector(backHash) forControlEvents:UIControlEventTouchUpInside];
            [ev.ok addTarget:self action:@selector(changeState) forControlEvents:UIControlEventTouchUpInside];
            return ev;
        }else{
            //EDIT MODE
            
            view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width,40)];
            UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,50,30)];
            //but.backgroundColor = [UIColor colorWithHex:colorHex];
            [but setTitle:@"CANCEL" forState:UIControlStateNormal];
            but.titleLabel.textAlignment = NSTextAlignmentCenter;
            but.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Regular" size:15];
            [but setTitleColor:[UIColor colorWithHex:@"F23827"] forState:UIControlStateNormal];
            [but addTarget:self action:@selector(resetMode) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *a = [[UIBarButtonItem alloc]initWithCustomView:but];
            
            UIButton *tbut = [[UIButton alloc]initWithFrame:CGRectMake(0,0,50,30)];
            NSString *title;
            switch (sb.edit_mode) {
                case edit_pre_exchange:
                {
                    title = @"NEXT";
                }
                    break;
                case edit_normal:
                {
                    //throw
                }
                    break;
                default:
                {
                    title = @"OK";
                }
                    break;
            }
            [tbut setTitle:title forState:UIControlStateNormal];
            tbut.titleLabel.textAlignment = NSTextAlignmentCenter;
            tbut.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Regular" size:15];
            [tbut setTitleColor:[UIColor colorWithHex:@"009EA4"] forState:UIControlStateNormal];
            [tbut addTarget:self action:@selector(changeState) forControlEvents:UIControlEventTouchUpInside];
            UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:tbut];

            ClearToolBar *ct = [[ClearToolBar alloc]initWithFrame:view.bounds];
            [view addSubview:ct];
            UIBarButtonItem *ga = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
            ct.items = @[fix,a,ga,b,fix];
        }
    }else if(section == 1){
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width,10.0)];
    }else{
        view = [[UIView alloc] initWithFrame:CGRectMake(0, 0,self.frame.size.width,0.1)];
    }
    return view;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if(sb.edit_mode != edit_normal){
        return 1;
    }else{
        return 2;
    }
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return list.count;
    }else if (section == 1){
        if(root){
            //rootモード時は非ルートのボタンも表示
            NSInteger num = 1;
            //messageモードの時section1は非表示にします
            if(selectUID){
                NSInteger sta = [selectUID[@"state"] integerValue];
                if(sta != 3){
                    num = 1;
                }else{
                    num = 0;
                }
            }
            return num;
        }
        //非rootモードなので表示しません
    }
    return 0;
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if(sb.edit_mode == edit_normal){
            if(indexPath.row == 0){[cellHeight removeAllObjects];}
            dummyCell.article.text = [list[indexPath.row][@"comment"] length] == 0 ? @"Add line" : list[indexPath.row][@"comment"];
            CGSize si = [dummyCell.article sizeThatFits:CGSizeMake(dummyCell.article.frame.size.width,9999)];
            CGFloat height = ceilf(si.height) + fixedHeight;
            [cellHeight addObject:[NSNumber numberWithFloat:height]];
            return height;
        }
        return UITableViewAutomaticDimension;
    }else{
        return 48;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        
        if(sb.edit_mode == edit_normal){
            if(selectUID){
                if([selectUID[@"state"] integerValue] == 3){
                    return [cellHeight[indexPath.row] floatValue];
                }
            }
        }
        return UITableViewAutomaticDimension;
    }else{
        return 48;
    }
}


-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    if(indexPath.section == 0){
        switch (sb.edit_mode) {
            case edit_normal:
            {
                return  [self setNormalCell:indexPath];
            }
                break;
            default:
            {
                return [self setTreeCell:indexPath];
            }
                break;
        }
    }else if (indexPath.section == 1){
        // no title message
        if(selectUID){
            return [self setUnDecoCell:selectUID[@"uid"]];
        }else{
            return [self setUnDecoCell:@""];
        }
        
    }else{
        return [self setNormalCell:indexPath];
    }
}

#pragma mark - edit mode

- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewCellEditingStyleNone;
}


- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    return YES;
}

- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
    NSInteger sourceRow = fromIndexPath.row;
    NSInteger destRow = toIndexPath.row;
    id object = [list objectAtIndex:sourceRow];
    
    [list removeObjectAtIndex:sourceRow];
    [list insertObject:object atIndex:destRow];
}

- (BOOL)tableView:(UITableView*)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath*)indexPath
{
    return NO;
}


#pragma mark - cell design

-(SliderCell *)setSlideCell:(NSIndexPath *)indexPath{
    SliderCell *cell = [self.table dequeueReusableCellWithIdentifier:@"Slide"];
    
    cell.indexPath = indexPath;
    cell.label.text = [list[indexPath.row][@"comment"] length] != 0 ? list[indexPath.row][@"comment"] : @"ADD LINE";
    if([[sb getUUID] isEqualToString:list[indexPath.row][@"pid"]]){
        [cell setButton:[list[indexPath.row][@"state"] integerValue] == 2 ? 0 : 1];
    }else{
        [cell setButton:[list[indexPath.row][@"state"] integerValue] == 2 ? 2 : 3];
    }
    dispatch_async(dispatch_get_main_queue(),^{
        cell.mess.text = [NSString stringWithFormat:@"%ld",(long)[sb getTreeNumbar:list[indexPath.row][@"uid"]]];
    });
    return cell;
}


-(UnDecoCell *)setUnDecoCell:(NSString *)uid{
    UnDecoCell *cell = [self.table dequeueReusableCellWithIdentifier:@"Cell3"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    dispatch_async(dispatch_get_main_queue(),^{
        cell.mess.text = [NSString stringWithFormat:@"%ld",(long)[[sb getListManager:selectUID ? selectUID[@"uid"] : @"" root:NO] count]];
    });
    return cell;
}


-(DecoCell *)setDecoCell:(NSIndexPath *)indexPath{
    DecoCell *cell = [self.table dequeueReusableCellWithIdentifier:@"Cell1"];
    //cell1.tag = indexPath.row;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.label.text =  [list[indexPath.row][@"comment"] length] != 0 ? list[indexPath.row][@"comment"] : @"ADD LINE";
    dispatch_async(dispatch_get_main_queue(),^{
        cell.mess.text = [NSString stringWithFormat:@"%ld",(long)[sb getTreeNumbar:list[indexPath.row][@"uid"]]];
    });
    
    //[cell setState:[list[indexPath.row][@"state"] integerValue]];
    return cell;
}

-(TreeListCell *)setTreeCell:(NSIndexPath *)indexPath{
    TreeListCell *cell = [self.table dequeueReusableCellWithIdentifier:@"Tree"];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.label.text = [list[indexPath.row][@"comment"] isEqualToString:@""] ? @"Add line" : list[indexPath.row][@"comment"];
    [cell setState:[list[indexPath.row][@"state"] integerValue]];
    return cell;
}


-(void)setSelectTapCell:(NSIndexPath *)indexPath{
    if(selectUID){
        NSInteger sta = [selectUID[@"state"] integerValue];
        if(sta == 2){
            selectUID = list[indexPath.row];
            [self setPickupForMessage];
            [self setHashToMessageBoard:selectUID board:NO];
            list = [[sb getListManager:selectUID[@"uid"] root:YES] mutableCopy];
            [self.table reloadData];
        }else{
            //表示機能の実装可能
        }
    }else{
        selectUID = list[indexPath.row];
        [self setPickupForMessage];
        [self setHashToMessageBoard:selectUID board:NO];
        list = [[sb getListManager:selectUID[@"uid"] root:YES] mutableCopy];
        [self.table reloadData];
    }
}


-(NormalCell *)setNormalCell:(NSIndexPath *)indexPath{
    NormalCell *cell = [self.table dequeueReusableCellWithIdentifier:@"Cell"];
    NSDictionary *dic = list[indexPath.row];
    cell.tag = indexPath.row;
    cell.indexPath = indexPath;
    cell.uid = dic[@"uid"];
    cell.name.text = dic[@"pname"];
    
    BOOL hol = [[sb getUUID] isEqualToString:list[indexPath.row][@"pid"]] == YES ? YES : NO;
    [cell setButton:[list[indexPath.row][@"state"] integerValue] holder:hol];
    if([list[indexPath.row][@"state"] integerValue] == 2 || [list[indexPath.row][@"state"] integerValue] == 3){
        cell.date.text = [NSString stringWithFormat:@"%ld",(long)[sb getTreeNumbar:list[indexPath.row][@"uid"]]];
    }else{
        cell.date.text = [DateFormatter customDataForJulian:[dic[@"date"] doubleValue]];
    }
    cell.article.text = [dic[@"comment"] length] == 0 ? @"Add line" : dic[@"comment"];
    return cell;
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self selectCell:indexPath];
}

-(void)selectCell:(NSIndexPath *)indexPath{
    switch (sb.edit_mode) {
        case edit_order://systemに任せます
        {
            
        }
            break;
        case edit_pre_exchange://複数の移動
        {
            //決定ボタンを押した後で処理が開始します。
            //selectUID = list[indexPath.row];
            //[self setEditMode:sb.edit_mode object:@[selectUID]];
        }
            break;
        case edit_exchange://移動先
        {
            selectUID = list[indexPath.row];
            [self setEditMode:sb.edit_mode object:@[selectUID]];
        }
            break;
        case edit_exclusion:
        {
            
        }
            break;
        case edit_delete:
        {
            
        }
            break;
        case edit_normal:
        {
            if(root == YES){
                if(indexPath.section == 0){
                    root = YES;
                    [self setSelectTapCell:indexPath];
                }else if (indexPath.section == 1){
                    root = NO;
                    if(selectUID){
                        list = [[sb getListManager:selectUID[@"uid"] root:NO] mutableCopy];
                        [self setHashToMessageBoard:selectUID board:NO];
                        //[self setPickupForMessage];
                        [self.table reloadData];
                    }else{
                        list = [[sb getListManager:@"" root:NO] mutableCopy];
                        [self setHashToMessageBoard:selectUID board:NO];
                        [self.table reloadData];
                        
                    }
                }else{
                    
                }
            }
            [tw setToolMode:selectUID root:root];
            
        }
            break;
        default:
        {
            
        }
            break;
    }
    
}



- (void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (sb.edit_mode) {
        case edit_pre_exchange://複数の移動
        {
            
        }
            break;
        default:
            break;
    }
}



#pragma mark - NSNotification Center

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //clear but
        {
            if(selectPath[@"uid"] != [NSNull null]){
                NormalCell *cell = (NormalCell *)[self.table cellForRowAtIndexPath:selectPath[@"path"]];
                cell.spot.layer.borderColor = [RGBA(255,255,255,0.0)CGColor];
                selectPath = @{@"uid":[NSNull null],@"path":@""};
            }
        }
            break;
        case 1:
        {
            //old select path を処理
            NSIndexPath *path = [center userInfo][@"state"][@"path"];
            NormalCell *cell = (NormalCell *)[self.table cellForRowAtIndexPath:path];
            cell.spot.layer.borderColor = [RGBA(255,255,255,0.0)CGColor];
            if(selectPath[@"uid"] == [NSNull null]){
                selectPath = [center userInfo][@"state"];
            }else if([selectPath[@"uid"] isEqualToString:[center userInfo][@"state"][@"uid"]]){
                selectPath = @{@"uid":[NSNull null],@"path":@""};
            }else{
                selectPath = [center userInfo][@"state"];
            }
        }
            break;
        case 2:
        {
            [self.table scrollToRowAtIndexPath:[center userInfo][@"state"][@"path"] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
            break;
        case 3:
        {
            //強制でselect pathの変更
            NSDictionary *dic = [center userInfo][@"state"];
            if(selectPath[@"uid"] == [NSNull null] && dic[@"uid"] == [NSNull null]){
                //both null
            }else if(selectPath[@"uid"] == [NSNull null]){
                NSIndexPath *oldpath = [center userInfo][@"state"][@"path"];
                NormalCell *cell = (NormalCell *)[self.table cellForRowAtIndexPath:oldpath];
                cell.spot.layer.borderColor = [RGBA(155,155,155,1.0)CGColor];
            }else if(dic[@"uid"] == [NSNull null]){
                NormalCell *oldcell = (NormalCell *)[self.table cellForRowAtIndexPath:selectPath[@"path"]];
                oldcell.spot.layer.borderColor = [RGBA(255,255,255,0.0)CGColor];
            }else{
                NormalCell *oldcell = (NormalCell *)[self.table cellForRowAtIndexPath:selectPath[@"path"]];
                oldcell.spot.layer.borderColor = [RGBA(255,255,255,0.0)CGColor];
                
                NSIndexPath *path = [center userInfo][@"state"][@"path"];
                NormalCell *cell = (NormalCell *)[self.table cellForRowAtIndexPath:path];
                cell.spot.layer.borderColor = [RGBA(155,155,155,1.0)CGColor];
            }
            selectPath = [center userInfo][@"state"];
        }
            break;
        case 4:
        {   //rename comment
            /*
             リネームはコメントの管理者（発言者）のみ変更可能です
             変更した内容は接続者に伝える必要があります。
             
             
             データベースの更新及び通知
             
             */
            //送信データはuidとコメントと時間です
            //送信用の種を作ります
            if([sb setUpdateComment:[center userInfo]]){
                [self reloadTable];
            }
        }
            break;
        case 10:
        {
            self.table.scrollEnabled = NO;
        }
            break;
        case 11:
        {
            self.table.scrollEnabled = YES;
        }
            break;
        case 12:
        {
            [self selectCell:[center userInfo][@"indexPath"]];
        }
            break;
        case 20:
        {//edit mode
            //selectListNum = [center userInfo][@"indexPath"];
            [self.table setEditing:NO animated:YES];
            [self modeChageType:[[center userInfo][@"state"] integerValue] object:@[list[[[center userInfo][@"indexPath"] row]]]];
        }
            break;
        case 21:
        {//end edit
            [self resetMode];
        }
            break;
        case 22:
        {//edit tool window
            switch ([[center userInfo][@"state"] integerValue]) {
                case 0:
                {
                    sb.edit_mode = edit_order;
                    [self setEditMode:edit_order object:@[]];
                }
                    break;
                case 1:
                {
                    sb.edit_mode = edit_delete;
                    [self setEditMode:edit_delete object:@[]];
                }
                    break;
                case 2:
                {
                    self.table.editing = NO;
                    sb.edit_mode = edit_pre_exchange;
                    [self setEditMode:edit_pre_exchange object:@[]];
                }
                    break;
                case 3:
                {
                    self.table.editing = NO;
                   [[NSNotificationCenter defaultCenter] postNotificationName:@"sendCanvas" object:nil userInfo:@{@"type":@0,@"uid":selectUID ? selectUID[@"uid"] : [NSNull null]}];
                }
                    break;
                default:
                    break;
            }
            
        }
            break;
        case 23:
        {//delete exclusion ok button tool window
            if(sb.edit_mode == edit_exclusion){
                //所属しているデータを全取得
                NSMutableArray *new = [@[] mutableCopy];
                for(NSDictionary *dic in editUIDs){
                    [new addObjectsFromArray:[sb getListWithoutException:dic[@"uid"]]];
                }
                if(new.count != 0){
                    [sb deleteUidFromList:new];
                }
                [sb setUpdateList:[ListEdit exclusionList:editUIDs]];
            }else if(sb.edit_mode == edit_delete){
                NSMutableArray *new = [@[] mutableCopy];
                for(NSDictionary *dic in editUIDs){
                    [new addObjectsFromArray:[sb getListWithoutException:dic[@"uid"]]];
                }
                if(new.count != 0){
                    [sb deleteUidFromList:new];
                }
                [sb deleteUidFromList:editUIDs];
            }
            [self resetMode];
        }
            break;
        case 24:
        {
            [self resetMode];
        }
            break;
        case 25:
        {//restore exclusionしたリストを復活させます。
            sb.edit_mode = edit_restore;
            [self setEditMode:edit_restore object:@[]];
        }
            break;
        case 99:
        {
            if(sb.edit_mode == edit_normal){
                [self updateTable];
            }
        }
            break;
        case 100:
        {//first contact data exchange

            [self setItem];

        }
            break;
        default:
            break;
    }
}


#pragma mark - table mode chage
-(void)modeChageType:(NSInteger)state object:(NSArray *)obj{
    switch (state) {
        case 0:
        {//order 順序変更
            sb.edit_mode = edit_order;
            [self setEditMode:sb.edit_mode object:obj];
        }
            break;
        case 1:
        {//exchange 属性変更
            sb.edit_mode = edit_exchange;
            [self setEditMode:sb.edit_mode object:obj];
        }
            break;
        case 2:
        {//rename リネーム
            //edit modeの変更を行いません。
            [self setEditMode:edit_rename object:obj];
        }
            break;
        case 3:
        {//exclusion 除外処理
            sb.edit_mode = edit_exclusion;
            editUIDs = [obj mutableCopy];
            [tw deleteMode:YES];
            [tw displayToggle];
        }
            break;
        case 4:
        {//delete 削除
            sb.edit_mode = edit_delete;
            editUIDs = [obj mutableCopy];
            [tw deleteMode:YES];
            [tw displayToggle];
        }
            break;
        case 5:
        {//display 表示
            
        }
            break;
        case 6:
        {//closw
            [self resetMode];
            [tw displayToggle];
        }
            break;
        default:
            
            break;
    }
}


-(void)setEditMode:(editMode)mode object:(NSArray *)object{
    switch (mode) {
        case edit_order:
        {
            self.table.allowsMultipleSelection = NO;
            [self.table setEditing:YES animated:YES];
            [self.table reloadData];
        }
            break;
        case edit_delete:
        {
            self.table.allowsMultipleSelection = YES;
            list = [[sb getListManager:selectUID ? selectUID[@"uid"] : NULL root:root] mutableCopy];
            [self.table reloadData];
        }
            break;
        case edit_pre_exchange:
        {
            self.table.allowsMultipleSelection = YES;
            list = [[sb getListManager:selectUID ? selectUID[@"uid"] : NULL root:root] mutableCopy];
            [self.table reloadData];
        }
            break;
        case edit_exchange:
        {
            if(editUIDs.count != 0){
                list = [[sb getListManager:selectUID ? selectUID[@"uid"] : NULL root:YES] mutableCopy];
                [self.table reloadData];
            }else{
                //init
                
                selectUID = NULL;//一旦初期化させます。
                editUIDs = [object mutableCopy];
                list = [[sb getTreeList] mutableCopy];
                [self.table reloadData];
            }
        }
            break;
        case edit_rename:
        {
            NSDictionary *dic = @{@"type":@6,@"uid":object[0][@"uid"],@"comment":object[0][@"comment"]};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:dic];
        }
            break;
        case edit_restore:
        {
            self.table.allowsMultipleSelection = YES;
            list = [[sb getRestoreList] mutableCopy];
            [self.table reloadData];
        }
        default:
            break;
    }
}


//決定ボタンをおした時の処理です
-(void)changeState{
    switch (sb.edit_mode) {
        case edit_order:
        {
            [sb setUpdateNumber:[ListEdit resetListNum:sb.pathOrder list:list]];
            [self resetMode];
        }
            break;
        case edit_pre_exchange:
        {
            sb.edit_mode = edit_exchange;
            NSArray *ary = [ListEdit setHitList:[self.table indexPathsForSelectedRows] list:list];
            if(ary.count != 0){
                [self setEditMode:sb.edit_mode object:ary];
            }else{
                [self resetMode];
            }
        }
            break;
        case edit_exchange:
        {
            //決定の処理
            /*
             selectUIDも監視してください。
             */
            //exchangeのルール
            /*
             移動先は子要素がある場合
             3 -> 2の変更のみ認めます。
             2の移動は孫要素の関係で不可です。
             その場合新規作成で登録してもらえれば良いので
             その為editUIDsの値を監視して可能かどうかを判断して下さい。
             
             
             ここでは移動可能なものだけを移動させます。
             移動できない要素も存在するということです。
             
             titleは移動不可で順番のみの変更可能(新しく作ればいいだけなので)
             subtitleはtitleへの昇格のみ有効(新しく作ればいいだけなので)
             path系はtitleとsubtitleへの移動は不可とします。
             自由に動けるのはメッセージ系だけです。
             
             
             3      - > 2, 3
             0, 1   - > 3
             4      - > 2, 3
             
             変更する部分はstateとpauidです
             
             */
            //selectUIDが設定されていない場合は-1を設定
            NSInteger sd = selectUID ? [selectUID[@"state"] integerValue] : -1;
            for(int i = 0; i < editUIDs.count; i++){
                NSInteger st = [editUIDs[i][@"state"] integerValue];
                switch (st) {
                    case 0:
                    {
                        if(sd == 3){
                            editUIDs[i][@"pauid"] = selectUID[@"uid"];
                        }
                    }
                        break;
                    case 1:
                    {
                        if(sd == 3){
                            editUIDs[i][@"pauid"] = selectUID[@"uid"];
                        }
                    }
                        break;
                    case 3:
                    {
                        if(sd == 2){
                            editUIDs[i][@"pauid"] = selectUID[@"uid"];
                        }else if (sd == -1){
                            // -> title
                            editUIDs[i][@"state"] = @2;
                            editUIDs[i][@"pauid"] = [NSNull null];
                        }
                    }
                        break;
                    case 4:
                    {
                        if(sd == -1){
                            editUIDs[i][@"state"] = @2;
                            editUIDs[i][@"pauid"] = [NSNull null];
                        }else if(sd == 2){
                            editUIDs[i][@"state"] = @3;
                            editUIDs[i][@"pauid"] = selectUID[@"uid"];
                        }else if(sd == 3){
                            editUIDs[i][@"pauid"] = selectUID[@"uid"];
                        }
                    }
                        break;
                    default:
                    {
                    }
                        break;
                }
            }
            [sb setUpdateList:editUIDs];
            [self resetMode];//初期化
        }
            break;
        case edit_rename:
        {
            //throw
        }
            break;
        case edit_exclusion:
        {
            
        }
            break;
        case edit_delete:
        {
            editUIDs = [[ListEdit setHitList:[self.table indexPathsForSelectedRows] list:list] mutableCopy];
            [tw deleteMode:YES];
            [tw displayToggle];
        }
            break;
        case edit_display:
        {
            
        }
            break;
        case edit_restore:
        {
            [sb setUpdateList:[ListEdit restoreList:[ListEdit setHitList:[self.table indexPathsForSelectedRows] list:list]]];
            [self resetMode];
        }
            break;
        default:
            break;
    }
}

-(void)cancelEdit{
    
    
}

-(void)deleteItem{
    
    
}

-(void)resetMode{
    [editUIDs removeAllObjects];
    self.table.editing = NO;
    self.table.allowsMultipleSelection = NO;
    sb.edit_mode = edit_normal;
    [self updateTable];
}

-(void)updateTable{
    if(selectUID){
        [tw setToolMode:selectUID root:root];
        list = [[sb getListManager:selectUID[@"uid"] root:root] mutableCopy];
    }else{
        [tw setToolMode:NULL root:root];
        list = [[sb getListManager:@"" root:root] mutableCopy];
    }
    [self.table reloadData];
}


-(void)backHash{
    if(root == NO){
        root = YES;
        if(selectUID){
            list = [[sb getListManager:selectUID[@"uid"] root:YES] mutableCopy];
            [self.table reloadData];
        }else{
            [self setItem];
        }
    }else{
        if(selectUID){
            selectUID = selectUID[@"pauid"] != [NSNull null] ? [sb getItemToList:selectUID[@"pauid"]] : NULL;
            if(selectUID){
                [self setPickupForMessage];
                [self setHashToMessageBoard:selectUID board:NO];
                list = [[sb getListManager:selectUID[@"uid"] root:YES] mutableCopy];
                [self.table reloadData];
            }else{
                [self setItem];
            }
        }else{
            [self setItem];
        }
    }
}


-(void)setItem{
    root = YES;
    selectUID = NULL;
    if(sb.edit_mode == edit_normal){
        [self setPickupForMessage];
        [self setHashToMessageBoard:selectUID board:NO];
    }
    list = [[sb getTreeList] mutableCopy];
    [tw setToolMode:0 root:root];
    [self.table reloadData];
}

-(void)reloadTable{
    sb.edit_mode = edit_normal;
    if(sb.mode == 4 || sb.mode == 3){
        list = [[sb getListManager:selectUID ? selectUID[@"uid"] : nil root:NO] mutableCopy];
    }else{
        list = [[sb getListManager:selectUID ? selectUID[@"uid"] : nil root:YES] mutableCopy];
    }
    
    [self.table reloadData];
}

#pragma mark - send massage board


-(void)setTextBoard:(UIButton *)but{
    [self setHashToMessageBoard:selectUID board:YES];
    
}


-(void)setHashToMessageBoard:(NSDictionary *)item board:(BOOL)open{
    NSDictionary *dic = @{@"type":@3,@"item":item ? item : [NSNull null],@"board":[NSNumber numberWithBool:open]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:dic];
}

#pragma mark - send pickup message
/*
 dicは通常のパス及びメッセージの形式でOK
 ピックアップしたデータのみの追加をして下さい
 */
-(void)setPickupForMessage{
    //[[NSNotificationCenter defaultCenter] postNotificationName:@"setMessageTag" object:nil userInfo:@{@"type":@0,@"item":selectUID ? selectUID : [NSNull null]}];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setPickUp" object:nil userInfo:@{@"type":@0,@"item":selectUID ? selectUID : [NSNull null]}];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@10,@"item":selectUID ? selectUID : [NSNull null]}];

}


#pragma mark -display


-(void)delayReload{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketchList" object:nil userInfo:@{@"type":@0}];
}

-(void)fadeOut{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketchList" object:nil userInfo:@{@"type":@0}];
}

-(void)toolToggle{
    [tw displayToggle];
}

@end
