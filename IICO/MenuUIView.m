/*
 MenuUIView.h
 MenuWindow
 
 Created by moca on 2014/02/10.
 Copyright (c) 2014年 mokako. All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */


#import "MenuUIView.h"
#import "CustomCell.h"


@implementation MenuUIView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        isFirst = YES;

        titleArray = [[NSArray alloc]initWithObjects:
                      NSLocalizedString(@"Board",@""),
                      NSLocalizedString(@"Theme",@""),
                      NSLocalizedString(@"Album",@""),
                      NSLocalizedString(@"Link",@""),
                      NSLocalizedString(@"Log",@""),
                      NSLocalizedString(@"Exit",@""),
                       NSLocalizedString(@"Help",@"")
                      , nil];
        imgArray = [[NSArray alloc]initWithObjects:@"7",@"c",@"g",@"8",@"0",@"k",@"g",nil];
        fontArray = [[NSArray alloc]initWithObjects:@"font-icon-l-02",@"font-icon-l-01",@"font-icon-l-02",@"font-icon-l-00",@"font-icon-l-03",@"font-icon-l-03",@"font-icon-l-01",nil];


        select = RGBA(242,129,28,1.0);
        selectTitle = RGBA(242,129,28,1.0);
        defaultColor = RGBA(130,105,105,0.8);
        backGround = RGBA(255,255,255,0.0);//RGBA(0,15,15,1.0);
        
        
        gradient = [CAGradientLayer layer];
        gradient.type = kCAGradientLayerAxial;
        gradient.frame = self.bounds;
        gradient.colors = @[
                            (id)[UIColor colorWithHex:@"#70e1f5"].CGColor,
                            (id)[UIColor colorWithHex:@"#ffd194"].CGColor
                            ];
        gradient.startPoint = CGPointMake(0, 0);
        gradient.endPoint = CGPointMake(1,1);
        [self.layer addSublayer:gradient];
        oldSelected = [NSIndexPath indexPathForRow:0 inSection:0];
        [self initializeTableView];
        
        [self createAndLoadBanner];
    }
    return self;
}


-(void)update{
    isOrientation = [DeviceData deviceOrientation];
    gradient.frame = self.bounds;
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        if(isOrientation){
            height = 50;
        }else{
            height = 60;
        }
    }else{
        height = 90;
    }
    GADAdSize size = GADAdSizeFromCGSize(CGSizeMake(self.frame.size.width - 48,height));
    banner.adSize = size;
    [banner setCenter:CGPointMake(self.frame.size.width / 2 - 24,self.frame.size.height - banner.bounds.size.height/2)];
}



-(void)initializeTableView
{
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        // iPhone
        layoutWidth = 70.0;
        layoutHeight = 70.0;
        textSize = 12;
        scale = 1;
    }
    else{
        // iPad
        layoutWidth = 100.0;
        layoutHeight = 100.0;
        textSize = 12;
        scale = 1.3;
    }
    
    
    layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(layoutWidth,layoutHeight);
    layout.minimumInteritemSpacing = 0.0f;
    layout.minimumLineSpacing = 0.0f;

    collectview = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    collectview.translatesAutoresizingMaskIntoConstraints = NO;
    [collectview setDelegate:self];
    [collectview setDataSource:self];
    [collectview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CELL"];
    [collectview setBackgroundColor:backGround];
    
    [self addSubview:collectview];
       
    NSArray *clw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]-40-|"
                                            options:0
                                            metrics:@{@"width":@40}
                                              views:@{@"view":collectview}];
    [self addConstraints:clw];
    
    NSArray *clh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":collectview}];
    [self addConstraints:clh];
    
}

- (void)createAndLoadBanner {
    
    GADAdSize size = GADAdSizeFromCGSize(CGSizeMake(self.frame.size.width - 48,60));
    
    banner = [[GADBannerView alloc]initWithAdSize:size];
    [banner setCenter:CGPointMake(self.frame.size.width / 2 - 24,self.frame.size.height - banner.bounds.size.height/2)];
    
    UIViewController* root = [[[[UIApplication sharedApplication] delegate] window] rootViewController];

    banner.rootViewController = root;
    banner.adUnitID = @"ca-app-pub-0635832485550282/2171212642";
    
    [self addSubview:banner];
    GADRequest *request = [GADRequest request];
    request.testDevices = @[ @"47eb100b6ff927cb8361491950a35afe",@"c974abebdaddc702fd96aed2e27dedc5",@"e76551613465bc3a56bb49551e26f7ce" ];
    [banner loadRequest:request];
}

- (void)adView:(GADBannerView *)bannerView
didFailToReceiveAdWithError:(GADRequestError *)error {
    banner.hidden = YES;
}
//loadRequest成功
- (void)adViewDidReceiveAd:(GADBannerView *)bannerView {
    banner.hidden = NO;
}
//CELL数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return titleArray.count;
}
//collection数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0,0,70 * scale,70 * scale)];
    label.font = [UIFont fontWithName:fontArray[indexPath.row] size:(30.0 * scale)];
    label.text = imgArray[indexPath.row];
    label.textAlignment = NSTextAlignmentCenter;
    
    label.tag = 1;
    [cell.contentView addSubview:label];
    
    UILabel *title = [[UILabel alloc]initWithFrame:CGRectMake(0,60 * scale,70 * scale,10 * scale)];
    title.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:textSize];
    title.textAlignment = NSTextAlignmentCenter;
    title.text = titleArray[indexPath.row];
    title.tag = 2;
    [cell.contentView addSubview:title];
    
    if(isFirst && indexPath.row == 0){
        label.textColor = select;
        title.textColor = selectTitle;
    }else{
        label.textColor = defaultColor;
        title.textColor = defaultColor;
    }
    
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    //とりあえずペイントを初期化
    if(indexPath.row == DescriptionNumber || indexPath.row == LogNumbar){
        //throw
        //oldSelected = indexPath;
    }else{
        ((UILabel *)[[collectionView cellForItemAtIndexPath:oldSelected].contentView viewWithTag:1]).textColor = defaultColor;
        ((UILabel *)[[collectionView cellForItemAtIndexPath:oldSelected].contentView viewWithTag:2]).textColor = defaultColor;

        oldSelected = indexPath;
        ((UILabel *)[[collectionView cellForItemAtIndexPath:indexPath].contentView viewWithTag:1]).textColor = select;
        ((UILabel *)[[collectionView cellForItemAtIndexPath:indexPath].contentView viewWithTag:2]).textColor = selectTitle;
    }
    [self.delegate receiveTableRow:indexPath];
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    /*
    if(oldSelected.row == DescriptionNumber || oldSelected.row == LogNumbar){
        //throw
    }else{
        
        ((UILabel *)[[collectionView cellForItemAtIndexPath:indexPath].contentView viewWithTag:1]).textColor = defaultColor;
        ((UILabel *)[[collectionView cellForItemAtIndexPath:indexPath].contentView viewWithTag:2]).textColor = defaultColor;
    }
     */
}

-(void)stepPaint{
    //過去の選択を初期化
    ((UILabel *)[[collectview cellForItemAtIndexPath:oldSelected].contentView viewWithTag:1]).textColor = defaultColor;
    ((UILabel *)[[collectview cellForItemAtIndexPath:oldSelected].contentView viewWithTag:2]).textColor = defaultColor;
    
    
    NSIndexPath *dindexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    oldSelected = dindexPath;
    ((UILabel *)[[collectview cellForItemAtIndexPath:dindexPath].contentView viewWithTag:1]).textColor = select;
    ((UILabel *)[[collectview cellForItemAtIndexPath:dindexPath].contentView viewWithTag:2]).textColor = selectTitle;
}


-(void)stepLayer{
    //過去の選択を初期化
    ((UILabel *)[[collectview cellForItemAtIndexPath:oldSelected].contentView viewWithTag:1]).textColor = defaultColor;
    ((UILabel *)[[collectview cellForItemAtIndexPath:oldSelected].contentView viewWithTag:2]).textColor = defaultColor;
    
    
    NSIndexPath *dindexPath = [NSIndexPath indexPathForRow:2 inSection:0];
    oldSelected = dindexPath;
    ((UILabel *)[[collectview cellForItemAtIndexPath:dindexPath].contentView viewWithTag:1]).textColor = select;
    ((UILabel *)[[collectview cellForItemAtIndexPath:dindexPath].contentView viewWithTag:2]).textColor = selectTitle;
}

-(void)stepAlbum{
    //過去の選択を初期化
    ((UILabel *)[[collectview cellForItemAtIndexPath:oldSelected].contentView viewWithTag:1]).textColor = defaultColor;
    ((UILabel *)[[collectview cellForItemAtIndexPath:oldSelected].contentView viewWithTag:2]).textColor = defaultColor;
    
    
    NSIndexPath *dindexPath = [NSIndexPath indexPathForRow:3 inSection:0];
    oldSelected = dindexPath;
    ((UILabel *)[[collectview cellForItemAtIndexPath:dindexPath].contentView viewWithTag:1]).textColor = select;
    ((UILabel *)[[collectview cellForItemAtIndexPath:dindexPath].contentView viewWithTag:2]).textColor = selectTitle;
}
























@end