//
//  DataListManager.m
//  ICOI
//
//  Created by mokako on 2016/03/21.
//  Copyright © 2016年 moca. All rights reserved.
//
//主にデータリスト内の設定に関わる部分を担当
#import "DataListManager.h"

@implementation DataListManager
-(instancetype)init{
    self = [super init];
    if(self){
    }
    return self;
}

-(void)setServiceName:(NSString *)name{
    self.service = name;
    dataList = [[FileManager loadDataPlist:self.service] mutableCopy];
}


-(void)initDataList{
    self.dataInfo = [dataList objectForKey:@"dataInfo"] != nil ? [dataList[@"dataInfo"] isEqual:[NSNull null]] ? [NSNull null] : [dataList[@"dataInfo"] mutableCopy] : [NSNull null];
    self.settingInfo = [dataList objectForKey:@"settingInfo"] != nil ? [dataList[@"settingInfo"] mutableCopy] : [self setSettings];
    self.version = [dataList objectForKey:@"version"] != nil ? [dataList objectForKey:@"version"]: @"0.1.9";
}


-(NSDictionary *)setSettings{
    return @{
             @"type" : @1
             
             
             
             };
}


-(BOOL)save{
    if([FileManager saveDataPlist:self.service data:@{
                                                       @"settings"  : self.settingInfo,
                                                       @"version"   : self.version,
                                                       @"dataInfo"  : self.dataInfo
                                                       }]){
        return YES;
    }else{
        return NO;
    }
}
@end
