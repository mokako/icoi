//
//  DescriptionSetter.m
//  ICOI
//
//  Created by mokako on 2015/07/11.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "DescriptionSetter.h"
#import "CustomCell.h"
@implementation DescriptionSetter
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        sectionList = @[
                        @"ICOI",
                        NSLocalizedString(@"icoi-support",@""),
                        NSLocalizedString(@"icoi-use",@"")
                        ];
        mode = desc_normal;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    UINib *nib = [UINib nibWithNibName:@"DescriptionSetterCell" bundle:nil];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.estimatedRowHeight = 44.0;
    [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];

    setHTML = [SetDescHTML view];
    setHTML.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:setHTML];
    NSArray *sww =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":setHTML}];
    [self addConstraints:sww];
    
    NSArray *sh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":setHTML}];
    [self addConstraints:sh];
    
    [setHTML setConstraint:sww];
    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    if(mode == desc_normal){
        return [sectionList count];
    }else{
        return 1;
    }
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return sectionList[section];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 0:
        {
            return 1;
        }
            break;
        case 1:
        {
            return 3;
        }
            break;
        case 2:
        {
            return 1;
        }
            break;
        default:
            break;
            return 0;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
        DescriptionSetterCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
        switch (indexPath.section) {
            case 0:
            {
                cell.label.text = NSLocalizedString(@"icoi-about",@"");
                cell.icon.text = @"I";
            }
                break;
            case 1:
            {//サポート
                switch (indexPath.row) {
                    case 0:
                    {
                        cell.label.text = NSLocalizedString(@"icoi-account",@"");
                        cell.icon.text = @"i";
                    }
                        break;
                    case 1:
                    {
                        cell.label.text = NSLocalizedString(@"icoi-guide",@"");
                        cell.icon.text = @"9";
                    }
                        break;
                    case 2:
                    {
                        cell.label.text = @"Q&A";
                        cell.icon.text = @"?";
                    }
                        break;
                    default:
                        break;
                }
                
            }
                break;
            case 2:
            {
                switch (indexPath.row) {
                    case 0:
                    {
                        cell.label.text = NSLocalizedString(@"icoi-terms",@"");
                        cell.icon.text = @"d";
                    }
                        break;
                    default:
                        break;
                }
                
            }
                break;
            default:
                break;
        }
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    switch (indexPath.section) {
        case 0:
        {
            [setHTML setHtmlSetting:@"AboutICOI"];
            [setHTML fadeIn];
        }
            break;
        case 1:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [setHTML setHtmlSetting:@"AccountSetting"];
                    [setHTML fadeIn];
                }
                    break;
                case 1:
                {
                    [setHTML setHtmlSetting:@"HowToUse"];
                    [setHTML fadeIn];
                }
                    break;
                case 2:
                {
                    [setHTML setHtmlSetting:@"Troubleshooting"];
                    [setHTML fadeIn];
                }
                default:
                    break;
            }
        }
            break;
        case 2:
        {
            switch (indexPath.row) {
                case 0:
                {
                    [setHTML setHtmlSetting:@"Policy"];
                    [setHTML fadeIn];
                }
                    break;
                default:
                    break;
            }
        }
            break;
        default:
            break;
    }
    
}














@end