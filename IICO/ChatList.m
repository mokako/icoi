//
//  ChatList.m
//  ICOI
//
//  Created by mokako on 2015/11/27.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "ChatList.h"

@implementation ChatList
-(id)init{
    self = [super init];
    if(self){
        
        list = [[NSMutableArray alloc]init];
        
        self.clipsToBounds = YES;
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchChatEvent:) name:@"toChatView" object:nil];
        defaultFont = [UIFont fontWithName:@"AvenirNext-Medium" size:15];
        pageCount = 0;//表示ページ
        //articleCount = 50;//表示件数
        //isRefChat = NO;
        //setHash = @"";
        cd = [[ChatData alloc]init];
        
        
        colorHex = @"DCF0F0";
        mainColorHex = @"FFFFFF";
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServiceManager:) name:@"sendServiceManagerSeed" object:nil];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendNormalList" object:nil];
        //[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPath:) name:@"addPath" object:nil];
        
        
        //[self setChatData];
        [self setup];

        
        
    }
    return self;
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setServiceManager:(NSNotification *)center{
    sb = [center userInfo][@"manager"];
    //[self setList:[sb getPathtList:maxCount offset:0]];
}

-(void)setChatData{
    //pagecountはarticlecount分取得します。
    //NSInteger count = [cd getChatCount];
    //pageCount = count - articleCount;
    //list = [NSMutableArray arrayWithArray:[cd getChat:pageCount limit:articleCount]];
}

-(void)setup{
    UINib *nib = [UINib nibWithNibName:@"MessageCell" bundle:nil];
    
    table = [[UITableView alloc]initWithFrame:CGRectZero style:UITableViewStyleGrouped];
    table.backgroundColor = [UIColor clearColor];
    table.dataSource = self;
    table.delegate = self;
    table.backgroundColor = RGBA(255,255,255,0.0);
    
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    [table registerNib:nib forCellReuseIdentifier:@"Cell"];
    table.translatesAutoresizingMaskIntoConstraints = NO;
    table.rowHeight = UITableViewAutomaticDimension;
    table.estimatedRowHeight = 115.0;
    [self addSubview:table];
    [table setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0,0,0.1,0.1)]];
    
    UIRefreshControl *refreshControl = [[UIRefreshControl alloc] init];
    [refreshControl addTarget:self action:@selector(onRefresh:) forControlEvents:UIControlEventValueChanged];
    [table addSubview:refreshControl];
    
    
}

-(void)onRefresh:(UIRefreshControl *) refreshControl{
    [refreshControl beginRefreshing];
    
    //[self getPreviousChat];
    
    [refreshControl endRefreshing];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
