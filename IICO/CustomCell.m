//
//  CustomCell.m
//  ICOI
//
//  Created by mokako on 2014/02/14.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "CustomCell.h"

@implementation CustomCell
-(instancetype)initWithStyleAndColor:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier defaultColor:(UIColor *)defaultColor selectColor:(UIColor *)selectColor defaultBack:(UIColor *)defaultBack selectBack:(UIColor *)backColor
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        d = defaultColor;
        s = selectColor;
        b = backColor;
        c = defaultBack;
    }
    return self;
}

- (void)willTransitionToState:(UITableViewCellStateMask)state
{
    [super willTransitionToState:state];
    if ((state & UITableViewCellStateShowingDeleteConfirmationMask) == UITableViewCellStateShowingDeleteConfirmationMask)
    {
        //willTransition
    }
    
}

-(void) setHighlighted:(BOOL)highlighted animated:(BOOL)animated
{
    if (highlighted)
    {
        if (self.selectionStyle == UITableViewCellSelectionStyleNone)
        {
            self.backgroundColor = b; //選択状態の色をセット
            self.textLabel.backgroundColor = [UIColor clearColor];
            self.textLabel.textColor = s;
        }
    }
    else
    {
        self.backgroundColor = c;  //非選択状態の色をセット
        self.textLabel.textColor = d;
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setFrame:(CGRect)frame
{
    frame.origin.x += self.inWidth;
    frame.size.width -= 2 * self.inWidth;
    [super setFrame:frame];
}

@end
