//
//  TreeListCell.m
//  ICOI
//
//  Created by mokako on 2015/11/29.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "TreeListCell.h"

@implementation TreeListCell

- (void)awakeFromNib {
    // Initialization code

    colorList = @[
                  RGBA(145,145,145,0.5),
                  RGBA(88,179,49,0.5),
                  RGBA(0,159,232,0.5),
                  RGBA(236,111,0,0.5),
                  RGBA(240,96,96,0.5),
                  [UIColor colorWithHex:@"D300F8"],
                  [UIColor colorWithHex:@"D300F8"]
                  ];
    self.icon.layer.cornerRadius = 12;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    if(selected){
        self.icon.backgroundColor = [UIColor colorWithHex:@"A09F9B"];
    }else{
        self.icon.backgroundColor = RGBA(145,145,145,0.0);
    }
    // Configure the view for the selected state
}

-(void)setState:(NSInteger)state{
    self.icon.textColor = colorList[state];
    switch (state) {
        case 0://no comment path
        {
            self.icon.font = [UIFont fontWithName:@"font-icon-l-01" size:20.0];
            self.icon.text = @"e";
        }
            break;
        case 1:// comment path
        {
            self.icon.font = [UIFont fontWithName:@"font-icon-l-01" size:20.0];
            self.icon.text = @"e";
        }
            break;
        case 2:// title
        {
            self.icon.font = [UIFont fontWithName:@"font-icon-l-02" size:20.0];
            self.icon.text = @"o";
        }
            break;
        case 3:// sub
        {
            self.icon.font = [UIFont fontWithName:@"font-icon-l-01" size:20.0];
            self.icon.text = @"w";
        }
            break;
        case 4:// message
        {
            self.icon.font = [UIFont fontWithName:@"font-icon-l-01" size:20.0];
            self.icon.text = @"f";
        }
            break;
        case 5:
        {
            self.icon.font = [UIFont fontWithName:@"select3" size:20.0];
            self.icon.text = @"J";
        }
            break;
        case 6:
        {
            self.icon.font = [UIFont fontWithName:@"select3" size:20.0];
            self.icon.text = @"J";
        }
            break;
        default:
            break;
    }
}

@end
