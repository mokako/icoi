//
//  CustomCell.h
//  ICOI
//
//  Created by mokako on 2014/02/14.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>


#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


@interface CustomCell : UITableViewCell
{
    UIColor *s;
    UIColor *d;
    UIColor *b;
    UIColor *c;
}
-(instancetype)initWithStyleAndColor:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier defaultColor:(UIColor *)defaultColor selectColor:(UIColor *)selectColor defaultBack:(UIColor *)defaultBack selectBack:(UIColor *)backColor;
@property (nonatomic) CGFloat inWidth;
@end
