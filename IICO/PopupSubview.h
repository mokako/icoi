//
//  PopupSubview.h
//  ICOI
//
//  Created by mokako on 2015/09/05.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

typedef NS_ENUM(NSInteger, event){
    OK,
    Reject,
    Cancel,
};

@protocol PopupSubviewDelegate <NSObject>
-(void)hide;
-(void)popupview:(event)eventState;
@end

typedef NS_ENUM(NSInteger, type){
    Caution,
    Warning,
    Fatal
};

@interface PopupSubview : UIView
{
    
}
@property (nonatomic) UILabel *title;
@property (nonatomic) UITextView *textView;

-(void)type:(type)type;
-(void)setTitle:(NSString *)title text:(NSString *)text;
@property(nonatomic, weak)id<PopupSubviewDelegate>delegate;

@end
