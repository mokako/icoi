//
//  RemoveStackResponse.h
//  ICOI
//
//  Created by mokako on 2015/01/18.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"


@protocol RemoveStackResponseDelegate <NSObject>
-(void)eraserBut:(UIButton *)but;

@end

@interface RemoveStackResponse : UIView
{
    UIView *back;
    
    
    BOOL isIPhone;
    BOOL isRotate;
    UIColor *fontColor;
    UIColor *selectColor;
    CGFloat fontSize;
}

@property (nonatomic, weak) id<RemoveStackResponseDelegate>delegate;
-(void)display;
-(void)hidden;
@end
