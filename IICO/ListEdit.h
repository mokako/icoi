//
//  NormalListEdit.h
//  ICOI
//
//  Created by mokako on 2016/01/19.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ListEdit : NSObject



+(NSArray *)resetListNum:(NSArray *)listID list:(NSArray *)list;
+(NSArray *)resetListPauid:(NSArray *)list uid:(NSString *)uid;
+(NSArray *)setHitList:(NSArray *)index list:(NSArray *)list;
+(NSArray *)exclusionList:(NSArray *)list;
+(NSArray *)restoreList:(NSArray *)list;
+(NSArray *)displayList:(NSArray *)list;
@end
