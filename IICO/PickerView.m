//
//  PickerView.m
//  ICOI
//
//  Created by mokako on 2014/09/29.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "PickerView.h"

@implementation PickerView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* path = [bundle pathForResource:@"Config" ofType:@"plist"];
        NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
        fileUploadInterval = [dic[@"fileUploadInterval"] floatValue];
        oldDate = [NSDate dateWithTimeIntervalSinceNow:-1 * fileUploadInterval];
        setScale = 1.0;
        
        [self initializeView];
        self.clipsToBounds = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/


-(void)initializeView
{    
    UIColor *fontColor = RGBA(100,100,100,1.0);//font
    
    sc = [[UIScrollView alloc]initWithFrame:CGRectZero];
    sc.backgroundColor = RGBA(45,45,45,1.0);
    [sc setMinimumZoomScale:0.1];
    [sc setMaximumZoomScale:3.0];
    sc.delegate = self;
    sc.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:sc];
    NSArray *scw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":sc}];
    [self addConstraints:scw];
    
    NSArray *sch =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":sc}];
    [self addConstraints:sch];
    
    
    //Image state change
    //画像の倍率を変更させます。
    imageSizeTable = [[ImageSIzeTable alloc]initWithFrame:CGRectZero];
    imageSizeTable.translatesAutoresizingMaskIntoConstraints = NO;
    imageSizeTable.delegate = self;
    [self addSubview:imageSizeTable];
    //self.frame = CGRectMake(r.size.width / 2 - 125,r.size.height - 300,250,240);
    
    NSArray *imw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":imageSizeTable}];
    [self addConstraints:imw];
    
    NSArray *imh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]-40-|"
                                            options:0
                                            metrics:@{@"height":@240}
                                              views:@{@"view":imageSizeTable}];
    [self addConstraints:imh];
    
    
    
    
    //prev


    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    

    UIButton *step1 = [ICOIImage setIcon:[UIFont fontWithName:@"point" size:24] icon:@"l" title:NSLocalizedString(@"Prev",@"") color:fontColor];
    step1.tag = 0;
    [step1 addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *a = [[UIBarButtonItem alloc]initWithCustomView:step1];
    
    
    
    //next

    
    UIButton *step2 = [ICOIImage setIcon:[UIFont fontWithName:@"point" size:24] icon:@"o" title:NSLocalizedString(@"Next",@"") color:fontColor];
    step2.tag = 1;
    [step2 addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:step2];
    
    
    
    
    //resize
    UIButton *step3 = [ICOIImage setIcon:[UIFont fontWithName:@"font-icon-01" size:24] icon:@"a" title:NSLocalizedString(@"Resize",@"") color:fontColor];
    step3.tag = 2;
    [step3 addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *c = [[UIBarButtonItem alloc]initWithCustomView:step3];
    
    
    
    
    //set
    UIButton *step4 = [ICOIImage setIcon:[UIFont fontWithName:@"point" size:24] icon:@"h" title:NSLocalizedString(@"Set",@"") color:fontColor];
    step4.tag = 3;
    [step4 addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *d = [[UIBarButtonItem alloc]initWithCustomView:step4];
    
 
    
    
    
    
    //back
    UIButton *step5 = [ICOIImage setIcon:[UIFont fontWithName:@"point" size:24] icon:@"9" title:NSLocalizedString(@"Back",@"") color:fontColor];
    step5.tag = 4;
    [step5 addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *e = [[UIBarButtonItem alloc]initWithCustomView:step5];

    tool = [[ClearToolBar alloc]initWithFrame:CGRectZero];
    tool.translatesAutoresizingMaskIntoConstraints = NO;
    tool.backgroundColor = RGBA(245,245,245,1.0);
    tool.items = [NSArray arrayWithObjects:a,gap,b,gap,c,gap,d,gap,e,nil];
    [self addSubview:tool];
    
    NSArray *tw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":tool}];
    [self addConstraints:tw];
    
    NSArray *th =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]|"
                                            options:0
                                            metrics:@{@"height":@40}
                                              views:@{@"view":tool}];
    [self addConstraints:th];
    
#pragma mark - nk
    nk = [[UIView alloc]initWithFrame:CGRectZero];
    nk.hidden = YES;
    nk.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:nk];
    NSArray *nkw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[message]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"message":nk}];
    [self addConstraints:nkw];
    nkh = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[message(height)]|"
                                                  options:0
                                                  metrics:@{@"height":@100}
                                                    views:@{@"message":nk}];
    [self addConstraints:nkh];
    
    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.translatesAutoresizingMaskIntoConstraints = NO;
    [nk addSubview:visualEffectView];
    NSArray *vw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[visualEffectView]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"visualEffectView":visualEffectView}];
    [nk addConstraints:vw];
    NSArray *vh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[visualEffectView]|"
                                                  options:0
                                                  metrics:nil
                                                    views:@{@"visualEffectView":visualEffectView}];
    [nk addConstraints:vh];
    
    
    
    
    
    UIButton *sender = [[UIButton alloc]initWithFrame:CGRectZero];
    sender.tag = 1;
    sender.layer.cornerRadius = 15;
    [sender setBackgroundColor:RGBA(59,192,195,1.0)];
    [sender setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    sender.titleLabel.font = [UIFont fontWithName:@"font-icon-00" size:24.0];
    [sender setTitle:@"3" forState:UIControlStateNormal];
    sender.translatesAutoresizingMaskIntoConstraints = NO;
    [sender addTarget:self action:@selector(sendButStat:) forControlEvents:UIControlEventTouchUpInside];
    [nk addSubview:sender];
    /*
    
    */
    UIButton *can = [[UIButton alloc]initWithFrame:CGRectZero];
    can.tag = 0;
    [can setBackgroundColor:RGBA(255,255,255,0.0)];
    [can setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    can.titleLabel.font = [UIFont fontWithName:@"font-icon-00" size:20.0];
    [can setTitle:@"t" forState:UIControlStateNormal];
    can.titleEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 0);
    can.translatesAutoresizingMaskIntoConstraints = NO;
    [can addTarget:self action:@selector(sendButStat:) forControlEvents:UIControlEventTouchUpInside];
    [nk addSubview:can];
    
    
    UILabel *ct = [[UILabel alloc]initWithFrame:CGRectZero];
    ct.text = NSLocalizedString(@"Back",@"");
    ct.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
    ct.textColor = RGBA(255,255,255,1.0);
    ct.textAlignment = NSTextAlignmentCenter;
    ct.translatesAutoresizingMaskIntoConstraints = NO;
    [can addSubview:ct];
    
    
    
    
    commentText = [[UITextField alloc]initWithFrame:CGRectZero];
    [[commentText layer]setBorderColor:[RGBA(125,125,125,1.0) CGColor]];
    [[commentText layer]setBorderWidth:0.5];
    commentText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"FileName",@"") attributes:@{NSForegroundColorAttributeName: RGBA(155,150,145,1.0) , NSFontAttributeName :  [UIFont fontWithName:@"Avenir Next" size:10.0]}];
    commentText.textAlignment = NSTextAlignmentCenter;
    commentText.backgroundColor = RGBA(255,255,255,1.0);
    commentText.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    commentText.textColor = RGBA(35,30,25,1.0);
    commentText.layer.cornerRadius = 5;
    [commentText setClearButtonMode:UITextFieldViewModeWhileEditing];
    commentText.translatesAutoresizingMaskIntoConstraints = NO;
    [nk addSubview:commentText];
    
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectZero];
    label.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
    label.textColor = RGBA(230,230,230,1.0);
    label.textAlignment = NSTextAlignmentCenter;
    label.text = NSLocalizedString(@"FileNameDescription",@"");
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [nk addSubview:label];
    
    
    NSArray *cw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-5-[back(30)]-20-[commentText]-20-[set(30)]-5-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"back":can,@"commentText":commentText,@"set":sender}];
    [nk addConstraints:cw];
    NSArray *lw = [NSLayoutConstraint constraintsWithVisualFormat:@"|-65-[label]-65-|"
                                                          options:0
                                                          metrics:nil
                                                            views:@{@"label":label}];
    [nk addConstraints:lw];
    NSArray *ch =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[commentText(30)]-5-[label(15)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"commentText":commentText,@"label":label}];
    [nk addConstraints:ch];
    NSArray *bh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[back(30)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"back":can}];
    [nk addConstraints:bh];
    NSArray *sh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[set(30)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"set":sender}];
    [nk addConstraints:sh];
    NSArray *ctw = [NSLayoutConstraint constraintsWithVisualFormat:@"|[label]|"
                                                           options:0
                                                           metrics:nil
                                                             views:@{@"label":ct}];
    [can addConstraints:ctw];
    NSArray *cth =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[label(10)]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"label":ct}];
    [can addConstraints:cth];
    
    
    sizeLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    sizeLabel.translatesAutoresizingMaskIntoConstraints = NO;
    sizeLabel.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
    sizeLabel.layer.cornerRadius = 10.0;
    sizeLabel.clipsToBounds = YES;
    sizeLabel.minimumScaleFactor = 0.5;
    sizeLabel.textColor = RGBA(255,255,255,1.0);
    sizeLabel.textAlignment = NSTextAlignmentCenter;
    sizeLabel.backgroundColor = RGBA(25,20,15,0.8);
    sizeLabel.text = [NSString stringWithFormat:@"width %0.2f : height %0.2f : scale %@",sc.contentSize.width,sc.contentSize.height,@"1.0"];
    [self addSubview:sizeLabel];
    
    NSArray *siw = [NSLayoutConstraint constraintsWithVisualFormat:@"[label(150)]-30-|"
                                                           options:0
                                                           metrics:nil
                                                             views:@{@"label":sizeLabel}];
    [self addConstraints:siw];
    NSArray *sih =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[label(20)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"label":sizeLabel}];
    [self addConstraints:sih];
    
    
    [self setResizeTable];
}

-(void)setResizeTable{
    
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void)endTask
{
    [self endMessage];
    [commentText endEditing:YES];
}

-(void)sendButStat:(UIButton *)stat
{
    switch (stat.tag) {
        case 0:
        {
            [self endMessage];
            [commentText endEditing:YES];
        }
            break;
        case 1:
        {
            NSDate *now = [NSDate new];
            float tmp= [now timeIntervalSinceDate:oldDate];
            
            
            
            if(tmp > fileUploadInterval){
                //まずは入力された文字列が実際に付けれる文字列かどうかを判別します。
                if([DataManager textFiledCheck:commentText.text maxLength:20 minLength:1]){
                    
                    [self endMessage];
                    [commentText endEditing:YES];
                    [self.delegate setImageName:commentText.text scale:setScale];
                    commentText.text = @"";
                    oldDate = now;
                }else{
                    UIAlertView *alert =
                    [[UIAlertView alloc]
                     initWithTitle:@"Invalid error."
                     message:NSLocalizedString(@"InvalidError",@"")
                     delegate:nil
                     cancelButtonTitle:nil
                     otherButtonTitles:NSLocalizedString(@"OK",@""), nil
                     ];
                    [alert show];
                }
            }else{
                UIAlertView *alert =
                [[UIAlertView alloc]
                 initWithTitle:@"Invalid error."
                 message:NSLocalizedString(@"photo-post-image",@"")
                 delegate:nil
                 cancelButtonTitle:nil
                 otherButtonTitles:NSLocalizedString(@"OK",@""), nil
                 ];
                [alert show];
            }
        }
            break;
        default:
            break;
    }
}

-(void)setImageWithPicker:(UIImage *)img index:(NSInteger)index
{
    
    UIImageView *ima = [[UIImageView alloc]initWithImage:img];
    for (UIView *view in [sc subviews]) {
        [view removeFromSuperview];
    }
    [sc addSubview:ima];
    sc.contentSize = ima.frame.size;
    [imageSizeTable defaultWidthAndHeight:sc.contentSize.width height:sc.contentSize.height];
    sizeLabel.text = [NSString stringWithFormat:@"W:%0.2f H:%0.2f S:%@",sc.contentSize.width,sc.contentSize.height,@"1.0"];
}

- (UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView{
    return [sc subviews][0];
}

- (void)scrollViewDidEndZooming:(UIScrollView*)scrollView withView:(UIView*)view atScale:(CGFloat)scale
{
    setScale = scale;
    sizeLabel.text = [NSString stringWithFormat:@"width %0.2f : height %0.2f : scale %0.2f",sc.contentSize.width,sc.contentSize.height,scale];
}
#pragma mark - 表示されている画像のサイズを変更する
-(void)resizeScale:(CGFloat)scale{
    [sc setZoomScale:scale animated:YES];
}


-(UIButton *)createBut:(CGRect)rec tag:(NSInteger)tag border:(BOOL)border
{
    UIButton *but = [[UIButton alloc]initWithFrame:rec];
    if(border){
        [[but layer] setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
        [[but  layer] setBorderWidth:2.0];
        but.layer.cornerRadius = rec.size.width / 2;
    }
    but.tag = tag;
    [but addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    return but;
}


#pragma mark - ここに画像設定用の処理を載せてください。


-(void)touchBut:(UIButton *)but
{
    switch (but.tag) {
        case 0: //next
        {
            [imageSizeTable hide];
            [self.delegate setImageFromIndex:YES];
        }
            break;
        case 1: //previous
        {
            [imageSizeTable hide];
            [self.delegate setImageFromIndex:NO];
        }
            break;
        case 2:
        {
            [imageSizeTable displayToggle];
        }
            break;
        case 3: //
        {
#pragma mark - 以前の投稿から5分以上経っているか調べてから処理を開始します。
            [imageSizeTable hide];
            [self openMessage];
            [commentText becomeFirstResponder];
            
        }
            break;
        case 4:
        {
            for (UIView *view in [sc subviews]) {
                [view removeFromSuperview];
            }
            sc.contentSize = CGSizeMake(0,0);
            [imageSizeTable hide];
            [self.delegate closePicker];
        }
            break;
        default:
            break;
    }
}


-(void)keyboardWillChange:(NSNotification *)notification {
    CGRect rec = [DeviceData deviceMainScreen];
    nk.hidden = NO;
    keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [self removeConstraints:nkh];
    nkh = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[message(height)]-bottom-|"
                                                  options:0
                                                  metrics:@{@"height":@100,@"bottom":[NSNumber numberWithFloat:self.frame.size.height - keyboardRect.origin.y]}
                                                    views:@{@"message":nk}];
    [self addConstraints:nkh];
    [UIView animateWithDuration:duration animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL comp){
        if(rec.size.height == keyboardRect.origin.y){
            nk.hidden = YES;
        }
    }];
}


-(void)openMessage{
    nk.hidden = NO;
}
-(void)endMessage{
    nk.hidden = YES;
}


@end
