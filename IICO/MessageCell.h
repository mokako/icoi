//
//  MessageCell.h
//  ICOI
//
//  Created by mokako on 2015/09/21.
//  Copyright (c) 2015年 moca. All rights reserved.
//
/*
 構想
 個人識別用画像機能の搭載を予定
 
 
 
 */
#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface MessageCell : UITableViewCell
{
    CGRect rec;
}
@property (nonatomic) IBInspectable UIColor *defaultBack;
@property (nonatomic) IBInspectable UIColor *hilightBack;




@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UITextView *article;
@property (weak, nonatomic) IBOutlet UILabel *date;
@property (weak, nonatomic) IBOutlet UIView *base;
@property (weak, nonatomic) IBOutlet UIButton *clip;
@property (weak, nonatomic) IBOutlet UIButton *ref;
@property (weak, nonatomic) IBOutlet UIButton *link;
-(void)setArticleFrame:(NSString *)str;
@end
