//
//  UIFlexibleLabel.m
//  ICOI
//
//  Created by mokako on 2015/07/18.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "UIFlexibleLabel.h"

@implementation UIFlexibleLabel
- (id)init{
    if (self = [super init]) {
        self.radius = 0;
        self.padding = 0;
        self.clipsToBounds = YES;
        self.animatable = NO;//default;
        NSURL *popupSoundURL = [[NSBundle mainBundle] URLForResource:@"decision6_1" withExtension:@"mp3"];
        AudioServicesCreateSystemSoundID ((__bridge CFURLRef)popupSoundURL, &popupSound);
    }
    return self;
}


-(void)popupSoundType:(soundType)type{
    switch (type) {
        case type0:
        {
            NSURL *popupSoundURL = [[NSBundle mainBundle] URLForResource:@"decision6_1" withExtension:@"mp3"];
            AudioServicesCreateSystemSoundID ((__bridge CFURLRef)popupSoundURL, &popupSound);
        }
            break;
        case type1:
        {
            NSURL *popupSoundURL = [[NSBundle mainBundle] URLForResource:@"decision5_1" withExtension:@"mp3"];
            AudioServicesCreateSystemSoundID ((__bridge CFURLRef)popupSoundURL, &popupSound);
        }
            break;
        case type2:
        {
            NSURL *popupSoundURL = [[NSBundle mainBundle] URLForResource:@"cartoon_whistle_1" withExtension:@"mp3"];
            AudioServicesCreateSystemSoundID ((__bridge CFURLRef)popupSoundURL, &popupSound);
        }
            break;
        default:
            break;
    }
}

-(void)setValue:(NSString *)text{
    //情報の優先度はpadding > padding_
    if(self.hidden){
        if(self.animatable){
            if(![self.trigger isEqualToString:text]){
                [self setLabelText:text];
                self.hidden = NO;
                self.transform = CGAffineTransformMakeScale(0,0);
                
                dispatch_async(dispatch_get_main_queue(),^{
                    AudioServicesPlaySystemSound(popupSound);
                    [UIView animateWithDuration:0.5 animations:^{
                        self.transform = CGAffineTransformMakeScale(1,1);
                    } completion:^(BOOL comp){}];
                });
            }
        }else{
            if([self.trigger isEqualToString:text]){
                self.hidden = YES;
            }else{
                self.hidden = NO;
            }
            [self setLabelText:text];
        }
    }else{
        if([self.trigger isEqualToString:text]){
            self.hidden = YES;
        }else{
            self.hidden = NO;
        }
        [self setLabelText:text];
    }
}

-(void)setLabelText:(NSString *)text{
    CGPoint center = self.center;
    CGSize   maxSize = CGSizeMake(10000,10000);
    NSMutableParagraphStyle *paragraphStyle = [NSMutableParagraphStyle new];
    paragraphStyle.firstLineHeadIndent = 0;
    paragraphStyle.headIndent = 0;
    paragraphStyle.lineBreakMode = NSLineBreakByCharWrapping;
    NSDictionary *attr = @{NSParagraphStyleAttributeName:paragraphStyle,NSFontAttributeName:self.font,NSKernAttributeName:@0.5};
    CGSize modifiedSize = [text boundingRectWithSize:maxSize
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attr
                                             context:nil
                           ].size;
    self.text = text;
    self.minWidth = self.minWidth == 0 ? modifiedSize.height : self.minWidth;
    CGFloat width = modifiedSize.width > self.minWidth ? modifiedSize.width : self.minWidth;
    self.frame = CGRectMake(0,0,width + (self.padding * 2),modifiedSize.height + (self.padding * 2));
    self.layer.cornerRadius = self.frame.size.height * self.radius;
    self.center = center;
    dispatch_async(dispatch_get_main_queue(),^{
        [self setNeedsDisplay];
    });
}
@end
