//
//  CommentTimeLineView.m
//  ICOI
//
//  Created by mokako on 2014/12/15.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "CommentTimeLineView.h"
#import "CustomCell.h"

@implementation CommentTimeLineView

- (id)init
{
    self = [super init];
    if (self) {
        CGRect r = [[UIScreen mainScreen] bounds];
        self.frame = CGRectMake(0,-1 * r.size.height,r.size.width,r.size.height);
        self.backgroundColor = RGBA(255,255,255,0.0);
        comments = [[NSMutableArray alloc]init];
        [self initializeView];
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



-(void)initializeView{
#pragma mark - Comment time line
    timeLineBoard = [[UIView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    timeLineBoard.clipsToBounds = YES;
    [self addSubview:timeLineBoard];
    commentTimeLine = [self createTable:CGRectMake(0,0,self.frame.size.width,self.frame.size.height) tag:1];
    [timeLineBoard addSubview:commentTimeLine];
    [commentTimeLine setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.1, 0.1)]];
    
    
    
    fadeBut = [[UIView alloc]initWithFrame:CGRectMake(self.frame.size.width / 2 - 26,self.frame.size.height - 20,52,23)];
    fadeBut.backgroundColor = RGBA(0,129,150,0.7);
    fadeBut.layer.cornerRadius = 3;
    [timeLineBoard addSubview:fadeBut];
    
    UIButton *butf = [[UIButton alloc]initWithFrame:CGRectMake(2,0,48,20)];
    [fadeBut addSubview:butf];
    butf.titleLabel.font = [UIFont fontWithName:@"point" size:15.0];
    butf.titleEdgeInsets = UIEdgeInsetsMake(3, 0, 0, 0);
    [butf setTitle:@"z" forState:UIControlStateNormal];
    [butf setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [butf addTarget:self action:@selector(fadeTab) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    hideTable  = [[UIView alloc]initWithFrame:CGRectMake(-25,self.frame.size.height / 2 - 25,50,50)];
    hideTable.layer.cornerRadius = 25;
    hideTable.backgroundColor = RGBA(0,129,150,0.7);
    [self addSubview:hideTable];
    
    UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,50,50)];
    but.layer.cornerRadius = 25;
    but.titleEdgeInsets = UIEdgeInsetsMake(0, 15, 0, 0);
    but.titleLabel.font = [UIFont fontWithName:@"point" size:15.0];
    [but setTitle:@"x" forState:UIControlStateNormal];
    [but setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [but addTarget:self action:@selector(hideTable) forControlEvents:UIControlEventTouchUpInside];
    [hideTable addSubview:but];
    
}



-(void)addComment:(NSDictionary *)context comment:(BOOL)is{
    if(comments.count != 0){
        [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reloadTable) object:nil];
        //すでに配列が存在するなら
        [comments insertObject:context atIndex:0];
        [commentTimeLine beginUpdates];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        NSArray *ary = [[NSArray alloc]initWithObjects:indexPath, nil];
        [commentTimeLine insertRowsAtIndexPaths:ary withRowAnimation:UITableViewRowAnimationAutomatic];
        [commentTimeLine endUpdates];
        [self performSelector:@selector(reloadTable) withObject:nil afterDelay:3];
    }else{
        //まだ配列が空なら
        [comments addObject:context];
        [commentTimeLine reloadData];
    }
}

-(void)reloadTable{
    [commentTimeLine reloadData];
}


-(void)update{
    self.transform = CGAffineTransformIdentity;
    
    CGRect r = [[UIScreen mainScreen] bounds];
    
    self.frame = CGRectMake(0,-1 * r.size.height,r.size.width,r.size.height);
    timeLineBoard.frame = CGRectMake(0,0,r.size.width,r.size.height);
    fadeBut.frame = CGRectMake(self.frame.size.width / 2 - 26,r.size.height - 20,52,23);
    commentTimeLine.frame = CGRectMake(0,0,r.size.width,r.size.height);
    [commentTimeLine reloadData];
    hideTable.frame = CGRectMake(-25,self.frame.size.height / 2 - 25,50,50);
}

-(void)pullTab{
    //拡張モードに入ります
    CGRect r = [[UIScreen mainScreen] bounds];
    [UIView animateWithDuration:0.6 animations:^{
        if(self.frame.origin.y < 0){
            CGAffineTransform transform = CGAffineTransformMakeTranslation(0,r.size.height);
            self.transform = transform;
        }else{
    
        }
    } completion:^(BOOL finished){
    }];
}

-(void)fadeTab{
    //CGRect r = [[UIScreen mainScreen] bounds];
    [UIView animateWithDuration:0.6 animations:^{
        if(self.frame.origin.y >= 0){
            self.transform = CGAffineTransformIdentity;
        }else{
            
        }
    } completion:^(BOOL finished){
        //閉じたことを知らせます。
        [self.delegate setEditTab:32];
    }];
}


-(void)hideTable{
    
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return comments.count;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    // 表示したい文字列
    NSString *text = comments[indexPath.row][@"comment"];
    

    // 表示最大幅・高さ
    CGSize     maxSize = CGSizeMake(self.frame.size.width - 40,200);
    // 表示するフォントサイズ
    NSDictionary *attr = @{NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:13.0],NSKernAttributeName:@0.5};
    
    // 以上踏まえた上で、表示に必要なサイズ
    CGSize modifiedSize = [text boundingRectWithSize:maxSize
                                             options:NSStringDrawingUsesLineFragmentOrigin
                                          attributes:attr
                                             context:nil
                           ].size;
    
    return MAX(modifiedSize.height + 50, 70);
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    cell = [tableView dequeueReusableCellWithIdentifier:@"CELL"];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(0,129,150,1.0) selectColor:RGBA(0,129,150,1.0) defaultBack:RGBA(255,255,255,1.0) selectBack:RGBA(255,255,255,1.0)];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont fontWithName:@"AvenirNext-Bold" size:13.0];
    cell.textLabel.textColor = RGBA(0,129,150,1.0);
    cell.textLabel.text = comments[indexPath.row][@"pname"];
    NSString *str;
    if(((NSString *)comments[indexPath.row][@"comment"]).length == 0){
        str = @"Add line.";
    }else{
        str = comments[indexPath.row][@"comment"];
    }
    
    cell.detailTextLabel.attributedText = [[NSMutableAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName:[UIFont fontWithName:@"Avenir-Book" size:13.0],NSKernAttributeName:@0.5}];
    cell.detailTextLabel.textColor = RGBA(35,30,25,1.0);
    cell.detailTextLabel.numberOfLines = 0;
    
    UIButton *ac = [[UIButton alloc]initWithFrame:CGRectMake(0,0,48,48)];
    ac.titleLabel.font = [UIFont fontWithName:@"font-icon-03" size:25.0];
    [ac setTitleColor:RGBA(255,130,125,1.0) forState:UIControlStateNormal];
    [ac setTitle:@"n" forState:UIControlStateNormal];

    ac.tag = indexPath.row;
    [ac addTarget:self action:@selector(tapCell:) forControlEvents:UIControlEventTouchUpInside];

    
    
    cell.accessoryView = ac;
    

    return cell;
}


-(void)tapCell:(UIButton *)but{
    [self.delegate tapSpotPointWithCGpoint:[((NSValue *)comments[but.tag][@"path"][@"point"]) CGPointValue]];
}




- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0,cell.contentView.frame.size.height - 0.5,self.frame.size.width,0.5)];
    line.backgroundColor = RGBA(230,230,230,1.0);
    [cell.contentView addSubview:line];
}













-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStyleGrouped];
    atable.backgroundColor = RGBA(255,255,255,1.0);
    atable.separatorStyle = UITableViewCellSeparatorStyleNone;
    atable.delegate = self;
    atable.dataSource = self;
    atable.tag = tag;
    atable.rowHeight = 70.0;
    atable.showsVerticalScrollIndicator = YES;
    return atable;
}




@end
