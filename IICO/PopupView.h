//
//  PopupView.h
//  ICOI
//
//  Created by mokako on 2014/12/06.
//  Copyright (c) 2014年 moca. All rights reserved.
//
/*
 生存時間を設定しその時間を過ぎると消えます。
 */
#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "PopupSubview.h"

typedef NS_ENUM(NSInteger, vertical){
    Top,
    Topmiddle,
    Center,
    Centermiddle,
    Under
};



@protocol PopupViewDelegate <NSObject>
-(void)popupview:(event)eventState;
@end

@interface PopupView : UIView<PopupSubviewDelegate>
{
    vertical ver;
    UIView *_blank;
    UIView *_mount;
}

@property (nonatomic, weak)id<PopupViewDelegate>delegate;
@property (nonatomic)BOOL mountHidden;
@property (nonatomic)PopupSubview *popSub;
-(void)setView:(UIView *)object;
-(void)setVertical:(vertical)vertical;
-(void)setMountColor:(UIColor *)mountColor;
-(void)show;
-(void)hide;
@end
