//
//  CameraView.m
//  ICOI
//
//  Created by mokako on 2014/02/13.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "CameraView.h"

@implementation CameraView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeMethods];
    }
    return self;
}
- (void)dealloc {
    [session stopRunning];
    session = nil;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)initializeMethods
{
    camera = [[UIView alloc]initWithFrame:CGRectMake(0,0,70,70)];
    camera.center = CGPointMake(self.bounds.size.width/ 2,self.bounds.size.height - 35);
    camera.backgroundColor = RGBA(255,255,255,0.3);
    
    [[camera layer] setBorderColor:[[UIColor darkGrayColor] CGColor]];
    [[camera layer] setBorderWidth:2.0];
    camera.layer.cornerRadius = 35.0;
    camera.clipsToBounds = true;
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0,0,48,48)];
    label.text = @"8";
    label.font = [UIFont fontWithName:@"icomoon" size:30];
    label.center = CGPointMake(35,35);
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(captureMode)];
    [camera addGestureRecognizer:tap];
    [camera addSubview:label];
    [self addSubview:camera];
    [self cameraInit];

}


-(void)cameraInit
{
    AVCaptureDevice* device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    //入力作成
    NSError *error = nil;
    AVCaptureDeviceInput* deviceInput = [AVCaptureDeviceInput deviceInputWithDevice:device error:&error];
    
    if (deviceInput) {
        
        // セッション初期化
        session = [[AVCaptureSession alloc] init];
        [session addInput:deviceInput];
        [session beginConfiguration];
        session.sessionPreset = AVCaptureSessionPresetPhoto;
        [session commitConfiguration];
        
        // プレビュー表示
        AVCaptureVideoPreviewLayer *previewLayer = [AVCaptureVideoPreviewLayer layerWithSession:session];
        previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        previewLayer.frame = CGRectMake(0,0,self.bounds.size.width,self.bounds.size.height);
        [self.layer insertSublayer:previewLayer atIndex:0];
        
        // セッション開始
        imageOutput = [[AVCaptureStillImageOutput alloc] init];
        [session addOutput:imageOutput];
        
        
        [self addSubview:camera];

    }
    // エラー
    else {
        NSLog(@"ERROR:%@", error);
    }
}
-(void)cameraStart
{
    [session startRunning];
}
-(void)cameraStop
{
    [session stopRunning];
}
// CMSampleBufferRefをUIImageへ
- (UIImage *)imageFromSampleBufferRef:(CMSampleBufferRef)sampleBuffer
{
    // イメージバッファの取得
    CVImageBufferRef    buffer;
    buffer = CMSampleBufferGetImageBuffer(sampleBuffer);
    
    // イメージバッファのロック
    CVPixelBufferLockBaseAddress(buffer, 0);
    // イメージバッファ情報の取得
    uint8_t*    base;
    size_t      width, height, bytesPerRow;
    base = CVPixelBufferGetBaseAddress(buffer);
    width = CVPixelBufferGetWidth(buffer);
    height = CVPixelBufferGetHeight(buffer);
    bytesPerRow = CVPixelBufferGetBytesPerRow(buffer);
    
    // ビットマップコンテキストの作成
    CGColorSpaceRef colorSpace;
    CGContextRef    cgContext;
    colorSpace = CGColorSpaceCreateDeviceRGB();
    cgContext = CGBitmapContextCreate(
                                      base, width, height, 8, bytesPerRow, colorSpace,
                                      kCGBitmapByteOrder32Little | kCGImageAlphaPremultipliedFirst);
    CGColorSpaceRelease(colorSpace);
    
    // 画像の作成
    CGImageRef  cgImage;
    UIImage*    image;
    cgImage = CGBitmapContextCreateImage(cgContext);
    image = [UIImage imageWithCGImage:cgImage scale:1.0f
                          orientation:UIImageOrientationUp];
    CGImageRelease(cgImage);
    CGContextRelease(cgContext);
    
    // イメージバッファのアンロック
    CVPixelBufferUnlockBaseAddress(buffer, 0);
    return image;
}

-(void)captureMode
{
    AVCaptureConnection *connection = [[imageOutput connections] lastObject];
    [imageOutput captureStillImageAsynchronouslyFromConnection:connection
                                                  completionHandler:^(CMSampleBufferRef imageDataSampleBuffer, NSError *error) {
                                                      
                                                      NSData *data = [AVCaptureStillImageOutput jpegStillImageNSDataRepresentation:imageDataSampleBuffer];
                                                      UIImage *image = [UIImage imageWithData:data];
                                                      ALAssetsLibrary *library = [[ALAssetsLibrary alloc] init];
                                                      [library writeImageToSavedPhotosAlbum:image.CGImage
                                                                                orientation:(ALAssetOrientation)image.imageOrientation
                                                                            completionBlock:^(NSURL *assetURL, NSError *error) {
                                                                                [self loadData];
                                                                            }];
                                                  }];
    
}

-(void)loadData
{
    [self.delegate reloadAlbum];
}

@end
