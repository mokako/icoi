//
//  NormalListEdit.m
//  ICOI
//
//  Created by mokako on 2016/01/19.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ListEdit.h"

@implementation ListEdit


+(NSArray *)resetListNum:(NSArray *)listID list:(NSArray *)list{
    NSMutableArray *new = [@[] mutableCopy];
    for(int i = 0; i < listID.count; i++){
        NSMutableDictionary *dic = [list[i] mutableCopy];
        dic[@"nid"] = listID[i];
        [new addObject:dic];
    }
    return new;
}

+(NSArray *)resetListPauid:(NSArray *)list uid:(NSString *)uid{
    NSMutableArray *new = [@[] mutableCopy];
    for(int i = 0; i < list.count; i++){
        NSMutableDictionary *dic = [list[i] mutableCopy];
        dic[@"pauid"] = uid;
        [new addObject:dic];
    }
    return list;
}

+(NSArray *)setHitList:(NSArray *)index list:(NSArray *)list{
    NSMutableArray *new = [NSMutableArray new];
    for(NSIndexPath *item in index){
        [new addObject:list[item.row]];
    }
    return new;
}




+(NSArray *)exclusionList:(NSArray *)list{
    NSMutableArray *new = [@[] mutableCopy];
    for(int i = 0; i < list.count; i++){
        NSMutableDictionary *dic = [list[i] mutableCopy];
        if([dic[@"state"] integerValue] == 2){
            dic[@"state"] = @5;
        }else if([dic[@"state"] integerValue] == 3){
            dic[@"state"] = @6;
        }
        [new addObject:dic];
    }
    return new;
}

+(NSArray *)restoreList:(NSArray *)list{
    NSMutableArray *new = [@[] mutableCopy];
    for(int i = 0; i < list.count; i++){
        NSMutableDictionary *dic = [list[i] mutableCopy];
        if([dic[@"state"] integerValue] == 5){
            dic[@"state"] = @2;
        }else if([dic[@"state"] integerValue] == 6){
            dic[@"state"] = @3;
        }
        [new addObject:dic];
    }
    return new;
}
+(NSArray *)displayList:(NSArray *)list{
    NSMutableArray *new = [NSMutableArray new];
    for(NSDictionary *item in list){
        if([item[@"state"] integerValue] == 1 || [item[@"state"] integerValue] == 0)
        [new addObject:item];
    }
    return new;
}

@end
