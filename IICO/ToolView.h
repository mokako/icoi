//
//  ToolView.h
//  ICOI
//
//  Created by mokako on 2015/10/04.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "UIFlexibleLabel.h"

@protocol ToolViewDelegate<NSObject>

-(void)setEditTab:(NSInteger)state;
@end

@interface ToolView : UIView
{
    UIFlexibleLabel *label;
    UIFlexibleLabel *insert;
    NSInteger count;
    UIView *view;
}

@property (nonatomic, weak)id<ToolViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIButton *menu;
@property (weak, nonatomic) IBOutlet UIButton *chat;
@property (weak, nonatomic) IBOutlet UIButton *list;

@property (weak, nonatomic) IBOutlet UIView *base;
@property (weak, nonatomic) IBOutlet UIButton *undo;
@property (weak, nonatomic) IBOutlet UIButton *redo;
@property (weak, nonatomic) IBOutlet UIButton *del;
@property (weak, nonatomic) IBOutlet UIButton *pinch;
@property (weak, nonatomic) IBOutlet UIButton *pallete;
@property (weak, nonatomic) IBOutlet UIButton *brush;
@property (weak, nonatomic) IBOutlet UIButton *share;
@property (weak, nonatomic) IBOutlet UIButton *fade;
+(instancetype)view;
@end
