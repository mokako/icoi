//
//  AnimationToolView.h
//  ICOI
//
//  Created by mokako on 2015/10/08.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@protocol AnimationToolViewDelegate<NSObject>
-(void)setEditTab:(NSInteger)state;
@end


@interface AnimationToolView : UIView
{
    UIView *view;
}



+(instancetype)view;

@property (nonatomic, weak)id<AnimationToolViewDelegate>delegate;

@property (weak, nonatomic) IBOutlet UIView *base;
@property (weak, nonatomic) IBOutlet UIButton *list;
@property (weak, nonatomic) IBOutlet UIButton *play;
@property (weak, nonatomic) IBOutlet UIButton *paste;
@property (weak, nonatomic) IBOutlet UIButton *del;
@property (weak, nonatomic) IBOutlet UIButton *setting;
@property (weak, nonatomic) IBOutlet UIButton *lock;

@property (weak, nonatomic) IBOutlet UIButton *menu;
@property (weak, nonatomic) IBOutlet UIButton *chat;
@property (weak, nonatomic) IBOutlet UIButton *fade;

@end
