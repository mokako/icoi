//
//  ImageTreat.m
//  ICOI
//
//  Created by mokako on 2015/06/19.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "ICOIImage.h"

@implementation ICOIImage
#pragma mark -画像処理
//2点の中間の座標

+(NSArray *)setThumbnailLayerImagesFromDatalist:(NSString *)serviceName dataInfo:(NSArray *)dataInfo size:(NSInteger)size
{
    NSMutableArray *imges = [[NSMutableArray alloc]init];
    
    NSInteger h = [dataInfo count];
    int i = 0;
    for(i = 0; i < h; ++i){
        NSString *path = [NSString stringWithFormat:@"%@.%@",[dataInfo[i] objectForKey:@"fileName"],[dataInfo[i] objectForKey:@"exte"]];
        UIImage *img = [[UIImage alloc]init];
        if([FileManager checkFile:serviceName fileName:path] == YES)
        {
            NSData *data = [[NSData alloc]initWithData:[FileManager loadFile:serviceName name:path]];
            img = [UIImage imageWithData:data];
            
            [imges addObject:[ICOIImage thumbnail:img size:size]];
        }else{
            [imges addObject:img];
        }
    }
    return imges;
}

+ (UIImage *)rotateImage:(UIImage *)image rotate:(CGFloat)angle{
    CGSize imgSize = {image.size.width, image.size.height};
    UIGraphicsBeginImageContext(imgSize);
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextTranslateCTM(context, image.size.width/2, image.size.height/2); // 回転の中心点を移動
    CGContextScaleCTM(context, 1.0, -1.0); // Y軸方向を補正
    
    CGContextRotateCTM(context, angle);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(-image.size.width/2, -image.size.height/2, image.size.width, image.size.height), image.CGImage);
    
    UIImage *rotatedImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return rotatedImage;
}

+ (UIImage *)imageFromView:(UIView *)view
{
    UIImage* image;
    UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [view.layer renderInContext:context];
    image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(CGPoint)centerPoint:(CGPoint)pointA pointB:(CGPoint)pointB
{
    CGFloat dx = ( pointB.x + pointA.x ) / 2;
    CGFloat dy = ( pointB.y + pointA.y ) / 2;
    return CGPointMake(dx, dy);
}
//２点間の距離を算出
+(CGFloat)distanceWithPoint:(CGPoint)pointA pointB:(CGPoint)pointB
{
    CGFloat dx = fabs( pointB.x - pointA.x );
    CGFloat dy = fabs( pointB.y - pointA.y );
    return sqrt(dx * dx + dy * dy);
}
+(UIImage *)createImage:(CGRect)rect color:(UIColor *)color
{
    UIGraphicsBeginImageContextWithOptions(rect.size,NO,1.0);
    CGContextRef contextRef = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(contextRef, [color CGColor]);
    CGContextFillRect(contextRef, rect);
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return img;
}

+(UIImage *)thumbnail:(UIImage *)image size:(CGFloat)size
{
    
    UIImage *newImage;
    
    
    CGFloat w = image.size.width / 2;
    CGFloat h = image.size.height / 2;
    CGFloat max = w >= h ? h : w;
    CGImageRef cutImage = CGImageCreateWithImageInRect(image.CGImage, CGRectMake(w - max, h - max, max * 2,max * 2));
    UIImage *img = [UIImage imageWithCGImage:cutImage];
    CGImageRelease(cutImage);
    
    UIGraphicsBeginImageContext(CGSizeMake(size,size));
    CGRect rec = CGRectMake(0, 0, size, size);
    [img drawInRect:rec];
    newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage *)resizeImage:(UIImage *)base scale:(CGFloat)scale
{
    
    // 取得した画像の縦サイズ、横サイズを取得する
    int imageW = base.size.width;
    int imageH = base.size.height;
    
    // リサイズする倍率を作成する。
    
    // 比率に合わせてリサイズする。
    // ポイントはUIGraphicsXXとdrawInRectを用いて、リサイズ後のサイズで、
    // aImageを書き出し、書き出した画像を取得することで、
    // リサイズ後の画像を取得します。
    CGSize resizedSize = CGSizeMake(imageW * scale, imageH * scale);
    UIGraphicsBeginImageContextWithOptions(resizedSize,NO,[[UIScreen mainScreen] scale]);
    [base drawInRect:CGRectMake(0, 0, resizedSize.width, resizedSize.height)];
    UIImage *aImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return aImage;
}

//imageをresizeします
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize
{
    UIGraphicsBeginImageContext(newSize);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

+(UIImage *)buttonImage:(CGRect)rect text:(NSString *)text font:(UIFont *)font color:(UIColor *)color background:(UIColor *)back
{
    UILabel *label = [[UILabel alloc]initWithFrame:rect];
    label.text = text;
    label.font = font;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = color;
    label.backgroundColor = back;
    
    return [self imageFromView:label];
}
+(UIImage *)convertToImage:(NSString *)text font:(UIFont *)font
{
    
    // 描画する文字列のフォントを設定。
    CGSize rec = CGSizeMake(768,1024);
    // オフスクリーン描画のためのグラフィックスコンテキストを作る。
    if (&UIGraphicsBeginImageContextWithOptions != NULL)
        UIGraphicsBeginImageContextWithOptions(rec, NO, 0);
    else
        UIGraphicsBeginImageContext(rec);
    
    /* Shadowを付ける場合は追加でこの部分の処理を行う。
     CGContextRef ctx = UIGraphicsGetCurrentContext();
     CGContextSetShadowWithColor(ctx, CGSizeMake(1.0f, 1.0f), 5.0f, [[UIColor grayColor] CGColor]);
     */
    
    // 文字列の描画領域のサイズをあらかじめ算出しておく。
    CGRect textAreaSize = [text boundingRectWithSize:rec options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:font} context:nil];
    // 描画対象領域の中央に文字列を描画する。
    
    
    /// Make a copy of the default paragraph style
    NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
    /// Set line break mode
    paragraphStyle.lineBreakMode = NSLineBreakByWordWrapping;
    /// Set text alignment
    paragraphStyle.alignment = NSTextAlignmentCenter;
    
    NSDictionary *attributes = @{ NSFontAttributeName: font,
                                  NSParagraphStyleAttributeName: paragraphStyle };
    
    
    CGRect rect = CGRectMake((rec.width - textAreaSize.size.width) * 0.5f,(rec.height - textAreaSize.size.height) * 0.5f,textAreaSize.size.width,textAreaSize.size.height);
    
    [text drawInRect:rect withAttributes:attributes];
    
    // コンテキストから画像オブジェクトを作成する。
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    return image;
}
+(UIImage *)drawTail:(UIColor *)color{
    UIImage* image;
    CGSize size = CGSizeMake(30,30);
    UIBezierPath *bez = [UIBezierPath bezierPath];
    
    [bez moveToPoint:CGPointMake(0,0)];
    [bez addLineToPoint:CGPointMake(10,7.5)];
    [bez addQuadCurveToPoint:CGPointMake(10,22.5) controlPoint:CGPointMake(23,15)];
    [bez addLineToPoint:CGPointMake(0,30)];
    [bez closePath];
    
    UIGraphicsBeginImageContextWithOptions(size,NO,0.0);
    
    [[UIColor clearColor] setStroke];
    [color setFill];
    [bez stroke];
    [bez fill];
    
    
    // 描画した画像をcanvasにセットして、画面に表示します。
    image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    return image;
}



//UIButton 背景用
+ (UIImage *)imageWithColor:(UIColor *)color {
    CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
    UIGraphicsBeginImageContext(rect.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return image;
}

+(UIButton *)setIcon:(UIFont *)font icon:(NSString *)icon title:(NSString *)title color:(UIColor *)color{
    UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,40,40)];
    [but setBackgroundImage:[ICOIImage imageWithColor:RGBA(255,255,255,0.0)] forState:UIControlStateNormal];
    [but setBackgroundImage:[ICOIImage imageWithColor:RGBA(235,235,235,0.8)] forState:UIControlStateHighlighted];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0,0,40,25)];
    label.font = font;
    label.text = icon;
    label.textAlignment = NSTextAlignmentCenter;
    label.textColor = color;
    [but addSubview:label];
    
    
    UILabel *label2 = [[UILabel alloc]initWithFrame:CGRectMake(0,25,40,15)];
    label2.font = [UIFont fontWithName:@"AvenirNextCondensed-Regular" size:10];
    label2.text = title;
    label2.textAlignment = NSTextAlignmentCenter;
    label2.textColor = color;
    [but addSubview:label2];
    return but;
}


@end
