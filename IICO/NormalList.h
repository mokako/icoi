//
//  NormalList.h
//  ICOI
//
//  Created by mokako on 2015/10/29.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ClearToolBar.h"

#import "ServiceManager.h"
#import "ToolWindow.h"
#import "ListEdit.h"

/*  table header windows */
#import "ExchageHeaderView.h"
#import "NormalHeaderView.h"
#import "NewTitleHeaderView.h"


/* custom cells */
#import "NormalCell.h"
#import "TreeListCell.h"
#import "DecoCell.h"
#import "UnDecoCell.h"
#import "SliderCell.h"

#define maxCount 100
@interface NormalList : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSMutableArray *list;
    NSMutableArray *chatList;
    ServiceManager *sb;
    NSInteger pageCount;
    
    NSInteger selectCell;
    NSDictionary *selectPath;
    
    NSMutableDictionary *visitList;
    
    NSString *hash;
    
    
    NSString *topHash;
    NSInteger oldMode;
    
    //methods
    UIBarButtonItem *fix;
    
    NSArray *setArray;
    NSArray *indexPathArray;
    NSIndexPath *selectListNum;
    NSMutableArray *editUIDs;
    NSDictionary *selectUID;
    
    BOOL root;//tableのルート表示を制御します ルート : YES　非ルート : NO 
    //cell edit boolean
    BOOL cell_move;
    BOOL cell_delete;
    
    NormalCell *dummyCell;
    CGFloat fixedHeight;
    CGFloat fixedWidth;
    UITextView *dummyTextView;
    //toolwindow
    ToolWindow *tw;
    NSMutableArray *cellHeight;
}


@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIButton *menu;
@property (weak, nonatomic) IBOutlet UIButton *close;
@property (weak, nonatomic) IBOutlet UIButton *update;


+ (instancetype)view;

@end
/* modeの分岐
 * 0 はindexlist表示をしているのでステータス２のデータのみ通します。
 * 1 は指定されたuidの要素のみを通します。
 * 2 は全データを通します。
 *
 *
 */