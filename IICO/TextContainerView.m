//
//  TextContainerView.m
//  ICOI
//
//  Created by mokako on 2014/09/23.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "TextContainerView.h"

@implementation TextContainerView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeView];
        
        
        
    }
    return self;
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)initializeView
{
    
}


-(NSArray *)setView
{
    //文章とフォント
    NSAttributedString *astr = [[NSAttributedString alloc]initWithString:self.text attributes:@{NSFontAttributeName : self.font}];
    
    //テキストストレージを用意
    storage = [[NSTextStorage alloc]initWithAttributedString:astr];
    
    //layoutmanagerを作成
    manager = [[NSLayoutManager alloc]init];
    
    //ストレージにレイアウトマネージャーをセット
    [storage addLayoutManager:manager];
    
    
    return [self createContainer];
}


-(NSArray *)createContainer
{
    NSRange range = NSMakeRange(0,0);
    while (NSMaxRange(range) < manager.numberOfGlyphs) {
        NSTextContainer *cont = [[NSTextContainer alloc]initWithSize:self.size];
        [manager addTextContainer:cont];
        
        
        range = [manager glyphRangeForTextContainer:cont];
    }
    
    self.numOfPage = [manager.textContainers count] / 2 + ([manager.textContainers count] % 2);
    [self dispTextView];
    return [manager textContainers];
}



-(void)dispTextView
{
    //NSLog(@"manager %@",[manager textContainers]);
}


-(void)setRect:(CGSize)size
{
    self.size = size;
}

@end
