//
//  DataManager.h
//  ICOI
//
//  Created by moca on 2014/01/02.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FileManager.h"
#import <MultipeerConnectivity/MCPeerID.h>
#import <QuartzCore/QuartzCore.h>
#import "FMDatabaseQueue.h"
#import "FMDatabaseAdditions.h"
#import "DateFormatter.h"


typedef NS_ENUM(NSInteger, GetDataMode) {
    GetDataAll  = 0, //指定されたキーにぶら下がる全データ
    GetDataLevel  = 1, //副キー
    GetDataOther  = 2, //主キーに属さないデータ
    GetDataLevelOther  = 3, //主キーにぶら下がる副キー以外のデータ
    GetDataALL_2 = 4,// (2)以外のすべてを取得
    GetDataALL_3 = 5,//(3)以外のすべてを取得
    GetRestoreData = 6//(5,6)の値を取得
};





@interface DataManager : NSObject
+(BOOL)textFiledCheck:(NSString *)str maxLength:(NSInteger)max minLength:(NSInteger)min;
+(BOOL)serviceTypeCheck:(NSString *)str maxLength:(NSInteger)max minLength:(NSInteger)min;
+(NSString *)getHash;
+(void)alert:(NSString *)title text:(NSString *)text;
+(NSArray *)exceptFromArray:(NSArray *)array object:(id)object;
+(NSArray *)sortDataInfo:(NSMutableArray *)so array:(NSMutableArray *)yourdata;
+(BOOL)checkDataInfo:(NSMutableArray *)so data:(NSDictionary *)data;
+(NSArray *)changeState:(NSArray *)array name:(NSString *)name state:(NSInteger)state;
+(NSDictionary *)checkFileToDataList:(NSString *)serviceName dataList:(NSDictionary *)dic;
+(NSMutableDictionary *)checkDictionaryCompare:(NSMutableDictionary *)current new:(NSDictionary *)sub;
//database
+(void)initServiceTable:(FMDatabaseQueue *)db;
+(void)initializeTable:(FMDatabaseQueue *)db;
+(void)updateDataTable:(FMDatabaseQueue *)db;
+(void)updateComment:(FMDatabaseQueue *)db serviceName:(NSString *)serviceName commnet:(NSString *)comment;
+(void)updateServiceDate:(FMDatabaseQueue *)db serviceName:(NSString *)serviceName;
+(void)deleteService:(FMDatabaseQueue *)db serviceName:(NSString *)serviceName;
+(void)insertService:(FMDatabaseQueue *)db serviceName:(NSString *)serviceName displayName:(NSString *)displayName displayUUID:(NSString *)uuid date:(NSDate *)date;
+(NSArray *)serviceList:(FMDatabaseQueue *)db;
+(NSDictionary *)existService:(FMDatabaseQueue *)db service:(NSString *)service;


+(void)insertFileName:(FMDatabaseQueue *)db name:(NSString *)name;
+(void)addFileData:(FMDatabaseQueue *)db data:(NSArray *)ary;
+(NSArray *)getAllFileName:(FMDatabaseQueue *)db;

+(void)addPathData:(FMDatabaseQueue *)db data:(NSDictionary *)dic;
+(BOOL)insertOrUpdateList:(FMDatabaseQueue *)db data:(NSArray *)list;
+(void)updatePath:(FMDatabaseQueue *)db fid:(NSString *)fid uid:(NSString *)uid retain:(BOOL)save;
+(NSArray *)getMessageList:(FMDatabaseQueue *)db;
+(BOOL)deleteSelectedPath:(FMDatabaseQueue *)db fileID:(NSString *)fid uid:(NSString *)uid;
+(BOOL)deletePath:(FMDatabaseQueue *)db fileID:(NSString *)fileID;
+(BOOL)updateWhithDate:(FMDatabaseQueue *)db list:(NSArray *)list;//条件付き（更新時間)
+(BOOL)updateWhithList:(FMDatabaseQueue *)db list:(NSArray *)list;//無条件更新（内部での更新に使用)
+(BOOL)updateWithNumber:(FMDatabaseQueue *)db list:(NSArray *)list;
//tree list search
+(NSDictionary *)getItem:(FMDatabaseQueue *)db fileID:(NSString *)fileID uid:(NSString *)uid;
+(NSDictionary *)getItemForUID:(FMDatabaseQueue *)db uid:(NSString *)uid;
+(NSArray *)getTreeList:(FMDatabaseQueue *)db fileID:(NSString *)fileID;
+(NSArray *)getBranch:(FMDatabaseQueue *)db fileID:(NSString *)fileID uid:(NSString *)uid  mode:(GetDataMode)mode;

//delete or omit
+(BOOL)deleteSelectedPaths:(FMDatabaseQueue *)db fileID:(NSString *)fileID list:(NSArray *)list;




@end
