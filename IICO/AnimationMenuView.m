//
//  AnimationMenuView.m
//  ICOI
//
//  Created by mokako on 2015/01/20.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "AnimationMenuView.h"
#import "CustomCell.h"
/*
 menu5 = [[UIView alloc]initWithFrame:CGRectMake(55,self.frame.size.height / 2 - 100,250,240)];
 [[menu5 layer] setBorderColor:[lineColor CGColor]];
 [[menu5 layer] setBorderWidth:0.5];
 menu5.backgroundColor = bColor;
 menu5.layer.cornerRadius = 2.5;
 menu5.clipsToBounds = YES;
 [self addSubview:menu5];
 
 
 UIView *rowLine = [[UIView alloc]initWithFrame:CGRectMake(0,0,45,48)];
 rowLine.backgroundColor = fontColor;
 [menu5 addSubview:rowLine];
 
 UIView *speed = [[UIView alloc]initWithFrame:CGRectMake(0,0,250,48)];
 [menu5 addSubview:speed];
 
 UILabel *sp = [[UILabel alloc]initWithFrame:CGRectMake(0,0,45,48)];
 sp.text = NSLocalizedString(@"Interval",@"");
 sp.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
 sp.textAlignment = NSTextAlignmentCenter;
 sp.textColor = RGBA(255,255,255,1.0);
 [speed addSubview:sp];
 
 for(int i = 0; i <= 4; i++){
 UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(52 + (34 + 5) * i,7,34,34)];
 but.layer.cornerRadius = 17;
 [but setTitle:flipSpeed[i] forState:UIControlStateNormal];
 but.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
 but.titleEdgeInsets = UIEdgeInsetsMake(3, 0, 0, 0);
 [but addTarget:self action:@selector(touchSpeed:) forControlEvents:UIControlEventTouchUpInside];
 
 [[but layer] setBorderColor:[RGBA(200,200,200,1.0) CGColor]];
 [[but layer] setBorderWidth:0.5];
 
 if(i == 0){
 spca = but;
 [but setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
 [but setBackgroundColor:RGBA(45,40,35,1.0)];
 }else{
 [but setTitleColor:RGBA(45,40,35,1.0) forState:UIControlStateNormal];
 [but setBackgroundColor:RGBA(255,255,255,255)];
 }
 [speed addSubview:but];
 
 }
 
 //setimage
 UIView *ki = [[UIView alloc]initWithFrame:CGRectMake(0,48,250,48)];
 [menu5 addSubview:ki];
 
 UILabel *kil = [[UILabel alloc]initWithFrame:CGRectMake(10,0,180,48)];
 kil.text = NSLocalizedString(@"PlaceImage",@"");
 kil.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
 kil.textAlignment = NSTextAlignmentLeft;
 kil.textColor = fontColor;
 [ki addSubview:kil];
 
 UISwitch *sw = [[UISwitch alloc]init];
 sw.center = CGPointMake(220,24);
 sw.onTintColor = RGBA(125,125,125,1.0);
 sw.tag = 0;
 sw.on = YES;
 [sw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
 [ki addSubview:sw];
 
 //keep
 UIView *kp = [[UIView alloc]initWithFrame:CGRectMake(0,96,250,48)];
 [menu5 addSubview:kp];
 
 UILabel *kpl = [[UILabel alloc]initWithFrame:CGRectMake(10,0,180,47.5)];
 kpl.text = NSLocalizedString(@"KeepPath",@"");
 kpl.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
 kpl.textAlignment = NSTextAlignmentLeft;
 kpl.textColor = fontColor;
 [kp addSubview:kpl];
 
 UISwitch *kw = [[UISwitch alloc]init];
 kw.center = CGPointMake(220,24);
 kw.onTintColor = RGBA(125,125,125,1.0);
 kw.tag = 1;
 kw.on = NO;
 [kw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
 [kp addSubview:kw];
 
 //reverse
 UIView *rv = [[UIView alloc]initWithFrame:CGRectMake(0,144,250,48)];
 [menu5 addSubview:rv];
 
 UILabel *rvl = [[UILabel alloc]initWithFrame:CGRectMake(10,0,180,48)];
 rvl.text = NSLocalizedString(@"ReverseMode",@"");
 rvl.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
 rvl.textAlignment = NSTextAlignmentLeft;
 rvl.textColor = fontColor;
 [rv addSubview:rvl];
 
 UISwitch *rw = [[UISwitch alloc]init];
 rw.center = CGPointMake(220,24);
 rw.onTintColor = RGBA(125,125,125,1.0);
 rw.tag = 2;
 rw.on = NO;
 [rw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
 [rv addSubview:rw];
 
 //remove
 UIView *rm = [[UIView alloc]initWithFrame:CGRectMake(0,192,250,48)];
 
 [menu5 addSubview:rm];
 
 
 UIButton *rmb = [[UIButton alloc]initWithFrame:CGRectMake(0,0,250,48)];
 [rmb setTitle:NSLocalizedString(@"DeletePath",@"") forState:UIControlStateNormal];
 rmb.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
 rmb.titleLabel.textAlignment = NSTextAlignmentLeft;
 [rmb setTitleColor:selectColor forState:UIControlStateHighlighted];
 [rmb setTitleColor:RGBA(125,125,125,1.0) forState:UIControlStateNormal];
 rmb.tag = 23;
 [rmb addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
 [rm addSubview:rmb];
 [self menuState:2];
 
 */
@implementation AnimationMenuView
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        self.hidden = YES;
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor clearColor];
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
            // iPhone
            isIPhone = YES;
        }
        else{
            // iPad
            isIPhone = NO;
        }
        
        fontColor = RGBA(105,105,105,1.0);
        selectColor = RGBA(205,205,205,1.0);
        fontSize = 12.0;
        
        
        
    }
    return self;
}

-(void)setConstraint:(NSArray *)width height:(NSArray *)height{
    w = width;
    h = height;
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:visualEffectView];
    NSArray *cw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[visualEffectView]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"visualEffectView":visualEffectView}];
    [self addConstraints:cw];
    NSArray *ch =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[visualEffectView]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"visualEffectView":visualEffectView}];
    [self addConstraints:ch];
    [self setMethod];
}


-(void)setMethod{
    //設定画面を閉じます
    done = [[UIButton alloc]initWithFrame:CGRectMake(0,0,80,40)];
    [done setTitle:NSLocalizedString(@"Done",@"") forState:UIControlStateNormal];
    [done setTitleColor:selectColor forState:UIControlStateHighlighted];
    [done setTitleColor:RGBA(105,105,105,1.0) forState:UIControlStateNormal];
    done.titleLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-SemiBold" size:13.0];
    [done addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"Config" ofType:@"plist"];
    NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
    flipSpeed = [NSArray arrayWithArray:[dic objectForKey:@"FlipSpeed"]];
    speed = [[UIView alloc]initWithFrame:CGRectMake(0,0,200,48)];
    
    for(int i = 0; i <= 4; i++){
        UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(40 * i + 3,7,34,34)];
        but.layer.cornerRadius = 17;
        [but setTitle:flipSpeed[i] forState:UIControlStateNormal];
        but.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
        but.titleEdgeInsets = UIEdgeInsetsMake(3, 0, 0, 0);
        [but addTarget:self action:@selector(touchSpeed:) forControlEvents:UIControlEventTouchUpInside];
        
        [[but layer] setBorderColor:[RGBA(200,200,200,1.0) CGColor]];
        [[but layer] setBorderWidth:0.5];
        
        if(i == 0){
            spca = but;
            [but setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
            [but setBackgroundColor:RGBA(0,193,255,1.0)];
        }else{
            [but setTitleColor:RGBA(45,40,35,1.0) forState:UIControlStateNormal];
            [but setBackgroundColor:RGBA(255,255,255,255)];
        }
        [speed addSubview:but];
        
    }
    
    
    
    
    table = [[UITableView alloc] initWithFrame:CGRectZero  style:UITableViewStyleGrouped];
    table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
    table.backgroundColor = [UIColor clearColor];
    table.delegate = self;
    table.dataSource = self;
    table.rowHeight = 50.0;
    table.scrollEnabled = NO;
    table.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:table];
    
    
    
    NSArray *tw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                                          options:0
                                                          metrics:nil
                                                            views:@{@"view":table}];
    [self addConstraints:tw];
    NSArray *th =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":table}];
    [self addConstraints:th];

}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *head = [[UIView alloc]initWithFrame:CGRectMake(0,0,table.frame.size.width,40)];
    UILabel *label = [[UILabel alloc]initWithFrame:head.frame];
    label.font = [UIFont fontWithName:@"Avenir Next" size:13.0];
    label.textAlignment = NSTextAlignmentCenter;
    label.text = @"Config";
    label.backgroundColor = RGBA(250,250,250,1.0);
    label.textColor = RGBA(105,105,105,1.0);
    [head addSubview:label];
    
    done.frame = CGRectMake(self.frame.size.width - 80,0,80,40);
    [head addSubview:done];
    
    return head;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 40;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(80,100,112,1.0) selectColor:RGBA(80,100,112,1.0) defaultBack:RGBA(255,255,255,0.0) selectBack:RGBA(255,255,255,0.0)];
   
    }
    for(UIView *view in [cell.contentView subviews]){
        [view removeFromSuperview];
    }
    cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    switch (indexPath.row) {
        case 0:
        {
            cell.textLabel.text = NSLocalizedString(@"Interval",@"");
            cell.accessoryView = speed;
        }
            break;
        case 1:
        {
            cell.textLabel.text = NSLocalizedString(@"KeepPath",@"");
            
            UISwitch *kw = [[UISwitch alloc]init];
            kw.center = CGPointMake(220,24);
            kw.onTintColor = RGBA(0,193,255,1.0);
            kw.tag = 1;
            kw.on = NO;
            [kw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
            cell.accessoryView = kw;
        }
            break;
        case 2:
        {
            cell.textLabel.text = NSLocalizedString(@"ReverseMode",@"");
            
            UISwitch *rw = [[UISwitch alloc]init];
            rw.center = CGPointMake(220,24);
            rw.onTintColor = RGBA(0,193,255,1.0);
            rw.tag = 2;
            rw.on = NO;
            [rw addTarget:self action:@selector(switchValue:) forControlEvents:UIControlEventValueChanged];
            cell.accessoryView = rw;

        }
            break;
        case 3:
        {
            cell.textLabel.text = NSLocalizedString(@"DeletePath",@"");
            UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,70,35)];
            but.layer.cornerRadius = 3;
            but.backgroundColor = RGBA(255,26,12,1.0);
            [but setTitle:NSLocalizedString(@"Delete",@"") forState:UIControlStateNormal];
            [but setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
            [but setTitleColor:RGBA(5,5,5,1.0) forState:UIControlStateHighlighted];
            but.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:13.0];
            [[but layer] setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
            [[but layer] setBorderWidth:2.0];
            [but addTarget:self action:@selector(deleteAll) forControlEvents:UIControlEventTouchUpInside];
            cell.accessoryView = but;
        }
            break;
        default:
            break;
    }
    
    
    
    
    
    

    return cell;
}

-(void)doneBut{
    
}

-(void)touchSpeed:(UIButton *)but
{
    [(UIButton *)spca setTitleColor:RGBA(45,40,35,1.0) forState:UIControlStateNormal];
    [(UIButton *)spca setBackgroundColor:RGBA(255,255,255,255)];
    [but setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [but setBackgroundColor:RGBA(0,193,255,1.0)];
    [self.delegate setSpeed:[but.titleLabel.text floatValue]];
    spca = but;
}

-(void)switchValue:(UISwitch *)sw{
    [self.delegate switchValue:sw];
}




-(void)fadeIn{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toChatView" object:nil userInfo:@{@"type":@100}];
    self.hidden = NO;
    
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    [superView removeConstraints:h];
    
    
    if(isIPhone){
        w =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":self}];
        [superView addConstraints:w];
        h =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":self}];
        [superView addConstraints:h];
    }else{
        w =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                                options:0
                                                metrics:@{@"padding":[NSNumber numberWithFloat:0],@"width":[NSNumber numberWithFloat:300]}
                                                  views:@{@"view":self}];
        [superView addConstraints:w];
        h =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":self}];
        [superView addConstraints:h];
    }
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    }];
}

-(void)fadeOut{
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    [superView removeConstraints:h];
    
    
    if(isIPhone){
        w =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":self}];
        [superView addConstraints:w];
        h =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(width)]-padding-|"
                                                options:0
                                                metrics:@{@"padding":[NSNumber numberWithFloat:-superView.frame.size.height],@"width":[NSNumber numberWithFloat:superView.frame.size.height]}
                                                  views:@{@"view":self}];
        [superView addConstraints:h];
    }else{
        w =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                                options:0
                                                metrics:@{@"padding":[NSNumber numberWithFloat:-300],@"width":[NSNumber numberWithFloat:300]}
                                                  views:@{@"view":self}];
        [superView addConstraints:w];
        h =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":self}];
        [superView addConstraints:h];
    }
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        self.hidden = YES;
    }];
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    
}

-(void)deleteAll{
    [self.delegate setEditTab:23];
}

@end
