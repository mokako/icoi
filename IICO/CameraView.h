//
//  CameraView.h
//  ICOI
//
//  Created by mokako on 2014/02/13.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>
#import <MobileCoreServices/MobileCoreServices.h>

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


@protocol CameraViewDelegate <NSObject>
-(void)reloadAlbum;
@end


@interface CameraView : UIView<AVCaptureVideoDataOutputSampleBufferDelegate>
{
    AVCaptureSession* session;
    UIImageView* imageView;
    AVCaptureStillImageOutput *imageOutput;
    UIView *camera;
}

-(void)cameraStart;
-(void)cameraStop;
@property (nonatomic, weak) id <CameraViewDelegate> delegate;
@end
