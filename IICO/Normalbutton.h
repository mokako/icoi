//
//  Normalbutton.h
//  ICOI
//
//  Created by mokako on 2016/03/27.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface NormalButton : UIButton
+ (instancetype)view;
-(void)setButtonState:(BOOL)state;
@end
