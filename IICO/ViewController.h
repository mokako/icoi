//
//  ViewController.h
//  ICOI
//
//  Created by moca on 2013/12/02.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "ICOI.h"

#import "VisitView.h"
#import "AccountView.h"
#import "ServiceListTable.h"
#import "PopupView.h"
#import "SetDescHTML.h"

static NSString * const kAppStoreID = @"1022911877";

@interface ViewController : UIViewController<ServiceListTableDelegate>
{
    FMDatabaseQueue *db;
    UIView *signin;
    UILabel *descript;
    UIView *serviceNameField;
    UIView *displayNameField;
    UILabel *accountIcon;
    UILabel *icoi;
    ServiceListTable *listTable;
    UIButton *accept;
    UIButton *decline;
    CGPoint original;
    NSString *serviceName;
    UIColor *fontColor;
    UIColor *backColor;
    UIWebView *webView;
    UIView *back;
    
    VisitView *visit;
    AccountView *account;
    NSArray *accountConstraint;
    
    PopupView *firstPop;
    SetDescHTML *setHTML;
    UIWebView *web;
    
    BOOL isKeybord;
    BOOL openKeyboard;
    BOOL isOrientation;
    BOOL iPhone;
    BOOL firstRun;
    BOOL isView;
    BOOL isStart;
    BOOL isOpen;
    BOOL isRotate;
    
    CGRect keyboardRect;
    NSTimeInterval duration;
}

@end
