//
//  PeerController.h
//  ICOI
//
//  Created by mokako on 2015/06/02.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PeerController : NSObject
{
    NSString *service;
    NSMutableDictionary *list;
}
-(instancetype)init:(NSString *)serviceName;

-(void)loadPeer;
-(void)addPeer:(NSString *)peerName;
-(void)removePeer:(NSString *)peerName;

-(void)addFile:(NSString *)fileName;
-(void)removeFile:(NSString *)fileName;

-(BOOL)checkPeer:(NSString *)peerName;
-(BOOL)checkFile:(NSString *)fileName;
@end
