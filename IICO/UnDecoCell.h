//
//  UnDecoCell.h
//  ICOI
//
//  Created by mokako on 2015/12/14.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UnDecoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UILabel *mess;
@end
