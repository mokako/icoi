//
//  PickerView.h
//  ICOI
//
//  Created by mokako on 2014/09/29.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ImageSIzeTable.h"
#import "ClearToolBar.h"




@protocol PickerViewDelegate <NSObject>
-(void)setImageFromIndex:(BOOL)as;
-(void)setImageName:(NSString *)str scale:(CGFloat)scale;
-(void)closePicker;
@end

@interface PickerView : UIView<UIScrollViewDelegate,ImageSIzeTableDelegate>
{
    UIScrollView *sc;
    UIView *sideMenu;
    
    
    UIView *menu1;
    ImageSIzeTable *imageSizeTable;
    UILabel *scaleLabel;
    CGFloat setScale;
    
    UIView *nk;
    ClearToolBar *tool;
    UITextField *commentText;
    
    UILabel *sizeLabel;
    NSDate *oldDate;
    CGFloat fileUploadInterval;
   
    
    
    CGRect keyboardRect;
    NSTimeInterval duration;
    NSArray *nkh;

}
@property (nonatomic, weak)id<PickerViewDelegate>delegate;
-(void)setImageWithPicker:(UIImage *)img index:(NSInteger)index;
-(void)endTask;
@end
