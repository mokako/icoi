//
//  ConvertFileView.m
//  ICOI
//
//  Created by mokako on 2014/08/24.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "ConvertFileView.h"

@implementation ConvertFileView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        strArray = [[NSMutableArray alloc]init];
        
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)initializeView
{
    //プレビューを表示させる領域
    /*
     全てのビューは一端それぞれの処理を施した後にこのviewに設置します。
     */
    foundationView = [[UIView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    [self addSubview:foundationView];
    
    
    //imageを表示する為のもので表示する際はfoundationViewに登録します。
    previewField = [[UIScrollView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    previewField.delegate = self;
    previewField.bounces = NO;
    previewField.backgroundColor = RGBA(35,30,25,1.0);
    

    
    
    pageTable = [[PageListTable alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    pageTable.delegate = self;
    
    
    //
    imageArray = [[NSArray alloc]initWithObjects:@"JPG",@"jpg",@"PNG",@"png",@"JPEG",@"jpeg",nil];
    
    table = [self createTable:CGRectMake(0,0,self.frame.size.width,self.frame.size.height) tag:1];
    [self addSubview:table];
    
    table.hidden = YES;
    NSBundle* bundle = [NSBundle mainBundle];
    //読み込むファイルパスを指定
    NSString* path = [bundle pathForResource:@"fileExtension" ofType:@"plist"];
    extensionDictionary = [[NSDictionary alloc]initWithDictionary:[NSDictionary dictionaryWithContentsOfFile:path][@"extensionList"]];
    
    
    
    
    
    //
    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    tool = [[ClearToolBar alloc]initWithFrame:CGRectMake(0,self.frame.size.height - 48,self.frame.size.width,48)];
    tool.hidden = NO;
    
    UIButton *cl = [[UIButton alloc]initWithFrame:CGRectMake(0,0,44,44)];
    cl.titleLabel.font = [UIFont fontWithName:@"playIcon" size:20.0];
    [cl setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    cl.backgroundColor = RGB(35,30,25);
    
    [[cl layer] setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
    [[cl  layer] setBorderWidth:2.0];
    cl.layer.cornerRadius = cl.frame.size.width / 2;
    
    
    [cl setTitle:@"r" forState:UIControlStateNormal];
    
    cl.tag = 1;
    [cl addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *a = [[UIBarButtonItem alloc]initWithCustomView:cl];
    
    
    UIButton *co = [[UIButton alloc]initWithFrame:CGRectMake(0,0,44,44)];
    co.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:13.0];
    [co setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [co setTitle:@"Conf" forState:UIControlStateNormal];
    [[co layer] setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
    [[co  layer] setBorderWidth:2.0];
    co.layer.cornerRadius = cl.frame.size.width / 2;
    co.backgroundColor = RGB(35,30, 25);
    co.tag = 2;
    [co addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:co];
    
    

    
    
    tool.items = [NSArray arrayWithObjects:gap,a,b,gap,nil];
    
    [self addSubview:tool];
    
    
    
    pages = [[UILabel alloc]initWithFrame:CGRectMake(self.frame.size.width - 100, 10,100, 10)];
    pages.textAlignment = NSTextAlignmentCenter;
    pages.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
    pages.hidden = YES;
    [self addSubview:pages];
    
}

-(UIButton *)createBut:(CGRect)rec tag:(NSInteger)tag color:(UIColor *)color borderColor:(UIColor *)boderColor setTitile:(NSString *)title titleColor:(UIColor *)titleColor size:(CGFloat)size
{
    UIButton *but = [[UIButton alloc]initWithFrame:rec];;
    but.backgroundColor = color;
    [[but layer] setBorderColor:[boderColor CGColor]];
    [[but  layer] setBorderWidth:2.0];
    but.layer.cornerRadius = rec.size.width / 2;
    but.titleLabel.font = [UIFont fontWithName:@"icomoon" size:size];
    [but setTitleColor:titleColor forState:UIControlStateNormal];
    [but setTitle:title forState:UIControlStateNormal];
    but.tag = tag;
    [but addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    return but;
}

-(void)touchBut:(UIButton *)but
{
    switch (but.tag) {
        case 1:
        {

            for (UIView *view in [foundationView subviews]) {
                [view removeFromSuperview];
            }
            [self.delegate touchProc:10];
        }
            break;
        case 2:
        {
            [pageTable setLog];
        }
            break;
        default:
            break;
    }
    
}

-(void)review
{
    table.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    foundationView.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    previewField.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    pageTable.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    [pageTable review];
    tool.frame = CGRectMake(0,self.frame.size.height - 48,self.frame.size.width,48);
    pages.frame = CGRectMake(self.frame.size.width - 100, 10,100, 10);
}

#pragma mark - fileSetting
#pragma mark -- 指定されたファイルをどうするかを決め、コンバートを行うかどうかサービスに採用するかどうか削除するかどうか等を決めます。
-(void)fileSetting
{
    //まずはファイルを読み込みそのまま使えるのかどうかを調べます。
    NSString *sr = [self.fileName pathExtension];
    
    
    
    if(extensionDictionary[sr]){
        //support file type
#pragma mark - ファイルの処理方法を増やしたいときはここを追加してください。
        switch ([extensionDictionary[sr] integerValue]) {
            case 0:
            { //image
                /*
                 imageを処理する場合倍率や
                 */
                [self setImage];
            }
                break;
            case 1:
            { //text
                /*
                TextContainerView *cont = [[TextContainerView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
                cont.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
                cont.text =  [FileManager readTextFileOfTmp:self.fileName];
                cont.size = CGSizeMake(self.frame.size.width,self.frame.size.height);

                [pageTable setTableState:[cont setView]];
                [foundationView addSubview:pageTable];
                 */
                [self setText];
            }
                break;
            case 2:
            { //pdf
                //[self setPDF];
            }
                
            default:
                break;
        }
        
        
    }else{
        //unsupported file type
       // NSLog(@"ed %ld",(long)[extensionDictionary[sr] integerValue]);
      
    }
    
    
    //NSLog(@"file attribute %@",[FileManager getAttributesOfFile:self.fileName]);
}

-(void)setImage
{
    pages.hidden = YES;
    
    
    UIImage *img = [FileManager readImageOfTmp:self.fileName];
    UIImageView *view = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,img.size.width,img.size.height)];
    view.image = img;
    previewField.contentSize = CGSizeMake(view.frame.size.width,view.frame.size.height);
    [previewField addSubview:view];
    
}

-(void)setText
{
    pages.hidden = NO;
    
    
    
    
    NSString *str = [FileManager readTextFileOfTmp:self.fileName];
    
    NSMutableArray *lines = [NSMutableArray array];
    
    [str enumerateLinesUsingBlock:^(NSString *line, BOOL *stop) {
        [lines addObject:line];
    }];
    
    pageTable.width = 1200;
    pageTable.height = 1700;
    
    //実際の表示領域には余白が発生する為あらかじめ余白分を引いた上で領域を計算します。
    UITextView *view = [[UITextView alloc]initWithFrame:CGRectMake(0,0,pageTable.width,pageTable.height)];
    view.contentInset = UIEdgeInsetsMake(10,10,10,10);
    view.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
    
    
    
    view.text = str;
    //文字数を算出
    NSRange allGlyphs = [view.layoutManager glyphRangeForTextContainer:view.textContainer];
    pageTable.font = view.font;
    
    
    
    CGPoint startPoint = view.contentOffset;
    UITextPosition *startPosition = [view closestPositionToPoint:startPoint];
    NSInteger location = [view offsetFromPosition:view.beginningOfDocument
                                       toPosition:startPosition];
    CGPoint endPoint = startPoint;
    endPoint.x += view.frame.size.width;
    endPoint.y += view.frame.size.height;
    
    
    UITextPosition *endPosition = [view closestPositionToPoint:endPoint];
    
    
    
    NSInteger length = [view offsetFromPosition:startPosition
                                     toPosition:endPosition];
    NSRange range = NSMakeRange(location, length);

    __block NSUInteger minCount = 0;
    [view.layoutManager enumerateLineFragmentsForGlyphRange:range
                                                 usingBlock:^(CGRect rect, CGRect usedRect, NSTextContainer *textContainer, NSRange glyphRange, BOOL *stop) {
                                                     
                                                     if(rect.origin.y + usedRect.size.height > view.frame.size.height){
                                                         *stop = YES;
                                                     }else{
                                                         minCount++;
                                                     }
                                                 }];
    
    
    
    
    pageTable.lineCount = minCount;
    NSLog(@"minCount %d",minCount);
    __block NSUInteger count = 0;
    [strArray removeAllObjects];
    
    NSMutableString *rastr = [NSMutableString string];
    [view.layoutManager enumerateLineFragmentsForGlyphRange:allGlyphs
                                                 usingBlock:^(CGRect rect, CGRect usedRect, NSTextContainer *textContainer, NSRange glyphRange, BOOL *stop) {
                                                     [rastr appendString:[str substringWithRange:glyphRange]];
                                                     if(count == minCount){
                                                         count = 0;
                                                         [strArray addObject:[NSString stringWithString:rastr]];
                                                         [rastr setString:@""];
                                                     }else{
                                                         count++;
                                                     }
                                                     
                                                 }];
    //指定された行数に到達していない場合文字列が保管されていないので最後に回収します。
    if(rastr){
        [strArray addObject:rastr];
    }
    
    for(NSString *str in strArray){
        CGSize textSize = [str boundingRectWithSize:CGSizeMake(view.frame.size.width, view.frame.size.height) options:NSStringDrawingTruncatesLastVisibleLine|NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName:view.font} context:nil].size;
        NSLog(@"text size %@",NSStringFromCGSize(textSize));
    }
    
    pageTable.pages = strArray.count;
    
#pragma mark --page view controll
    for (UIView *view in [foundationView subviews]) {
        [view removeFromSuperview];
    }
    [pageTable setTableState:strArray];
    [foundationView addSubview:pageTable];
    
    
    //[pageTable setTableState:strArray];
    
    
    //NSLog(@"%@",strArray.description);
    
    
    
    /*
     
     CGSize size = [view sizeThatFits:view.frame.size];
     
     previewField.contentSize = size;
     [previewField addSubview:view];
     
     */
}


- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat pageWidth = previewField.frame.size.width;
    CGPoint point = scrollView.contentOffset;
    point.y = 0;
    scrollView.contentOffset = point;
    if ((NSInteger)fmod(previewField.contentOffset.x , pageWidth) == 0) {
        // ページコントロールに現在のページを設定
        pageControl.currentPage = previewField.contentOffset.x / self.frame.size.width;
    }
}


-(void)setPage:(NSInteger)allPages page:(NSInteger)page
{
    pages.text = [NSString stringWithFormat:@"%d/%d",page + 1,allPages];
}





-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(section == 0){
        //file state
        /*
         初期値は１でステータスボタンを押した時だけ展開出来る様にします。
         方法としてボタンを設置しタグの初期値を１に変更すれば２に変わる様にする。
         */
        return 1;
    }else if(section == 1){
        //convert menu
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    //NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    //NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(82,188,174,1.0) selectColor:RGBA(55,55,55,1.0) defaultBack:RGBA(255,255,255,0.9) selectBack:RGBA(245,245,245,0.9)];
    }
    if(indexPath.section == 0){
        
    }else if(indexPath.section == 1){
        
    }
    return cell;
}

-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStylePlain];
    atable.backgroundColor = RGBA(45,45,45,0.6);
    atable.separatorStyle = UITableViewCellSeparatorStyleNone;
    atable.delegate = self;
    atable.dataSource = self;
    atable.tag = tag;
    atable.rowHeight = 60.0;
    atable.showsVerticalScrollIndicator = NO;
    return atable;
}

@end
