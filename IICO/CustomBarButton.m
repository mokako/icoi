//
//  CustomBarButton.m
//  ICOI
//
//  Created by mokako on 2016/02/04.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "CustomBarButton.h"

@implementation CustomBarButton
- (instancetype)initWithTitle:(nullable NSString *)title style:(UIBarButtonItemStyle)style target:(nullable id)target action:(nullable SEL)action{
    self = [super initWithTitle:title style:style target:target action:action];
    if(self){
        [self setTitleTextAttributes:@{NSFontAttributeName : [UIFont fontWithName:@"select3" size:15.0]} forState:UIControlStateNormal];
    }
    return self;
}
- (nullable instancetype)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithHex:@"CFCFCF"],NSFontAttributeName : [UIFont fontWithName:@"select3" size:17.0]} forState:UIControlStateNormal];
    }
    return self;
}
@end
