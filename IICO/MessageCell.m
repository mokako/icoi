//
//  MessageCell.m
//  ICOI
//
//  Created by mokako on 2015/09/21.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "MessageCell.h"

@implementation MessageCell

- (void)awakeFromNib {
    rec = CGRectZero;
    // Initialization code
    self.article.scrollEnabled = NO;
    self.article.showsVerticalScrollIndicator = NO;
    self.article.textContainerInset = UIEdgeInsetsZero;
    self.article.editable = NO;
    self.article.font = [UIFont fontWithName:NSLocalizedString(@"lang_01",@"") size:13];
    self.name.font = [UIFont fontWithName:NSLocalizedString(@"lang_01_title",@"") size:15];
    
    
    [self.clip addTarget:self action:@selector(setHash:) forControlEvents:UIControlEventTouchUpInside];
    [self.ref addTarget:self action:@selector(setHash:) forControlEvents:UIControlEventTouchUpInside];
    [self.link addTarget:self action:@selector(setHash:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    if (highlighted)
    {
        self.backgroundColor = self.hilightBack;
    }
    else
    {
        self.backgroundColor = self.defaultBack;
    }
}

-(void)setArticleFrame:(NSString *)str{
    self.article.text = str;
    CGSize size = [self.article sizeThatFits:CGSizeMake(self.article.frame.size.width - 14,10000)];
    rec = CGRectMake(self.article.frame.origin.x,self.article.frame.origin.y, self.article.frame.size.width,size.height);
    self.article.frame = rec;

}

-(void)setHash:(UIButton *)setBut{
    NSDictionary *dic = @{
                          @"type"   : [NSNumber numberWithInteger:setBut.tag],
                          @"tag"    : [NSNumber numberWithInteger:self.tag]
                          };
    [[NSNotificationCenter defaultCenter] postNotificationName:@"toChatView" object:self userInfo:dic];
}
@end
