//
//  AnimateConsoleView.h
//  ICOI
//
//  Created by mokako on 2014/06/15.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"
#import "DataManager.h"
#import <QuartzCore/CALayer.h>

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
@protocol AnimateConsoleViewDelegate <NSObject>
-(void)setEditTab:(NSInteger)stat;
@end
@interface AnimateConsoleView : UIView<UITableViewDataSource, UITableViewDelegate>
{
    UITableView *root;
    UIButton *closeBut;
    UISlider *s;
}
@property (nonatomic)CGFloat speed;
@property (nonatomic)BOOL keep;
@property (nonatomic)BOOL reverse;


-(void)review;
@property(nonatomic,weak)id<AnimateConsoleViewDelegate>delegate;
@end
