//
//  PlaceHolderTextView.h
//  ICOI
//
//  Created by mokako on 2014/02/20.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UIPlaceHolderTextView : UITextView

@property (nonatomic, retain) NSString *placeholder;
@property (nonatomic, retain) UIColor *placeholderColor;
@property (nonatomic, retain) UIFont *placeholderFont;
@property (nonatomic, retain) UILabel *placeHolderLabel;

-(void)textChanged:(NSNotification*)notification;

@end
