//
//  TreeListCell.h
//  ICOI
//
//  Created by mokako on 2015/11/29.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
@interface TreeListCell : UITableViewCell
{
    NSArray *colorList;
}
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *icon;

-(void)setState:(NSInteger)state;
@end
