//
//  AnimationMenuView.h
//  ICOI
//
//  Created by mokako on 2015/01/20.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"


@protocol AnimationMenuViewDelegate <NSObject>
-(void)setSpeed:(CGFloat)value;
-(void)setEditTab:(NSInteger)state;
-(void)switchValue:(UISwitch *)sw;
@end
@interface AnimationMenuView : UIView<UITableViewDataSource, UITableViewDelegate>
{
    CAGradientLayer *gradient;
    BOOL isIPhone;
    BOOL isRotate;
    UIView *speed;
    UITableView *table;
    UIColor *fontColor;
    UIColor *selectColor;
    CGFloat fontSize;
    NSArray *flipSpeed;
    id spca;
    UIImageView *tail;
    UIButton *done;
    NSArray *w;
    NSArray *h;
}
@property (nonatomic,weak)id<AnimationMenuViewDelegate>delegate;
-(void)fadeIn;
-(void)fadeOut;
-(void)setConstraint:(NSArray *)width height:(NSArray *)height;
@end
