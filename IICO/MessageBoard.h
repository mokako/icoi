//
//  MessageBoard.h
//  ICOI
//
//  Created by mokako on 2015/12/28.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "MessageBoardConfig.h"
#import "ClearToolBar.h"
#import "UIColor+Hex.h"

#import "NormalButton.h"


@interface MessageBoard : UIView<UITextViewDelegate>
{
    NSArray *slh;
    NSArray *mbh;
    UIView *main;
    UIBarButtonItem *ca,*cb,*cc,*cd,*mc,*ml,*gap;
    NormalButton *defaultBut;
    UIColor *fontColor,*selectColor,*selectFont,*unKeep,*keep;
    CGFloat radius;
    CGSize offset;
    
    
    NSInteger tagNum;
    NSString *hash;

    
    BOOL isOpen;
    BOOL isReturnEnd;
    
    NSString *text;
    NSString *oldAddText,*oldMessage;
    NSTimeInterval duration;
    CGRect keyboardRect;
    
    NSDictionary *selectPath;
    NSMutableDictionary *selectDic;
    
    
    UITextView *textView;
    NSArray *textContinerHeight;
    
    NSInteger oldState;
    messageType messm;
    MessageBoardConfig *msc;
    NSDictionary *selectedUID;
}

@property (weak, nonatomic) IBOutlet UIView *base;
@property (weak, nonatomic) IBOutlet UIView *textContiner;
@property (weak, nonatomic) IBOutlet ClearToolBar *ct;
@property (weak, nonatomic) IBOutlet UIButton *send;
@property (weak, nonatomic) IBOutlet UIButton *down;
@property (weak, nonatomic) IBOutlet UIButton *mess;
@property (weak, nonatomic) IBOutlet UIButton *keepTag;
@property (weak, nonatomic) IBOutlet UILabel *rename;
@property (weak, nonatomic) IBOutlet UILabel *pick;
+ (instancetype)view;

-(void)setLayout:(NSArray *)layout;

@end
