//
//  DecoCell.h
//  ICOI
//
//  Created by mokako on 2015/12/13.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DecoCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *mess;
@property (weak, nonatomic) IBOutlet UILabel *visi;
@end
