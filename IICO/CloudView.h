//
//  CloudView.h
//  ICOI
//
//  Created by mokako on 2014/04/13.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClearToolBar.h"
#import "TempDirTable.h"
#import <DropboxSDK/DropboxSDK.h>
#import "EVCircularProgressView.h"
#import "ClearToolBar.h"

@protocol CloudViewDelegate <NSObject>
-(void)moveFile:(NSString *)path;
@end

@interface CloudView : UIView<DBRestClientDelegate, DBSessionDelegate, DBNetworkRequestDelegate, UITableViewDataSource, UITableViewDelegate, TempDirTableDelegate>
{
    DBRestClient* restClient;
    NSMutableArray *folderList;
    NSMutableArray *dirList;
    UITableView *table;
    NSString *dirPath;
    UIViewController *controller;
    NSString *lastElement;
    NSMutableDictionary *stack;
    NSMutableDictionary *allPath;
    UIToolbar *toolbar;
    NSArray *extensionList;
    TempDirTable *tmp;
    UIToolbar *navi;
    NSDictionary *extensionDictionary;

}

-(void)setCloudSession:(NSUInteger)sessionService;
-(void)setController:(UIViewController *)viewController;
-(void)review;
@property (nonatomic, weak) id <CloudViewDelegate>delegate;
@end
