//
//  ToolCell.h
//  ICOI
//
//  Created by mokako on 2015/10/10.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ToolCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *view;
@property (weak, nonatomic) IBOutlet UIView *base;



-(void)setWidth:(CGFloat)width;
@end
