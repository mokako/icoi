//
//  SketchView.m
//  ICOI
//
//  Created by mokako on 2014/03/10.
//  Copyright (c) 2014年 moca. All rights reserved.
//


/* 要望
 
 UIKeycommandの導入を検討してください
 
 
 
 
 */

#import "SketchView.h"

#define CAPACITY 100

typedef struct
{
    CGPoint firstPoint;
    CGPoint secondPoint;
} LineSegment;


@interface SketchView()<UIScrollViewDelegate,RemoveStackResponseDelegate,ToolViewDelegate>
{
    int moveCount;
    NSArray *toolArray;
    CGPoint pts[5];
    CGPoint pte[5];
    uint ctr;
    CGPoint pointsBuffer[CAPACITY];
    uint bufIdx;
    dispatch_queue_t drawingQueue;
    BOOL isFirstTouchPoint;
    LineSegment lastSegmentOfPrev;
    CGPoint lastPoint;
    NSMutableArray *pointArray;
    RemoveStackResponse *stack;

    id spca;
    
    //animation
    CGFloat Speed;
    BOOL isKeep;
    BOOL isReverse;

    //AnimationMenuView *animenu;
}


@property(nonatomic)UIBezierPath *bezierPath;
@property(nonatomic)UIBezierPath *dummyPath;
@property(nonatomic)UIBezierPath *currentPath;
@property(nonatomic)CGPoint lastTouchPoint;
@property(nonatomic)CALayer *atNewLayer;
@property(nonatomic)UIImage *atNewImage;
@property(nonatomic)UIImage *atDummyImage;
@property(nonatomic)CALayer *atDummyLayer;

@property(nonatomic)NSMutableArray *stacking; //短期パス配列
@property(nonatomic)NSInteger stackingCount;
@property(nonatomic)NSMutableArray *redoStack;

@property(nonatomic)UIView *ocToolbar;

@property(nonatomic)UITextView *lineWidthText;




@end
@implementation SketchView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
            // iPhone  
            iPhone = YES;
        }
        else{
            // iPad
            iPhone = NO;
        }
        sketchState = 0;
        zoom = 1.0;
        
        
        scaleTransform = CGAffineTransformIdentity;
        moveTransform = CGAffineTransformIdentity;
        
        
        self.stacking = [[NSMutableArray alloc]init];
        self.stackingCount = 0;
        self.redoStack = [[NSMutableArray alloc]init];
        pathList = [[NSMutableArray alloc]init];
        privatePath = [[NSMutableArray alloc]init];
        pointArray = [[NSMutableArray alloc]init];
        displayPath = [[NSMutableArray alloc]init];
        
#pragma mark - 文字の太さ
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* path = [bundle pathForResource:@"Config" ofType:@"plist"];
        NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
        
        widthArray = [NSArray arrayWithArray:[dic objectForKey:@"LineWidth"]];
        lineWidthArray = [NSArray arrayWithArray:[dic objectForKey:@"OtherLineWidth"]];
        flipSpeed = [NSArray arrayWithArray:[dic objectForKey:@"FlipSpeed"]];
        
        self.tag = 10;
        self.clipsToBounds = YES;
        isAnimate = NO;//アニメーションを現在行っているかどうか
        isComplete = NO;//flipの再生が完了しているかどうか
        isPaint = YES; //flipモード操作時に偽となりペイントモード時に真となります。
        isAnimTool = NO;
        isPath = NO;
        
        isPen = NO;
        isFirstTouchPoint = NO;
        
        isComment = NO;
        
        FF = .03;
        LOWER = 0.01;
        setColor = RGBA(0,0,0,1.0);
        setAlpha = 1.0;
        setLineWidth = 0.9;
        setOtherLine = 1.0;
        
        Speed = 0.3;
        isKeep = NO;
        isReverse = NO;
        isOrientation = NO;
        isCanvas = YES;
        
        sendMode = 5;
        
        UIDHash = nil;
        
        //scale initialize
        viewType = 0;
        self.drawScale = [[UIScreen mainScreen] scale];
        
        
        
        [self useTool];
        [self initializeMethods];
        [self initializeTool];

        
        self.mainDrawScale = 1.0;

        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendSketch" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchCanvasEvent:) name:@"sendCanvas" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addPath:) name:@"addPath" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setItem:) name:@"setItem" object:nil];
    }
    return self;
}


-(void)layoutSubviews{
    [super layoutSubviews];
    gradient.frame = self.bounds;
}


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)setServiceManager:(ServiceManager *)servicemanager{
    sb = servicemanager;
}

-(void)review
{

    
    //scrollView.frame = self.bounds;
    
    //[self.pathView update:iPhone rotate:isOrientation];
}

-(void)useTool{
    //sound
    NSURL *removeSoundURL = [[NSBundle mainBundle] URLForResource:@"paper" withExtension:@"mp3"];
    //音声ファイル登録
    AudioServicesCreateSystemSoundID ((__bridge CFURLRef)removeSoundURL, &removeSound);
    
    NSURL *sendSoundURL = [[NSBundle mainBundle] URLForResource:@"decision6" withExtension:@"mp3"];
    //音声ファイル登録
    AudioServicesCreateSystemSoundID ((__bridge CFURLRef)sendSoundURL, &sendSound);
    
}


-(void)initializeMethods
{
    
    
    
    base = [[UIView alloc]initWithFrame:CGRectZero];
    base.backgroundColor = RGBA(245,245,245,0.0);
    base.clipsToBounds = YES;
    base.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:base];
    
    
    NSArray *bw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":base}];
    [self addConstraints:bw];
    NSArray *bh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":base}];
    [self addConstraints:bh];
    
    
    
    
    gradient = [CAGradientLayer layer];
    gradient.frame = self.bounds;
    gradient.colors = @[
                        (id)[UIColor colorWithHex:@"#70e1f5"].CGColor,
                        (id)[UIColor colorWithHex:@"#ffd194"].CGColor
                        ];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1,1);
    [base.layer addSublayer:gradient];
    
    
    
    root = [[UIView alloc]initWithFrame:CGRectZero];
    root.backgroundColor = [UIColor clearColor];
    root.tag = 100;
    [base addSubview:root];
    
    //defaultの画像を最初に表示します。
    

    canvas = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,self.bounds.size.width,self.bounds.size.height)];
    canvas.image = nil;
    canvas.tag = 200;
    
    //canvas.userInteractionEnabled = YES;
    [root addSubview:canvas];
    scrollFlag = NO;
    
    
    scaleTransform = CGAffineTransformMakeScale(1,1);
    moveTransform = CGAffineTransformTranslate(moveTransform,0,0);
    
    
    
    dummyCanvas = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,self.bounds.size.width,self.bounds.size.height)];
    dummyCanvas.image = nil;
    
    subCanvas = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,self.bounds.size.width,self.bounds.size.height)];
    subCanvas.tag = 300;
    
    scrollView = [[UIScrollView alloc]initWithFrame:self.bounds];
    scrollView.hidden = YES;
    scrollView.delegate = self;
    scrollView.contentSize = canvas.frame.size;
    scrollView.bounces = NO;
    [scrollView setMaximumZoomScale:2.0];
    [scrollView setMinimumZoomScale:0.5];
    [base addSubview:scrollView];
    [scrollView addSubview:subCanvas];
    zoom = 1.0;
    oldTag = 1;
    
    
    


}
#pragma mark - pathViewの設定
-(void)initializeTool
{

    
#pragma mark -- initialize menu
    //[self initPaintToolBut];
    SketchListTabView *stab = [SketchListTabView view];
    stab.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:stab];
    
    
    
    slv = [SketchListView view];
    slv.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:slv];
    
    pt = [PickUpTab view];
    pt.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:pt];
    
    // message board
    mb = [MessageBoard view];
    mb.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:mb];
    
    //pt
    
    [self addConstraint:[NSLayoutConstraint constraintWithItem:pt  attribute:NSLayoutAttributeCenterX  relatedBy:NSLayoutRelationEqual  toItem:self  attribute:NSLayoutAttributeCenterX  multiplier:1  constant:0]];
    NSArray *ptw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:pt.frame.size.width]}
                                              views:@{@"view":pt}];
    [self addConstraints:ptw];
    NSArray *pth =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:pt.frame.size.height]}
                                              views:@{@"view":pt}];
    [self addConstraints:pth];
    
    //stab
    NSArray *sth =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-100-[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:stab.frame.size.height]}
                                              views:@{@"view":stab}];
    [self addConstraints:sth];
    
    
    NSArray *stslh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[stab(stabWidth)][slv(slvWidth)]"
                                            options:0
                                            metrics:@{@"stabWidth":[NSNumber numberWithFloat:stab.frame.size.width],@"slvWidth":[NSNumber numberWithFloat:slv.frame.size.width]}
                                              views:@{@"stab":stab,@"slv":slv}];
    [self addConstraints:stslh];
    
    NSArray *vw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view]-width-|"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:-slv.frame.size.width]}
                                              views:@{@"view":slv}];
    
    
    [self addConstraints:vw];
    NSArray *mw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":mb}];
    [self addConstraints:mw];
    
    
    
    slh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":slv}];
    [self addConstraints:slh];
    
    
    mbh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:-mb.bounds.size.height]}
                                              views:@{@"view":mb}];
    [self addConstraints:mbh];
    
    
    [slv setConstraint:vw];
    
    
    
    
    
    
    pu = [PickUp view];
    pu.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:pu];
    
    NSArray *pw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":pu}];
    [self addConstraints:pw];
    
    NSArray *kl =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:0]}
                                              views:@{@"view":pu}];
    [self addConstraints:kl];
    [pu setLayout:kl];
    //@{@"height":[NSNumber numberWithFloat:pu.bounds.size.height]
    NSArray *ph =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[pt][view][mb]"
                                            options:0
                                            metrics:nil
                                              views:@{@"pt":pt,@"view":pu,@"mb":mb}];
    
    [self addConstraints:ph];
    
#pragma mark - init pullComment

    
    
#pragma mark - tool menu

//brush
    btv = [BrushToolView view];
    [base addSubview:btv];
    [btv set];
//color
    ctv = [ColorToolView view];
    [base addSubview:ctv];
    [ctv set];
//slider
    sliderView = [ScaleSliderView view];
    sliderView.translatesAutoresizingMaskIntoConstraints = NO;
    [sliderView.slider addTarget:self action:@selector(scale:) forControlEvents:UIControlEventValueChanged];
    [sliderView.ok addTarget:self action:@selector(endScrollMode) forControlEvents:UIControlEventTouchUpInside];
    [base addSubview:sliderView];
    [sliderView set];
//
    
    toolv = [ToolView view];
    toolv.translatesAutoresizingMaskIntoConstraints = NO;
    toolv.delegate = self;
    mv = [[UIView alloc]initWithFrame:CGRectZero];
    mv.translatesAutoresizingMaskIntoConstraints = NO;
    [base addSubview:mv];
    [mv addSubview:toolv];
    NSArray *mvw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view(width)]"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:toolv.frame.size.width]}
                                              views:@{@"view":mv}];
    [self addConstraints:mvw];
    NSArray *nvh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:toolv.frame.size.height]}
                                              views:@{@"view":mv}];
    [self addConstraints:nvh];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:mv  attribute:NSLayoutAttributeCenterY  relatedBy:NSLayoutRelationEqual  toItem:self  attribute:NSLayoutAttributeCenterY  multiplier:1  constant:0]];
    
    NSArray *tow =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":toolv}];
    [mv addConstraints:tow];
    NSArray *toh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":toolv}];
    [mv  addConstraints:toh];
    
    
#pragma mark - scale slider view
    
    
    
    
    
    
    
#pragma mark - Animation tool
    /*
    atv = [AnimationToolView view];
    atv.delegate = self;
    atv.translatesAutoresizingMaskIntoConstraints = NO;
    
    amv = [[UIView alloc]initWithFrame:CGRectZero];
    amv.translatesAutoresizingMaskIntoConstraints = NO;
    amv.hidden = YES;
    [base addSubview:amv];
    [amv addSubview:atv];
    NSArray *amvw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view(width)]"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:atv.frame.size.width]}
                                              views:@{@"view":amv}];
    [self addConstraints:amvw];
    NSArray *amvh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:atv.frame.size.height]}
                                              views:@{@"view":amv}];
    [self addConstraints:amvh];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:amv  attribute:NSLayoutAttributeCenterY  relatedBy:NSLayoutRelationEqual  toItem:self  attribute:NSLayoutAttributeCenterY  multiplier:1  constant:0]];
    
    NSArray *atmw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":atv}];
    [amv addConstraints:atmw];

    NSArray *atmh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":atv}];
    [amv  addConstraints:atmh];
    */
    
    stack = [[RemoveStackResponse alloc]initWithFrame:CGRectZero];
    stack.delegate = self;
    stack.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:stack];
    
    NSArray *lw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":stack}];
    [self addConstraints:lw];
    
    NSArray *lh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":stack}];
    [self  addConstraints:lh];
    
    
    
    
#pragma mark - animation settings
    /*
    animenu = [[AnimationMenuView alloc]initWithFrame:CGRectZero];
    animenu.delegate = self;
    animenu.translatesAutoresizingMaskIntoConstraints = NO;
    [base addSubview:animenu];
    if(iPhone){
        NSArray *w =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":animenu}];
        [base addConstraints:w];
        NSArray *h =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(width)]-padding-|"
                                                options:0
                                                metrics:@{@"padding":[NSNumber numberWithFloat:-base.frame.size.height],@"width":[NSNumber numberWithFloat:base.frame.size.height]}
                                                  views:@{@"view":animenu}];
        [base addConstraints:h];
        [animenu setConstraint:w height:h];
    }else{
        NSArray *w =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                                options:0
                                                metrics:@{@"padding":[NSNumber numberWithFloat:-300],@"width":[NSNumber numberWithFloat:300]}
                                                  views:@{@"view":animenu}];
        [base addConstraints:w];
        NSArray *h =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":animenu}];
        [base addConstraints:h];
        [animenu setConstraint:w height:h];
        
    }
     */
    
    [self performSelector:@selector(reConstraints) withObject:nil afterDelay:0];
    
}


#pragma mark - Path List
-(void)initPathList
{
    
    
    
    
    
    // old
    //self.pathView = [[SketchPathView alloc]init];
    //self.pathView.delegate = self;
    //[base addSubview:self.pathView];
}

-(void)reConstraints{
    [mb setLayout:mbh];
}

//ペイント系のボタン
//他にフェードアウトするクラスと画面回転時の位置を再設定するクラスを作成してください。

#pragma mark - Pain tool

-(void)touchWidth:(UIButton *)but{
    setLineWidth = [widthArray[but.tag] floatValue];
    setOtherLine = [lineWidthArray[but.tag] floatValue];
}

-(void)changeBrushLineWidth:(NSInteger)num{
    setLineWidth = [widthArray[num] floatValue];
    setOtherLine = [lineWidthArray[num] floatValue];
}

-(void)scale:(UISlider*)slider{
    [self setScaleLabel:slider.value];
}

-(void)setOpactiy:(CGFloat)opacity{
    setAlpha = opacity;
    setColor = [setColor colorWithAlphaComponent:setAlpha];
}


#pragma mark -- パスの削除用アラート分岐
-(void)eraserBut:(UIButton *)but
{
    switch (but.tag) {
        case 0:
        {//スタックパスの削除
            [self clearStack];
        }
            break;
        case 1:
        {//全パスの削除
            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"AlertDelete",@"") message:NSLocalizedString(@"DeleteBezPathAll", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
            alert.tag = 1;
            [alert show];
        }
            break;
        default:
            break;
    }
}

#pragma mark - Comment View

-(void)updateCommentView{
    
}

-(void)fadeOut{
    [UIView animateWithDuration:0.5 animations:^{
        base.frame = CGRectMake(0,0,base.frame.size.width,base.frame.size.height);
        isComment = NO;
    } completion:^(BOOL finish){
    }];
}


-(void)fadeToggle{
    [UIView animateWithDuration:0.5 animations:^{
        if(isComment){
            base.frame = CGRectMake(0,0,base.frame.size.width,base.frame.size.height);
            isComment = NO;
        }else{
            base.frame = CGRectMake(0,120,base.frame.size.width,base.frame.size.height);
            isComment = YES;
        }
    } completion:^(BOOL finish){
    }];
}


-(void)menuSwitch:(BOOL)state{
    if(state){
        mv.hidden = NO;
        //amv.hidden = YES;
    }else{
        mv.hidden = YES;
        //amv.hidden = NO;
    }
}


-(void)rejectionPeer:(NSString *)peerName{
    //[self.delegate appealPeerToConsole:peerName];
}


#pragma mark - CommentTimeLine delegate

-(void)tapSpotPointWithCGpoint:(CGPoint)point{
    moveTransform = CGAffineTransformMakeTranslation(root.center.x - spotPoint.x, root.center.y - spotPoint.y);
    canvas.transform = CGAffineTransformConcat(scaleTransform,moveTransform);
}


//sketchPathの横幅を通知します
-(void)viewPosition:(BOOL)state{
}


#pragma mark - Button process
-(void)touchMenu:(UIButton *)but
{

}


-(void)setSpeed:(CGFloat)value
{

    Speed = value;

}




-(void)switchValue:(UISwitch *)sw
{
    switch (sw.tag) {
        case 0:
        {//place image

            [self setEditTab:24];
        }
            break;
        case 1:
        {//keep path
            isKeep = sw.on;
        }
            break;
        case 2:
        {//reverse mode
            isReverse = sw.on;
        }
            break;
        default:
            break;
    }
}


-(void)menuState:(NSInteger)state
{
    switch (state) {
        case 0:
        {//paint画面以外
            [stack hidden];
            [ctv fadeOut];
            [sliderView fadeOut];
        }
            break;
        case 1:
        {//paint画面


        }
            break;
        case 2:
        {
            //[animenu fadeOut];
        }
            break;
        case 3:
        {
        }
            break;
        default:
            break;
    }
}







//共通の形式に変更
-(void)touchBut:(UIButton *)but
{
    [self setEditTab:but.tag];
}



-(void)setIndexPathRow:(NSInteger)stat
{
    
}


#pragma - Set image
-(void)setImage:(NSString *)stat img:(UIImage *)img name:(NSString *)name
{

    /*
     まずDataPlistよりデータの形式を調べます。
     

     */
    /*
     新たにfileNameというアクセスメソッドを追加し、

     
     
     
     keyがあるか調べ無ければ作りあれば配列データを配置します。
     
     
     公開レイヤー private
     自分レイヤー public
     
     
     送信されてくるデータがなんのデータか判別しデータにあった処理を施します。
     
     対応するファイル形式を指定してください。
     
    */
    
    //初期化処理
    
    canvas.transform = CGAffineTransformIdentity;
    root.frame = self.bounds;
    subCanvas.transform = CGAffineTransformIdentity;
    zoom = 1.0;
    NSString *str = [NSString stringWithFormat:@"%ld%%",(long)100];
    scaleLabel.text = str;
    setBaseImage = img;
    serviceName = stat;
    root.frame = CGRectMake(0,0,img.size.width,img.size.height);
    canvas.frame = CGRectMake(0,0,img.size.width,img.size.height);
    subCanvas.frame = CGRectMake(0,0,img.size.width,img.size.height);
    dummyCanvas.frame = CGRectMake(0,0,img.size.width,img.size.height);
    originRect = canvas.bounds;
    
    
    baseSize = img.size;
    canvas.image = img;
    subCanvas.image = img;
    CALayer *sub = [CALayer layer];
    sub.frame = subCanvas.bounds;
    if(subCanvas.layer.sublayers.count != 0){
        [subCanvas.layer replaceSublayer:[subCanvas.layer.sublayers objectAtIndex:0] with:sub];
    }else{
        [subCanvas.layer addSublayer:sub];
    }
    
    [scrollView setContentSize:CGSizeMake(img.size.width + 100,img.size.height + 100)];
    
    
    CALayer *private = [CALayer layer];
    private.frame = canvas.bounds;
    CALayer *public = [CALayer layer];
    public.frame = canvas.bounds;
    
    
    if(canvas.layer.sublayers.count != 0){
        [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:0] with:public];
        [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:private];
    }else{
        [canvas.layer addSublayer:public];
        [canvas.layer addSublayer:private];
    }
    [privatePath removeAllObjects];
    [self.stacking removeAllObjects];
    [self.redoStack removeAllObjects];
    [displayPath removeAllObjects];
}

#pragma mark -- 画像パスを描画します。 stateは新規データか過去データかを分別します。新規データは正となります。

-(void)addPath:(NSNotification *)center{
    NSDictionary *path = [center userInfo][@"path"];
    BOOL hit = NO;
    if(UIDHash.length != 0){
        if(path[@"pauid"] != [NSNull null]){
            if([UIDHash isEqualToString:path[@"pauid"]]){
                hit = YES;
            }
        }
    }else{
        if(path[@"pauid"] == [NSNull null]){
            hit = YES;
        }
    }
    if(hit){
        [drawPathList addObject:[center userInfo][@"path"]];
        dispatch_async(dispatch_get_main_queue(),^{
            [self setPath:[center userInfo][@"path"]];
        });
    }
}

-(void)setPath:(NSDictionary *)data
{
    //stateは送信内容の属性です。0と1がパスデータ現在含んでいます。
    if([data[@"state"] integerValue] < 2){
        for (NSMutableDictionary *path in data[@"path"][@"path"]) {
            @autoreleasepool{
                [displayPath addObject:path];
                UIGraphicsBeginImageContextWithOptions(canvas.image.size,NO,self.drawScale);
                // 描画領域に、前回までに描画した画像を、描画します。
                [canvas.image drawAtPoint:CGPointZero];
                UIBezierPath *bez = [UIBezierPath bezierPath];
                bez.lineCapStyle = kCGLineCapRound;
                [bez appendPath:path[@"line"]];
                if([path[@"pathState"] integerValue] == 0){
                    bez.lineWidth = [path[@"lineWidth"] floatValue];
                    [path[@"color"] setStroke];
                    //現在のキャンバスサイズに大きさを合わせてあげます。
                    [bez stroke];
                }else if([path[@"pathState"] integerValue] == 1){
                    [[UIColor clearColor] setStroke];
                    [path[@"color"] setFill];
                    [bez stroke];
                    [bez fill];
                }
                // 描画した画像をcanvasにセットして、画面に表示します。
                canvas.image = UIGraphicsGetImageFromCurrentImageContext();
                // 描画を終了します。
                UIGraphicsEndImageContext();
            }
        }
        //コメントの有無を調べあるならtimeLineに追加
#pragma mark -- addTimeline
    }
}



-(void)setColor:(UIColor *)color
{
    setColor = [color colorWithAlphaComponent:setAlpha];
}


#pragma mark - 各処理の切り替え用 主要な処理はここを通します
-(void)paintSettings:(NSInteger)state{
    
    
    
    
}



#pragma mark - setEditTab
-(void)setEditTab:(NSInteger)state
{
    switch (state) {
        case 0://board
        {
            isCanvas = YES;
            zoom = 1.0;
            [self menuState:2];
            viewType = 0;
            isPaint = YES;
            root.hidden = NO;
            scrollView.hidden = YES;
            [self menuSwitch:YES];
            
        }
            break;
        case 1:
        {
            [self endScrollMode];
            //send path
            if([self.stacking count] > 0)
            {
#pragma mark -- 送信設定
                NSInteger mode = 1;

                if(setText.length > MAX_LENGTH){
                    AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
                    [DataManager alert:NSLocalizedString(@"Caution",@"") text:NSLocalizedString(@"LongString",@"")];
                    return;
                }
                if(setText.length == 0){
                    setText = @"-";
                }
                    //規定内でパスありなので送信準備を続行
                    
                // id(-) fid(fileName) pid(MCPeerID) uid(UniqueID) path(PATH) paid(Paret MCPeerID) pauid(parent UniqueID) date
                
                /*
                 uid ... パスを発行時に付けられる名前です。
                 
                 paid , pauid は親パス情報です。現在未実装ですが将来実装の為に設定だけしてあります。
                 
                 */
                
                NSString *hash = [DataManager getHash];
                NSArray *pathArray = [[NSArray alloc]initWithArray:self.stacking];
                CGPoint center = CGPointMake((lowX + maxX) / 2,(lowY + maxY) / 2);
                NSDictionary *path = [[NSDictionary alloc]initWithObjectsAndKeys:pathArray,@"path",[NSValue valueWithCGPoint:center],@"point",nil];
                
                NSDictionary *dic = @{
                                      @"fid"  : sb.selectFile,
                                      @"pid"  : [sb getUUID],
                                      @"pname": [sb getDisplay],
                                      @"uid"  : hash,
                                      @"path" : path,
                                      @"paid" : [NSNull null],
                                      @"paname" : [NSNull null],
                                      @"pauid" : UIDHash.length == 0 ? [NSNull null] : UIDHash,
                                      @"comment" : setText.length == 0 ? @"" : setText,
                                      @"state" : [NSNumber numberWithInteger:mode],
                                      @"date" : [NSNumber numberWithDouble:[sb getJulianDay:[NSDate date]]]
                                      };
            
                
                NSDictionary *formatteDic = @{
                                              @"fid"  : sb.selectFile,
                                              @"pid"  : [sb getUUID],
                                              @"pname": [sb getDisplay],
                                              @"uid"  : hash,
                                              @"path" : path,
                                              @"paname" : [NSNull null],
                                              @"paid" : [NSNull null],
                                              @"pauid" : UIDHash.length == 0 ? [NSNull null] : UIDHash,
                                              @"comment" : setText.length == 0 ? @"" : setText,
                                              @"state" : [NSNumber numberWithInteger:mode],
                                              @"date" : [NSNumber numberWithDouble:[sb getJulianDay:[NSDate date]]]
                                              };
                
                [self clearStack];
                AudioServicesPlaySystemSound(1004);
                [[NSNotificationCenter defaultCenter] postNotificationName:@"removeInsert" object:nil userInfo:nil];
                [sb setSendData:dic]; //送信
                [[NSNotificationCenter defaultCenter] postNotificationName:@"addPath" object:nil userInfo:@{@"path":formatteDic}];
                [self.stacking removeAllObjects];

                setText = @"";
            }else{
                //パスがない為
                AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
            }
        }
            break;
        case 2:
        {
            [btv fadeOut];
            [sliderView fadeOut];
            [ctv toggle];
            [self endScrollMode];
            [stack hidden];
            
        }
            break;
        case 3:
        {//undo
            [ctv fadeOut];
            [sliderView fadeOut];
            [self endScrollMode];
            [self undoBtnPressed];
        }
            break;
        case 4:
        {//redo
            [ctv fadeOut];
            [sliderView fadeOut];
            [self endScrollMode];
            [self redoBtnPressed];
        }
            break;
        case 5:
        {
#pragma mark -- menu2　の表示（削除処理）
            
            //この部分の変更を予告します
            //削除内容を全パスとスタックパスの２種類に変更します。
            [ctv fadeOut];
            [sliderView fadeOut];
            //削除用の窓を開きます。
            [stack display];
        }
            break;
        case 6:
        {//scrollmode
            [ctv fadeOut];
            [btv fadeOut];
            [sliderView toggle];
            [stack hidden];
            [self scrollMode];
        }
            break;
        case 7:
        {
            [stack hidden];
            [btv toggle];
            [self endScrollMode];
            [ctv fadeOut];
            [sliderView fadeOut];
        }
            break;
        case 8:
        {
            //描画不可 menu1~3まで非表示
            isPaint = NO;
            [self menuState:0];
            
        }
            break;
        case 9:
        {
            [ctv fadeOut];
            [sliderView fadeOut];
            [stack hidden];

        }
            break;
#pragma mark - Flip mode
        case 10:
        {//animation tool view 表示非表示
            [self endScrollMode];
            [btv fadeOut];
            //メインの表示を入れ替えます。
            scrollFlag = YES;
            isCanvas = NO;
            scrollView.zoomScale = 1.0;
            zoom = 1.0;
            [scrollView setContentOffset:CGPointMake(0,0)];
            root.hidden = YES;
            scrollView.hidden = NO;
            
            //viewType = 1;

            [self menuSwitch:NO];
            isPath = NO;
            [self menuState:3];
        }
            break;
        case 11:
        {
            //[animenu fadeIn];
            //[self resetAnimation];
        }
            break;
        case 12:
        {//
            [slv displayToggle];
            
        }
            break;
        case 13:
        {//メッセージ用ボード
            //
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:@{@"type":@99}];
        }
            break;
#pragma mark -setBut の処理　14 ~ 16
        case 14:
        { // setBut set
#pragma mark --   set path on the canvas
            //ボードにパスをセットするのでboardモードに変更します
            //[animenu fadeOut];
            //[self resetAnimation];
            //[self.animateTool setLineAnimation:0];
            //処理が終わり次第flipモードに戻します
        }
            break;
        case 15:
        {
#pragma mark --   path state change
            //[self.animateTool setLineAnimation:1];
            //[animenu fadeOut];
        }
            break;
        case 16:
        {
#pragma mark --   path delete
            //[animenu fadeOut];
        }
            break;
        case 20:
        {//animation path table
            isPath = isPath == YES ? NO : YES;
        }
            break;
#pragma mark - animation start or stop
        case 21:
        {
            //[animenu fadeOut];
            //以前のflipが全て再生し終えているなら新規の再生と判断します。
            if(isComplete){
                isComplete = NO;
            }
            
            isPath = NO;
  
        }
            break;
        case 22://animation tool window
        {
            isPath = YES;
        }
            break;
        case 23:
        { // 指定されたデータを削除します。consoleViewまで伝播させてください。
            if(!isAnimate){//アニメーション中は削除出来ない
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"AlertDelete",@"") message:NSLocalizedString(@"DeleteStackPath", @"") delegate:self cancelButtonTitle:NSLocalizedString(@"Cancel", @"") otherButtonTitles:NSLocalizedString(@"OK", @""), nil];
                alert.tag = 0;
                [alert show];
            }
        }
            break;
        case 24:
        {//背景画像をセットもしくは解除

        }
            break;
            
#pragma mark - 30- comment 及び　timeline
        case 30:
        {//　コメントタイムラインを表示

        }
            break;
        case 31:
        {// canvasの指定されたポイントに画像を移動させます。
            moveTransform = CGAffineTransformMakeTranslation(root.center.x - spotPoint.x, root.center.y - spotPoint.y);
            canvas.transform = CGAffineTransformConcat(scaleTransform,moveTransform);
        }
            break;
        case 32:
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendCommentWindow" object:nil userInfo:@{@"type":@0}];
            [self fadeToggle];
            
        }
            break;
#pragma mark - 40- pathList 及び pathDataSetting
        case 40:
        {//pathDataSettingのviewの開閉
        }
            break;
        case 53:
        {//paint mode に強制移行
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:20 inSection:1];
            [self.delegate receiveTableRow:indexPath];
        }
            break;
        case 54:
        {

        }
            break;
        case 55:
        {//end
            
            //[self hiddenBut:isTool];
            //表示変更の為キーボード処理を終了させます

        }
            break;
        case 56:
        {
            [self fadeOut];
        }
            break;
        case 99:
        {//paint mode 変遷
            [self.delegate touchBut:3];
        }
            break;
        case 100:
        {//menu slide open
            
            [slv fadeOut];
            [self fadeOut];
            [self resetAnimation];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:@{@"type":@100}];
            [self.delegate tapTab:0];
        }
            break;
        default:
            break;
    }
}
-(void)select{
}

- (void)undoBtnPressed
{
    if([privatePath count] == 0){
    }else{
        // undoスタックからパスを取り出しredoスタックに追加します。
        
        NSDictionary *undoPath = [privatePath lastObject];
        [privatePath removeLastObject];
        [self.stacking removeLastObject];
        [self.redoStack addObject:undoPath];
        self.stackingCount -= 1;
        
        // 画面にパスを描画します。
        [self setDrawAssist:privatePath];
    }
}

- (void)redoBtnPressed
{
    // redoスタックからパスを取り出しundoスタックに追加します。
    if([self.redoStack count] == 0){}else{
        NSDictionary *redoPath = [[NSDictionary alloc]initWithDictionary:self.redoStack.lastObject];
        
        
        [self.redoStack removeLastObject];
        [privatePath addObject:redoPath];
        self.stackingCount += 1;
        if(self.stackingCount >= 1){
            [self.stacking addObject:redoPath];
        }
        
        // 画面にパスを描画します。
        [self setDrawAssist:privatePath];
    }
}


- (UIView *)viewForZoomingInScrollView:(UIScrollView *)_scrollView {
    if(isCanvas){
        return canvas;
    }else{
        return subCanvas;
    }
}


-(void)reDrawPrivateCanvas{
    if(self.stacking.count > 0){
        CALayer *oldLayer = [canvas.layer.sublayers objectAtIndex:1];
        oldLayer.contentsScale = [[UIScreen mainScreen] scale];
        CALayer *newLayer = [CALayer layer];
        newLayer.frame = oldLayer.bounds;
        newLayer.contentsScale = [[UIScreen mainScreen] scale];
        for (NSDictionary *path in privatePath) {
            @autoreleasepool{
                UIGraphicsBeginImageContextWithOptions(canvas.frame.size,NO,self.drawScale);
                // 描画領域に、前回までに描画した画像を、描画します。
                
                UIImage *old = [[UIImage alloc]initWithCGImage:(CGImageRef)newLayer.contents scale:self.drawScale orientation:UIImageOrientationUp];
                [old drawAtPoint:CGPointZero];
                // 色をセットします。
                UIBezierPath *bez = [UIBezierPath bezierPath];
                bez.lineCapStyle = kCGLineCapRound;
                [bez appendPath:path[@"line"]];
                if([path[@"pathState"] integerValue] == 0){
                    bez.lineWidth = [path[@"lineWidth"] floatValue];
                    [path[@"color"] setStroke];
                    //現在のキャンバスサイズに大きさを合わせてあげます。
                    CGAffineTransform trans = CGAffineTransformMakeScale(zoom,zoom);
                    [bez applyTransform:trans];
                    [bez stroke];
                }else if([path[@"pathState"] integerValue] == 1){
                    [[UIColor clearColor] setStroke];
                    [path[@"color"] setFill];
                    //現在のキャンバスサイズに大きさを合わせてあげます。
                    CGAffineTransform trans = CGAffineTransformMakeScale(zoom,zoom);
                    [bez applyTransform:trans];
                    [bez stroke];
                    [bez fill];
                }
                // 描画した画像をcanvasにセットして、画面に表示します。
                newLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                // 描画を終了します。
                UIGraphicsEndImageContext();
            }
        }
        [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:newLayer];
    }
}


-(void)setDrawAssist:(NSArray *)ary{
    CALayer *oldLayer = [canvas.layer.sublayers objectAtIndex:1];
    oldLayer.contentsScale = [[UIScreen mainScreen] scale];
    CALayer *newLayer = [CALayer layer];
    newLayer.frame = oldLayer.bounds;
    newLayer.contentsScale = [[UIScreen mainScreen] scale];
    for (NSDictionary *path in ary) {
        @autoreleasepool{
            UIGraphicsBeginImageContextWithOptions(canvas.frame.size,NO,self.drawScale);
            // 描画領域に、前回までに描画した画像を、描画します。
            
            UIImage *old = [[UIImage alloc]initWithCGImage:(CGImageRef)newLayer.contents scale:self.drawScale orientation:UIImageOrientationUp];
            [old drawAtPoint:CGPointZero];
            // 色をセットします。
            UIBezierPath *bez = [UIBezierPath bezierPath];
            bez.lineCapStyle = kCGLineCapRound;
            [bez appendPath:path[@"line"]];
            if([path[@"pathState"] integerValue] == 0){
                bez.lineWidth = [path[@"lineWidth"] floatValue];
                [path[@"color"] setStroke];
                //現在のキャンバスサイズに大きさを合わせてあげます。
                CGAffineTransform trans = CGAffineTransformMakeScale(zoom,zoom);
                [bez applyTransform:trans];
                [bez stroke];
            }else if([path[@"pathState"] integerValue] == 1){
                [[UIColor clearColor] setStroke];
                [path[@"color"] setFill];
                //現在のキャンバスサイズに大きさを合わせてあげます。
                CGAffineTransform trans = CGAffineTransformMakeScale(zoom,zoom);
                [bez applyTransform:trans];
                [bez stroke];
                [bez fill];
            }
            // 描画した画像をcanvasにセットして、画面に表示します。
            newLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
            // 描画を終了します。
            UIGraphicsEndImageContext();
        }
    }
    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:newLayer];
}


#pragma mark - 描画処理パスの種類で分岐させて処理してください。
-(void)drawSubCanvas:(NSDictionary *)data keep:(BOOL)keep
{//実際にパスをcanvasに描画
    if([data[@"state"] integerValue] < 2){
        //[self setDrawAssist:data[@"path"][@"path"] keep:keep];
        
        CALayer *oldLayer = [subCanvas.layer.sublayers objectAtIndex:0];
        CALayer *newLayer = [CALayer layer];
        newLayer.frame = oldLayer.bounds;
        if(keep){
            newLayer.contents = [[subCanvas.layer.sublayers objectAtIndex:0] contents];
        }
        for (NSMutableDictionary *path in data[@"path"][@"path"]) {
            @autoreleasepool{
                UIGraphicsBeginImageContextWithOptions(subCanvas.frame.size,NO,self.mainDrawScale);
                // 描画領域に、前回までに描画した画像を、描画します。
                UIImage *old = [[UIImage alloc]initWithCGImage:(CGImageRef)newLayer.contents];
                [old drawAtPoint:CGPointZero];
                UIBezierPath *bez = [UIBezierPath bezierPath];
                [bez appendPath:path[@"line"]];
                if([path[@"pathState"] integerValue] == 0){
                    //[[UIColor clearColor] setStroke];
                    //現在のキャンバスサイズに大きさを合わせてあげます。
                    CGAffineTransform trans = CGAffineTransformMakeScale(zoom,zoom);
                    [bez applyTransform:trans];
                    [bez stroke];
                }else if([path[@"pathState"] integerValue] == 1){
                    [[UIColor clearColor] setStroke];
                    [path[@"color"] setFill];
                    //現在のキャンバスサイズに大きさを合わせてあげます。
                    CGAffineTransform trans = CGAffineTransformMakeScale(zoom,zoom);
                    [bez applyTransform:trans];
                    [bez stroke];
                    [bez fill];
                }
                newLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                // 描画を終了します。
                UIGraphicsEndImageContext();
            }
        }
        [subCanvas.layer replaceSublayer:[subCanvas.layer.sublayers objectAtIndex:0] with:newLayer];
    }
}

-(void)scrollMode
{
    if(scrollFlag == NO)
    {
        [self startScrollMode];
        
    }else{
        
        [self endScrollMode];
        
    }
}


#pragma mark - scroll mode start
-(void)startScrollMode
{
    oldSketchstate = sketchState;
    sketchState = 100;
    scrollFlag = YES;
    //[sliderView fadeOut];
    
}
//end scroll mode
-(void)endScrollMode
{
    sketchState = oldSketchstate;
    scrollFlag = NO;
    [sliderView fadeOut];
    [self reDrawPrivateCanvas];
}


-(void)scrollViewDidScroll:(UIScrollView *)scroll
{
    //offset = CGPointMake(ceil(scrollView.contentOffset.x), ceil(scrollView.contentOffset.y));
}
- (void)scrollViewDidZoom:(UIScrollView *)view
{
    //offset = CGPointMake(ceil(scrollView.contentOffset.x), ceil(scrollView.contentOffset.y));
}
-(void)scrollViewDidEndZooming:(UIScrollView *)scroll withView:(UIView *)view atScale:(CGFloat)scale
{
    CGFloat s = ceilf(scale * 10);
    sliderView.state.text = [NSString stringWithFormat:@"Scale %ld%%",(long)ceilf(s * 10)];
    zoom = s / 10;
    [scrollView setZoomScale:s / 10 animated:YES];
}

-(void)setScaleLabel:(CGFloat)scale{
    CGFloat s = ceilf(scale * 10);
    sliderView.state.text = [NSString stringWithFormat:@"Scale %ld%%",(long)ceilf(s * 10)];
    zoom = s / 10;
    scaleTransform = CGAffineTransformMakeScale(zoom,zoom);
    
    canvas.transform = CGAffineTransformConcat(scaleTransform,moveTransform);
}

#pragma mark -- パスの削除方法
- (void)clearAll
{
    // 保持しているパスを全部削除します。
    [privatePath removeAllObjects];
    [self.redoStack removeAllObjects];
    [self.stacking removeAllObjects];
    [displayPath removeAllObjects];
    // 画面をクリアします。
    self.stackingCount = 0;
    canvas.transform = CGAffineTransformIdentity;
    canvas.image = setBaseImage;
    
    CALayer *old = [canvas.layer.sublayers objectAtIndex:1];
    CALayer *layer0 = [CALayer layer];
    layer0.frame = CGRectMake(0,0,old.frame.size.width,old.frame.size.height);
    CALayer *layer1 = [CALayer layer];
    layer1.frame = CGRectMake(0,0,old.frame.size.width,old.frame.size.height);
    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:0] with:layer0];
    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:layer1];
    canvas.transform = CGAffineTransformConcat(scaleTransform,moveTransform);
    [stack hidden];
}

-(void)clearStack
{
    [privatePath removeAllObjects];
    [self.redoStack removeAllObjects];
    [self.stacking removeAllObjects];
    
    self.stackingCount = 0;
    canvas.transform = CGAffineTransformIdentity;
    CALayer *old = [canvas.layer.sublayers objectAtIndex:1];
    CALayer *layer = [CALayer layer];
    layer.frame = CGRectMake(0,0,old.frame.size.width,old.frame.size.height);
    layer.contentsScale = [[UIScreen mainScreen] scale];
    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:layer];
    canvas.transform = CGAffineTransformConcat(scaleTransform,moveTransform);
    [stack hidden];
}

/*
 
 描画の変更点
 
 canvasの大きさと実際の描画の範囲の誤差があります。
 表示されている部分と実際の描画される部分を合わせてください。
 これにより移動した部分にも描画が可能となります。
 
 */

#pragma mark - Touches process

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // タッチした座標を取得します。
    if(isPaint){
        [stack hidden];
        UITouch *touch = [touches anyObject];
        
        switch (sketchState) {
            case 0:
            {
                isStartPaint = YES;
                isFirstTouchPoint = YES;
                
                CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                [self setEditTab:54];
                // ボタン上の場合は処理を終了します。
                if(canvas.layer.sublayers.count == 0){
                    CALayer *private = [CALayer layer];
                    private.frame = canvas.bounds;
                    CALayer *public = [CALayer layer];
                    public.frame = canvas.bounds;
                    [canvas.layer addSublayer:public];
                    [canvas.layer addSublayer:private];
                    
                }else if(canvas.layer.sublayers.count == 1){
                    CALayer *private = [CALayer layer];
                    private.frame = canvas.bounds;
                    [canvas.layer addSublayer:private];
                }
                self.atNewLayer = [canvas.layer.sublayers objectAtIndex:1];
                self.atNewLayer.contentsScale = [[UIScreen mainScreen] scale];
                self.atDummyLayer = [canvas.layer.sublayers objectAtIndex:1];
                self.atDummyLayer.contentsScale = [[UIScreen mainScreen] scale];
                
                self.atNewImage = [[UIImage alloc]initWithCGImage:(CGImageRef)self.atNewLayer.contents scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationUp];
                self.atDummyImage = [[UIImage alloc]initWithCGImage:(CGImageRef)self.atDummyLayer.contents scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationUp];
                // パスを初期化します。retina 対応
                //NSLog(@"canvas %@ scale %f",NSStringFromCGSize(canvas.frame.size),canvas.layer.contentsScale);
                //NSLog(@"image size %@ scale %f",NSStringFromCGSize(self.atNewImage.size),self.atNewImage.scale);
                //NSLog(@"layer size %@ scale %f",NSStringFromCGSize(self.atNewLayer.frame.size),self.atNewLayer.contentsScale);
                //NSLog(@"<---------------------------------->");
                self.bezierPath = [UIBezierPath bezierPath];
                self.bezierPath.lineCapStyle = kCGLineCapRound;
                self.bezierPath.lineWidth = setOtherLine / zoom;
                [self.bezierPath moveToPoint:currentPoint];
                
                self.dummyPath = [UIBezierPath bezierPath];
                self.currentPath = [UIBezierPath bezierPath];
                self.dummyPath.lineCapStyle = kCGLineCapRound;
                [self.dummyPath moveToPoint:currentPoint];
                self.dummyPath.lineWidth = setOtherLine / zoom;
                
                
                
                //firstMovedFlg = NO;
                
                // タッチした座標を保持します。
                
                
                
                self.lastTouchPoint = currentPoint;
                moveCount = 0;
            }
                break;
            case 1:
            {

                isStartPaint = YES;
                isFirstTouchPoint = YES;
                ctr = 0;
                bufIdx = 0;
                
                CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
            
                currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                pts[0] = currentPoint;
                [self setEditTab:54];
                
                if(canvas.layer.sublayers.count == 0){
                    CALayer *private = [CALayer layer];
                    private.frame = canvas.bounds;
                    CALayer *public = [CALayer layer];
                    public.frame = canvas.bounds;
                    [canvas.layer addSublayer:public];
                    [canvas.layer addSublayer:private];
                }else if(canvas.layer.sublayers.count == 1){
                    CALayer *private = [CALayer layer];
                    private.frame = canvas.bounds;
                    [canvas.layer addSublayer:private];
                }
                self.atNewLayer = [canvas.layer.sublayers objectAtIndex:1];
                self.atNewLayer.contentsScale = [[UIScreen mainScreen] scale];
                self.atDummyLayer = [canvas.layer.sublayers objectAtIndex:1];
                self.atDummyLayer.contentsScale = [[UIScreen mainScreen] scale];
                
                self.atNewImage = [[UIImage alloc]initWithCGImage:(CGImageRef)self.atNewLayer.contents scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationUp];
                self.atDummyImage = [[UIImage alloc]initWithCGImage:(CGImageRef)self.atDummyLayer.contents scale:[[UIScreen mainScreen] scale] orientation:UIImageOrientationUp];
                
                self.bezierPath = [UIBezierPath bezierPath];
                self.dummyPath = [UIBezierPath bezierPath];
                self.currentPath = [UIBezierPath bezierPath];
                self.dummyPath.lineCapStyle = kCGLineCapRound;
                [self.dummyPath moveToPoint:currentPoint];
            
                self.bezierPath.lineWidth = 0.0;
                self.dummyPath.lineWidth = 1.0;
                self.lastTouchPoint = currentPoint;
                moveCount = 0;
                
            
                lowX = currentPoint.x;
                maxX = currentPoint.x;
                lowY = currentPoint.y;
                maxY = currentPoint.y;
                
            }
                break;
            case 100: //scroll
            {
                startLocation = [touch locationInView:self];
            }
                break;
            default:
                break;
        }
        
        
        
        
    }
}


- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isPaint){
        // タッチ開始時にパスを初期化していない場合は処理を終了します。
        switch (sketchState) {
            case 0:
            {
                if(isStartPaint){
                    // タッチ開始時にパスを初期化していない場合は処理を終了します。
                    if (self.bezierPath == nil){
                        return;
                    }
                    
                    // タッチした座標を取得します。
                    CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                    currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                    
                    // 最初の移動はスキップします。
                    if (isFirstTouchPoint){
                        isFirstTouchPoint = NO;
                        self.lastTouchPoint = currentPoint;
                        return;
                    }
                    
                    // 中点の座標を取得します。
                    CGPoint middlePoint = CGPointMake((self.lastTouchPoint.x + currentPoint.x) / 2,
                                                      (self.lastTouchPoint.y + currentPoint.y) / 2);
                    
                    // パスにポイントを追加します。
                    [self.bezierPath addQuadCurveToPoint:middlePoint controlPoint:self.lastTouchPoint];
                    [self.dummyPath addQuadCurveToPoint:middlePoint controlPoint:self.lastTouchPoint];
                    
                    
                    
                    // 線を描画します。
                    UIGraphicsBeginImageContextWithOptions(canvas.frame.size, NO,self.drawScale);
                    [self.atDummyImage drawAtPoint:CGPointZero];
                    
                    // 色をセットします。
                    [setColor  setStroke];
                    
                    // 線を引きます。
                    [self.dummyPath stroke];
                    
                    // 描画した画像をcanvasにセットして、画面に表示します。
                    self.atDummyLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:self.atDummyLayer];
                    
                    
                    // 描画を終了します。
                    UIGraphicsEndImageContext();
                    
                    // タッチした座標を保持します。
                    moveCount ++;
                    self.lastTouchPoint = currentPoint;
                }
            }
                break;
            case 1:
            {
                if(isStartPaint){
                    CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                    currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                    if(currentPoint.x < lowX){
                        lowX = currentPoint.x;
                    }else if(currentPoint.x > maxX){
                        maxX = currentPoint.x;
                    }
                    
                    if(currentPoint.y < lowY){
                        lowY = currentPoint.y;
                    }else if(currentPoint.y > maxY){
                        maxY = currentPoint.y;
                    }
                    
                    ctr++;
                    pts[ctr] = currentPoint;
                    [pointArray addObject:[NSValue valueWithCGPoint:currentPoint]];
                    if(pointArray.count > 3) [pointArray removeObjectAtIndex:0];
                    
                    if (ctr == 4)
                    {
                        pts[3] = CGPointMake((pts[2].x + pts[4].x)/2.0, (pts[2].y + pts[4].y)/2.0);
                        for ( int i = 0; i < 4; i++)
                        {
                            pointsBuffer[bufIdx + i] = pts[i];
                        }
                        bufIdx += 4;
                        
                        if (bufIdx == 0) return;
                        
                        LineSegment ls[4];
                        
                        int i = 0;
                        if (isFirstTouchPoint) // ................. (3)
                        {
                            ls[0] = (LineSegment){pointsBuffer[0], pointsBuffer[0]};
                            [self.currentPath moveToPoint:ls[0].firstPoint];
                            [self.bezierPath moveToPoint:ls[0].firstPoint];
                            isFirstTouchPoint = NO;
                        }else{
                            ls[0] = lastSegmentOfPrev;
                        }
                        float frac1 = FF/clamp(len_sq(pointsBuffer[i], pointsBuffer[i+1]), LOWER, setLineWidth); // ................. (4)
                        float frac2 = FF/clamp(len_sq(pointsBuffer[i+1], pointsBuffer[i+2]), LOWER, setLineWidth);
                        float frac3 = FF/clamp(len_sq(pointsBuffer[i+2], pointsBuffer[i+3]), LOWER, setLineWidth);
                        
                        
                        
                        ls[1] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[i], pointsBuffer[i+1]} ofRelativeLength:frac1]; // ................. (5)
                        ls[2] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[i+1], pointsBuffer[i+2]} ofRelativeLength:frac2];
                        ls[3] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[i+2], pointsBuffer[i+3]} ofRelativeLength:frac3];
                        
                        [self.currentPath moveToPoint:ls[0].firstPoint]; // ................. (6)
                        [self.currentPath addCurveToPoint:ls[3].firstPoint controlPoint1:ls[1].firstPoint controlPoint2:ls[2].firstPoint];
                        [self.currentPath addLineToPoint:ls[3].secondPoint];
                        [self.currentPath addCurveToPoint:ls[0].secondPoint controlPoint1:ls[2].secondPoint controlPoint2:ls[1].secondPoint];
                        [self.currentPath closePath];
                        
                        
                        lastSegmentOfPrev = ls[3]; // ................. (7)
                        
                        
                        [self.bezierPath appendPath:self.currentPath];
                        [self.currentPath removeAllPoints];
                        
                        
                        bufIdx = 0;
                        lastPoint = pts[4];
                        pts[0] = pts[3];
                        pts[1] = pts[4];
                        ctr = 1;
                    }
                    
                    //if(self.lastPoint.x - currentPoint.x >= -15 && self.lastPoint.x - currentPoint.x  <= 15 && self.lastPoint.y - currentPoint.y >= -15 && self.lastPoint.y - currentPoint.y  <= 15)return;
                    
                    // タッチした座標を取得します。
                    // パスにポイントを追加します。
                    
                    CGPoint middlePoint = CGPointMake((self.lastTouchPoint.x + currentPoint.x) / 2, (self.lastTouchPoint.y + currentPoint.y) / 2);
                    
                    
                    // パスにポイントを追加します。
                    [self.dummyPath addCurveToPoint:currentPoint controlPoint1:middlePoint controlPoint2:self.lastTouchPoint];
                    //[self.dummyPath addQuadCurveToPoint:middlePoint controlPoint:self.lastPoint];
                    
                    
                    // 線を描画します。
                    UIGraphicsBeginImageContextWithOptions(canvas.frame.size,NO,self.drawScale);
                    [self.atDummyImage drawAtPoint:CGPointZero];
                    // 描画領域に、前回までに描画した画像を、描画します。
                    

                    //CGAffineTransform trans = CGAffineTransformMakeScale(zoom, zoom);
                    //[self.dummyPath applyTransform:trans];
                    
                    
                    
                    // 色をセットします。
                    [setColor  setStroke];
                    
                    // 線を引きます。
                    [self.dummyPath stroke];
                    
                    // 描画した画像をcanvasにセットして、画面に表示します。
                    self.atDummyLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:self.atDummyLayer];
                    
                    if(moveCount % 30 == 0){
                        [self.dummyPath removeAllPoints];
                        [self.dummyPath moveToPoint:currentPoint];
                        self.atDummyImage = [[UIImage alloc]initWithCGImage:(CGImageRef)self.atDummyLayer.contents scale:self.drawScale orientation:UIImageOrientationUp];
                    }
                    UIGraphicsEndImageContext();
                    
                    self.lastTouchPoint = currentPoint;
                    moveCount ++;
                    
                }
            }
                break;
            case 100:
            {
                CGPoint pi = [[touches anyObject] locationInView:self];
                CGPoint point = CGpointSum(startLocation,pi);
                //root.frame = CGRectMake(root.frame.origin.x + point.x,root.frame.origin.y + point.y, root.frame.size.width, root.frame.size.height);
                moveTransform = CGAffineTransformTranslate(moveTransform,point.x,point.y);
                canvas.transform = CGAffineTransformConcat(scaleTransform,moveTransform);
                
                
                startLocation = pi;
            }
                break;
            default:
                break;
        }
        
    }
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isPaint){
        switch (sketchState) {
            case 0:
            {
                if(isStartPaint){
                    // タッチ開始時にパスを初期化していない場合は処理を終了します。
                    if (self.bezierPath == nil){
                        return;
                    }
                    
                    // タッチした座標を取得します。
                    CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                    currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                    // パスにポイントを追加します。
                    [self.bezierPath addQuadCurveToPoint:currentPoint controlPoint:self.lastTouchPoint];
                    
                    
                    // 線を描画します。
                    UIGraphicsBeginImageContextWithOptions(canvas.frame.size, NO,self.drawScale);
                    
                    // 描画領域に、前回までに描画した画像を、描画します。
                    
                    
                    [self.atNewImage drawAtPoint:CGPointZero];
                    
                    
                    // 描画した画像をcanvasにセットして、画面に表示します。
                    UIBezierPath *bez = [UIBezierPath bezierPath];
                    bez.lineCapStyle = kCGLineCapRound;
                    bez.lineWidth = setOtherLine / zoom;
                    [bez appendPath:self.bezierPath];
                    
                    
                    
                    CGAffineTransform trans = CGAffineTransformMakeScale(1 / zoom,1 / zoom);
                    [self.bezierPath applyTransform:trans];
                    [setColor setStroke];
                    [bez stroke];
                    
                    self.atNewLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:self.atNewLayer];

                    UIGraphicsEndImageContext();
                    
                    NSMutableDictionary *pi = [@{
                                                 @"color" : setColor,
                                                 @"line"  : self.bezierPath,
                                                 @"pathState" : @(sketchState),
                                                 @"lineWidth" : @(setOtherLine / zoom)
                                                 } mutableCopy];
                    [privatePath addObject:pi];
                    [self.stacking addObject:pi];
                    self.stackingCount += 1;
                    [self.redoStack removeAllObjects];
                    
                    self.bezierPath = nil;
                    moveCount = 0;
                    [pointArray removeAllObjects];
                }
                isStartPaint = NO;
                
            }
                break;
            case 1:
            {
                if(isStartPaint){
                    CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                    currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                    [pointArray addObject:[NSValue valueWithCGPoint:currentPoint]];
                    
                    if(!isFirstTouchPoint){
                        UIBezierPath *bez = [UIBezierPath bezierPath];
                        
                        int i = 0;
                        int equalPoint;
                        for(id point in pointArray){
                            if(CGPointEqualToPoint([point CGPointValue],lastPoint)){equalPoint = i;}
                            i++;
                        }
                        
                        LineSegment ls[4];
                        ls[0] = lastSegmentOfPrev;
                        switch (equalPoint) {
                            case 0:
                            {//3point
                                pts[3] = [pointArray[2] CGPointValue];
                                pts[2] = [pointArray[1] CGPointValue];
                                pts[4] = [pointArray[3] CGPointValue];
                            }
                                break;
                            case 1:
                            {//2point -- 中央にカーブを作成
                                /*
                                 0-1-2-3
                                 
                                 */
                                pts[3] = CGPointMake(([pointArray[2] CGPointValue].x + [pointArray[3] CGPointValue].x)/2.0, ([pointArray[2] CGPointValue].y + [pointArray[3] CGPointValue].y)/2.0);
                                pts[2] = CGPointMake(([pointArray[2] CGPointValue].x + [pointArray[1] CGPointValue].x)/2.0, ([pointArray[2] CGPointValue].y + [pointArray[1] CGPointValue].y)/2.0);
                                pts[4] = [pointArray[3] CGPointValue];
                            }
                                break;
                            case 2:
                            {//1point --
                                pts[2] = CGPointMake(([pointArray[2] CGPointValue].x + [pointArray[3] CGPointValue].x)/2.0, ([pointArray[2] CGPointValue].y + [pointArray[3] CGPointValue].y)/2.0);
                                pts[3] = CGPointMake((pts[2].x + [pointArray[3] CGPointValue].x)/2.0, (pts[2].y + [pointArray[3] CGPointValue].y)/2.0);
                                pts[4] = [pointArray[3] CGPointValue];
                            }
                            default:
                                break;
                        }
                        
                        for ( int i = 0; i < 4; i++)
                        {
                            pointsBuffer[i] = pts[i];
                        }
                        
                        float frac1 = FF/clamp(len_sq(pointsBuffer[0], pointsBuffer[1]), LOWER, setLineWidth); // ................. (4)
                        float frac2 = FF/clamp(len_sq(pointsBuffer[1], pointsBuffer[2]), LOWER, setLineWidth);
                        float frac3 = FF/clamp(len_sq(pointsBuffer[2], pointsBuffer[3]), LOWER, setLineWidth);
                        ls[1] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[0], pointsBuffer[1]} ofRelativeLength:frac1]; // ................. (5)
                        ls[2] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[1], pointsBuffer[2]} ofRelativeLength:frac2];
                        ls[3] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[2], pointsBuffer[3]} ofRelativeLength:frac3];
                        
                        [bez moveToPoint:ls[0].firstPoint]; // ................. (6)
                        [bez addCurveToPoint:ls[3].firstPoint controlPoint1:ls[1].firstPoint controlPoint2:ls[2].firstPoint];
                        [bez addCurveToPoint:ls[0].secondPoint controlPoint1:ls[2].secondPoint controlPoint2:ls[1].secondPoint];
                        [bez closePath];
                        
                        [self.bezierPath appendPath:bez];
                        
                    }else{
                        //座標数で何を描くかを出す
#pragma mark -- 最小描画の設定
                        UIBezierPath *bev = [UIBezierPath bezierPath];
                        switch (pointArray.count) {
                            case 1:
                            {//円形
                                [bev appendPath:[UIBezierPath bezierPathWithOvalInRect:CGRectMake([pointArray[0] CGPointValue].x,[pointArray[0] CGPointValue].y, setOtherLine / 2 ,setOtherLine / 2)]];
                            }
                                break;
                            case 2:
                            {//指定された幅の半分の幅の線
                                float frac = FF/clamp(len_sq(pts[0],[pointArray[1] CGPointValue]), LOWER, setLineWidth);
                                LineSegment catch = [self lineSegmentPerpendicularTo:(LineSegment){pts[0], [pointArray[1] CGPointValue]} ofRelativeLength:frac];
                                
                                [bev moveToPoint:pts[0]];
                                [bev addQuadCurveToPoint:[pointArray[1] CGPointValue] controlPoint:catch.firstPoint];
                                [bev addQuadCurveToPoint:pts[0] controlPoint:catch.secondPoint];
                                [bev closePath];
                                
                            }
                                break;
                            case 3:
                            {
                                float frac = FF/clamp(len_sq(pts[0],[pointArray[2] CGPointValue]), LOWER, setLineWidth);
                                LineSegment catch = [self lineSegmentPerpendicularTo:(LineSegment){pts[0], [pointArray[2] CGPointValue]} ofRelativeLength:frac];
                                
                                [bev moveToPoint:pts[0]];
                                [bev addQuadCurveToPoint:[pointArray[2] CGPointValue] controlPoint:catch.firstPoint];
                                [bev addQuadCurveToPoint:pts[0] controlPoint:catch.secondPoint];
                                [bev closePath];
                                
                            }
                                break;
                            case 4:
                            {
                                float frac1 = FF/clamp(len_sq(pts[0], [pointArray[1] CGPointValue]), LOWER, setLineWidth); // ................. (4)
                                float frac2 = FF/clamp(len_sq([pointArray[1] CGPointValue],[pointArray[2] CGPointValue]), LOWER, setLineWidth);
                                float frac3 = FF/clamp(len_sq([pointArray[2] CGPointValue], [pointArray[3] CGPointValue]), LOWER, setLineWidth);
                                
                                LineSegment ls1 = [self lineSegmentPerpendicularTo:(LineSegment){pts[0], [pointArray[1] CGPointValue]} ofRelativeLength:frac1]; // ................. (5)
                                LineSegment ls2 = [self lineSegmentPerpendicularTo:(LineSegment){[pointArray[1] CGPointValue], [pointArray[2] CGPointValue]} ofRelativeLength:frac2];
                                LineSegment ls3 = [self lineSegmentPerpendicularTo:(LineSegment){[pointArray[2] CGPointValue], [pointArray[3] CGPointValue]} ofRelativeLength:frac3];
                                
                                
                                [bev moveToPoint:pts[0]]; // ................. (6)
                                [bev addCurveToPoint:ls3.firstPoint controlPoint1:ls1.firstPoint controlPoint2:ls2.firstPoint];
                                [bev addCurveToPoint:pts[0] controlPoint1:ls2.secondPoint controlPoint2:ls1.secondPoint];
                                [bev closePath];
                                
                            }
                                break;
                            default:
                                break;
                        }
                        [self.bezierPath appendPath:bev];
                    }
                    
                    [self.bezierPath closePath];
                    
                    // 線を描画します。
                    UIGraphicsBeginImageContextWithOptions(canvas.frame.size,NO,self.drawScale);//1.0
                    // 描画領域に、前回までに描画した画像を、描画します。
                    
                    [self.atNewImage drawAtPoint:CGPointZero];
                    
                    
                    
                    [[UIColor clearColor] setStroke];
                    [setColor setFill];
                    
                    //scalingしたパスと1.0サイズのパスを用意します。
                    UIBezierPath *bez = [UIBezierPath bezierPath];
                    
                    [bez appendPath:self.bezierPath];
                    
                    
                    
                    CGAffineTransform trans = CGAffineTransformMakeScale(1 / zoom,1 / zoom);
                    [self.bezierPath applyTransform:trans];
                    
                    [bez stroke];
                    [bez fill];
                    
                    
                    // 描画した画像をcanvasにセットして、画面に表示します。
                    self.atNewLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:self.atNewLayer];
                    
                    
                    // 描画を終了します。
                    UIGraphicsEndImageContext();
                    
                    NSMutableDictionary *pi = [@{
                                                 @"color" : setColor,
                                                 @"line"  : self.bezierPath,
                                                 @"pathState" : @(sketchState),
                                                 @"lineWidth" : @(0)
                                                 } mutableCopy];
                    [privatePath addObject:pi];
                    [self.stacking addObject:pi];
                    self.stackingCount += 1;
                    [self.redoStack removeAllObjects];
                    
                    self.bezierPath = nil;
                    moveCount = 0;
                    [pointArray removeAllObjects];
                    
                    
                    
                }
                isStartPaint = NO;
            }
                break;
            default:
                break;
        }
        
    }
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    if(isPaint){
        switch (sketchState) {
            case 0:
            {
                if(isStartPaint){
                    // タッチ開始時にパスを初期化していない場合は処理を終了します。
                    if (self.bezierPath == nil){
                        return;
                    }
                    
                    // タッチした座標を取得します。
                    CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                    currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                    // パスにポイントを追加します。
                    [self.bezierPath addQuadCurveToPoint:currentPoint controlPoint:self.lastTouchPoint];
                    
                    // 線を描画します。
                    UIGraphicsBeginImageContextWithOptions(canvas.frame.size, NO,self.drawScale);
                    
                    // 描画領域に、前回までに描画した画像を、描画します。
                    [self.atNewImage drawAtPoint:CGPointZero];
                    
                    // 色をセットします。
                    
                    // 描画した画像をcanvasにセットして、画面に表示します。
                    UIBezierPath *bez = [UIBezierPath bezierPath];
                    bez.lineCapStyle = kCGLineCapRound;
                    bez.lineWidth = setOtherLine / zoom;
                    [bez appendPath:self.bezierPath];
                    
                    
                    
                    CGAffineTransform trans = CGAffineTransformMakeScale(1 / zoom,1 / zoom);
                    [self.bezierPath applyTransform:trans];
                    [setColor setStroke];
                    [bez stroke];
                    self.atNewLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:self.atNewLayer];
                    
                    // 描画を終了します。
                    UIGraphicsEndImageContext();
                    
                    NSMutableDictionary *pi = [@{
                                                 @"color" : setColor,
                                                 @"line"  : self.bezierPath,
                                                 @"pathState" : @(sketchState),
                                                 @"lineWidth" : @(setOtherLine / zoom)
                                                 } mutableCopy];
                    [privatePath addObject:pi];
                    [self.stacking addObject:pi];
                    self.stackingCount += 1;
                    [self.redoStack removeAllObjects];
                    
                    self.bezierPath = nil;
                    moveCount = 0;
                    [pointArray removeAllObjects];
                }
                isStartPaint = NO;
            }
                break;
            case 1:
            {
                if(isStartPaint){
                    CGPoint currentPoint = [[touches anyObject] locationInView:canvas];
                    currentPoint = CGPointMake(currentPoint.x * zoom, currentPoint.y * zoom);
                    [pointArray addObject:[NSValue valueWithCGPoint:currentPoint]];
                    
                    if(!isFirstTouchPoint){
                        UIBezierPath *bez = [UIBezierPath bezierPath];
                        
                        int i = 0;
                        int equalPoint;
                        for(id point in pointArray){
                            if(CGPointEqualToPoint([point CGPointValue],lastPoint)){equalPoint = i;}
                            i++;
                        }
                        
                        LineSegment ls[4];
                        ls[0] = lastSegmentOfPrev;
                        switch (equalPoint) {
                            case 0:
                            {//3point
                                pts[3] = [pointArray[2] CGPointValue];
                                pts[2] = [pointArray[1] CGPointValue];
                                pts[4] = [pointArray[3] CGPointValue];
                            }
                                break;
                            case 1:
                            {//2point -- 中央にカーブを作成
                                /*
                                 0-1-2-3
                                 
                                 */
                                pts[3] = CGPointMake(([pointArray[2] CGPointValue].x + [pointArray[3] CGPointValue].x)/2.0, ([pointArray[2] CGPointValue].y + [pointArray[3] CGPointValue].y)/2.0);
                                pts[2] = CGPointMake(([pointArray[2] CGPointValue].x + [pointArray[1] CGPointValue].x)/2.0, ([pointArray[2] CGPointValue].y + [pointArray[1] CGPointValue].y)/2.0);
                                pts[4] = [pointArray[3] CGPointValue];
                            }
                                break;
                            case 2:
                            {//1point --
                                pts[2] = CGPointMake(([pointArray[2] CGPointValue].x + [pointArray[3] CGPointValue].x)/2.0, ([pointArray[2] CGPointValue].y + [pointArray[3] CGPointValue].y)/2.0);
                                pts[3] = CGPointMake((pts[2].x + [pointArray[3] CGPointValue].x)/2.0, (pts[2].y + [pointArray[3] CGPointValue].y)/2.0);
                                pts[4] = [pointArray[3] CGPointValue];
                            }
                            default:
                                break;
                        }
                        
                        for ( int i = 0; i < 4; i++)
                        {
                            pointsBuffer[i] = pts[i];
                        }
                        
                        float frac1 = FF/clamp(len_sq(pointsBuffer[0], pointsBuffer[1]), LOWER, setLineWidth); // ................. (4)
                        float frac2 = FF/clamp(len_sq(pointsBuffer[1], pointsBuffer[2]), LOWER, setLineWidth);
                        float frac3 = FF/clamp(len_sq(pointsBuffer[2], pointsBuffer[3]), LOWER, setLineWidth);
                        ls[1] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[0], pointsBuffer[1]} ofRelativeLength:frac1]; // ................. (5)
                        ls[2] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[1], pointsBuffer[2]} ofRelativeLength:frac2];
                        ls[3] = [self lineSegmentPerpendicularTo:(LineSegment){pointsBuffer[2], pointsBuffer[3]} ofRelativeLength:frac3];
                        
                        [bez moveToPoint:ls[0].firstPoint]; // ................. (6)
                        [bez addCurveToPoint:ls[3].firstPoint controlPoint1:ls[1].firstPoint controlPoint2:ls[2].firstPoint];
                        [bez addCurveToPoint:ls[0].secondPoint controlPoint1:ls[2].secondPoint controlPoint2:ls[1].secondPoint];
                        [bez closePath];
                        
                        [self.bezierPath appendPath:bez];
                        
                    }else{
                        //座標数で何を描くかを出す
#pragma mark -- 最小描画の設定
                        UIBezierPath *bev = [UIBezierPath bezierPath];
                        switch (pointArray.count) {
                            case 1:
                            {//円形
                                [bev appendPath:[UIBezierPath bezierPathWithOvalInRect:CGRectMake([pointArray[0] CGPointValue].x,[pointArray[0] CGPointValue].y, setOtherLine / 2 ,setOtherLine / 2)]];
                            }
                                break;
                            case 2:
                            {//指定された幅の半分の幅の線
                                float frac = FF/clamp(len_sq(pts[0],[pointArray[1] CGPointValue]), LOWER, setLineWidth);
                                LineSegment catch = [self lineSegmentPerpendicularTo:(LineSegment){pts[0], [pointArray[1] CGPointValue]} ofRelativeLength:frac];
                                
                                [bev moveToPoint:pts[0]];
                                [bev addQuadCurveToPoint:[pointArray[1] CGPointValue] controlPoint:catch.firstPoint];
                                [bev addQuadCurveToPoint:pts[0] controlPoint:catch.secondPoint];
                                [bev closePath];
                                
                            }
                                break;
                            case 3:
                            {
                                float frac = FF/clamp(len_sq(pts[0],[pointArray[2] CGPointValue]), LOWER, setLineWidth);
                                LineSegment catch = [self lineSegmentPerpendicularTo:(LineSegment){pts[0], [pointArray[2] CGPointValue]} ofRelativeLength:frac];
                                
                                [bev moveToPoint:pts[0]];
                                [bev addQuadCurveToPoint:[pointArray[2] CGPointValue] controlPoint:catch.firstPoint];
                                [bev addQuadCurveToPoint:pts[0] controlPoint:catch.secondPoint];
                                [bev closePath];
                                
                            }
                                break;
                            case 4:
                            {
                                float frac1 = FF/clamp(len_sq(pts[0], [pointArray[1] CGPointValue]), LOWER, setLineWidth); // ................. (4)
                                float frac2 = FF/clamp(len_sq([pointArray[1] CGPointValue],[pointArray[2] CGPointValue]), LOWER, setLineWidth);
                                float frac3 = FF/clamp(len_sq([pointArray[2] CGPointValue], [pointArray[3] CGPointValue]), LOWER, setLineWidth);
                                
                                LineSegment ls1 = [self lineSegmentPerpendicularTo:(LineSegment){pts[0], [pointArray[1] CGPointValue]} ofRelativeLength:frac1]; // ................. (5)
                                LineSegment ls2 = [self lineSegmentPerpendicularTo:(LineSegment){[pointArray[1] CGPointValue], [pointArray[2] CGPointValue]} ofRelativeLength:frac2];
                                LineSegment ls3 = [self lineSegmentPerpendicularTo:(LineSegment){[pointArray[2] CGPointValue], [pointArray[3] CGPointValue]} ofRelativeLength:frac3];
                                
                                
                                [bev moveToPoint:pts[0]]; // ................. (6)
                                [bev addCurveToPoint:ls3.firstPoint controlPoint1:ls1.firstPoint controlPoint2:ls2.firstPoint];
                                [bev addCurveToPoint:pts[0] controlPoint1:ls2.secondPoint controlPoint2:ls1.secondPoint];
                                [bev closePath];
                                
                            }
                                break;
                            default:
                                break;
                        }
                        [self.bezierPath appendPath:bev];
                    }
                    
                    [self.bezierPath closePath];
                    
                    // 線を描画します。
                    UIGraphicsBeginImageContextWithOptions(canvas.frame.size,NO,self.drawScale);//1.0
                    // 描画領域に、前回までに描画した画像を、描画します。
                    CGContextRef context = UIGraphicsGetCurrentContext();
                    CGContextSetShouldAntialias(context, YES);
                    
                    [self.atNewImage drawAtPoint:CGPointZero];
                    
                    
                    //scalingしたパスと1.0サイズのパスを用意します。
                    UIBezierPath *bez = [UIBezierPath bezierPath];
                    [bez appendPath:self.bezierPath];
                    
                    
                    
                    CGAffineTransform trans = CGAffineTransformMakeScale(1 / zoom,1 / zoom);
                    [self.bezierPath applyTransform:trans];
                    [setColor setStroke];
                    [bez stroke];
                    
                    
                    // 描画した画像をcanvasにセットして、画面に表示します。
                    self.atNewLayer.contents = (id)[UIGraphicsGetImageFromCurrentImageContext() CGImage];
                    [canvas.layer replaceSublayer:[canvas.layer.sublayers objectAtIndex:1] with:self.atNewLayer];
                    
                    
                    // 描画を終了します。
                    UIGraphicsEndImageContext();
                    
                    NSMutableDictionary *pi = [@{
                                                @"color" : setColor,
                                                @"line"  : self.bezierPath,
                                                @"pathState" : @(sketchState),
                                                @"lineWidth" : @(0)
                                                } mutableCopy];
                    [privatePath addObject:pi];
                    [self.stacking addObject:pi];
                    self.stackingCount += 1;
                    [self.redoStack removeAllObjects];
                    
                    self.bezierPath = nil;
                    moveCount = 0;
                    [pointArray removeAllObjects];
                    
                    
                    
                }
                isStartPaint = NO;
            }
                break;
            default:
                break;
        }
        
    }
}

-(CALayer *)catchLayerWithName:(NSArray *)layers name:(NSString *)name
{
    CALayer *layer = nil;
    for(layer in layers) if([layer.name isEqualToString:name])break;
    return layer;
    
}

#pragma mark - 警告処理 
#pragma mark -- UIAlertControllerにはまだ移行していません。
-(void)alertView:(UIAlertView*)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    switch (alertView.tag) {
        case 0:
        {
            switch (buttonIndex) {
                case 0:
                    
                    break;
                case 1:
                    break;
            }

        }
            break;
        case 1:
        {//全パスの削除処理
            switch (buttonIndex) {
                case 0:
                    
                    break;
                case 1:
                    [self clearAll];
                    break;
            }
        }
        default:
            break;
    }
}



////
#pragma mark path  console settings パスの処理を決定します。
//仮描画
-(void)setExhibitionPath:(NSInteger)pathIndex
{
    [self drawSubCanvas:pathList[pathIndex] keep:NO];
}

////////
#pragma mark - animetion View mode
//publicに保存されたパス情報を表示していきます。
//アニメーション化する配列を生成
-(void)animationView:(NSArray *)ary
{
#pragma mark - 現在動いているアニメーションを止める
    if(isAnimate){
        [scheduleTimer invalidate];

        isAnimate = NO;
    }else{
        isAnimate = YES;
        NSInteger count = ary.count;
        NSInteger max = pathList.count;
        if(count == 0){//全てを再生

            [self animationStart:pathList];
        }else if (count == 1){//指定された場所から再生
            NSInteger start = (long)((NSIndexPath *)ary[0]).row;
            
            NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(start,max - start)];
            // 配列から複数の要素を取得
            [self animationStart:[pathList objectsAtIndexes:indexes]];
        }else if(count == 2){//指定された区間だけ再生
            NSInteger a = (long)((NSIndexPath *)ary[0]).row;
            NSInteger b = (long)((NSIndexPath *)ary[1]).row;
            NSInteger start = a < b ? a : b;
            NSInteger end = a < b ? b : a;
            
            NSIndexSet *indexes = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(start,end - start + 1)];
            // 配列から複数の要素を取得
            [self animationStart:[pathList objectsAtIndexes:indexes]];
            
        }else{//指定されたcellだけ再生
            NSMutableArray *nary = [[NSMutableArray alloc]init];
            for(NSIndexPath *i in ary){
                [nary addObject:pathList[i.row]];
            }
            
            [self animationStart:nary];
            
        }
    }
}

//アニメーション化する配列と各種設定を添えて開始する
-(void)animationStart:(NSArray *)array
{
    if(isReverse)
    {//降順
        currentCount = array.count - 1;
    }else{
            //昇順
        currentCount = 0;
    }
    if(animetionCount == 0){//初期化するかどうか
        CALayer *sub = [CALayer layer];
        sub.frame = subCanvas.bounds;
        [subCanvas.layer replaceSublayer:[subCanvas.layer.sublayers objectAtIndex:0] with:sub];
    }
    
    //set invocation
    SEL selector = @selector(progressionController:keep:reverse:);
    NSMethodSignature *signature = [self methodSignatureForSelector:selector];
    // get method signeture
    // make NSInvocation instance
    if(signature){
      
        NSInvocation* invocation = [ NSInvocation invocationWithMethodSignature: signature ];
        [invocation setSelector:selector];
        [invocation setTarget:self];
        [invocation setArgument:&array atIndex:2];
        [invocation setArgument:&isKeep atIndex:3];
        [invocation setArgument:&isReverse atIndex:4];
        
        
        scheduleTimer = [NSTimer scheduledTimerWithTimeInterval:Speed invocation:invocation repeats:YES];
    }

}

-(void)resetAnimation{
    [scheduleTimer invalidate];
    //[self.animateTool endAnimate];
    animetionCount = 0;
    isAnimate = NO;
    
    //強制でアニメーションが解除されているため終了したと判断します。
    isComplete = YES;
}

//実際の１コマとなる描画
-(void)progressionController:(NSArray *)list keep:(BOOL)keep reverse:(BOOL)reverse
{//パスを描画する指示
    //指定のカウントになるとアニメーション終了
    if(list.count == animetionCount)
    {
        [scheduleTimer invalidate];

        isComplete = YES;
        animetionCount = 0;
        isAnimate = NO;
    }else{
        
        [self drawSubCanvas:list[currentCount] keep:keep];
        if(reverse){
            currentCount--;
        }else{
            currentCount++;
        }
        animetionCount++;
    }
}


-(LineSegment) lineSegmentPerpendicularTo: (LineSegment)pp ofRelativeLength:(float)fraction
{
    CGFloat x0 = pp.firstPoint.x, y0 = pp.firstPoint.y, x1 = pp.secondPoint.x, y1 = pp.secondPoint.y;
    
    CGFloat dx, dy;
    dx = x1 - x0;
    dy = y1 - y0;
    
    CGFloat xa, ya, xb, yb;
    xa = x1 + fraction/2 * dy;
    ya = y1 - fraction/2 * dx;
    xb = x1 - fraction/2 * dy;
    yb = y1 + fraction/2 * dx;
    
    return (LineSegment){ (CGPoint){xa, ya}, (CGPoint){xb, yb} };
}





float len_sq(CGPoint p1, CGPoint p2)
{
    float dx = p2.x - p1.x;
    float dy = p2.y - p1.y;
    return dx * dx + dy * dy;
}

float clamp(float value, float lower, float higher)
{
    if (value < lower) return lower;
    if (value > higher) return higher;
    return value;
}

-(void)tapCell
{
    [self resetAnimation];
}

#pragma mark - Notification center

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //board fadeIn
        {
            [self fadeOut];
        }
            break;
        case 1:
        {
            sendMode = [[center userInfo][@"mode"] integerValue];
            if(sendMode == 1){
                //パスとともに送信なので一旦setTextに保存
                [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:@{@"type":@100}];
                setText = [center userInfo][@"text"];
                if([setText length] > 0){
                    //文字がセットされている
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"setInsert" object:nil userInfo:nil];
                }else{
                    //新しい要素として親が設定されている場合のみ文字がセットされていなくても可能となります。
                    //文字がセットされていないもしくは削除された場合
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"removeInsert" object:nil userInfo:nil];
                }
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"removeInsert" object:nil userInfo:nil];
                NSString *str = [center userInfo][@"text"];
                if(str.length > MAX_LENGTH || str.length == 0)return;
                NSString *hash = [DataManager getHash];
                NSDate *date = [NSDate date];
                NSDictionary *dic = @{
                                      @"fid"  : sb.selectFile,
                                      @"pid"  : [sb getUUID],
                                      @"pname": [sb getDisplay],
                                      @"uid"  : hash,
                                      @"path" : @"",
                                      @"paname" : [NSNull null],
                                      @"paid" : [NSNull null],
                                      @"pauid" : [center userInfo][@"uid"] != [NSNull null] ? [center userInfo][@"uid"] : [NSNull null],
                                      @"comment" : str.length == 0 ? @"" : str,
                                      @"state" : [NSNumber numberWithInteger:sendMode],
                                      @"date" : [NSNumber numberWithDouble:[sb getJulianDay:date]]
                                      };
                
                
                
                //送信内容と受信内容の差異を解消します。（NSDateがユリアスディに対応していないためです。)
                NSDictionary *formatteDic = @{
                                      @"fid"  : sb.selectFile,
                                      @"pid"  : [sb getUUID],
                                      @"pname": [sb getDisplay],
                                      @"uid"  : hash,
                                      @"path" : @"",
                                      @"paname" : [NSNull null],
                                      @"paid" : [NSNull null],
                                      @"pauid" : [center userInfo][@"uid"] != [NSNull null] ? [center userInfo][@"uid"] : [NSNull null],
                                      @"comment" : str.length == 0 ? @"" : str,
                                      @"state" : [NSNumber numberWithInteger:sendMode],
                                      @"date" : [NSNumber numberWithDouble:[sb getJulianDay:date]]
                                      };

                
                [self setPath:dic];
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                    AudioServicesPlaySystemSound(1004);
                    [sb setSendData:dic]; //送信
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"addPath" object:nil userInfo:@{@"path":formatteDic}];
                });
            }
            
        }
            break;
        case 2://change line width
        {
            [self changeBrushLineWidth:[[center userInfo][@"state"] integerValue]];
        }
            break;
        case 3://change brush paint
        {
            sketchState = [[center userInfo][@"state"] integerValue];
            oldSketchstate = sketchState;
        }
            break;
        case 4: // set color
        {
            [self setColor:[center userInfo][@"state"]];
        }
            break;
        case 5: // set opacity
        {
            [self setOpactiy:[[center userInfo][@"state"] floatValue]];
        }
            break;
        case 10:
        {
            if([center userInfo][@"item"] != [NSNull null]){
                UIDHash = [center userInfo][@"item"][@"uid"];
            }else{
                UIDHash = @"";
            }
        }
            break;
        case 20:
        {
            
        }
            break;
        default:
            break;
    }
}

-(void)catchCanvasEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0:
        {//表示用uid
            drawPathList = [[sb getListManager:[center userInfo][@"uid"] root:YES] mutableCopy];
            for(NSDictionary *dic in drawPathList){
                dispatch_async(dispatch_get_main_queue(),^{
                    [self setPath:dic];
                });
            }
        }
            break;
        default:
            break;
    }
}

-(void)setItem:(NSNotification *)center{
    [self setImage:[sb getService] img:[center userInfo][@"image"] name:[center userInfo][@"name"]];
}



@end
















