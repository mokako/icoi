//
//  SketchView.h
//  ICOI
//
//  Created by mokako on 2014/03/10.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AudioToolbox/AudioServices.h>
#import "ICOI.h"
#import "ServiceManager.h"

//Path controller view
#import "SketchListView.h"
#import "SketchListTabView.h"

//sketch console
#import "RemoveStackResponse.h"
#import "ToolView.h"
#import "BrushToolView.h"
#import "ColorToolView.h"
#import "ScaleSliderView.h"
//message board
#import "MessageBoard.h"
#import "PickUp.h"
#import "PickUpTab.h"
#import "MessageTagView.h"

//Animation console
//#import "AnimationMenuView.h"
//#import "AnimationToolView.h"



#import "UIColor+Hex.h"


@import MultipeerConnectivity;

@protocol SketchViewDelegate<NSObject>
-(void)touchBut:(NSInteger)sender;
-(void)receiveTableRow:(NSIndexPath *)indexPath;
-(void)tapTab:(NSInteger)state;
@end

@interface SketchView : UIView<UITextFieldDelegate>
{
    
    ServiceManager *sb;
    
    
    UIView *root;
    UIView *base;
    BOOL firstFlag;
    UIColor *setColor;
    CGFloat setAlpha;
    
    UIImageView *canvas;
    UIImageView *subCanvas;
    UIImageView *dummyCanvas;
    UIImage *setBaseImage;
    UIImage *emptyImage;
    BOOL scrollFlag;
    UIScrollView *scrollView;
    CGPoint offset;
    CGFloat zoom;
    CGSize baseSize;
    CGRect originRect;
    NSInteger oldTag;
    CGFloat lowX,lowY,maxX,maxY;//中心点を算出するためのポイントを算出
    
    NSString *serviceName;
    
    
    //sketch tool button
    UILabel *scaleLabel;
    //
    
    NSMutableArray *drawPathList;//stack 用のパス配列です。
    
    NSMutableArray *pathList;
    NSMutableArray *privatePath;
    NSMutableArray *displayPath; //現在表示されているパス情報
    
    
    NSInteger animetionCount;
    NSInteger currentCount;
    
    BOOL isPaint;
    BOOL isAnimate;
    BOOL isComplete;
    BOOL isAnimTool;
    BOOL isPath;
    BOOL isStartPaint;
    BOOL iPhone;
    BOOL isOrientation;
    BOOL isCanvas;
    BOOL isComment;
    NSArray *widthArray;
    NSArray *lineWidthArray;
    NSArray *flipSpeed;
    
    NSInteger CAPACITY;
    CGFloat FF;
    CGFloat LOWER;
    CGFloat setLineWidth;
    CGFloat setOtherLine;
    
    BOOL isPen;
    NSTimer *scheduleTimer;
    
    NSInteger setPathState;
    
    
    NSString *UIDHash;
    
    /*
     comment
     */
    
    MessageBoard *mb;
    PickUp *pu;
    PickUpTab *pt;
    
    CGPoint original;
    BOOL isKeybord;
    BOOL openKeyboard;
    
    UIView *addCommentView;
    UILabel *commentStat;
    NSString *setText;
    NSInteger oldLength;
    BOOL prov;
    
    
    CGPoint spotPoint;
    CAGradientLayer *gradient;
    NSInteger viewType;
    NSInteger sendMode;
    //Sound
    SystemSoundID removeSound;
    SystemSoundID sendSound;

    UIView *mv;
    NSArray *slh;
    NSArray *mbh;
    
    ToolView *toolv;
    BrushToolView *btv;
    ColorToolView *ctv;
    ScaleSliderView *sliderView;
    
    SketchListView *slv;
    //NSArray *bh;
    //NSArray *sh;
    
    //UIView *amv;
    //AnimationToolView *atv;
    
    CGAffineTransform scaleTransform;
    CGAffineTransform moveTransform;
    
    NSInteger sketchState;
    NSInteger oldSketchstate;
    
    //scale
    CGPoint startLocation;
    
}
@property (nonatomic, weak) id <SketchViewDelegate> delegate;
@property (nonatomic)CGFloat drawScale;
@property (nonatomic)CGFloat mainDrawScale;


-(void)setServiceManager:(ServiceManager *)servicemanager;

//-(void)progressionController:(NSTimer *)timer list:(NSArray *)list;
//-(void)setData:(NSString *)stat data:(NSData *)data exte:(NSString *)exte;
-(void)setPath:(NSDictionary *)data;
-(void)setEditTab:(NSInteger)state;
-(void)review;


@end
