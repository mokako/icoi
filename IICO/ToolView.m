//
//  ToolView.m
//  ICOI
//
//  Created by mokako on 2015/10/04.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "ToolView.h"

@implementation ToolView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clipsToBounds = YES;
        view = [[UIView alloc]initWithFrame:CGRectMake(-20,0,40,40)];
        view.backgroundColor = [UIColor clearColor];
        view.hidden = YES;
        [self insertSubview:view atIndex:0];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = view.bounds;
        [view addSubview:visualEffectView];
        
        
        UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,40,40)];
        but.titleEdgeInsets = UIEdgeInsetsMake(0,20, 0, 0);
        but.titleLabel.font = [UIFont fontWithName:@"select" size:20];
        [but setTitle:@"w" forState:UIControlStateNormal];
        but.titleLabel.textAlignment = NSTextAlignmentCenter;
        [but setTitleColor:RGBA(103,113,118,1.0) forState:UIControlStateNormal];
        [but addTarget:self action:@selector(fadeIn) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:but];
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addBadge) name:@"addLogBadge" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeBadge) name:@"removeLogBadge" object:nil];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setComment) name:@"setInsert" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeComment) name:@"removeInsert" object:nil];

    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];

    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = self.base.bounds;
    [self.base insertSubview:visualEffectView atIndex:0];
    self.base.layer.borderColor = [UIColor colorWithHex:@"cccccc"].CGColor;
    self.base.layer.borderWidth = 0.5;
    
    [self.chat addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.list addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    self.redo.transform = CGAffineTransformMakeScale(-1,1);
    
    
    [self.undo addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.redo addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.del addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.pinch addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.pallete addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.brush addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.share addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.menu addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.fade addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    
    label = [[UIFlexibleLabel alloc]init];
    label.font = [UIFont fontWithName:@"AvenirNext-Medium" size:10.0];
    label.center = CGPointMake(30,10);
    label.textColor = RGBA(255,255,255,1.0);
    label.backgroundColor = RGBA(59,192,195,1.0);
    label.textAlignment = NSTextAlignmentCenter;
    label.trigger = @"0";
    label.radius = 0.5;
    label.padding = 1;
    label.animatable = YES;
    [label popupSoundType:type1];
    [label setValue:@"0"];
    [self.menu addSubview:label];
    
    
    
    insert = [[UIFlexibleLabel alloc]init];
    insert.font = [UIFont fontWithName:@"AvenirNext-Medium" size:10.0];
    insert.center = CGPointMake(60,25);
    insert.textColor = RGBA(255,255,255,1.0);
    insert.backgroundColor = RGBA(46,151,216,1.0);
    insert.textAlignment = NSTextAlignmentCenter;
    insert.trigger = @"0";
    insert.radius = 0.5;
    insert.padding = 1;
    insert.animatable = YES;
    [insert popupSoundType:type2];
    [insert setValue:@"0"];
    [self.share addSubview:insert];
    
    
    
    // 初期化
    //self.label.text = NSStringFromClass([self class]);
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

- (IBAction)buttonTapped:(id)sender
{
    //NSLog(@"tapped.");
}


-(void)fadeOut{
    [self childViewFadeOut];
    UIView *superView = [self superview];
    [UIView animateWithDuration:0.25 animations:^{
        self.base.frame = CGRectMake(-self.base.frame.size.width,self.base.frame.origin.y,self.base.frame.size.width,self.base.frame.size.height);
    } completion:^(BOOL comp){
        superView.frame = CGRectMake(superView.frame.origin.x,superView.frame.origin.y + 40,40,40);
        view.hidden = NO;
    }];
}

-(void)fadeIn{
    view.hidden = YES;
    UIView *superView = [self superview];
    superView.frame = CGRectMake(superView.frame.origin.x,superView.frame.origin.y - 40,self.base.frame.size.width,self.base.frame.size.height);
    [UIView animateWithDuration:0.25 animations:^{
        self.base.frame = CGRectMake(0,self.base.frame.origin.y,self.base.frame.size.width,self.base.frame.size.height);
    }];
}

-(void)setBut:(UIButton *)but{
    [self.delegate setEditTab:but.tag];
    
}

#pragma bazge
-(void)addBadge{
    count++;
    [label setValue:[NSString stringWithFormat:@"%ld",(long)count]];
}

-(void)removeBadge{
    count = 0;
    [label setValue:[NSString stringWithFormat:@"%ld",(long)count]];
}

-(void)setComment{
    [insert setValue:@"+"];
}
-(void)removeComment{
    [insert setValue:@"0"];
}

-(void)childViewFadeOut{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"fadeOut" object:nil userInfo:nil];
}
@end
