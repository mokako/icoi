//
//  LayerView.h
//  ICOI
//
//  Created by mokako on 2014/02/11.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ServiceManager.h"
#import "CustomCell.h"
#import "PeerController.h"
#import "PopupView.h"



@interface LayerView : UIView<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    
    UICollectionView *collectview;
    UICollectionViewFlowLayout *layout;
    
    UIView *ma;
    NSMutableArray *layerArray;
    NSMutableDictionary *imageDic;
    NSMutableArray *layerImageArray;
    NSIndexPath *selectedIndexPath;
    UILabel *label;
    NSArray *accessoryType;
    NSArray *accessoryColor;

    NSMutableDictionary *select;
    NSString *selectedHash;
    NSString *defaultHash;
    
    CGFloat layoutWidth;
    CGFloat layoutHeight;
    CGFloat scale;
    UIColor *fontColor;
    
    PopupView *popup;
    //popup
    UILabel *ptitle;
    UILabel *fileName;
    UILabel *fileID;
    UILabel *donorName;
    UILabel *donorID;
    UILabel *donorDate;
    UIButton *rejectButton;
    NSString *rejectionHash;
    NSInteger imageState;
    NSString *myUuid;
    //
    ServiceManager *sb;
    
    PeerController *pcon;
    NSOperationQueue* queue;
    
    UIVisualEffectView *visualEffectView;
    
}

-(void)setServiceManager:(ServiceManager *)servicemanager;

-(void)setPeerUUID:(NSString *)uuid;
-(void)reloadTable:(NSDictionary *)list;
-(void)setLayerImage:(NSArray *)list;
-(void)addLayerImage:(NSString *)file list:(NSDictionary *)list;
-(void)addLayerItem:(NSString *)item list:(NSDictionary *)list;
-(void)insertImage:(NSDictionary *)dic;
@end