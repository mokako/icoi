//
//  FileManager.m
//  ICOI
//
//  Created by moca on 2013/12/26.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import "FileManager.h"


@implementation FileManager



#pragma mark - NSFileManager methods
//////////////////////////////////////////////////////////////////////////////////////////
-(NSString *)DocumentRoot
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [paths objectAtIndex:0];
}


#pragma mark -- temporary directory

+(NSArray *)loadTempList
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    return [fileManager contentsOfDirectoryAtPath:dirPath error:&error];
}

+(NSDictionary *)getAttributesOfFile:(NSString *)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"] stringByAppendingPathComponent:file];
    NSError *error = nil;
    return [[NSFileManager defaultManager] attributesOfItemAtPath:path error:&error];
}

+(NSDictionary *)getAttributesOfFileSystem:(NSString *)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"] stringByAppendingPathComponent:file];
    NSError *error = nil;
    return [[NSFileManager defaultManager] attributesOfFileSystemForPath:path error:&error];
}

+(BOOL)removeTmpFile:(NSString *)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"] stringByAppendingPathComponent:file];
    NSError *error = nil;
    return [[NSFileManager defaultManager] removeItemAtPath:path error:&error];
}

+(UIImage *)readImageOfTmp:(NSString *)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"] stringByAppendingPathComponent:file];
    return [UIImage imageWithContentsOfFile:path];
}

+(NSString *)readTextFileOfTmp:(NSString *)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"] stringByAppendingPathComponent:file];
    
#pragma mark -- TEST
    //実験
    NSFileManager* filemngr = [NSFileManager defaultManager];
    
    NSMutableDictionary* attributes = [[NSMutableDictionary alloc]initWithDictionary:[filemngr attributesOfItemAtPath:path error:nil]];
    
    [attributes setObject:@"USER-METHOD" forKey:@"USER-METHOD"];
    [filemngr setAttributes:attributes ofItemAtPath:path error:nil];
    
    
    //実験終了
    NSFileHandle *fileHandle = [NSFileHandle fileHandleForReadingAtPath:path];
    NSData *data = [fileHandle readDataToEndOfFile];
    return [[NSString alloc]initWithData:data
                         encoding:NSUTF8StringEncoding];
}



#pragma  mark -- service directory
//サービス名を使ってフォルダの有無を調べて無ければ作成する
+(BOOL)checkServiceDir:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSFileManager *fm = [NSFileManager defaultManager];
    return [fm fileExistsAtPath:dirPath];
}

+(BOOL)createServiceDir:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    return [fm createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
}

+(BOOL)deleteServiceDir:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    return [fm removeItemAtPath:dirPath error:&error];
}

+(BOOL)createSysDir:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"sys"];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    return [fm createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
}

+(BOOL)checkDatabaseFile:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"sys"] stringByAppendingPathComponent:@"icoi_data.db"];
    
    NSFileManager *fm = [NSFileManager defaultManager];
    return [fm fileExistsAtPath:dirPath];
}


+(BOOL)createFileDir:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    return [fm createDirectoryAtPath:dirPath withIntermediateDirectories:YES attributes:nil error:&error];
}


// manifesto.plist　定義部分(追加させる場合はここを追加してください）
+(BOOL)initManifestoPlist:(NSString *)serviceName
{
    NSArray *key = [[NSArray alloc]initWithObjects:@"host",@"hostUser",@"hostID",@"connectID",@"lastUpdate",@"connects",@"addData",@"addCount",@"itemCount",nil];
    NSArray *value = [[NSArray alloc]initWithObjects:@0,@0,@"",@"",[NSDate date],@0,@1,@0,@99,nil];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjects:value forKeys:key];
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    NSString *path = [syspath stringByAppendingPathComponent:@"manifesto.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
    return [self saveDictionary:data path:path];
}



+(BOOL)saveManifestoPlist:(NSDictionary *)dic service:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    NSString *path = [syspath stringByAppendingPathComponent:@"manifesto.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
    return [self saveDictionary:data path:path];
}

+(NSDictionary *)loadManifestoPlist:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    NSString *path = [syspath stringByAppendingPathComponent:@"manifesto.plist"];
    return [self loadDictionary:path];
}

/*
+(void)getDir:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    
    //@"%@",syspath;
}
 */
//
/*
 data.plistの構成
 
 {
 @"serviceName" = serviceName,
 @"hostID"      = hostID,
 @"sync"       = syncStump,
 @"dataInfo"    = @[{
                    @"mime"     = mime,
                    @"lastUpdate"  = lastUpdate,
                    @"exte"     = exte,
                    @"fileName" = fileHash
                },...]
 }
 
 
 
 
 */
+(BOOL)initDataPlist:(NSString *)serviceName
{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSDictionary *dic = @{@"hostID":[NSNull null],@"version":version,@"dataInfo":[NSNull null]};
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    NSString *path = [syspath stringByAppendingPathComponent:@"data.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
    return [self saveDictionary:data path:path];
}

+(NSDictionary *)loadDataPlist:(NSString *)serviceName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    NSString *path = [syspath stringByAppendingPathComponent:@"data.plist"];
    return [self loadDictionary:path];
}
+(BOOL)saveDataPlist:(NSString *)serviceName data:(NSDictionary *)dic
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *dirPath = [[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName];
    NSString *syspath = [dirPath stringByAppendingPathComponent:@"sys"];
    NSString *path = [syspath stringByAppendingPathComponent:@"data.plist"];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dic];
    return [self saveDictionary:data path:path];
}

+(BOOL)saveDictionary:(NSData *)data path:(NSString *)path
{
    NSError* error = nil;
    return [data writeToFile:path options:NSDataWritingFileProtectionComplete error:&error];
}
+(NSDictionary *)loadDictionary:(NSString *)path
{
   return [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataWithContentsOfFile:path]];
}

+(BOOL)savePlist:(NSString *)serviceName fileName:(NSString *)fileName file:(NSDictionary *)dic{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"]stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"sys"] stringByAppendingPathComponent:fileName];
    return [dic writeToFile:filePath atomically:YES];
}
+(BOOL)saveFile:(NSString *)serviceName fileName:(NSString *)fileName file:(id)file
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"]stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"] stringByAppendingPathComponent:fileName];
    NSData *data = [[NSData alloc]initWithData:file];
    NSError* error = nil;
    return [data writeToFile:filePath options:NSDataWritingFileProtectionComplete error:&error];
}

+(NSDictionary *)loadPlist:(NSString *)serviceName name:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"sys"] stringByAppendingPathComponent:fileName];
    return [NSDictionary dictionaryWithContentsOfFile:filePath];
}
+(id)loadFile:(NSString *)serviceName name:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"] stringByAppendingPathComponent:fileName];
    return [NSData dataWithContentsOfFile:filePath];
}

+(NSString *)setFilePath:(NSString *)serviceName name:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    return [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"] stringByAppendingPathComponent:fileName];
}

+(id)getData:(NSString *)path
{
    return [[NSData alloc]initWithContentsOfFile:path];
}

+(BOOL)checkFile:(NSString *)serviceName fileName:(NSString *)fileName
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"] stringByAppendingPathComponent:fileName];
    NSFileManager *fm = [NSFileManager defaultManager];
    return [fm fileExistsAtPath:filePath];
}

+(BOOL)removeFile:(NSString *)serviceName fileName:(NSString *)fileName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"] stringByAppendingPathComponent:fileName];
    NSFileManager *fm = [NSFileManager defaultManager];
    NSError *error;
    return [fm removeItemAtPath:filePath error:&error];
}
/*
+(void)readFileDir:(NSString *)serviceName{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:serviceName] stringByAppendingPathComponent:@"file"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *error;
    NSArray *list = [fileManager contentsOfDirectoryAtPath:path
                                                     error:&error];
    
    
    //@"file dir %@",list;
    
}
*/

@end
