//
//  CosoleViewController.h
//  ICOI
//
//  Created by moca on 2013/12/02.
//  Copyright (c) 2013年 moca. All rights reserved.





// base color RGBA(82,188,174,1.0);






#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ICOI.h"
#import "ServiceManager.h"

#import "LayerView.h"
#import "MenuUIView.h"
#import "ClearToolBar.h"
#import "PhotoPicker.h"
#import "SketchView.h"
#import "SVProgressHUD.h"
//#import "CloudView.h"
#import "PeerView.h"
#import "ToolbarMenuView.h"
#import "DescriptionSetter.h"
#import "PeerController.h"
#import "LogView.h"
#import "PopupView.h"


#define APPNAME @"Interactive communication interface"
#define APPVERSION 0.8

#define SERVICEBASENAME @"ICOI"



#define requestMaxCount 2

@class ConsoleViewController;

@protocol ConsoleViewControllerDelegate <NSObject>
@required
-(void)consoleViewControllerClosed:(ConsoleViewController *)consoleView;
@end

//cloudeを設置する時にCloudViewDelegateを追加してください。
@interface ConsoleViewController : UIViewController <UITextFieldDelegate,UINavigationControllerDelegate, UIScrollViewDelegate,MenuUIViewDelegate,SketchViewDelegate,PeerViewDelegate,ToolbarMenuViewDelegate,DescriptionSetterDelegate,PopupViewDelegate>
{
    BOOL stackCall;
    BOOL isOrientation;
    BOOL iPhone;
    BOOL isFull;
    BOOL isStart;
    NSString *uniqueID;
    FMDatabase *db;
    NSString *selectFileName;
    BOOL isRotate;
    SVProgressHUD *hud;
    NSDate *started;
    NSMutableDictionary *avoidanceList;
    //dispatch_queue_t proc_queue;
    PopupView *pop;
    NSArray *baseViewWidth;
    
    ServiceManager *sb;
    
    UIView *mv;
}
@property (nonatomic, weak) id <ConsoleViewControllerDelegate> delegate;

- (instancetype)initWithDisplayName:(NSString *)displayUUID serviceType:(NSString *)serviceType displayName:(NSString *)displayName;


@end
