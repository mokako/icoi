//
//  UnDecoCell.m
//  ICOI
//
//  Created by mokako on 2015/12/14.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "UnDecoCell.h"

@implementation UnDecoCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {

    }
    return self;
}
- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.clipsToBounds = YES;
    self.view.layer.cornerRadius = 5;
    self.backgroundColor = [UIColor clearColor];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
