//
//  ViewController.m
//  ICOI
//
//  Created by moca on 2013/12/02.
//  Copyright (c) 2013年 moca. All rights reserved.
//
/*
 
 トップのデザイン変更
 
 トップで出来る事の追加
 サービスへのログインだけではなくサービスの管理も新たに追加させます。
 
 サービスの管理はデータベースで管理させます。
 
 
 ⭐️サービスログインまでの流れ
 
 サービスログインへの誘導用button
 過去データからのサービスログイン
 
 の２種類を表示します。
 
 
 [サービスログインへの誘導用button]
 通常のログインでサービス名を打ち込んでもらいそのサービスが存在するならデータベースよりdisplaynameを取得しconsoleViewに送ります。
 無ければ新しいサービスとしてデータベースに登録してからconsoleViewに移動します。
 どちらの状態でも一旦クライアントに報告する義務が有ります。それはサービスの重複の恐れが有るからです。
 
 [過去データからのサービスログイン]
 選択されるとまず専用のUIViewで作られた画面を表示します。ここでは編集とサービスログインを可能となる様にします。
 編集はサービス削除の機能を持たせます。名前変更等の編集はあまり好ましく無い為見送りとします（要望が有れば可能に）。
 
 
 
 
 サービスへのログインはまずサービス名を指定しサービスのディレクトリを調べ無ければ新しいサービスなのでサービスで使う名前を決めますが、
 有るならそのままログインします。
 
 
 
 
 
 
 
 
 
 
 
 */
#import "ViewController.h"
#import "ConsoleViewController.h"


#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]



@interface ViewController () <UITextFieldDelegate,ConsoleViewControllerDelegate>

@property (nonatomic)UIView *rootView;

@property (nonatomic)UITextField *serviceAccount;
@property (nonatomic)ConsoleViewController *console;
@property (nonatomic)UIView *textField;
@property (nonatomic)NSInteger defaultWidth;
@property (nonatomic)NSInteger defaultHeight;
@end

@implementation ViewController

- (void)viewDidLoad
{
    
    
    
    [super viewDidLoad];
    self.view.clipsToBounds = YES;
    CALayer *layer = [CALayer layer];
    [self.view.layer addSublayer:layer];
    isStart = YES;
    fontColor = RGBA(255,255,255,1.0);
    backColor = RGBA(255,255,255,0.2);
    isOrientation = NO;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        //iphone
        iPhone = YES;
    }
    else{
        iPhone = NO;
    }
    firstRun = NO;
    [self initService];
    [self initializeView];
    keyboardRect = CGRectZero;
    duration = 0.0;
    [self updateCheck];
    isView = YES;
    isOpen = NO;
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"appFront" object:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    //文字を白くする
    return UIStatusBarStyleLightContent;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}


// キーボードが表示される時に呼び出されますー
- (void)keyboardWillChange:(NSNotification *)notification {

    keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    [self.view removeConstraints:accountConstraint];
    accountConstraint =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":@(self.view.frame.size.height - keyboardRect.origin.y)}
                                              views:@{@"view":account}];
    [self.view addConstraints:accountConstraint];
    [UIView animateWithDuration:duration animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL comp){

    }];
}


//一番最初の起動時に設定します。
-(void)initService
{//まずは管理用service.dbを作成
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *path = [[paths objectAtIndex:0] stringByAppendingPathComponent:@"service.db"];
    NSFileManager *fm = [NSFileManager defaultManager];
    
    db = [FMDatabaseQueue databaseQueueWithPath:path];
    if(![fm fileExistsAtPath:path]){
        [DataManager initServiceTable:db];
    }
    
    
    //テンポラリディレクトリを無い場合は作成
    //まだ不必要なために作成しません。
    /*
    NSString *tmpPath =  [[paths objectAtIndex:0] stringByAppendingPathComponent:@"tmp"];
    BOOL dic;
    if(![fm fileExistsAtPath:tmpPath isDirectory:&dic]){
        NSError *error;
        [fm createDirectoryAtPath:tmpPath withIntermediateDirectories:YES attributes:nil error:&error];
    }
    
    */

}

-(void)initializeView
{

    
    UIImageView *imv = [[UIImageView alloc]initWithFrame:CGRectZero];
    imv.translatesAutoresizingMaskIntoConstraints = NO;
    UIImage *img = [UIImage imageWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"icoi-top01" ofType:@"jpg"]];
    imv.image = img;
    imv.contentMode = UIViewContentModeScaleAspectFill;
    [self.view addSubview:imv];
    NSArray *w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[imv]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"imv":imv}];
    [self.view addConstraints:w];
    
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[imv]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"imv":imv}];
    [self.view addConstraints:h];
    
    
    
    /*
     
     
     // list　@"Service list"
     
     
     
     
     
     
     //description button
     UIButton *c = [[UIButton alloc]initWithFrame:CGRectMake(visit.frame.size.width / 2 - 15,240,30,30)];
     c.tag = 9;
     //[[b layer]setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
     //[[b layer]setBorderWidth:1.0];
     [c setBackgroundColor:RGBA(101,108,126,0.0)];
     c.layer.cornerRadius = 15;
     c.titleLabel.font = [UIFont fontWithName:@"font-icon-l-01" size:30.0];
     c.titleEdgeInsets = UIEdgeInsetsMake(0, 0, 0, 0);
     [c setTitle:@"g" forState:UIControlStateNormal];
     [c setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
     [c setTitleColor:RGBA(82,188,174,1.0) forState:UIControlStateHighlighted];
     [c addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
     [visit addSubview:c];
     
     */
    /*
    icoi = [[UILabel alloc]initWithFrame:CGRectMake(0,0,200,50)];
    icoi.textAlignment = NSTextAlignmentCenter;
    icoi.textColor = [UIColor colorWithHex:@"FFFFFF"];
    icoi.font = [UIFont fontWithName:@"Julius Sans One" size:14];
    icoi.text = @"ICOI";
    icoi.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:icoi];
    
    NSArray *vih =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[icoi(height)]"
                                            options:0
                                            metrics:@{@"height":@(icoi.frame.size.height)}
                                              views:@{@"icoi":icoi}];
    [self.view addConstraints:vih];
    
    NSArray *vih =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[icoi(height)]"
                                            options:0
                                            metrics:@{@"height":@(icoi.frame.size.height)}
                                              views:@{@"icoi":icoi}];
    [self.view addConstraints:vih];
     */
    /*
    
    UIButton *button = [[UIButton alloc]initWithFrame:CGRectZero];
    button.translatesAutoresizingMaskIntoConstraints = NO;
    [button setBackgroundColor:[UIColor colorWithHex:@"FFFFFF"]];
    [button addTarget:self action:@selector(setTest) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    NSArray *bw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[bu(width)]"
                                            options:0
                                            metrics:@{@"width":@30}
                                              views:@{@"bu":button}];
    [self.view addConstraints:bw];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:button  attribute:NSLayoutAttributeCenterX  relatedBy:NSLayoutRelationEqual  toItem:self.view  attribute:NSLayoutAttributeCenterX  multiplier:1  constant:0]];
    
    NSArray *bh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[bu]"
                                            options:0
                                            metrics:nil
                                              views:@{@"bu":button}];
    [self.view addConstraints:bh];
     */

#pragma mark - visit view
    visit = [VisitView view];
    visit.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:visit];
    
    NSArray *viw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[vis(width)]"
                                            options:0
                                            metrics:@{@"width":@(visit.frame.size.width)}
                                              views:@{@"vis":visit}];
    [self.view addConstraints:viw];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:visit  attribute:NSLayoutAttributeCenterX  relatedBy:NSLayoutRelationEqual  toItem:self.view  attribute:NSLayoutAttributeCenterX  multiplier:1  constant:0]];
    
    NSArray *vih =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[vis(height)]"
                                            options:0
                                            metrics:@{@"height":@(visit.frame.size.height)}
                                              views:@{@"vis":visit}];
    [self.view addConstraints:vih];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:visit  attribute:NSLayoutAttributeCenterY  relatedBy:NSLayoutRelationEqual  toItem:self.view  attribute:NSLayoutAttributeCenterY  multiplier:1  constant:0]];
    
    [visit.signin addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [visit.login addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    

#pragma mark - account
    account = [AccountView view];
    account.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:account];
    

    NSArray *acw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[ac]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"ac":account}];
    [self.view addConstraints:acw];
    
    NSArray *ach =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[ac(height)]"
                                            options:0
                                            metrics:@{@"height":@(account.frame.size.height)}
                                              views:@{@"ac":account}];
    [self.view addConstraints:ach];
    
    accountConstraint =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[ac]-height-|"
                                            options:0
                                            metrics:@{@"height":@(0)}
                                              views:@{@"ac":account}];
    [self.view addConstraints:accountConstraint];
    
    account.desc.text = NSLocalizedString(@"account_view_log_serviceName_desc",@"");
    
    account.accountText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ServiceName",@"") attributes:@{NSForegroundColorAttributeName: RGBA(235,230,235,0.6) , NSFontAttributeName :  [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:15.0]}];

    [serviceNameField addSubview:self.serviceAccount];
    [signin addSubview:serviceNameField];
    
    
#pragma mark - list table
    //list用 描画領域　h : 340 - 85
    
    listTable = [ServiceListTable view];
    listTable.hidden = YES;
    listTable.delegate = self;
    listTable.db = db;
    listTable.clipsToBounds = YES;
    listTable.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:listTable];
    NSArray *lw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[listTable]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"listTable":listTable}];
    [self.view addConstraints:lw];
    
    NSArray *lh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[listTable]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"listTable":listTable}];
    [self.view addConstraints:lh];
    [listTable setView];
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if([defaults boolForKey:@"firstRun"]){
        firstRun = YES;
        firstPop = [[PopupView alloc]initWithFrame:CGRectZero];
        [firstPop setMountColor:RGBA(205,205,205,0.2)];
        firstPop.translatesAutoresizingMaskIntoConstraints = NO;
        [self.view addSubview:firstPop];
        
        
        NSArray *fw =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|[pop]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"pop":firstPop}];
        [self.view addConstraints:fw];
        
        NSArray *fh =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[pop]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"pop":firstPop}];
        [self.view addConstraints:fh];
        
        
        
        
        
        
        
        
        
        UIView *popBase = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,360)];
        popBase.backgroundColor = RGBA(255,255,255,0.9);
        popBase.clipsToBounds = YES;
        popBase.layer.cornerRadius = 5;
        [[popBase layer]setBorderColor:[RGBA(101,108,126,0.7) CGColor]];
        [[popBase layer] setBorderWidth:1.0];
    
        [self.view addSubview:popBase];
        
        setHTML = [SetDescHTML view];
        setHTML.frame = CGRectMake(0,0,300,320);
        setHTML.back.hidden = YES;
        [popBase addSubview:setHTML];
        [setHTML setHtmlSetting:@"FirstSupport"];
        
        
        UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(popBase.frame.size.width / 2 - 40,323,80,30)];
        [but setTitle:@"OK" forState:UIControlStateNormal];
        but.titleLabel.textAlignment = NSTextAlignmentCenter;
        but.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
        [but setBackgroundColor:RGBA(68,194,179,1.0)];
        [but setTitleColor:RGB(255,255,255) forState:UIControlStateNormal];
        but.layer.cornerRadius = 5;
        [but addTarget:self action:@selector(hidePop) forControlEvents:UIControlEventTouchUpInside];
        [popBase addSubview:but];
        [firstPop setView:popBase];
        [firstPop show];
    
    }
    
    [self serviceExit];

}

-(void)serviceExit{
    if([[DataManager serviceList:db] count] == 0){
        visit.login.hidden = YES;
    }else{
        visit.login.hidden = NO;
    }
}


-(void)hidePop{
    [firstPop hide];
}


-(void)setBut:(UIButton *)but{
    [self changeState:but.tag];
}
-(void)changeState:(NSInteger)state
{
    switch (state) {
        case 1:
        {//sign in
            visit.hidden = YES;
            account.hidden = NO;
            icoi.hidden = YES;
            [account.accountText becomeFirstResponder];
        }
            break;
        case 2:
        {
            
        }
            break;
        case 3:
        {//serviceNameが入力されました。
            
            if([DataManager serviceTypeCheck:account.accountText.text maxLength:10 minLength:2]){
                NSDictionary *dic = [[NSDictionary alloc]initWithDictionary:[DataManager existService:db service:account.accountText.text]];
                if([[dic allKeys]count] != 0){
                    [account.accountText endEditing:YES];
                    UIAlertView *alert =
                    [[UIAlertView alloc]
                     initWithTitle:@"Invalid error."
                     message:NSLocalizedString(@"exitService",@"")
                     delegate:nil
                     cancelButtonTitle:nil
                     otherButtonTitles:NSLocalizedString(@"OK",@""), nil
                     ];
                    [alert show];
                }else{
                    //無い場合はdisplayNameを要求します。その後新規作成しデータベースにデータを保存しconsoleViewに移ります。
                    serviceName = account.accountText.text;
                    account.done.tag = 5;
                    account.undo.tag = 6;
                    account.title.text = NSLocalizedString(@"account_view_displayName", @"Display name");
                    [account.accountText setText:@""];
                    account.accountText.placeholder = 
                    accountIcon.text = @"i";
                    account.desc.text = NSLocalizedString(@"account_view_log_displayName_desc",@"");
                }
            }else{
                //入力された値が規定通りではない
                UIAlertView *alert =
                [[UIAlertView alloc]
                 initWithTitle:@"ERROR"
                 message:NSLocalizedString(@"InvalidErrorService",@"")
                 delegate:nil
                 cancelButtonTitle:nil
                 otherButtonTitles:NSLocalizedString(@"OK",@""), nil
                 ];
                [alert show];
            }
        }
            break;
        case 4:
        {
            [account.accountText endEditing:YES];
            account.hidden = YES;
            visit.hidden = NO;
            icoi.hidden = NO;
        }
            break;
        case 5:
        {//入力されたサービスへのアクセス
            [account.accountText endEditing:YES];
            if(account.accountText.text.length <= 20 || account.accountText.text.length >= 2){
                
                NSUUID *uid = [NSUUID UUID];
                NSString *uuid = [uid UUIDString];
                [DataManager insertService:db serviceName:serviceName  displayName:account.accountText.text displayUUID:uuid date:[NSDate date]];
                [self startICOI:serviceName name:account.accountText.text uuid:uuid];
                
            }else{
                UIAlertView *alert =
                [[UIAlertView alloc]
                 initWithTitle:@"ERROR"
                 message:NSLocalizedString(@"InvalidErrorDisplay",@"")
                 delegate:nil
                 cancelButtonTitle:nil
                 otherButtonTitles:NSLocalizedString(@"OK",@""), nil
                 ];
                [alert show];
            }
        }
            break;
        case 6:
        {
            account.done.tag = 3;
            account.undo.tag = 4;
            accountIcon.text = @"5";
            account.title.text = NSLocalizedString(@"account_view_groupName", @"Group name");
            account.accountText.placeholder = @"Group name";
            account.desc.text = NSLocalizedString(@"account_view_log_serviceName_desc",@"");
            [account.accountText setText:serviceName];
        }
            break;
        case 7:
        { //リストを表示する
            NSArray *ary = [[NSArray alloc]initWithArray:[DataManager serviceList:db]];
            if(ary.count == 0){
                UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NSLocalizedString(@"LoginError", @"") message:NSLocalizedString(@"LoginErrorString", @"")  delegate:nil cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                [alert show];
            }else{
                [listTable reloadServiceList:ary];
                account.hidden = YES;
                visit.hidden = YES;
                icoi.hidden = YES;
                listTable.hidden = NO;
            }
        }
            break;
        case 8:
        { //リストからvisitへ
            [self serviceExit];
            listTable.hidden = YES;
            visit.hidden = NO;
            account.hidden = YES;
            icoi.hidden = NO;
            
        }
            break;
        case 9:
        {//説明画面へ移行 AccountDiscription
        }
            break;
        default:
            break;
    }
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    CGRect screenBounds = [DeviceData deviceMainScreen];
    float screenHeight = screenBounds.size.height;
    
    [UIView animateWithDuration:0.2
                     animations:^{
                         self.textField.frame = CGRectMake(0, 0, 320, screenHeight - 20);
                     }];
    [textField resignFirstResponder];
    return YES;
}


-(void)consoleViewControllerClosed:(ConsoleViewController *)consoleView
{
    [self.console dismissViewControllerAnimated:NO completion:^{
        [self.console willMoveToParentViewController:nil];
        [self.console.view removeFromSuperview];
        [self.console removeFromParentViewController];
    }];
    [self serviceExit];
    isView = YES;
}

-(void)startICOI:(NSString *)service name:(NSString *)display uuid:(NSString *)uuid
{
    [DataManager updateServiceDate:db serviceName:service];
    
    listTable.hidden = YES;
    account.hidden = YES;
    visit.hidden = NO;
    icoi.hidden = NO;
    accountIcon.text = @"5";
    isView = NO;
    self.serviceAccount.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"ServiceName",@"") attributes:@{NSForegroundColorAttributeName:RGBA(235,230,235,0.6) , NSFontAttributeName :  [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:15.0]}];
    [self.serviceAccount setText:serviceName];
    self.console = [[ConsoleViewController alloc]initWithDisplayName:uuid serviceType:service displayName:display];
    self.console.delegate = self;
    [account.accountText endEditing:YES];
    [self presentViewController:self.console animated:YES completion:nil];
    [self.console removeFromParentViewController];
}


-(void)setComment:(NSString *)service comment:(NSString *)comment
{
    
    [DataManager updateComment:db serviceName:service commnet:comment.length != 0 ? comment : @"No comment yet."];
    [listTable reloadServiceList:[DataManager serviceList:db]];
}

-(void)deleteService:(NSString *)service
{
    //フォルダから該当のフォルダを削除します。その後databaseからデータを削除します。
    [FileManager deleteServiceDir:service];
    [DataManager deleteService:db serviceName:service];
}


#pragma mark - Notification
-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0:
        {
            [self changeState:[[center userInfo][@"state"] integerValue]];
        }
            break;
        case 1:
        {
            
        }
            break;
        default:
            break;
    }
}

//
#pragma mark -UPDATE CHECK

-(NSString *)applicationVersion{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
}


-(void)updateCheck
{
    [self getLatestAppVersionAsynchronousWithCompletionBlock:^(NSString *latestAppVersion) {
        // 現行のアプリバージョンが、最新のアプリバージョンよりも古い場合(NSNumericSearchでバージョン番号での比較が可能)、
        if ([latestAppVersion compare:[self applicationVersion] options:NSNumericSearch] == NSOrderedDescending) {
            static BOOL isAlreadyShow = NO;
            // 通知中でなければ、
            if (!isAlreadyShow) {
                isAlreadyShow = YES;
                dispatch_async(dispatch_get_main_queue(), ^{
                    // ダイアログを表示するなど、通知の処理をここに記述。
                });
            }
        }
    }];
}

-(void)getLatestAppVersionAsynchronousWithCompletionBlock:(void(^)(NSString *))completionBlock
{
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://itunes.apple.com/lookup?id=%@", kAppStoreID]]
                                             cachePolicy:NSURLRequestReloadIgnoringLocalCacheData // キャッシュしない
                                         timeoutInterval:20.0];
    [NSURLConnection sendAsynchronousRequest:request
                                       queue:[[NSOperationQueue alloc] init]
                           completionHandler:^(NSURLResponse *response, NSData *data, NSError *error) {
                               if (data) {
                                   NSDictionary *versionSummary = [NSJSONSerialization JSONObjectWithData:data
                                                                                                  options:NSJSONReadingAllowFragments
                                                                                                    error:&error];
                                   NSDictionary *results = [[versionSummary objectForKey:@"results"] objectAtIndex:0];
                                   NSString *latestVersion = [results objectForKey:@"version"];
                                   completionBlock(latestVersion);
                               } else {
                               }
                           }];
}




@end