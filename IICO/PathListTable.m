//
//  PathListTable.m
//  ICOI
//
//  Created by mokako on 2014/05/01.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "PathListTable.h"

@implementation PathListTable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        
        pathList = [[NSMutableArray alloc]init];
        
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)initializeView
{
    table = [[UITableView alloc]initWithFrame:self.frame];
    table.dataSource = self;
    table.delegate = self;
    table.rowHeight = 40.0;
    table.separatorInset = UIEdgeInsetsZero;
    table.separatorColor = RGBA(200,200,200,1.0);
    [self addSubview:table];
}

-(void)review
{
    table.frame = self.frame;
}

- (NSInteger) numberOfSectionsInTableView : (UITableView *)tableView {
    return 1;
}
// テーブル要素数を返す
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [pathList count];
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConfigIdentifier"];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1
                                      reuseIdentifier: ident];
    }
    return cell;
}



@end
