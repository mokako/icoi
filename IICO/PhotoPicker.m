//
//  PhotoPicker.m
//  ICOI
//
//  Created by mokako on 2014/02/13.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "PhotoPicker.h"

@implementation PhotoPicker
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RGBA(250,255,250,1.0);

        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServiceManager:) name:@"sendServiceManagerSeed" object:nil];
        [self setAlbumArray];
        [self initializeView];
    }
    return self;
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)initializeView
{

    layoutWidth = 120.0;
    
    layout = [[UICollectionViewFlowLayout alloc]init];
    layout.itemSize = CGSizeMake(layoutWidth,layoutWidth);
    layout.minimumInteritemSpacing = 2.0f;
    layout.minimumLineSpacing = 2.0f;
    
    collectview = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:layout];
    [collectview setDelegate:self];
    [collectview setDataSource:self];
    [collectview registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"CELL"];
    [collectview setBackgroundColor:RGBA(0,15,10,0.0)];
    collectview.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:collectview];
    
    NSArray *w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-5-[view]-5-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":collectview}];
    [self addConstraints:w];
    
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":collectview}];
    [self addConstraints:h];
    
    
    
    picker = [[PickerView alloc]initWithFrame:CGRectZero];
    picker.delegate = self;
    picker.hidden = YES;
    picker.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:picker];
    
    NSArray *pw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":picker}];
    [self addConstraints:pw];
    
    NSArray *ph =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":picker}];
    [self addConstraints:ph];
    
 
}
- (void)closeSoftKeyboard {
    [self endEditing:YES];
}
-(void)setServiceManager:(NSNotification *)center{
    sb = [center userInfo][@"manager"];
}
-(void)endTask
{
    [picker endTask];
}
-(void)setImageName:(NSString *)str scale:(CGFloat)scale
{
    picker.hidden = YES;

    NSUUID *uid = [NSUUID UUID];
    //NSString *uuid = [NSString stringWithFormat:@"%@@2x",[uid UUIDString]];
    //検証
    NSString *uuid = [uid UUIDString];
    
    ALAsset *asset = [photos objectAtIndex:selectIndex];
    
/*
 
 UIImage *small = [UIImage imageWithCGImage:original.CGImage scale:0.25 orientation:original.imageOrientation];
 
 */
    //file name の保持とdataの保持
    
#pragma mark - SET DATA DICTIONARY 雛形です。すべてはこの方式でデータを保持してください。
    NSString *type = [self getTypeForPath:asset];
    
    NSArray *key = [NSArray arrayWithObjects:@"title",@"exte",@"donor",@"donorID",@"mime",@"lastUpdate",@"fileState",@"class", nil];
    NSArray *value = [NSArray arrayWithObjects:str,type,[sb getDisplay],[sb getUUID],@"image",[NSDate date],@"2",@"1", nil];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjects:value forKeys:key];
    NSMutableDictionary *newDic = [[NSMutableDictionary alloc]initWithObjectsAndKeys:dic,uuid,nil];
    NSString *fileName = [NSString stringWithFormat:@"%@.%@",uuid,type];
    
    NSInteger rout = [[asset valueForProperty:ALAssetPropertyOrientation]integerValue];
    UIImage *def = [UIImage imageWithCGImage:[[asset defaultRepresentation] fullResolutionImage] scale:[[UIScreen mainScreen] scale] orientation:rout];
    UIImage *image = [ICOIImage resizeImage:def scale:scale];
    NSData *dat = [[NSData alloc] initWithData:UIImageJPEGRepresentation(image,pickerQuality)];
    [FileManager saveFile:[sb getService] fileName:fileName file:dat];

    //databaseに登録
    [sb setNewFileData:newDic];
    
    
    UIAlertView *alert =
    [[UIAlertView alloc]
     initWithTitle:@"Complete"
     message:@"Set the image."
     delegate:nil
     cancelButtonTitle:nil
     otherButtonTitles:@"OK", nil
     ];
    [alert show];
    
}


//CELL数
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [photos count];
}
//collection数
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(layoutWidth,layoutWidth);
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CELL" forIndexPath:indexPath];
    ALAsset *asset = [photos objectAtIndex:indexPath.row];
    if (asset) {
        UIImage *img =[UIImage imageWithCGImage:[asset thumbnail]];
        cell.backgroundColor = [UIColor colorWithPatternImage:img];
    }
    return cell;
}
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    selectIndex = indexPath.row;
    
    
    
    NSInteger rout = [[[photos objectAtIndex:selectIndex] valueForProperty:ALAssetPropertyOrientation]integerValue];
    [picker setImageWithPicker:[UIImage imageWithCGImage:[[[photos objectAtIndex:selectIndex] defaultRepresentation] fullResolutionImage]scale:[[UIScreen mainScreen] scale] orientation:rout] index:selectIndex];
    picker.hidden = NO;
}

-(void)setImageFromIndex:(BOOL)as
{
    /*
     [UIImage imageWithCGImage:[assetRepresentation fullScreenImage]
     scale:2.0f
     orientation:UIImageOrientationUp];
     
     */
    if(as){
        selectIndex = selectIndex == [photos count] - 1 ? 0 : selectIndex + 1;
        NSInteger rout = [[[photos objectAtIndex:selectIndex] valueForProperty:ALAssetPropertyOrientation]integerValue];
        [picker setImageWithPicker:[UIImage imageWithCGImage:[[[photos objectAtIndex:selectIndex] defaultRepresentation] fullResolutionImage] scale:[[UIScreen mainScreen] scale] orientation:rout] index:selectIndex];
    }else{
        selectIndex = selectIndex == 0 ? [photos count] - 1 : selectIndex - 1;
        NSInteger rout = [[[photos objectAtIndex:selectIndex] valueForProperty:ALAssetPropertyOrientation]integerValue];
        [picker setImageWithPicker:[UIImage imageWithCGImage:[[[photos objectAtIndex:selectIndex] defaultRepresentation] fullResolutionImage] scale:[[UIScreen mainScreen] scale] orientation:rout] index:selectIndex];
    }
}

-(void)closePicker
{
    picker.hidden = YES;
}

-(void)reloadCollection
{
    [self setAlbumArray];
    [collectview reloadData];
}

- (void)setAlbumArray
{
    photos = [NSMutableArray array];
    ALAssetsLibrary *as = [PhotoPicker defaultAssetsLibrary];
    [as enumerateGroupsWithTypes:ALAssetsGroupSavedPhotos
                           usingBlock:^(ALAssetsGroup *group, BOOL *stop) {
                               [group enumerateAssetsUsingBlock:^(ALAsset *asset, NSUInteger index, BOOL *stop) {
                                   if (asset) {
                                       
                                       
                                       [photos addObject:asset];
                                   }
                               }];
                               [collectview reloadData];
                           }
                         failureBlock:^(NSError *error) {}
     ];
}

-(NSString *)getTypeForPath:(ALAsset *)asset
{
    NSString *url = [NSString stringWithFormat:@"%@",asset];
    NSString *fileName = [url lastPathComponent];
    NSString *fileExtension = [fileName pathExtension];
    
    NSError *error = nil;
    NSRegularExpression *regexp = [NSRegularExpression regularExpressionWithPattern:@"^(.*)\\?" options:0 error:&error];
    NSMutableArray *dats = [[NSMutableArray alloc] init];
    [regexp enumerateMatchesInString:fileExtension
                             options:0
                               range:NSMakeRange(0, [fileExtension length])
                          usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop){
                              [dats addObject: [fileExtension substringWithRange:[match rangeAtIndex:1]]];
                          }];
    return [dats objectAtIndex:0];
}


+ (ALAssetsLibrary *)defaultAssetsLibrary {
    static dispatch_once_t pred = 0;
    static ALAssetsLibrary *library = nil;
    dispatch_once(&pred, ^{
        library = [[ALAssetsLibrary alloc] init];
    });
    return library;
}

@end
