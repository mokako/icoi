//
//  ScaleSliderView.h
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface ScaleSliderView : UIView
{
    NSArray *w;
    NSArray *h;
    BOOL isToggle;
    BOOL isFirst;
}
@property (weak, nonatomic) IBOutlet UISlider *slider;
@property (weak, nonatomic) IBOutlet UIButton *ok;
@property (weak, nonatomic) IBOutlet UILabel *state;
+ (instancetype)view;
-(void)set;
-(void)toggle;
-(void)fadeOut;
@end
