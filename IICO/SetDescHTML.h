//
//  SetDescHTML.h
//  ICOI
//
//  Created by mokako on 2015/07/11.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"


@interface SetDescHTML : UIView<UIWebViewDelegate>
{
    IBOutlet UIWebView *web;
    NSArray *layout;
    NSString *path;
    NSString *urlBase;
    BOOL isOpen;
}
@property (weak, nonatomic) IBOutlet UIButton *back;
-(void)setConstraint:(NSArray *)layout;
+ (instancetype)view;
-(void)setHtmlSetting:(NSString *)htmlUrl;
-(void)fadeIn;
-(IBAction)fadeOut:(id)sender;
@end
