//
//  DateFormatter.m
//  ICOI
//
//  Created by mokako on 2015/06/19.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "DateFormatter.h"

@implementation DateFormatter


#pragma mark -- 日付処理



+(NSDate *)getNow
{
    return [NSDate dateWithTimeIntervalSinceNow:[[NSTimeZone systemTimeZone] secondsFromGMT]];
}
//ユリアス歴 -> date型
+(NSDate *)dateFromJulian:(double)julianDays{
    NSTimeInterval seconds = (julianDays - 2440587.50000) * 86400.0;
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:seconds];
    return date;
}

+(NSString *)customDataForJulian:(double)julianDays{
    return [self beforeDateTimeFromDate:[self dateFromJulian:julianDays]];
}

//ユリアス歴に対応した形式に変更
+(NSString *)dateFormatter:(NSDate *)date
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyy-MM-dd HH:mm:ss";
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [formatter stringFromDate:date];
}


+(NSInteger)dateFormatterWithInteger:(NSDate *)date
{
    NSDateFormatter* formatter = [[NSDateFormatter alloc] init];
    formatter.dateFormat = @"yyyyMMddHHmmss";
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    return [[formatter stringFromDate:date] integerValue];
}

+ (NSString *)beforeDateTimeString:(NSString *)dateTime {
    if (dateTime == nil) {
        return nil;
    }
    
    /*
     NSDate *date = [NSDate date];
     NSTimeZone *tz = [NSTimeZone systemTimeZone];
     NSInteger seconds = [tz secondsFromGMTForDate: date];
     NSDate *localDate = [date dateByAddingTimeInterval:seconds];
     
     
     */
    
    NSDate *date = [self localTime:dateTime];
    
    
    NSTimeInterval calcTimeInterval = [date timeIntervalSinceNow];
    if (calcTimeInterval < 0) {
        calcTimeInterval = -calcTimeInterval;
    }
    
    //分単位の場合を算出します。
    double calcMinitue = floor(calcTimeInterval / 60);
    //何分、何時間といった文字列を返します
    NSString *responseString;
    
    if (calcMinitue < 60) {
        if (calcMinitue <= 0) calcMinitue = 1;
        
        if(calcMinitue == 1){
            responseString = [NSString stringWithFormat:@"%d m",(integer_t)calcMinitue];
        }else{
            responseString = [NSString stringWithFormat:@"%d m",(integer_t)calcMinitue];
        }
        
    } else {
        //時間単位の場合を算出します
        double calcTime = floor(calcMinitue / 60);
        if (calcTime < 24) {
            if (calcTime == 1){
                responseString = [NSString stringWithFormat:@"%d h",(integer_t)calcTime];
            }else{
                responseString = [NSString stringWithFormat:@"%d h",(integer_t)calcTime];
            }
        } else {
            double calcDay = floor(calcTime / 24);
            if(calcDay < 30){
                if(calcDay == 1){
                    responseString = [NSString stringWithFormat:@"%d D",(integer_t)calcDay];
                }else{
                    responseString = [NSString stringWithFormat:@"%d D",(integer_t)calcDay];
                }
            }else{
                //これ以降も、月や年等の数値で計算していく事で、「○○月前」「○○年前」が計算できます。
                responseString = @"Long before";
            }
        }
    }
    return responseString;
}

+ (NSString *)beforeDateTimeFromDate:(NSDate *)date{
    NSTimeInterval calcTimeInterval = [date timeIntervalSinceNow];
    if (calcTimeInterval < 0) {
        calcTimeInterval = -calcTimeInterval;
    }
    
    //分単位の場合を算出します。
    double calcMinitue = floor(calcTimeInterval / 60);
    //何分、何時間といった文字列を返します
    NSString *responseString;
    
    if (calcMinitue < 60) {
        if (calcMinitue <= 0) calcMinitue = 1;
        
        if(calcMinitue == 1){
            responseString = [NSString stringWithFormat:@"%d m",(integer_t)calcMinitue];
        }else{
            responseString = [NSString stringWithFormat:@"%d m",(integer_t)calcMinitue];
        }
        
    } else {
        //時間単位の場合を算出します
        double calcTime = floor(calcMinitue / 60);
        if (calcTime < 24) {
            if (calcTime == 1){
                responseString = [NSString stringWithFormat:@"%d h",(integer_t)calcTime];
            }else{
                responseString = [NSString stringWithFormat:@"%d h",(integer_t)calcTime];
            }
        } else {
            double calcDay = floor(calcTime / 24);
            if(calcDay < 30){
                if(calcDay == 1){
                    responseString = [NSString stringWithFormat:@"%d D",(integer_t)calcDay];
                }else{
                    responseString = [NSString stringWithFormat:@"%d D",(integer_t)calcDay];
                }
            }else{
                //これ以降も、月や年等の数値で計算していく事で、「○○月前」「○○年前」が計算できます。
                responseString = @"Long before";
            }
        }
    }
    return responseString;
}

+ (NSTimeInterval)julianDate:(NSDate *)date
{
    return ([date timeIntervalSince1970] / 86400.0) + 2440587.50000;
}

+ (NSInteger)beforeDateTimeState:(NSString *)dateTime {
    if (dateTime == nil) {
        return 5;
    }
    
    /*
     NSDate *date = [NSDate date];
     NSTimeZone *tz = [NSTimeZone systemTimeZone];
     NSInteger seconds = [tz secondsFromGMTForDate: date];
     NSDate *localDate = [date dateByAddingTimeInterval:seconds];
     
     
     */
    
    NSDate *date = [self localTime:dateTime];
    
    
    NSTimeInterval calcTimeInterval = [date timeIntervalSinceNow];
    if (calcTimeInterval < 0) {
        calcTimeInterval = -calcTimeInterval;
    }
    
    //分単位の場合を算出します。
    double calcMinitue = floor(calcTimeInterval / 60);
    //何分、何時間といった文字列を返します
    NSInteger responseString;
    
    if (calcMinitue < 60) {
        if (calcMinitue <= 0) calcMinitue = 1;
        
        if(calcMinitue == 1){
            responseString = 0;
        }else{
            responseString = 1;
        }
        
    } else {
        //時間単位の場合を算出します
        double calcTime = floor(calcMinitue / 60);
        if (calcTime < 24) {
            responseString = 2;
        } else {
            double calcDay = floor(calcTime / 24);
            if(calcDay < 30){
                responseString = 3;
            }else{
                //これ以降も、月や年等の数値で計算していく事で、「○○月前」「○○年前」が計算できます。
                responseString = 4;
            }
        }
    }
    return responseString;
}




+(NSDate *)localTime:(NSString *)time
{
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *GMTdate = [dateFormatter dateFromString:time];
    NSTimeZone *tz = [NSTimeZone systemTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate:GMTdate];
    return [GMTdate dateByAddingTimeInterval:seconds];
}

+(NSString *)dateToString:(NSString *)time state:(NSInteger)state
{
    NSDate *date = [self localTime:time];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    switch (state) {
        case 0:
        {
            [formatter setDateFormat:@"yyyy-MM-dd"];
        }
            break;
        case 1:
        {
            [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        }
            break;
        case 2:
        {
            [formatter setDateFormat:@"HH:mm:ss"];
        }
            break;
        default:
            break;
    }
    return [formatter stringFromDate:date];
}



@end
