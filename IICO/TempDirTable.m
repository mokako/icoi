//
//  TempDirTable.m
//  ICOI
//
//  Created by mokako on 2014/07/25.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "TempDirTable.h"

@implementation TempDirTable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        fileAttribute = [[NSMutableArray alloc]init];
        fileList = [[NSMutableArray alloc]init];
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)initializeView
{
    //操作用のtoolbar
    
    toolbar = [[ClearToolBar alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,48)];
    toolbar.backgroundColor = RGBA(35,30,25,1.0);
    [self addSubview:toolbar];
    //
    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    //
    UIBarButtonItem *blankSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    blankSpace.width = 48;
    
    
    
    
    UIButton *window = [[UIButton alloc]initWithFrame:CGRectMake(0,0,150,32)];
    window.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:13];
    [[window layer]setBorderColor:[RGBA(255,255,255,1.0) CGColor]];
    [window setTitle:@"Close temporary." forState:UIControlStateNormal];
    window.tag = 10;
    [window addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *c = [[UIBarButtonItem alloc]initWithCustomView:window];
    
    
    toolbar.items = [NSArray arrayWithObjects:gap,blankSpace,c,blankSpace,gap,nil];
    
    
    table = [self createTable:CGRectMake(0,0,self.frame.size.width,self.frame.size.height) tag:1];
    [self addSubview:table];
    
    colorArray = [[NSArray alloc] initWithObjects:RGBA(0,200,255,1.0),RGBA(0,230,0,1.0),RGBA(255,224,0,1.0),RGBA(255,0,0,1.0),RGBA(190,190,190,1.0),RGBA(90,90,90,1.0), nil];
    
    //テンポラリの内容をテーブルで表示
    
    
    
    
    
    //ConvertFileView
    convert = [[ConvertFileView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    convert.hidden = YES;
    convert.delegate = self;
    [self addSubview:convert];
}

-(void)review
{
    table.frame = CGRectMake(0,48,self.frame.size.width,self.frame.size.height - 48);
    table.backgroundColor = RGBA(0,255,0,1.0);
    toolbar.frame = CGRectMake(0,0,self.frame.size.width,48);
    convert.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    convert.backgroundColor = RGBA(255,0,255,1.0);
    [convert review];
}


-(void)loadTmp
{
    fileList = [NSMutableArray arrayWithArray:[FileManager loadTempList]];
    [table reloadData];
}

#pragma mark -- ファイル操作系
//file manager
-(void)getFileData
{
    for(NSString *str in fileList){
        [fileAttribute addObject:[FileManager getAttributesOfFile:str]];
    }
}

-(void)touchBut:(UIButton *)but
{
    switch (but.tag) {
        case 10:
        {
            self.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}


-(void)removeAllLists
{
    while (fileList.count != 0) {
        [FileManager removeTmpFile:fileList[0]];
        [fileList removeObjectAtIndex:0];
    }
    [table reloadData];
}












-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return fileList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    //NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    //NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(82,188,174,1.0) selectColor:RGBA(55,55,55,1.0) defaultBack:RGBA(255,255,255,0.9) selectBack:RGBA(245,245,245,0.9)];
    }
    NSString *date = [DataManager dateFormatter:[[FileManager getAttributesOfFile:fileList[indexPath.row]] fileModificationDate]];
    
    
    UIView *lin = [[UIView alloc]initWithFrame:CGRectMake(0,0,5,60)];
    UILabel *fileState = [[UILabel alloc]initWithFrame:CGRectMake(0,0,32,32)];
    fileState.backgroundColor = RGBA(35,30,25,1.0);
    fileState.clipsToBounds = YES;
    fileState.layer.cornerRadius = 16;
    fileState.font =  [UIFont fontWithName:@"icomoon" size:28];
    fileState.textAlignment = NSTextAlignmentCenter;
    fileState.text = @"G";
    fileState.textColor = colorArray[[DataManager beforeDateTimeState:date]];
    cell.accessoryView = fileState;
    
    
    //世界標準時から機種が所属するタイムゾーンにあわせる
    
    if(indexPath.row % 2 == 0){
        lin.backgroundColor = RGBA(82,188,174,1.0);
    }else{
        lin.backgroundColor = RGBA(55,55,55,1.0);
    }
    
    
   
    
    UIView *under = [[UIView alloc]initWithFrame:CGRectMake(0,59.5,cell.frame.size.width,0.5)];
    under.backgroundColor = RGBA(82,188,174,0.3);
    [cell.contentView addSubview:under];
    [cell.contentView addSubview:lin];
    
        
    cell.textLabel.text = fileList[indexPath.row];
    cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
    
        
    cell.detailTextLabel.text = [DataManager beforeDateTimeString:date];
    cell.detailTextLabel.textColor = RGBA(125,125,125,1.0);
    cell.detailTextLabel.font = [UIFont fontWithName:@"Avenir Next" size:11.0];
    
   
    return cell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
#pragma mark -- 指定されたセルの情報をCovertFileViewに伝えて設定画面を開く
    convert.fileName = fileList[indexPath.row];
    [convert fileSetting];
    convert.hidden = NO;
    [self animConvert:YES];
    
}

-(void)animConvert:(BOOL)state
{
    
}

#pragma mark --選択したファイルの処理
-(void)touchProc:(NSInteger)state
{
    switch (state) {
        case 10:
        {//cancel
            NSLog(@"fade");
            convert.hidden = YES;
        }
            break;
            
        default:
            break;
    }
}



-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStylePlain];
    atable.backgroundColor = RGBA(45,45,45,0.6);
    atable.separatorStyle = UITableViewCellSeparatorStyleNone;
    atable.delegate = self;
    atable.dataSource = self;
    atable.tag = tag;
    atable.rowHeight = 60.0;
    atable.showsVerticalScrollIndicator = NO;
    return atable;
}

@end
