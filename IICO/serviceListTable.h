//
//  serviceListTable.h
//  ICOI
//
//  Created by mokako on 2014/07/05.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "CustomCell.h"
#import "ClearToolBar.h"
#import "PlaceHolderTextView.h"
#import "ServiceListTableCell.h"


@protocol ServiceListTableDelegate <NSObject>
-(void)startICOI:(NSString *)service name:(NSString *)display uuid:(NSString *)uuid;
-(void)deleteService:(NSString *)service;
-(void)setComment:(NSString *)service comment:(NSString *)comment;
@end


@interface ServiceListTable : UIView<UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate>
{
    UIToolbar *controll;
    UIView *comment;
    IBOutlet UITableView *service;
    UIView *listColumn;
    NSMutableArray *list;
    IBOutlet UIButton *re;
    IBOutlet UIButton *edit;
    UIButton *set;
    BOOL cedit;
    NSInteger editNumber;
    CGPoint original;
    BOOL isKeybord;
    BOOL openKeyboard;
    UITextField *commentText;
    NSNotificationCenter *center;
    CGRect keyboardRect;
    NSTimeInterval duration;
    BOOL startEdit;
    CGFloat keyboardHeight;
    NSArray *commentHeightContains;
}
@property (nonatomic) FMDatabaseQueue *db;
@property (nonatomic, weak) id<ServiceListTableDelegate>delegate;
-(void)setView;
-(void)reloadServiceList:(NSArray *)ary;
+ (instancetype)view;
@end
