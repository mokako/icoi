//
//  ImageSIzeTable.m
//  ICOI
//
//  Created by mokako on 2015/01/08.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "ImageSIzeTable.h"
#import "CustomCell.h"

@implementation ImageSIzeTable
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* path = [bundle pathForResource:@"Config" ofType:@"plist"];
        NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
        name = [NSArray arrayWithArray:[dic objectForKey:@"ImageSizeGroup"]];
        list = [NSMutableArray arrayWithArray:[dic objectForKey:@"ImageSizeList"]];
        self.hidden = YES;
        [[self layer] setBorderColor:[RGBA(125,129,120,0.6) CGColor]];
        [[self layer] setBorderWidth:0.5];
        self.backgroundColor = RGBA(55,50,45,1.0);
        self.layer.cornerRadius = 2.5;
        self.clipsToBounds = YES;
        [self initializeView];
    }
    return self;
}

-(void)initializeView{
    UIColor *fontColor = RGBA(255,255,255,1.0);
    UIColor *selectColor = RGBA(60,163,156,1.0);
    
    //CGRectMake(0,0,130,self.frame.size.height)
    UITableView *table = [[UITableView alloc]initWithFrame:CGRectZero];
    table.translatesAutoresizingMaskIntoConstraints = NO;
    table.delegate = self;
    table.dataSource = self;
    table.rowHeight = 60.0;
    table.separatorStyle = UITableViewCellSeparatorStyleNone;
    table.backgroundColor = RGBA(255,255,255,0.0);
    [self addSubview:table];
    
    
    UIButton *w = [[UIButton alloc]initWithFrame:CGRectZero];
    w.translatesAutoresizingMaskIntoConstraints = NO;
    w.layer.cornerRadius = 2.5;
    w.backgroundColor = RGBA(50,45,40,1.0);
    w.titleLabel.font = [UIFont fontWithName:@"font-icon-02" size:50.0];
    [w setTitle:@"7" forState:UIControlStateNormal];
    w.titleLabel.textAlignment = NSTextAlignmentCenter;
    [w setTitleColor:selectColor forState:UIControlStateHighlighted];
    [w setTitleColor:fontColor forState:UIControlStateNormal];
    w.tag = 0;
    [w addTarget:self action:@selector(resizeScale:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:w];
    
    
    //CGRectMake(135,125,110,110)
    UIButton *h = [[UIButton alloc]initWithFrame:CGRectZero];
    h.translatesAutoresizingMaskIntoConstraints = NO;
    h.layer.cornerRadius = 2.5;
    h.backgroundColor = RGBA(50,45,40,1.0);
    h.titleLabel.font = [UIFont fontWithName:@"font-icon-02" size:50.0];
    [h setTitle:@"7" forState:UIControlStateNormal];
    h.titleLabel.textAlignment = NSTextAlignmentCenter;
    [h setTitleColor:selectColor forState:UIControlStateHighlighted];
    [h setTitleColor:fontColor forState:UIControlStateNormal];
    h.transform = CGAffineTransformMakeRotation(M_PI/2);
    h.tag = 1;
    [h addTarget:self action:@selector(resizeScale:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:h];
    
    
    
    NSArray *tw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]-5-[button(width)]-10-|"
                                            options:0
                                            metrics:@{@"width":@100}
                                              views:@{@"view":table,@"button":w}];
    [self addConstraints:tw];
    
    NSArray *hw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]-5-[button(width)]-10-|"
                                            options:0
                                            metrics:@{@"width":@100}
                                              views:@{@"view":table,@"button":h}];
    [self addConstraints:hw];
    
    NSArray *bh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[w(height)]-20-[h(height)]-10-|"
                                            options:0
                                            metrics:@{@"height":@100}
                                              views:@{@"w":w,@"h":h}];
    [self addConstraints:bh];
    
    
    NSArray *th =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":table}];
    [self addConstraints:th];
    
    
    
    
}

-(void)resizeScale:(UIButton *)but{
    switch (but.tag) {
        case 0:
        {
            NSInteger l = [list[selectIndex] floatValue];
            [self.delegate resizeScale:l / [list[0] floatValue]];
        }
            break;
        case 1:
        {
            NSInteger l = [list[selectIndex + 1] floatValue];
            [self.delegate resizeScale:l / [list[0] floatValue]];
        }
            break;
        default:
            break;
    }
    
    
}


-(void)displayToggle{
    self.hidden = self.hidden == YES ? NO : YES;
}
-(void)hide{
    self.hidden = YES;
}

-(void)defaultWidthAndHeight:(CGFloat)width height:(CGFloat)height{
    list[0] = [NSNumber numberWithFloat:width];
    list[1] = [NSNumber numberWithFloat:height];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  name.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(255,255,255,1.0) selectColor:RGBA(60,163,156,1.0) defaultBack:RGBA(255,255,255,0.0) selectBack:RGBA(255,255,255,0.2)];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.textLabel.font = [UIFont fontWithName:@"Avenir Next" size:13.0];
    cell.textLabel.textColor = RGBA(255,255,255,1.0);
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    cell.textLabel.text = name[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    selectIndex = indexPath.row  * 2;
    NSInteger l = [list[selectIndex] floatValue];
    [self.delegate resizeScale:l / [list[0] floatValue]];
}







@end
