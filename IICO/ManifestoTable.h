//
//  ManifestTable.h
//  ICOI
//
//  Created by mokako on 2014/02/12.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FileManager.h"
#import "DataManager.h"
#import "sessionController.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@protocol ManifestoTableDelegate<NSObject>
-(NSDictionary *)setManifesto;
-(NSString *)setServiceName;
-(void)resetManifesto;
-(void)changeManifesto:(NSDictionary *)manifesto;
-(void)changeDataList:(NSDictionary *)datalist;
@end

@interface ManifestoTable : UIView<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableDictionary *manifesto;
    NSMutableDictionary *dataList;
    UITableView *table;
    NSString *serviceName;
    SessionController *sessionController;
    NSMutableArray *peers;
    BOOL AdvertiserFlag;

}
-(void)reloadManifestoTable;
-(void)setMember:(SessionController *)session peers:(NSArray *)peerIDs dataList:(NSDictionary *)datalist;
-(void)review;
@property (nonatomic, weak) id <ManifestoTableDelegate> delegate;
@end
