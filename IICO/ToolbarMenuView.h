//
//  ToolbarMenuView.h
//  ICOI
//
//  Created by mokako on 2014/05/17.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "UIFlexibleLabel.h"
#import "ClearToolBar.h"
#import "UIColor+Hex.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@protocol ToolbarMenuViewDelegate <NSObject>
-(void)tapTab:(NSInteger)state;
-(void)touchBut:(NSInteger)state;
@end

@interface ToolbarMenuView : UIView
{
    CAGradientLayer *gradient;
    UIToolbar *bar;
    UIColor *defaultColor;
    UIColor *selectColor;
    NSInteger count;
    UIFlexibleLabel *label;
    UILabel *title;
    
    UIButton *menu;
    
    
    UIBarButtonItem *a,*b,*c,*d,*e;
    
}
-(void)menuEditable:(NSInteger)type title:(NSString *)titleText;
-(void)update;
@property (nonatomic, weak)id <ToolbarMenuViewDelegate> delegate;
@end
