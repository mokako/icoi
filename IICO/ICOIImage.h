//
//  ImageTreat.h
//  ICOI
//
//  Created by mokako on 2015/06/19.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <QuartzCore/QuartzCore.h>
#import "FileManager.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@interface ICOIImage : NSObject

+(CGPoint)centerPoint:(CGPoint)pointA pointB:(CGPoint)pointB;
+(CGFloat)distanceWithPoint:(CGPoint)pointA pointB:(CGPoint)pointB;
+(UIImage *)createImage:(CGRect)rect color:(UIColor *)color;
+(UIImage *)resizeImage:(UIImage *)base scale:(CGFloat)scale;
+(UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize;
+(UIImage *)rotateImage:(UIImage *)image rotate:(CGFloat)angle;
+(UIImage *)imageFromView:(UIView *)view;
+(UIImage *)buttonImage:(CGRect)rect text:(NSString *)text font:(UIFont *)font color:(UIColor *)color background:(UIColor *)back;
+(UIImage *)drawTail:(UIColor *)color; //CGSize 23 * 30
+(UIImage *)convertToImage:(NSString *)text font:(UIFont *)font;
+(UIImage *)thumbnail:(UIImage *)image size:(CGFloat)size;
+(NSArray *)setThumbnailLayerImagesFromDatalist:(NSString *)serviceName dataInfo:(NSArray *)dataInfo size:(NSInteger)size;
+ (UIImage *)imageWithColor:(UIColor *)color;
+(UIButton *)setIcon:(UIFont *)font icon:(NSString *)icon title:(NSString *)title color:(UIColor *)color;
@end
