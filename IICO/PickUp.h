//
//  PickUp.h
//  ICOI
//
//  Created by mokako on 2016/01/03.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "PickUpCell2.h"
#import "ServiceManager.h"

#define slideMinHeight 10

@interface PickUp : UIView<UITableViewDelegate,UITableViewDataSource>
{
    NSMutableArray *add;
    
    CGPoint oldPoint;
    BOOL isSlide;
    BOOL isOpen;
    UIView *superView;
    NSArray *vartical;
    ServiceManager *sb;
    BOOL animation;
    
    
    PickUpCell2 *dummyCell;
    CGFloat fixedHeight;
    CGFloat fixedWidth;
    UITextView *dummyTextView;
    NSMutableArray *cellHeight;
}
@property (weak, nonatomic) IBOutlet UITableView *table;
+(instancetype)view;
-(void)setLayout:(NSArray *)layout;
@end
