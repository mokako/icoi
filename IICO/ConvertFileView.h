//
//  ConvertFileView.h
//  ICOI
//
//  Created by mokako on 2014/08/24.
//  Copyright (c) 2014年 moca. All rights reserved.
//
/*
 コンバート画面では画像かの為に設定をユーザーに指定させコンバートした内容をユーザーに提示します。
 その上でその情報を使うかどうか決め使うなら各サービスフォルダに保存します。その際名前も強制で付けてもらいます。
 （名前は付けてもらいますがあくまで便宜上の物で実際にはランダムで決めた名前がファイル名として配置されます。）
 コンバート内容を保持を望むならtemporaryに保持し、要らないなら削除してください。
 
 
 
 */

#import <UIKit/UIKit.h>
#import "PageListTable.h"
#import "CustomCell.h"
#import "ClearToolBar.h"
#import "FileManager.h"
//TEST
#import "TextContainerView.h"


@protocol ConvertFileViewDelegate <NSObject>

-(void)touchProc:(NSInteger)state;
@end


@interface ConvertFileView : UIView<UITableViewDataSource, UITableViewDelegate,UIScrollViewDelegate,NSLayoutManagerDelegate,PageListTableDelegate>
{
    UIView *foundationView;
    UIScrollView *previewField;
    UITableView *table;
    NSArray *imageArray;
    NSDictionary *extensionDictionary;
    UIToolbar *tool;
    NSMutableArray *strArray;
    UIPageControl *pageControl;
    PageListTable *pageTable;
    UILabel *pages;
    
}

@property (nonatomic) NSMutableDictionary *fileState;
@property (nonatomic) NSString *fileName;



@property (nonatomic, weak) id<ConvertFileViewDelegate>delegate;

-(void)fileSetting;
-(void)review;
@end
