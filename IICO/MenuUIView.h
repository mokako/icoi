/*
 MenuUIView.h
 MenuWindow
 
 Created by moca on 2014/02/10.
 Copyright (c) 2014年 mokako. All rights reserved.
 
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions
 are met:
 1. Redistributions of source code must retain the above copyright
 notice, this list of conditions and the following disclaimer.
 2. Redistributions in binary form must reproduce the above copyright
 notice, this list of conditions and the following disclaimer in the
 documentation and/or other materials provided with the distribution.
 
 
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR(S) ``AS IS'' AND ANY EXPRESS OR
 IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES
 OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE AUTHOR(S) BE LIABLE FOR ANY DIRECT, INDIRECT,
 INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF
 THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "UIColor+Hex.h"
@import GoogleMobileAds;

#define DescriptionNumber 7//説明画面番号（変更するならここも変えて下さい）
#define LogNumbar 5
@protocol MenuUIViewDelegate<NSObject>
-(void)receiveTableRow:(NSIndexPath *)indexPath;
@end

@interface MenuUIView : UIView<UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
{
    UICollectionView *collectview;
    UICollectionViewFlowLayout *layout;
    NSArray *titleArray,*imgArray,*fontArray;
    UIColor *labelColor;
    UIColor *background;
    BOOL isFirst;
    NSIndexPath *oldSelected;
    UIColor *backGround;
    UIColor *select;
    UIColor *selectTitle;
    UIColor *defaultColor;
    CGFloat layoutWidth;
    CGFloat layoutHeight;
    CGFloat iconSize;
    CGFloat textSize;
    NSInteger scale;
    CAGradientLayer *gradient;
    BOOL isOrientation;
    CGFloat height;
    
    GADBannerView *banner;
}
@property (nonatomic, weak) id <MenuUIViewDelegate> delegate;
-(void)update;
-(void)stepPaint;
-(void)stepLayer;
-(void)stepAlbum;
@end