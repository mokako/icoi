//
//  MessageBoardConfig.h
//  ICOI
//
//  Created by mokako on 2016/02/10.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>


#define MAX_HEIGHT 70


//message type number
typedef NS_ENUM(NSInteger, messageType) {
    message_type_comment = 0, //
    message_type_path  = 1, // path
    message_type_rename = 2
};




@interface MessageBoardConfig : NSObject
{
    
    NSString *a,*b,*c;
    NSString *hash;
}


//set
-(void)setString:(messageType)type text:(NSString *)string;
//get
-(NSString *)getString:(messageType)type;
//delete
-(void)deleteString:(messageType)type;


//hash
-(void)setHash:(NSString *)_hash;
-(NSString *)getHash;

@end
