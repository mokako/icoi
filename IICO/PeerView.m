
//
//  PeerView.m
//  ICOI
//
//  Created by mokako on 2014/04/23.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "PeerView.h"
#import "CustomCell.h"



@implementation PeerView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        connectPeers = [[NSMutableArray alloc]init];
        peerData = [[NSMutableDictionary alloc]init];
        
        self.hidden = YES;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendPeer" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(setServiceManager:) name:@"sendServiceManagerSeed" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    UINib *nib = [UINib nibWithNibName:@"PeerViewCell" bundle:nil];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.estimatedRowHeight = 76.0;
    [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];
    
    popup = [[PopupView alloc]initWithFrame:CGRectZero];
    popup.translatesAutoresizingMaskIntoConstraints = NO;
    [popup setMountColor:RGBA(145,145,145,0.2)];
    [self addSubview:popup];
    NSArray *popw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":popup}];
    [self addConstraints:popw];
    
    NSArray *poph =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":popup}];
    [self addConstraints:poph];
    
    
    //popup view
    UIView *po = [[UIView alloc]initWithFrame:CGRectMake(0,0,300,160)];
    po.layer.cornerRadius = 5;
    po.backgroundColor = RGBA(255,255,255,1.0);
    title = [[UILabel alloc]initWithFrame:CGRectMake(5,5,290,30)];
    title.layer.cornerRadius = 5;
    title.clipsToBounds = YES;
    title.textAlignment = NSTextAlignmentCenter;
    title.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    title.textColor = RGBA(255,255,255,1.0);
    
    
    UIButton *can = [[UIButton alloc]initWithFrame:CGRectMake(265,5,30,30)];
    can.titleLabel.font = [UIFont fontWithName:@"point" size:24];
    [can setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [can setTitle:@"9" forState:UIControlStateNormal];
    [can addTarget:self action:@selector(hidePopup) forControlEvents:UIControlEventTouchUpInside];
    
    
    pName = [[UILabel alloc]initWithFrame:CGRectMake(10,40,280,30)];
    pName.font = [UIFont fontWithName:@"Avenir Next" size:11.0];
    
    
    pID = [[UILabel alloc]initWithFrame:CGRectMake(10,70,280,20)];
    pID.font = [UIFont fontWithName:@"Avenir Next" size:11.0];
    
    setButton = [[UIButton alloc]initWithFrame:CGRectMake(po.frame.size.width / 2 - 35,120,70,30)];
    setButton.titleLabel.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    [setButton setTitle:@"OK" forState:UIControlStateNormal];
    [setButton setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [setButton addTarget:self action:@selector(set) forControlEvents:UIControlEventTouchUpInside];
    
    
    [po addSubview:title];
    [po addSubview:can];
    [po addSubview:pName];
    [po addSubview:pID];
    [po addSubview:setButton];
    
    
    [popup setView:po];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



-(void)setServiceManager:(NSNotification *)center{
    sb = [center userInfo][@"manager"];
    pcon = [[PeerController alloc]init:[sb getService]];
}
-(void)set{
    if([pcon checkPeer:setName]){
        [pcon removePeer:setName];
    }else{
        [pcon addPeer:setName];
    }
    [popup hide];
    
    [sb loadPeerController];
    
    [self peerSet];
}

-(void)hidePopup{
    [popup hide];
}

-(void)setData:(NSArray *)founds connects:(NSDictionary *)connects
{
    //connectsを配列化
    NSArray *con = [connects allKeys];
    [connectPeers addObjectsFromArray:con];
    connectPeers = [NSMutableArray arrayWithArray:[[NSSet setWithArray:connectPeers] allObjects]];
    [peerData setDictionary:connects];
    dispatch_async(dispatch_get_main_queue(), ^{
        [self.table reloadData];
    });
}

-(void)peerSet{
    [pcon loadPeer];
    [self.table reloadData];
}

/*
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *backgroundView = [[UIView alloc] initWithFrame:CGRectMake(0,0,table.frame.size.width,0)];
    return backgroundView;
}
-(CGFloat) tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0;
}
*/
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  connectPeers.count;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    PeerViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if([pcon checkPeer:connectPeers[indexPath.row]]){
        cell.ban.hidden = NO;
    }else{
        cell.ban.hidden = YES;
    }
    cell.name.text = peerData[connectPeers[indexPath.row]][@"name"];
    cell.uid = connectPeers[indexPath.row];
    [cell setNetworkState:[peerData[connectPeers[indexPath.row]][@"state"] integerValue]];

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if([pcon checkPeer:connectPeers[indexPath.row]]){
        title.text = NSLocalizedString(@"deleteRejectionPeer",@"");
        title.backgroundColor = RGBA(0,232,51,1.0);
        setButton.backgroundColor = RGBA(0,232,51,1.0);
        
    }else{
        title.text = NSLocalizedString(@"addRejectionPeer",@"");
        title.backgroundColor = RGBA(242,56,39,1.0);
        setButton.backgroundColor = RGBA(242,56,39,1.0);
    }
    pName.text = [NSString stringWithFormat:@"Peer Name : %@",peerData[connectPeers[indexPath.row]][@"name"]];
    pID.text = [NSString stringWithFormat:@"Peer ID : %@",connectPeers[indexPath.row]];
    setName = connectPeers[indexPath.row];
    [popup show];
}

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //reload
        {
            [self setData:nil connects:[center userInfo][@"list"]];
        }
            break;
        case 2:
        {
        }
            break;
        default:
            break;
    }
}


@end
