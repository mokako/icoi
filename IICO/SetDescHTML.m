//
//  SetDescHTML.m
//  ICOI
//
//  Created by mokako on 2015/07/11.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "SetDescHTML.h"

@implementation SetDescHTML
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        isOpen = NO;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.back.layer.cornerRadius = 15.0;
    web.delegate = self;
    NSString *_path = [[NSBundle mainBundle] pathForResource:@"temp" ofType:@"html"];
    [web loadRequest:[NSURLRequest requestWithURL:[NSURL fileURLWithPath:_path]]];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}
+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}
//ja,en
- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    NSString *lang = [[NSLocale preferredLanguages] objectAtIndex:0];
    NSString *textPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@-%@",urlBase,lang] ofType:@"txt"];
    NSError *error;
    NSString *text = [[NSString alloc] initWithContentsOfFile:textPath encoding:NSUTF8StringEncoding error:&error];
    if(text != NULL){
        [web stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"getLang('%@')",text]];
    }else{
        NSString *textPath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@-en",urlBase] ofType:@"txt"];
        NSString *text = [[NSString alloc] initWithContentsOfFile:textPath encoding:NSUTF8StringEncoding error:&error];
        [web stringByEvaluatingJavaScriptFromString:[NSString stringWithFormat:@"getLang('%@')",text]];
    }
}

-(void)setConstraint:(NSArray *)_layout{
    layout = _layout;
    [self fadeOut];
}


-(void)setHtmlSetting:(NSString *)htmlUrl{
    urlBase = htmlUrl;
    [web reload];
}

-(void)fadeIn
{
    [[self superview] removeConstraints:layout];
    layout =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self}];
    [[self superview] addConstraints:layout];
    [UIView animateWithDuration:0.5 animations:^{
        isOpen = YES;
        [[self superview] layoutIfNeeded];
        self.back.transform = CGAffineTransformMakeRotation(M_PI);
    }];
    
}

-(IBAction)fadeOut:(id)sender{
    [self fadeOut];
}

-(void)fadeOut{
    [[self superview] removeConstraints:layout];
    layout =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]-slide-|"
                                            options:0
                                            metrics:@{@"width":@([self superview].bounds.size.width),@"slide":@(-[self superview].bounds.size.width)}
                                              views:@{@"view":self}];
    [[self superview] addConstraints:layout];
    [UIView animateWithDuration:0.5 animations:^{
        isOpen = NO;
        [[self superview] layoutIfNeeded];
        self.back.transform = CGAffineTransformIdentity;
    }];
}

@end
