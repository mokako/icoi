//
//  ServiceManagerAssist.m
//  ICOI
//
//  Created by mokako on 2016/01/16.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ServiceManagerAssist.h"
#import "ICOI.h"

@implementation ServiceManagerAssist



+(NSDictionary *)adjustDictionary:(NSDictionary *)oldDic{
    NSMutableDictionary *dic = [oldDic mutableCopy];
    if(![dic[@"pauid"] isEqual:[NSNull null]]){
        if([dic[@"pauid"] length] == 0){
            //旧の形式を新しい形式に変更
            dic[@"pauid"] = [NSNull null];
            dic[@"paid"] = [dic[@"paid"] length] == 0 ? [NSNull null] : dic[@"paid"];
            dic[@"paname"] = [dic[@"paname"] length] == 0 ? [NSNull null] : dic[@"paname"];
            dic[@"date"] = [NSNumber numberWithDouble:[DateFormatter julianDate:[DateFormatter localTime:dic[@"date"]]]];
        }else{
            dic[@"pauid"] = dic[@"pauid"];
        }
    }
    return dic;
}



+(NSArray *)initIndexPath:(NSArray *)ary{
    NSMutableArray *new = [@[] mutableCopy];
    for(NSDictionary *dic in ary){
        [new addObject:dic[@"nid"]];
    }
    return new;
}
@end
