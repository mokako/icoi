//
//  SketchListView.h
//  ICOI
//
//  Created by mokako on 2015/10/17.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ServiceManager.h"
//#import "CommentWindow.h"
#import "NormalList.h"


@interface SketchListView : UIView
{
    
    
    UIView *mainView;
    CGPoint beginPoint;
    NSString *colorHex;
    NSArray *horizon;
    
    ServiceManager *sb;
    NormalList *nlist;
    BOOL slide;
    BOOL animation;
    UIColor *st0,*st1,*st2,*st3;
    
    CAGradientLayer *gradient;
    BOOL tap;
    UIButton *but1;
    
    BOOL drawHook;
    CGFloat sketchListX;
}
@property (weak, nonatomic) IBOutlet UIView *base;

+(instancetype)view;

-(void)displayToggle;
-(void)fadeOut;

-(void)setConstraint:(NSArray *)constraints;
@end
