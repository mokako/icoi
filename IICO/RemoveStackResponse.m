//
//  RemoveStackResponse.m
//  ICOI
//
//  Created by mokako on 2015/01/18.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "RemoveStackResponse.h"

@implementation RemoveStackResponse
- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.hidden = YES;
        self.alpha = 0.0;
        self.clipsToBounds = NO;
        
        
        
        
        fontColor = RGBA(105,105,105,1.0);
        selectColor = RGBA(205,205,205,1.0);
        fontSize = 15.0;
        [self initializeView];
        
    }
    return self;
}


-(void)initializeView{
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:visualEffectView];
    NSArray *vw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":visualEffectView}];
    [self addConstraints:vw];
    
    NSArray *vh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":visualEffectView}];
    [self  addConstraints:vh];
    
    
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(hidden)];
    // ダブルタップ
    tapGesture.numberOfTapsRequired = 1;
    
    // ビューにジェスチャーを追加
    [back addGestureRecognizer:tapGesture];

    
    
    UIButton *removeStack = [[UIButton alloc]initWithFrame:CGRectZero];
    removeStack.tag = 0;
    [removeStack setTitle:NSLocalizedString(@"RemoveS",@"") forState:UIControlStateNormal];
    [removeStack setTitleColor:RGBA(54,50,45,1.0) forState:UIControlStateNormal];
    removeStack.titleLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:fontSize];
    removeStack.backgroundColor = RGBA(255,255,255,0.3);
    removeStack.layer.cornerRadius = 20.0;
    [removeStack addTarget:self action:@selector(eraserBut:) forControlEvents:UIControlEventTouchUpInside];
    removeStack.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:removeStack];
    
    
    
    
    
    UIButton *removeAll = [[UIButton alloc]initWithFrame:CGRectZero];
    removeAll.tag = 1;
    [removeAll setTitle:NSLocalizedString(@"RemoveA",@"") forState:UIControlStateNormal];
    [removeAll setTitleColor:RGBA(54,50,45,1.0) forState:UIControlStateNormal];
    removeAll.titleLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:fontSize];
    removeAll.backgroundColor = RGBA(255,255,255,0.3);
    removeAll.layer.cornerRadius = 20.0;
    [removeAll addTarget:self action:@selector(eraserBut:) forControlEvents:UIControlEventTouchUpInside];
    removeAll.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:removeAll];
    
    
    UIButton *cancel = [[UIButton alloc]initWithFrame:CGRectZero];
    [cancel setTitle:NSLocalizedString(@"Cancel",@"") forState:UIControlStateNormal];
    [cancel setTitleColor:RGBA(254,250,245,1.0) forState:UIControlStateNormal];
    cancel.titleLabel.font = [UIFont fontWithName:@"AppleSDGothicNeo-Medium" size:fontSize];
    cancel.backgroundColor = RGBA(242,56,39,1.0);
    cancel.layer.cornerRadius = 20.0;
    [cancel addTarget:self action:@selector(hidden) forControlEvents:UIControlEventTouchUpInside];
    cancel.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:cancel];
    
    
    NSArray *w0 =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(200)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":removeStack}];
    [self addConstraints:w0];
    NSArray *w1 =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(200)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":removeAll}];
    [self addConstraints:w1];
    NSArray *w2 =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(200)]"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":cancel}];
    [self addConstraints:w2];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:removeStack  attribute:NSLayoutAttributeCenterX  relatedBy:NSLayoutRelationEqual  toItem:self  attribute:NSLayoutAttributeCenterX  multiplier:1  constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:removeAll  attribute:NSLayoutAttributeCenterX  relatedBy:NSLayoutRelationEqual  toItem:self  attribute:NSLayoutAttributeCenterX  multiplier:1  constant:0]];
    [self addConstraint:[NSLayoutConstraint constraintWithItem:cancel  attribute:NSLayoutAttributeCenterX  relatedBy:NSLayoutRelationEqual  toItem:self  attribute:NSLayoutAttributeCenterX  multiplier:1  constant:0]];
    
    
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view0(40)]-25-[view1(40)]-25-[view2(40)]-10-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view0":removeStack,@"view1":removeAll,@"view2":cancel}];
    [self  addConstraints:h];
    
    
    
}



-(void)display{
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL finished){
        back.hidden = NO;
    }];

}


-(void)hidden{
    
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished){
        self.hidden = YES;
        back.hidden = YES;
    }];
}

-(void)eraserBut:(UIButton *)but{
    [self.delegate eraserBut:but];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}






@end
