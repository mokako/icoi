//
//  DescriptionSetterCell.h
//  ICOI
//
//  Created by mokako on 2016/03/31.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DescriptionSetterCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *label;
@property (weak, nonatomic) IBOutlet UILabel *icon;
@end
