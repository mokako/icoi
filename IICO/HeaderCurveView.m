//
//  HeaderCurveView.m
//  ICOI
//
//  Created by mokako on 2016/03/26.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "HeaderCurveView.h"

@implementation HeaderCurveView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.color = [UIColor colorWithHex:@"EFEFF4"];
        self.angle = 10.0;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    CGContextRef c = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(c, self.color.CGColor);
    CGContextMoveToPoint(c, CGRectGetMinX(rect), CGRectGetMaxY(rect));
    CGContextAddArcToPoint(c, CGRectGetMinX(rect), CGRectGetMinY(rect), CGRectGetMinX(rect) + 20, CGRectGetMinY(rect), self.angle);
    
    CGContextAddArcToPoint(c, CGRectGetMaxX(rect), CGRectGetMinY(rect), CGRectGetMaxX(rect), CGRectGetMidY(rect), self.angle);
    CGContextAddLineToPoint(c, CGRectGetMaxX(rect), CGRectGetMaxY(rect));

    CGContextClosePath(c);
    CGContextFillPath(c);

}

@end
