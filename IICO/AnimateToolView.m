//
//  AnimateToolView.m
//  ICOI
//
//  Created by mokako on 2014/05/21.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "AnimateToolView.h"

@implementation AnimateToolView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        sw = NO;
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)review
{
    
    bar.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
}

-(void)initializeView
{
    self.backgroundColor = RGBA(35,30,25,1.0);

    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    bar = [[ClearToolBar alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    [self addSubview:bar];
    
    //bar.backgroundColor = RGBA(45,45,45,1.0);
    
    
    
    UIButton *un = [[UIButton alloc]initWithFrame:CGRectMake(0,0,BUTSIZE,BUTSIZE)];
    /*
    [[un layer] setBorderColor:[RGBA(200,200,200,1.0) CGColor]];
    [[un  layer] setBorderWidth:2.0];
    un.layer.cornerRadius = BUTSIZE / 2;
     */
    un.titleLabel.font = [UIFont fontWithName:@"icomoon" size:24];
    [un setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [un setTitle:@"3" forState:UIControlStateNormal];
    un.tag = 20;
    [un addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *a = [[UIBarButtonItem alloc]initWithCustomView:un];
    
    
    /*
    pb = [[UIButton alloc]initWithFrame:CGRectMake(0,0,60,60)];
    pb.titleLabel.font = [UIFont fontWithName:@"kip" size:28];
    [pb setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [pb setTitle:@"1" forState:UIControlStateNormal];
    pb.tag = 21;
    [pb addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:pb];
    */
    
    NSArray *arr = [NSArray arrayWithObjects:@"Play", @"Set", @"Save",@"Del", nil];
    seg = [[UISegmentedControl alloc] initWithItems:arr];
    seg.frame = CGRectMake(0, 0, 200, 40);
    seg.tintColor = RGBA(255,255,255,1.0);
    seg.selectedSegmentIndex = 0;
    [seg setTitleTextAttributes:[NSDictionary dictionaryWithObject:[UIFont fontWithName:@"Avenir Next" size:14] forKey:NSFontAttributeName] forState:UIControlStateNormal];
    [seg addTarget:self action:@selector(chengeSegment:) forControlEvents:UIControlEventValueChanged];
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:seg];
    
    
    
    UIButton *pl = [[UIButton alloc]initWithFrame:CGRectMake(0,0,BUTSIZE,BUTSIZE)];
    pl.titleLabel.font = [UIFont fontWithName:@"icomoon" size:24];
    [pl setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [pl setTitle:@"!" forState:UIControlStateNormal];
    pl.tag = 22;
    [pl addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *c = [[UIBarButtonItem alloc]initWithCustomView:pl];
    
    
    bar.items = [NSArray arrayWithObjects:gap,a,b,c,gap,nil];
    
    
    lineView = [[UIView alloc]initWithFrame:CGRectMake(0,0,0,3)];
    [self addSubview:lineView];
}

-(void)stopSegment
{
    for (int i = 0; i < seg.numberOfSegments;  i++) {
        [seg setEnabled:NO forSegmentAtIndex:i];
    }
}

-(void)startSegment
{
    for (int i = 0; i < seg.numberOfSegments;  i++) {
        [seg setEnabled:YES forSegmentAtIndex:i];
    }
    seg.selectedSegmentIndex = 0;
}


-(void)touchBut:(UIButton *)sender
{
    if(sender.tag == 21){
        if(sw){//停止中
            [sender setTitle:@"1" forState:UIControlStateNormal];
            sw = NO;
        }else{//再生可能状態
            [sender setTitle:@"2" forState:UIControlStateNormal];
            sw = YES;
        }
    }
    [self.delegate setEditTab:sender.tag];
}


-(void)chengeSegment:(UISegmentedControl *)segment
{//パスの処理方法をSketchViewに報告します。
    [self.delegate changeState:segment.selectedSegmentIndex];
    //[self menuEditable:3 title:<#(NSString *)#>]
}

-(void)setLineAnimation:(NSInteger)state
{
    NSArray *ary = [[NSArray alloc]initWithObjects:RGBA(2,170,176,1.0),RGBA(230,172,39,1.0),RGBA(191,77,40,1.0), nil];
    lineView.backgroundColor = ary[state];
    [UIView animateWithDuration:0.5f
                     animations:^{
                         lineView.frame = CGRectMake(0,0,self.frame.size.width,3);
                     }
                     completion:^(BOOL finished){
                         lineView.frame = CGRectMake(0,0,0,3);
                     }];
}


-(void)endAnimate
{
    [pb setTitle:@"1" forState:UIControlStateNormal];
    sw = NO;
}


@end
