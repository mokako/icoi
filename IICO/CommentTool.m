//
//  CommentTool.m
//  ICOI
//
//  Created by mokako on 2014/12/27.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "CommentTool.h"



@implementation CommentTool
- (id)init
{
    self = [super init];
    if (self) {
        self.frame = CGRectMake(0,0,192,48);
        self.backgroundColor = RGBA(35,30,25,1.0);
        self.layer.cornerRadius = 5;
        self.hidden = YES;
        isLock = NO;
        [self initializeView];
    }
    return self;
}


-(void)initializeView{
    
    
    //previous button
    UIButton *pre = [[UIButton alloc]initWithFrame:CGRectMake(0,0,48,48)];
    pre.titleLabel.font = [UIFont fontWithName:@"point" size:20];
    pre.titleLabel.textAlignment = NSTextAlignmentCenter;
    [pre setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [pre setTitle:@"x" forState:UIControlStateNormal];
    pre.tag = 0;
    [pre addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:pre];
    

    //lock
    lock = [[UIButton alloc]initWithFrame:CGRectMake(48,0,48,48)];
    lock.titleLabel.font = [UIFont fontWithName:@"icomoon" size:18];
    lock.titleLabel.textAlignment = NSTextAlignmentCenter;
    [lock setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [lock setTitle:@"%" forState:UIControlStateNormal];
    lock.tag = 2;
    [lock addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:lock];
    
    
    //up view
    UIButton *up = [[UIButton alloc]initWithFrame:CGRectMake(96,0,48,48)];
    up.titleLabel.font = [UIFont fontWithName:@"font-icon-00" size:20];
    up.titleLabel.textAlignment = NSTextAlignmentCenter;
    [up setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [up setTitle:@"3" forState:UIControlStateNormal];
    up.tag = 3;
    [up addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:up];
    
    //next
    UIButton *nex = [[UIButton alloc]initWithFrame:CGRectMake(144,0,48,48)];
    nex.titleLabel.font = [UIFont fontWithName:@"point" size:20];
    nex.titleLabel.textAlignment = NSTextAlignmentCenter;
    [nex setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    [nex setTitle:@"A" forState:UIControlStateNormal];
    nex.tag = 1;
    [nex addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    [nex addTarget:self action:@selector(longTouch:) forControlEvents:UIControlEventTouchDown];
    [self addSubview:nex];
    
}


 -(void)touchBut:(UIButton *)but{
     [self.delegate commentBut:but];
     switch (but.tag) {
         case 2:
         {
             if(isLock){
                 but.titleLabel.font = [UIFont fontWithName:@"icomoon" size:18];
                 [but setTitle:@"%" forState:UIControlStateNormal];
                 isLock = NO;
             }else{
                 but.titleLabel.font = [UIFont fontWithName:@"font-icon-03" size:18];
                 [but setTitle:@"e" forState:UIControlStateNormal];
                 isLock = YES;
             }
         }
             break;
         case 3:
         {
             [self performSelector:@selector(hideCommentTool) withObject:nil afterDelay:1];
         }
             break;
         default:
             break;
     }
     
     
}
-(void)update{
    self.hidden = YES;
    lock.titleLabel.font = [UIFont fontWithName:@"icomoon" size:18];
    [lock setTitle:@"%" forState:UIControlStateNormal];
    isLock = NO;
}
-(void)hideCommentTool{
    self.hidden = YES;
    lock.titleLabel.font = [UIFont fontWithName:@"icomoon" size:18];
    [lock setTitle:@"%" forState:UIControlStateNormal];
    isLock = NO;
}


-(void)longTouch:(UIButton *)but{
    //skipTarget = but.tag;
    //[self performSelector:@selector(skipComment) withObject:nil afterDelay:2];
}
-(void)skipComment{
    //[self displayComment:(BOOL)skipTarget];
    //[self performSelector:@selector(skipComment) withObject:nil afterDelay:0.2];
}

@end
