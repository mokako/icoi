//
//  NormalHeaderView.h
//  ICOI
//
//  Created by mokako on 2016/03/17.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface NormalHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIButton *undo;
@property (weak, nonatomic) IBOutlet UIButton *titleButton;
+(instancetype)view;
-(void)setButtonColor:(BOOL)state;
@end
