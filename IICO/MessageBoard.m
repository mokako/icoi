//
//  MessageBoard.m
//  ICOI
//
//  Created by mokako on 2015/12/28.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "MessageBoard.h"

@implementation MessageBoard

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.translatesAutoresizingMaskIntoConstraints = NO;
        fontColor = [UIColor colorWithHex:@"FFFFFF" alpha:1.0];
        selectFont = [UIColor colorWithHex:@"99C630" alpha:0.8];
        selectColor = [UIColor colorWithHex:@"99C630"];
        unKeep = [UIColor colorWithHex:@"99C630"];
        keep = [UIColor colorWithHex:@"99C630"];
        offset = CGSizeMake(0,0);
        radius = 2.0f;
        selectPath = @{@"uid":[NSNull null],@"path":@""};
        
        selectDic = [@{@"mess":@{@"uid":[NSNull null],@"path":@""},@"line":@{@"uid":[NSNull null],@"path":@""}} mutableCopy];
        
        self.layer.borderColor = [UIColor colorWithHex:@"cccccc"].CGColor;
        self.layer.borderWidth = 0.5;
        duration = 0.5;
        oldState = 0;
        msc = [[MessageBoardConfig alloc]init];
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendMessageBoard" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    [self initView];

    self.textContiner.layer.borderColor = [UIColor colorWithHex:@"D1D6D7"].CGColor;
    self.textContiner.layer.borderWidth = 0.5;
    self.send.titleLabel.adjustsFontSizeToFitWidth = YES;
    self.send.titleLabel.minimumScaleFactor = 0.5f;
    [self.send setTitle:NSLocalizedString(@"mess_board_send", @"Send") forState:UIControlStateNormal];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


-(void)layoutSubviews{
    [super layoutSubviews];
}


-(void)initView{
    [self setTopView];
    [self setUnderView];
}



-(void)setLayout:(NSArray *)layout{
    mbh = layout;
    [self reConstraints];
}

#pragma mark - top view
-(void)setTopView{
    
    
    [self.down addTarget:self action:@selector(close) forControlEvents:UIControlEventTouchUpInside];
    [self.send addTarget:self action:@selector(sendMessage) forControlEvents:UIControlEventTouchUpInside];
    [self.mess addTarget:self action:@selector(startEdit) forControlEvents:UIControlEventTouchUpInside];
    [self.keepTag addTarget:self action:@selector(setTag) forControlEvents:UIControlEventTouchUpInside];
    
    [self initToolBar];
    [self setToolbarItem:0];
}
/*
-(void)initContiner{
    [self removeConstraints:];
    textView.text = @"A";
    CGSize content = [textView sizeThatFits:CGSizeMake(textView.contentSize.width,9999)];
    mh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tv(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:content.height]}
                                              views:@{@"tv":textView}];
    [self addConstraints:mh];
    textView.text = @"";
}
*/





-(void)initToolBar{
    CGRect rec1 = CGRectMake(0,0,80,30);

    
    //title
    
    
    
    //title
    NormalButton *e = [NormalButton view];
    e.tag = 2;
    [e addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [e setTitle:NSLocalizedString(@"mess_board_title",@"Title") forState:UIControlStateNormal];
    [e setTitleColor:fontColor forState:UIControlStateNormal];
    e.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    e.titleLabel.adjustsFontSizeToFitWidth = YES;
    ca = [[UIBarButtonItem alloc]initWithCustomView:e];
    
    //sub title
    NormalButton *f = [NormalButton view];
    f.tag = 3;
    [f addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [f setTitle:NSLocalizedString(@"mess_board_subtitle",@"Sub title") forState:UIControlStateNormal];
    [f setTitleColor:fontColor forState:UIControlStateNormal];
    f.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    f.titleLabel.adjustsFontSizeToFitWidth = YES;
    cb = [[UIBarButtonItem alloc]initWithCustomView:f];
    
    
    NormalButton *d = [NormalButton view];
    d.tag = 4;
    [d addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [d setTitle:NSLocalizedString(@"mess_board_message",@"Message") forState:UIControlStateNormal];
    [d setTitleColor:fontColor forState:UIControlStateNormal];
    d.titleLabel.adjustsFontSizeToFitWidth = YES;
    d.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    //d.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    cc = [[UIBarButtonItem alloc]initWithCustomView:d];
    
    //add path
    NormalButton *g =  [NormalButton view];
    g.tag = 1;
    [g addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [g setTitle:NSLocalizedString(@"mess_board_path",@"Add path") forState:UIControlStateNormal];
    [g setTitleColor:fontColor forState:UIControlStateNormal];
    g.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    g.titleLabel.adjustsFontSizeToFitWidth = YES;
    //
    cd = [[UIBarButtonItem alloc]initWithCustomView:g];


    
    UIButton *h = [[UIButton alloc]initWithFrame:rec1];
    h.tag = 5;
    [h setTitleColor:fontColor forState:UIControlStateNormal];
    [h setTitle:@"Cancel" forState:UIControlStateNormal];
    [h setTitleColor:[UIColor colorWithHex:@"454545"] forState:UIControlStateNormal];
    h.titleLabel.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    [h setTitleColor:fontColor forState:UIControlStateNormal];
    [h addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    h.layer.cornerRadius = 15;
    h.layer.borderColor = [UIColor colorWithHex:@"CFCFCF"].CGColor;
    h.layer.borderWidth = 0.5;
    mc = [[UIBarButtonItem alloc]initWithCustomView:h];
    
    UILabel *i = [[UILabel alloc]initWithFrame:CGRectMake(0,0,160,40)];
    i.textColor = fontColor;
    i.font = [UIFont fontWithName:@"AvenirNextCondensed-Medium" size:13];
    i.text = @"Rename comment.";
    ml = [[UIBarButtonItem alloc]initWithCustomView:i];
    
    
    gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    gap.width = 40;
}



-(void)setToolbarItem:(NSInteger)num{
    switch (num) {
        case 0:
        {
            
            oldState = num;
            [self clearBut];
            self.ct.items = @[ca,cc,cd];
            defaultBut = (NormalButton *)ca.customView;
            [self setBut:defaultBut];
        }
            break;
        case 1:
        {
            oldState = num;
            [self clearBut];
            self.ct.items = @[cb,cc,cd];
            defaultBut = (NormalButton *)cb.customView;
            [self setBut:defaultBut];
        }
            break;
        case 2:
        {
            oldState = num;
            [self clearBut];
            self.ct.items = @[cc,cd];
            defaultBut = (NormalButton *)cc.customView;
            [self setBut:defaultBut];
        }
            break;
        case 3:
        {
            self.ct.items = @[mc,ml,gap];
        }
            break;
        default:
            break;
    }
}


-(void)chageToolBarForNum:(NSInteger)num{
    switch (num){
        case -1:
        {
             [self setToolbarItem:0];
        }
            break;
        case 0:
        {//path
            [self setToolbarItem:2];
        }
            break;
        case 1:
        {//path
            [self setToolbarItem:2];
            
        }
            break;
        case 2:
        {//title
            [self setToolbarItem:1];
        }
            break;
        case 3:
        {//sub title
            [self setToolbarItem:2];
        }
            break;
        case 4:
        {//message
            [self setToolbarItem:2];
        }
            break;
        case 5:
        {
            
        }
            break;
        default:
            break;
    }
}

-(void)clearBut{
    [defaultBut setButtonState:NO];
}

-(void)setBut:(NormalButton *)but{
    if(but.tag == 1){
        [defaultBut setButtonState:NO];
        [self.send setTitle:NSLocalizedString(@"mess_board_set", @"Set") forState:UIControlStateNormal];
        [msc setString:messm text:textView.text];
        messm = message_type_path;
        textView.text = [msc getString:message_type_path];
        defaultBut = but;
        [defaultBut setButtonState:YES];
    }else if (but.tag == 5){
        //cancel button
        messm = message_type_comment;
        [msc deleteString:message_type_rename];
        [self chageDsiplayMode:messm];
        [self setToolbarItem:oldState];
    }else{
        [defaultBut setButtonState:NO];
        [self.send setTitle:NSLocalizedString(@"mess_board_send", @"Send") forState:UIControlStateNormal];
        [msc setString:messm text:textView.text];
        messm = message_type_comment;
        textView.text = [msc getString:message_type_comment];
        defaultBut = but;
        [defaultBut setButtonState:YES];
    }
    
}


#pragma mark -set tag は現在指定されているメッサージを表示させます。表示場所は画面中央に別枠で表示します。
-(void)setTag{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setPickUp" object:nil userInfo:@{@"type":@10}];
}

#pragma mark - under view
-(void)setUnderView{
    
    self.send.layer.cornerRadius = 5;
    
    textView = [[UITextView alloc]initWithFrame:CGRectZero];
    textView.translatesAutoresizingMaskIntoConstraints = NO;
    textView.delegate = self;
    textView.textColor =  RGBA(35,45,45,1.0);
    textView.backgroundColor = RGBA(255,255,255,0.0);
    textView.font = [UIFont fontWithName:@"AvenirNext-Regular" size:14];
    textView.layoutManager.allowsNonContiguousLayout = NO;
    [self.textContiner addSubview:textView];
    
    NSArray *tw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[tv]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"tv":textView}];
    [self.textContiner addConstraints:tw];
    textContinerHeight =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tv]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"tv":textView}];
    [self.textContiner addConstraints:textContinerHeight];
    
    //[self reConstraints];
}




-(void)reConstraints{
    [self removeConstraints:textContinerHeight];
    textView.text = @"A";
    CGSize content = [textView sizeThatFits:CGSizeMake(textView.contentSize.width,9999)];
    textContinerHeight =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[tv(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:content.height]}
                                              views:@{@"tv":textView}];
    [self addConstraints:textContinerHeight];
    textView.text = @"";
    
    [self layoutIfNeeded];
    
    UIView *superView = [self superview];
    [superView removeConstraints:mbh];
    mbh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:-self.frame.size.height]}
                                              views:@{@"view":self}];
    [superView  addConstraints:mbh];
    
    [self fadeOut];
}




#pragma mark -display state

-(BOOL)fadeToggle{
    if(self.hidden){
        [self fadeIn];
        return YES;
    }else{
        [self close];
        return NO;
    }
}

-(void)close{
    [textView endEditing:YES];
    [self fadeOut];
}

-(void)fadeIn{
    [textView endEditing:YES];
    UIView *superView = [self superview];
    [superView removeConstraints:mbh];
    mbh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:0]}
                                              views:@{@"view":self}];
    [superView addConstraints:mbh];
    [UIView animateWithDuration:duration animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){

    }];
}


-(void)fadeOut{
    UIView *superView = [self superview];
    [superView removeConstraints:mbh];
    mbh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:-self.frame.size.height]}
                                              views:@{@"view":self}];
    [superView addConstraints:mbh];
    [UIView animateWithDuration:duration animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
    }];
}


#pragma mark -keyboard state
-(void)keyboardWillChange:(NSNotification *)notification {
    keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIView *superView = [self superview];
    [superView removeConstraints:mbh];
    mbh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:superView.frame.size.height -  keyboardRect.origin.y]}
                                              views:@{@"view":self}];
    [superView addConstraints:mbh];
    [UIView animateWithDuration:duration animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
    }];
}


-(void)startEdit{
    [textView becomeFirstResponder];
}

#pragma mark -textView
- (BOOL)textView:(UITextView *)_textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    CGRect caretRect = [textView caretRectForPosition:textView.selectedTextRange.start];
    [textView scrollRangeToVisible:textView.selectedRange];
    [textView scrollRectToVisible:caretRect animated:NO];
    return YES;
}
- (void)textViewDidChange:(UITextView *)tv
{
    switch (messm) {
        case message_type_comment:
        {
            [msc setString:message_type_comment text:textView.text];
        }
            break;
        case message_type_path:
        {
            [msc setString:message_type_path text:textView.text];
            //パス用はコメントを削除しません
        }
            break;
        case message_type_rename:
        {
            [msc setString:message_type_rename text:textView.text];
        }
            break;
        default:
            break;
    }
    
    if(textView.text.length != 0){
        self.base.backgroundColor = keep;
    }else{
        self.base.backgroundColor = unKeep;
    }
    
    CGSize content = [textView sizeThatFits:CGSizeMake(textView.contentSize.width,99999)];
    if(content.height < MAX_HEIGHT){
        [self removeConstraints:textContinerHeight];
        textContinerHeight =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tv(height)]|"
                                                options:0
                                                metrics:@{@"height":[NSNumber numberWithFloat:content.height]}
                                                  views:@{@"tv":textView}];
        [self addConstraints:textContinerHeight];
    }else{
        [self removeConstraints:textContinerHeight];
        textContinerHeight =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tv(height)]|"
                                                options:0
                                                metrics:@{@"height":[NSNumber numberWithFloat:MAX_HEIGHT]}
                                                  views:@{@"tv":textView}];
        [self addConstraints:textContinerHeight];
    }
    
    [UIView animateWithDuration:duration animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL comp){
    }];
}



-(void)resizeConttiner{
    UIView *superView = [self superview];
    CGSize content = [textView sizeThatFits:CGSizeMake(textView.contentSize.width,99999)];
    if(content.height < MAX_HEIGHT){
        [self removeConstraints:textContinerHeight];
        textContinerHeight =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tv(height)]|"
                                                options:0
                                                metrics:@{@"height":[NSNumber numberWithFloat:content.height]}
                                                  views:@{@"tv":textView}];
        [self addConstraints:textContinerHeight];
    }else{
        [self removeConstraints:textContinerHeight];
        textContinerHeight =
        [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tv(height)]|"
                                                options:0
                                                metrics:@{@"height":[NSNumber numberWithFloat:MAX_HEIGHT]}
                                                  views:@{@"tv":textView}];
        [self addConstraints:textContinerHeight];
    }
    
    [UIView animateWithDuration:duration animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        
    }];
    //state.text = [NSString stringWithFormat:@"%ld / 1000",(long)textView.text.length];
}

-(void)endEdit{
    isOpen = NO;
    [textView resignFirstResponder];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@0}];
    
    
}




-(void)sendMessage{
    switch (messm) {
        case message_type_comment:
        {
            if(textView.text.length > 0 && textView.text.length < MAX_LENGTH){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@1,@"mode":[NSNumber numberWithInteger:defaultBut.tag],@"text":[msc getString:message_type_comment],@"uid":hash.length == 0 ? [NSNull null] : hash}];
                //中身を初期化
                textView.text = @"";
                [msc deleteString:message_type_comment];
            }else{
                [DataManager alert:NSLocalizedString(@"Caution",@"") text:NSLocalizedString(@"",@"")];
            }
        }
            break;
        case message_type_path:
        {
            if([msc getString:message_type_path].length < MAX_LENGTH){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@1,@"mode":[NSNumber numberWithInteger:defaultBut.tag],@"text":[msc getString:message_type_path],@"uid":hash.length == 0 ? [NSNull null] : hash}];
                //パス用はコメントを削除しません
            }else{
                [DataManager alert:NSLocalizedString(@"Caution",@"") text:NSLocalizedString(@"LongString",@"")];
            }
        }
            break;
        case message_type_rename:
        {
            //メッセージの変更は変更枠で行われます
            if([msc getString:message_type_rename].length > 0 && [msc getString:message_type_rename].length < MAX_LENGTH){
                //行き先の変更　行くべきはnormal list 及び　database
                [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@4,@"text":[msc getString:message_type_rename],@"uid":[msc getHash]}];
                textView.text = @"";
                [msc deleteString:message_type_rename];
            }else{

            }
        }
            break;
        default:
            break;
    }
    if(textView.text.length != 0){
        self.base.backgroundColor = [UIColor colorWithHex:@"2E97D8"];
    }else{
        self.base.backgroundColor = [UIColor colorWithHex:@"D1D6D7"];
    }

    [self resizeConttiner];
}

#pragma mark - catch event
-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //board fadeIn
        {
            [textView becomeFirstResponder];
            isOpen = YES;
        }
            break;
        case 1://spot操作
        {
            
        }
            break;
        case 2:
            break;
        case 3:
        {
            [msc setString:messm text:textView.text];
            messm = message_type_comment;
            selectedUID = [center userInfo][@"item"];
            
            if([selectedUID isEqual:[NSNull null]]){
                hash = @"";
            }else if (2 == [selectedUID[@"state"] integerValue] || [selectedUID[@"state"] integerValue] == 3){
                hash = selectedUID[@"uid"];
            }else{
                hash = selectedUID[@"pauid"];
            }
            
            self.pick.text = [selectedUID isEqual:[NSNull null]] ? @"Top" : selectedUID[@"comment"];
            
            [self chageToolBarForNum:[selectedUID isEqual:[NSNull null]] ? -1 : [selectedUID[@"state"] integerValue]];
            if([[center userInfo][@"board"] boolValue]){
               [self fadeIn];
            }
        }
            break;
        case 4:
            break;
        case 5:
            break;
        case 6:
        {//rename mode
            [msc setString:messm text:textView.text];
            textView.text = [center userInfo][@"comment"];
            [msc setHash:[center userInfo][@"uid"]];
            [self chageDsiplayMode:message_type_rename];
            messm = message_type_rename;
            [self setToolbarItem:3];
            
            [self fadeIn];
        }
            break;
        case 99:
        {
            [self fadeIn];
        }
            break;
        case 100:
        {
            [self close];
        }
            break;
        default:
            break;
    }
    [self chageDsiplayMode:messm];
}

#pragma mark - methods


-(void)clearOldSelectPath{
    if(selectPath[@"uid"] != [NSNull null]){
        NSDictionary *dic = @{@"type":@0,@"state":selectPath};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:dic];
    }
}

-(void)scrollToCell{
    if(selectPath[@"uid"] != [NSNull null]){
        NSDictionary *dic = @{@"type":@2,@"state":selectPath};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:dic];
    }
}

-(void)chageDsiplayMode:(messageType)mes{
    if(mes == message_type_comment || mes == message_type_path){
        self.keepTag.hidden = NO;
    }else{//message_type_rename
        self.keepTag.hidden = YES;
    }
}


@end
