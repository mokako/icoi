//
//  SketchToolView.m
//  ICOI
//
//  Created by mokako on 2014/05/19.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "SketchToolView.h"

@implementation SketchToolView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        isScroll = NO;
        // Initialization code
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)review
{
    bar.frame = self.frame;
    ind.frame = CGRectMake(0,self.frame.size.height - 3,0,3);
}

-(void)initializeView
{
    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    
    
    
    bar = [[ClearToolBar alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    bar.backgroundColor = RGBA(255,255,255,0.0);
    [self addSubview:bar];
    
    ind = [[UIView alloc]initWithFrame:CGRectMake(0,self.frame.size.height - 3,0,3)];
    ind.backgroundColor = RGBA(255,120,0,1.0);
    [self addSubview:ind];
    
    
    
    colorView = [[UIButton alloc]initWithFrame:CGRectMake(0,0,35,35)];
    colorView.backgroundColor = RGBA(255,255,255,1.0);
    [[colorView layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [[colorView  layer] setBorderWidth:0.5];
    colorView.layer.cornerRadius = 5;
    colorView.tag = 2;
    [colorView addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *a = [[UIBarButtonItem alloc]initWithCustomView:colorView];
    
    UIButton *un = [[UIButton alloc]initWithFrame:CGRectMake(0,0,35 ,35)];
    un.backgroundColor = RGBA(255,255,255,0.85);
    [[un layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [[un  layer] setBorderWidth:0.5];
    un.layer.cornerRadius = 17.5;
    un.titleLabel.font = [UIFont fontWithName:@"icomoon" size:20];
    [un setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
    [un setTitle:@"\"" forState:UIControlStateNormal];
    un.tag = 3;
    [un addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *b = [[UIBarButtonItem alloc]initWithCustomView:un];
    
    
    
    UIButton *re = [[UIButton alloc]initWithFrame:CGRectMake(0,0,35,35)];
    re.backgroundColor = RGBA(255,255,255,0.85);
    [[re layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [[re  layer] setBorderWidth:0.5];
    re.layer.cornerRadius = 17.5;
    re.titleLabel.font = [UIFont fontWithName:@"icomoon" size:20];
    [re setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
    [re setTitle:@"#" forState:UIControlStateNormal];
    re.tag = 4;
    [re addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *c = [[UIBarButtonItem alloc]initWithCustomView:re];
    
    UIButton *el = [[UIButton alloc]initWithFrame:CGRectMake(0,0,35,35)];
    el.backgroundColor = RGBA(255,255,255,0.85);
    [[el layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [[el  layer] setBorderWidth:0.5];
    el.layer.cornerRadius = 17.5;
    el.titleLabel.font = [UIFont fontWithName:@"icomoon" size:20];
    [el setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
    [el setTitle:@"s" forState:UIControlStateNormal];
    el.tag = 5;
    [el addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *d = [[UIBarButtonItem alloc]initWithCustomView:el];
    
    scrollBut = [[UIButton alloc]initWithFrame:CGRectMake(0,0,35,35)];
    scrollBut.backgroundColor = RGBA(255,255,255,0.85);
    [[scrollBut layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [[scrollBut  layer] setBorderWidth:0.5];
    scrollBut.layer.cornerRadius = 17.5;
    scrollBut.titleLabel.font = [UIFont fontWithName:@"icomoon" size:20];
    [scrollBut setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
    [scrollBut setTitle:@"[" forState:UIControlStateNormal];
    scrollBut.tag = 6;
    [scrollBut addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *e = [[UIBarButtonItem alloc]initWithCustomView:scrollBut];
    
    UIButton *se = [[UIButton alloc]initWithFrame:CGRectMake(0,0,35,35)];
    se.backgroundColor = RGBA(255,255,255,0.85);
    [[se layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [[se  layer] setBorderWidth:0.5];
    se.layer.cornerRadius = 17.5;
    se.titleLabel.font = [UIFont fontWithName:@"icomoon" size:20];
    [se setTitleColor:RGBA(0,200,255,1.0) forState:UIControlStateNormal];
    [se setTitle:@"2" forState:UIControlStateNormal];
    se.tag = 1;
    [se addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *f = [[UIBarButtonItem alloc]initWithCustomView:se];
    
    
    bar.items = [NSArray arrayWithObjects:a,gap,b,gap,c,gap,d,gap,e,gap,f,nil];

    
}

-(void)setColor:(UIColor *)color
{
    colorView.backgroundColor = color;
}

-(void)touchBut:(UIButton *)sender
{
    [self.delegate setEditTab:sender.tag];
    if(sender.tag == 1)
    {
        [self animateInd];
        [scrollBut setBackgroundColor:RGBA(255,255,255,0.85)];
        [[scrollBut layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
        [scrollBut setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
        isScroll = NO;
    }else if(sender.tag == 6){
        if(isScroll){
            [scrollBut setBackgroundColor:RGBA(255,255,255,0.85)];
            [[scrollBut layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
            [scrollBut setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
            isScroll = NO;
        }else{
            [scrollBut setBackgroundColor:RGBA(45,45,45,0.85)];
            [[scrollBut layer] setBorderColor:[RGBA(255,255,255,0.7) CGColor]];
            [scrollBut setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
            isScroll = YES;
        }
    }else{
        [scrollBut setBackgroundColor:RGBA(255,255,255,0.85)];
        [[scrollBut layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
        [scrollBut setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];

        isScroll = NO;
    }
    
}


-(void)animateInd
{
    [UIView animateWithDuration:0.5f
                     animations:^{
                         ind.frame = CGRectMake(0,self.frame.size.height - 2,self.frame.size.width,2);
                     }
                     completion:^(BOOL finished){
                         ind.frame = CGRectMake(0,self.frame.size.height - 2,0,2);
                     }];
}

-(void)endScroll
{
    [scrollBut setBackgroundColor:RGBA(255,255,255,1.0)];
    [[scrollBut layer] setBorderColor:[RGBA(45,45,45,0.7) CGColor]];
    [scrollBut setTitleColor:RGBA(45,45,45,1.0) forState:UIControlStateNormal];
    isScroll = NO;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{

    
}

@end
