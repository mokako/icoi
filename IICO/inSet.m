//
//  inSet.m
//  ICOI
//
//  Created by mokako on 2014/09/14.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "inSet.h"

@implementation InsetableTableCell
-(id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}
- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}
- (void)setFrame:(CGRect)frame
{
    frame.origin.x += self.inset;
    frame.size.width -= 2 * self.inset;
    [super setFrame:frame];
}

@end
