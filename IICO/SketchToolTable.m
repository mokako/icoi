//
//  SketchToolTable.m
//  ICOI
//
//  Created by mokako on 2014/03/11.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "SketchToolTable.h"
#import "CustomCell.h"

@implementation SketchToolTable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = RGBA(255,255,255,0.0);
        sketchFlag = NO;
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)initializeView
{
    cellItem = [[NSArray alloc]initWithObjects:@"B",@"\"",@"#",@"s",@"[",@"2", nil];
    sketchTool = [self createTable:CGRectMake(0,0,60,self.bounds.size.height)  tag:0];
    [[sketchTool  layer] setBorderColor:[RGBA(100,100,100,0.6) CGColor]];
    [[sketchTool  layer] setBorderWidth:0.5];
    [self addSubview:sketchTool];
}

-(void)review
{
    sketchTool.frame = CGRectMake(0,0,60,self.bounds.size.height);
}

-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStylePlain];
    atable.backgroundColor = RGBA(255,255,255,0.8);
    atable.separatorInset = UIEdgeInsetsZero;
    atable.separatorColor = RGBA(255,94,0,0.3);
    atable.delegate = self;
    atable.dataSource = self;
    atable.scrollEnabled = YES;
    atable.tag = tag;
    atable.rowHeight = 60.0;
    atable.showsVerticalScrollIndicator = YES;
    return atable;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 6;
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    cell = [sketchTool dequeueReusableCellWithIdentifier:@"identifier"];
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc]initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(45,45,45,1.0) selectColor:RGBA(255,255,255,1.0) defaultBack:RGBA(255,255,255,0.0) selectBack:RGBA(230,70,0,0.7)];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.textLabel.font = [UIFont fontWithName:@"icomoon" size:30];
        cell.textLabel.text = [cellItem objectAtIndex:indexPath.row];
    }
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row != 4){
        [tableView deselectRowAtIndexPath:indexPath animated:YES];
        
        //強制的にscroll modeを解除します。
        
        NSIndexPath *path = [NSIndexPath indexPathForRow:4 inSection:0];
        [tableView deselectRowAtIndexPath:path animated:YES];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:path];
        cell.backgroundColor = RGBA(255,255,255,0.0);
        sketchFlag = NO;
        
    }else{
        if(sketchFlag == YES) [tableView deselectRowAtIndexPath:indexPath animated:YES];
        UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        cell.backgroundColor = sketchFlag == NO ? RGBA(230,70,0,0.7) : RGBA(255,255,255,0.0);
        sketchFlag = sketchFlag == YES ? NO : YES;
    }
    [self.delegate setIndexPathRow:indexPath];
}































@end
