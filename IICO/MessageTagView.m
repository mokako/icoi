//
//  MessageTagView.m
//  ICOI
//
//  Created by mokako on 2016/03/21.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "MessageTagView.h"

@implementation MessageTagView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"setMessageTag" object:nil];
        fontSize = 15.0;
        colorList = @[
                      RGBA(145,145,145,0.5),
                      RGBA(88,179,49,0.5),
                      RGBA(0,159,232,0.5),
                      RGBA(236,111,0,0.5),
                      RGBA(240,96,96,0.5)
                      ];
    }
    return self;
}



- (void)awakeFromNib
{
    [super awakeFromNib];
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 15;
}



-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

-(void)setIconState:(NSInteger)state{
    self.icon.textColor = colorList[state];
    switch (state) {
        case 0://no comment path
        {
            self.icon.text = @"4";
            
        }
            break;
        case 1:// comment path
        {
            self.icon.text = @"4";
        }
            break;
        case 2:// title
        {
            self.icon.text = @"n";
            
        }
            break;
        case 3:// sub
        {
            self.icon.text = @"d";
        }
            break;
        case 4:// message
        {
            self.icon.text = @"j";
        }
            break;
        default:
            break;
    }
}

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0:
        {//catch message
            NSDictionary *item = [center userInfo][@"item"];
            if(![item isEqual:[NSNull null]]){
                [self setIconState:[item[@"state"] integerValue]];
                self.title.text = item[@"comment"];
            }else{
                [self setIconState:2];
                self.title.text = @"TOP";
            }
        }
            break;
        case 1:
        {//open
        }
            break;
        case 2:
        {//close
            
        }
            break;
            default:
            break;
    }
}

@end
