//
//  ToolWindow.m
//  ICOI
//
//  Created by mokako on 2016/01/05.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ToolWindow.h"

@implementation ToolWindow
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.alpha = 0.0;
        self.hidden = YES;
        
        order = NSLocalizedString(@"skc_edit_order", @"Order");
        exchange = NSLocalizedString(@"skc_edit_exchange",@"Exchange");
        display = NSLocalizedString(@"skc_edit_display",@"Display");
        del = NSLocalizedString(@"skc_edit_delete",@"Delete");
        restore = NSLocalizedString(@"skc_edit_restore",@"Restore");
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    list = @[];
    icon = @[];
    self.desc_mess.text = NSLocalizedString(@"skc_edit_delete_message", @"");
    self.close.transform = CGAffineTransformMakeRotation(M_PI / 2);
    [self.close addTarget:self action:@selector(closeView) forControlEvents:UIControlEventTouchUpInside];
    [self.ok addTarget:self action:@selector(sendResult) forControlEvents:UIControlEventTouchUpInside];
    
    UINib *nib = [UINib nibWithNibName:@"ToolWindowCell" bundle:nil];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.rowHeight = UITableViewAutomaticDimension;
    self.table.estimatedRowHeight = 54.0;
    [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

-(void)setToolMode:(NSDictionary *)selectUID root:(BOOL)root{
    if(root){
        if(selectUID){
            self.title.text = selectUID[@"comment"];
            if([selectUID[@"state"] integerValue] == 2){
                t_mode = tool_mode_subtitle;
            }else{
                t_mode = tool_mode_other;
            }

        }else{
            t_mode = tool_mode_title;
            self.title.text = @"TOP";
        }
    }else{
        self.title.text = selectUID ? selectUID[@"comment"] : @"TOP";
        t_mode = tool_mode_external;
    }
    switch (t_mode) {
        case tool_mode_title:
        {
            list = @[order,del];
            icon = @[@"M",@"r"];
        }
            break;
        case tool_mode_subtitle:
        {
            list = @[order,del,exchange];
            icon = @[@"M",@"r",@"C"];
        }
            break;
        case tool_mode_other:
        {
            list = @[order,del,exchange,display];
            icon = @[@"M",@"r",@"C",@"4"];
        }
            break;
        case tool_mode_external:
        {
            list = @[order,del,exchange,display];
            icon = @[@"M",@"r",@"C",@"4"];
        }
            break;
        default:
            break;
    }
    [self.table reloadData];
}

-(void)displayToggle{
    if(self.hidden){
        [self openView];
    }else{
        [self endProc];
        [self closeView];
    }
}

-(void)deleteMode:(BOOL)state{
    self.table.hidden = state == YES ? YES : NO;
    self.overView.hidden = state == YES ? NO : YES;
}



#pragma mark - table view

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if(section == 0){
        return list.count;
    }else{
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ToolWindowCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    if(indexPath.section == 0){
        cell.icon.font = [UIFont fontWithName:@"select3" size:indexPath.row == 0 ? 23 : 15];
        if(t_mode == tool_mode_external && indexPath.row == 0){
            cell.title.textColor = RGBA(190,190,190,1.0);
        }
        cell.title.text = list[indexPath.row];
        cell.icon.text = icon[indexPath.row];
    }else{
        
        if(t_mode == tool_mode_external && indexPath.row == 0){
            cell.title.textColor = RGBA(190,190,190,1.0);
        }
        cell.title.text = NSLocalizedString(@"skc_edit_restore",@"Restore");;
        cell.icon.font = [UIFont fontWithName:@"select3" size:15];
        cell.icon.text = @"1";
    }
    return cell;
}

#pragma mark - NSNotification
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if(indexPath.section == 0){
        if(t_mode == tool_mode_external && indexPath.row == 0){
            //処理不可
        }else{
            [self closeView];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:
             @{@"type":@22,@"state":[NSNumber numberWithInteger:indexPath.row]}];
        }
    }else{
        [self closeView];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:
         @{@"type":@25,@"state":[NSNumber numberWithInteger:indexPath.row]}];
    }
}

#pragma mark - result send normal list
-(void)sendResult{
    [self closeView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:
     @{@"type":@23}];
}

//処理を通常の物に変更
-(void)endProc{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:
     @{@"type":@24}];
}

#pragma mark - view display

-(void)closeView{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL comp){
        [self deleteMode:NO];
        self.hidden = YES;
    }];
}

-(void)openView{
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL comp){
    }];
}


@end
