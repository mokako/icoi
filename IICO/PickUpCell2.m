//
//  PickUpCell2.m
//  ICOI
//
//  Created by mokako on 2016/03/30.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "PickUpCell2.h"

@implementation PickUpCell2

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //[self.spot addTarget:self action:@selector(touch:) forControlEvents:UIControlEventTouchUpInside];
    self.fr.layer.cornerRadius = 5.0;
    fontSize = 15.0;
    
    colorList = @[
                  RGBA(145,145,145,0.5),
                  RGBA(88,179,49,0.5),
                  RGBA(0,159,232,0.5),
                  RGBA(236,111,0,0.5),
                  RGBA(240,96,96,0.5)
                  ];
    titleList = @[
                  @"PATH",
                  @"ADD PATH",
                  @"TITLE",
                  @"SUB TITLE",
                  @"MESSAGE"
                  ];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if(highlighted){
        //self.backgroundColor = [UIColor colorWithHex:@"bcbcbc" alpha:0.5];
    }
}








-(void)setIconState:(NSInteger)state{
    self.stateIcon.textColor = colorList[state];
    self.backgroundColor = [UIColor clearColor];
    switch (state) {
        case 0://no comment path
        {
            self.stateIcon.text = @"4";
            
        }
            break;
        case 1:// comment path
        {
            self.stateIcon.text = @"4";
        }
            break;
        case 2:// title
        {
            self.stateIcon.text = @"n";
            
        }
            break;
        case 3:// sub
        {
            self.stateIcon.text = @"d";
        }
            break;
        case 4:// message
        {
            self.stateIcon.text = @"j";
        }
            break;
        default:
            break;
    }
}




-(void)setArticleText:(NSString *)str{
    self.article.text = str;
    [self.article setNeedsLayout];
}

@end
