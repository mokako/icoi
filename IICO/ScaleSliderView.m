//
//  ScaleSliderView.m
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "ScaleSliderView.h"

@implementation ScaleSliderView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.hidden = YES;
        self.alpha = 0.0;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fadeOut) name:@"fadeOut" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.ok.layer.borderColor = RGB(150,150,150).CGColor;
    self.ok.layer.borderWidth = 1;
    self.ok.layer.cornerRadius = 10;

    self.slider.minimumValue = 0.2;
    self.slider.maximumValue = 2.0;
    self.slider.minimumTrackTintColor = RGBA(96,96,85,1.0);
    self.slider.maximumTrackTintColor = RGBA(96,96,85,1.0);
    self.slider.value = 1.0;
    
    self.slider.transform = CGAffineTransformMakeRotation(-M_PI / 2);

    [self.slider setThumbImage:[UIImage imageNamed:@"ind.png"] forState:UIControlStateNormal];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

-(void)set{
    UIView *superView = [self superview];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:-self.frame.size.width],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:self.frame.size.height]}
                                              views:@{@"view":self}];
    [superView addConstraints:h];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:self  attribute:NSLayoutAttributeCenterY  relatedBy:NSLayoutRelationEqual  toItem:superView  attribute:NSLayoutAttributeCenterY  multiplier:1  constant:0]];
}

#pragma mark - display state change

-(void)toggle{
    if(isToggle){
        [self fadeOut];
    }else{
        isToggle = YES;
        [self fadeIn];
    }
}

-(void)fadeIn{
    self.hidden = NO;
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:80],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
        self.alpha = 1.0;
    }];
}


-(void)fadeOut{
    isToggle = NO;
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:-self.frame.size.width],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        self.hidden = YES;
    }];
}



@end
