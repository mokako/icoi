//
//  MessageTagView.h
//  ICOI
//
//  Created by mokako on 2016/03/21.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface MessageTagView : UIView
{
    CGFloat fontSize;
    NSArray *colorList;
}
@property (weak, nonatomic) IBOutlet UILabel *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;


+ (instancetype)view;


@end
