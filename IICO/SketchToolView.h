//
//  SketchToolView.h
//  ICOI
//
//  Created by mokako on 2014/05/19.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClearToolBar.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@protocol SketchToolViewDelegate <NSObject>
-(void)setEditTab:(NSInteger)state;
@end


@interface SketchToolView : UIView
{
    UIToolbar *bar;
    UIButton *colorView;
    UIView *ind;
    UIButton *scrollBut;
    BOOL isScroll;
}

-(void)setColor:(UIColor *)color;
-(void)endScroll;
-(void)review;
@property (nonatomic, weak)id <SketchToolViewDelegate> delegate;
@end
