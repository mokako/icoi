//
//  DefaultSetting.h
//  ICOI
//
//  Created by mokako on 2016/02/07.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger, use_font) {
    use_font_avenir  = 0, //指定されたキーにぶら下がる全データ
    use_font_avenir_c  = 1, //副キー
    use_font_applSD  = 2, //主キーに属さないデータ
    GetDataLevelOther  = 3 //主キーにぶら下がる副キー以外のデータ
};



@interface DefaultSetting : NSObject




//+(UIFont *)systemFont:
@end
