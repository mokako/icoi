//
//  AnimateToolView.h
//  ICOI
//
//  Created by mokako on 2014/05/21.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ClearToolBar.h"

#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


#define BUTSIZE 48

@protocol AnimateToolViewDelegate <NSObject>
-(void)setEditTab:(NSInteger)state;
-(void)changeState:(NSInteger)stat;
@end

@interface AnimateToolView : UIView
{
    UIToolbar *bar;
    UIButton *pb;
    UIView *lineView;
    BOOL sw;
    UIView *back;
    UISegmentedControl *seg;
}
-(void)endAnimate;
-(void)review;

-(void)stopSegment;
-(void)startSegment;
-(void)setLineAnimation:(NSInteger)state;
@property(nonatomic, weak)id<AnimateToolViewDelegate>delegate;
@end
