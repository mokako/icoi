//
//  SliderCell.m
//  ICOI
//
//  Created by mokako on 2016/02/03.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "SliderCell.h"

@implementation SliderCell
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        isSlide = NO;
        self.selected = NO;
        isOpen = NO;
    }
    return self;
}


- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    /*
     @[@"Order",@"Excahge",@"Rename",@"Omit",@"Delete",@"Display",@"Back"]];
     */
    self.arow.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    //excahge
    a = [[CustomBarButton alloc]initWithTitle:@"C" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    a.tag = 1;
    //rename
    b = [[CustomBarButton alloc]initWithTitle:@"d" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    b.tag = 2;
    //exclusion
    c = [[CustomBarButton alloc]initWithTitle:@"J" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    c.tag = 3;
    //delete
    d = [[CustomBarButton alloc]initWithTitle:@"r" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    d.tag = 4;
    
    gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    flex2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    flex2.width = 5.0;
    flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    flex.width = 20.0;
  
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}


-(void)setButton:(NSInteger)state{
    switch (state) {
        case 0:
        {
            self.ct.items = @[flex2,b,gap,c,gap,d,flex];
        }
            break;
        case 1:
        {
            self.ct.items = @[flex2,a,gap,b,gap,c,gap,d,flex];
        }
            break;
        case 2:
        {
            self.ct.items = @[flex2,c,gap,d,flex];
        }
            break;
        case 3:
        {
            self.ct.items = @[flex2,a,gap,c,gap,d,flex];
        }
            break;
        default:
            break;
    }
}

-(void)setItem:(CustomBarButton *)sta{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@20,@"state":[NSNumber numberWithInteger:sta.tag],@"indexPath":self.indexPath}];
}

#pragma mark - cell slide proc

-(void)slideView
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.forView.frame = CGRectMake(0,self.forView.frame.origin.y,self.frame.size.width,self.forView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         isOpen = NO;
                     }];
}

-(void)slideViewLeft
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.forView.frame = CGRectMake(self.frame.size.width * -1,self.forView.frame.origin.y,self.frame.size.width,self.forView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         self.forView.frame = CGRectMake(0,self.forView.frame.origin.y,self.frame.size.width,self.forView.frame.size.height);
                     }];
}
-(void)slideViewRight
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.forView.frame = CGRectMake(self.frame.size.width - 20,self.forView.frame.origin.y,self.frame.size.width,self.forView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         isOpen = YES;
                     }];
}

-(void)scrollStop
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@10}];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    oldPoint = currentPoint;
    isSlide = NO;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{

        //[super touchesMoved:touches withEvent:event];
        CGPoint currentPoint = [[touches anyObject] locationInView:self];
        CGFloat dif = currentPoint.x - oldPoint.x;
        //しきい値を設定
        if(dif < slideMinWidth && dif > -slideMinWidth){
            //低い値は無視してください
        }else{
            isSlide = YES;
            [self scrollStop];
            if(dif > 0){
                self.forView.frame = CGRectMake(dif,self.forView.frame.origin.y,self.frame.size.width,self.forView.frame.size.height);
            }else{
                //負の値は設定していません
            }
        }

    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    
    CGFloat dif = currentPoint.x - oldPoint.x;
    if(dif > 70){
        [self slideViewRight];
    }else{
        [self slideView];
    }
     [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@11}];
    if(!isSlide){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@12,@"indexPath":self.indexPath}];
    }
   
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [self slideView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@11}];
}






@end
