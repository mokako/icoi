//
//  CommentTimeLineView.h
//  ICOI
//
//  Created by mokako on 2014/12/15.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>


#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@protocol CommentTimeLineViewDelegate<NSObject>
-(void)tapSpotPointWithCGpoint:(CGPoint)point;
-(void)setEditTab:(NSInteger)state;
@end


@interface CommentTimeLineView : UIView<UITableViewDataSource,UITableViewDelegate>
{
    UIView *timeLineBoard;
    UITableView *commentTimeLine;
    UIView *fadeBut;
    UIView *hideTable;
    
    NSMutableArray *comments;
    CGFloat height;
}

@property (nonatomic, weak)id<CommentTimeLineViewDelegate>delegate;
-(void)addComment:(NSDictionary *)context comment:(BOOL)is;
-(void)update;
-(void)pullTab;
-(void)fadeTab;
@end
