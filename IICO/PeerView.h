//
//  PeerView.h
//  ICOI
//
//  Created by mokako on 2014/04/23.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ServiceManager.h"
#import "PeerController.h"
#import "PopupView.h"
#import "PeerViewCell.h"

@protocol PeerViewDelegate <NSObject>
@end

@interface PeerView : UIView<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *connectPeers;
    NSMutableDictionary *peerData;
    UILabel *labelA;
    UILabel *labelB;
    UIButton *but;
    PeerController *pcon;
    
    ServiceManager *sb;
    //popup
    PopupView *popup;
    UILabel *title;
    UILabel *pName;
    UILabel *pID;
    UIButton *setButton;
    NSString *setName;
}
@property (weak, nonatomic) IBOutlet UITableView *table;
-(void)setData:(NSArray *)founds connects:(NSDictionary *)connects;
@property (nonatomic, weak)id <PeerViewDelegate> delegate;
-(void)peerSet;
+(instancetype)view;
@end
