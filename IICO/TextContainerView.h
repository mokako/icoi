//
//  TextContainerView.h
//  ICOI
//
//  Created by mokako on 2014/09/23.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TextContainerView : UIView
{
    NSTextStorage *storage;
    NSLayoutManager *manager;
    UITextView *textView;
}




@property (nonatomic) NSString *text;
@property (nonatomic) UIFont *font;
@property (nonatomic) CGSize size;
@property (nonatomic) NSInteger numOfPage;



-(NSArray *)setView;
@end
