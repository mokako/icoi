//
//  MessageBoardConfig.m
//  ICOI
//
//  Created by mokako on 2016/02/10.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "MessageBoardConfig.h"

@implementation MessageBoardConfig
-(id)init{
    self = [super init];
    if(self){
        
    }
    return self;
}

-(void)setString:(messageType)type text:(NSString *)string{
    switch (type) {
        case message_type_comment:
        {
            a = string;
        }
            break;
        case message_type_path:
        {
            b = string;
        }
            break;
        case message_type_rename:
        {
            //un use
            c = string;
        }
            break;
        default:
            break;
    }
}

-(NSString *)getString:(messageType)type{
    switch (type) {
        case message_type_comment:
        {
            return a;
        }
            break;
        case message_type_path:
        {
            return b;
        }
            break;
        case message_type_rename:
        {
            // un use
            return c;
        }
            break;
        default:
            break;
    }
}

-(void)deleteString:(messageType)type{
    switch (type) {
        case message_type_comment:
        {
            a = @"";
        }
            break;
        case message_type_path:
        {
            b = @"";
        }
            break;
        case message_type_rename:
        {
            c = @"";
        }
            break;
        default:
            break;
    }
}

-(void)setHash:(NSString *)_hash{
    hash = _hash;
}
-(NSString *)getHash{
    return hash;
}

@end
