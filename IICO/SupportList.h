//
//  SupportList.h
//  ICOI
//
//  Created by mokako on 2015/04/07.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "SetDescHTML.h"
#import "UIColor+Hex.h"
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]
@interface SupportList : UIView<UITableViewDataSource,UITableViewDelegate>
{
    UIToolbar *tool;
    UITableView *table;
    NSArray *menuList;
    NSArray *menuDescList;
    NSArray *list;
    CAGradientLayer *gradient;
}
-(void)update;
-(void)fadeIn;
@end
