//
//  PickUpTab.m
//  ICOI
//
//  Created by mokako on 2016/03/20.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "PickUpTab.h"

@implementation PickUpTab
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        selectedColor = @"677176";
        normalColor = @"ffffff";
        tabBack = @"2E97D8";
        
        isSlide = NO;
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.tab.layer.cornerRadius = 12;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    oldPoint = currentPoint;
    isSlide = NO;
    self.tab.textColor = [UIColor colorWithHex:normalColor];
    self.tab.backgroundColor = [UIColor colorWithHex:selectedColor];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesMoved:touches withEvent:event];
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    CGFloat dif = currentPoint.y - oldPoint.y;
    
    //しきい値を設定
    isSlide = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setPickUp" object:nil userInfo:@{@"type":@20,@"y":[NSNumber numberWithFloat:-dif]}];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!isSlide){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"setPickUp" object:nil userInfo:@{@"type":@10}];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendMessageBoard" object:nil userInfo:@{@"type":@99}];
    }
    self.tab.textColor = [UIColor colorWithHex:selectedColor];
    self.tab.backgroundColor = [UIColor colorWithHex:tabBack alpha:0.0];
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    self.tab.textColor = [UIColor colorWithHex:selectedColor];
    self.tab.backgroundColor = [UIColor colorWithHex:tabBack alpha:0.0];
}



@end
