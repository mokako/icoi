//
//  CosoleViewController.m
//  ICOI
//
//  Created by moca on 2013/12/02.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import "ConsoleViewController.h"

#define ActionLayer (100)

@interface ConsoleViewController()
{
    NSMutableArray *myPeerIDList;
    BOOL isSketch;
    BOOL isOpen;
    CGFloat slideWidth;
    NSInteger slideViewContents;
    
    
    UIView *closeTabWindow;
}
@property (nonatomic) SessionController *sessionController;




-(void)initializeMethods;
-(void)initializeSetView;

//@property (nonatomic)NSString *displayUUID;
//@property (nonatomic)NSString *displayName;
//@property (nonatomic)NSString *serviceName;
//@property (nonatomic)NSString *serviceNameOfICOI;
//@property (nonatomic)NSMutableArray *sessionConnectAlert;
@property (nonatomic)NSMutableDictionary *dataList;



//-(void)createBrowser;


@property (nonatomic)MCPeerID *isNearHost; //直接的にhostに繋がっているか

@property (nonatomic)LayerView *layerView;
@property (nonatomic)UIView *baseView;
@property (nonatomic)UIView *rootView;
@property (nonatomic)UIView *menu;
@property (nonatomic)MenuUIView *menuView;
@property (nonatomic)UIView *tabView;
@property (nonatomic)SketchView *sketch;
//@property (nonatomic)CloudView *cloud;
@property (nonatomic)PeerView *peerView;
@property (nonatomic)ToolbarMenuView *toolbar;
@property (nonatomic)PhotoPicker *photo;
@property (nonatomic)DescriptionSetter *desc;
@property (nonatomic)LogView *log;



@end


enum {
    DROPBOX
};

@implementation ConsoleViewController

- (instancetype)initWithDisplayName:(NSString *)displayUUID serviceType:(NSString *)serviceType displayName:(NSString *)displayName
{
    self = [super init];
    if (self) {
        isStart = YES;
        
        
        sb = [[ServiceManager alloc]init];
        [sb initService:serviceType display:displayName uuid:displayUUID];
        
        
        //self.displayUUID = displayUUID;
        //"icoi-"と"icois"の２種類を作成できるようにする。
        //self.serviceNameOfICOI = [NSString stringWithFormat:@"%@%@",@"icoi-",serviceType];
        //self.serviceName = serviceType;
        //self.displayName = displayName;
        //self.dataList = [[NSMutableDictionary alloc]init];
        
        selectFileName = @"Image";
        
        isSketch = YES;
        isOpen = NO;
        isRotate = NO;
        //proc_queue = dispatch_queue_create("com.senpu-ki.proc", 0);
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
            // iPhone
            iPhone = YES;
        }
        else{
            // iPad
            iPhone = NO;
        }
        
        //rootのスライド量
        //menuの数
        slideViewContents = 3;
        
        CALayer *layer = [CALayer layer];
        [self.view.layer addSublayer:layer];
        
        
        
        
        
        [self performSelector:@selector(delayProc) withObject:nil afterDelay:4];
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self initializeMethods];
    [self setPopup];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopConnect) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startConnect) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendConsole" object:nil];
}

// キーボードを隠す処理
- (void)closeSoftKeyboard {
    [self.view endEditing: YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)viewWillLayoutSubviews{
}

-(void)viewDidLayoutSubviews{
    [self reloadView];
}
- (BOOL)shouldAutorotate;
{
    return isStart;                                           //回転許可
}
/*
- (NSUInteger) supportedInterfaceOrientations
{
    return isStart == YES ? -1 : 1;
}
*/


-(void)printOrientation:(UIInterfaceOrientation)orientation {
    
    
}

#pragma  mark -initialize メソッド初期化
-(void)initializeMethods{
    [SVProgressHUD setFont:[UIFont fontWithName:@"Avenir Next" size:11.0]];
    [SVProgressHUD setForegroundColor:RGBA(255,0,150,1.0)];
    [SVProgressHUD setBackgroundColor:RGBA(255,255,255,0.0)];
    [SVProgressHUD showWithStatus:@"Start ICOI" maskType:SVProgressHUDMaskTypeClear];
    self.isNearHost = nil;
    if([sb setFileManager]){
        [sb startService];
        [self initializeSetView];
    }else{
        [sb deleteService];
        //トップ画面に移る処理をいてください。
        [self.delegate consoleViewControllerClosed:self];
    }
}
#pragma mark -initialize ビュー初期化
-(void)initializeSetView
{
    
    
    
    
    self.menuView = [[MenuUIView alloc]initWithFrame:CGRectZero];
    self.menuView.translatesAutoresizingMaskIntoConstraints = NO;
    self.menuView.tag = 200;
    self.menuView.delegate = self;
    [self.view addSubview:self.menuView];
    
    NSArray *mw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.menuView}];
    [self.view addConstraints:mw];
    
    NSArray *mh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.menuView}];
    [self.view addConstraints:mh];
    
    
    
    self.toolbar = [[ToolbarMenuView alloc]init];
    self.toolbar.delegate = self;


    self.baseView = [[UIView alloc]initWithFrame:CGRectZero];
    self.baseView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.baseView];
    
    
    self.rootView = [[UIView alloc]initWithFrame:CGRectZero];
    self.rootView.backgroundColor = RGBA(20,20,40,0.0);
    self.rootView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.baseView addSubview:self.rootView];
    
    
    baseViewWidth =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.baseView}];
    [self.view addConstraints:baseViewWidth];
    
    NSArray *sh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.baseView}];
    [self.view addConstraints:sh];
    
    NSArray *rw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.rootView}];
    [self.baseView addConstraints:rw];
    
    NSArray *rh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.rootView}];
    [self.baseView addConstraints:rh];
    
    
    
    
    [self createSketch];
    [self createPeerView];
    
#pragma mark - layer view autolayout
    
    self.layerView = [[LayerView alloc]initWithFrame:CGRectZero];
    self.layerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.layerView setServiceManager:sb];
    [self.rootView addSubview:self.layerView];
    NSArray *lyw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.layerView}];
    [self.rootView addConstraints:lyw];
    
    NSArray *lyh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.layerView}];
    [self.rootView addConstraints:lyh];
    
#pragma mark - album view autolayout

    self.photo = [[PhotoPicker alloc]initWithFrame:CGRectZero];
    self.photo.translatesAutoresizingMaskIntoConstraints = NO;
    
    self.photo.hidden = YES;
    [self.rootView addSubview:self.photo];
    NSArray *alw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.photo}];
    [self.rootView addConstraints:alw];
    
    NSArray *alh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.photo}];
    [self.rootView addConstraints:alh];

 
    [self.baseView addSubview:self.toolbar];
    
 
    

    
    //最上位画面
    self.desc = [DescriptionSetter view];
    self.desc.translatesAutoresizingMaskIntoConstraints = NO;
    self.desc.delegate = self;
    [self.rootView addSubview:self.desc];
    
    NSArray *dlw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.desc}];
    [self.rootView addConstraints:dlw];
    
    NSArray *dlh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.desc}];
    [self.rootView addConstraints:dlh];
 
    
    
    
    
    self.log = [LogView view];
    self.log.translatesAutoresizingMaskIntoConstraints = NO;
    [self.rootView addSubview:self.log];
    NSArray *llw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.log}];
    [self.rootView addConstraints:llw];
    
    NSArray *llh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.log}];
    [self.rootView addConstraints:llh];
    
    
    //[self reloadView];
    //開閉用
    closeTabWindow = [[UIView alloc]initWithFrame:CGRectZero];
    closeTabWindow.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:closeTabWindow];
    closeTabWindow.hidden = YES;
    closeTabWindow.backgroundColor = RGBA(125,125,125,0.1);
    UITapGestureRecognizer *tapGesture =
    [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(closeTab)];
    // ダブルタップ
    // ビューにジェスチャーを追加
    [closeTabWindow addGestureRecognizer:tapGesture];
    NSArray *clw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]|"
                                            options:0
                                            metrics:@{@"width":@40}
                                              views:@{@"view":closeTabWindow}];
    [self.view addConstraints:clw];
    
    NSArray *clh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":closeTabWindow}];
    [self.view addConstraints:clh];
    
    
    [self receiveTableRow:0];
}

-(void)reloadView
{
    isOrientation = [DeviceData deviceOrientation];
    [self.menuView update];//banner用に残して下さい
    [self.toolbar update];
    
}

//遅延処理をさせたい
-(void)delayProc
{
    [sb ignition];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendServiceManagerSeed" object:self userInfo:@{@"manager":sb}];
    
    [sb selectImage:@"Image"];
    [SVProgressHUD dismiss];
    isStart = YES;
    
    
}



#pragma mark - FILE
////////////////////////////////////////////////////////////////////////////////////////////

#pragma mark  setDictionaryData サービス名と同じデータがあるなら読み込みます。無いなら新規作成します。

//

//バックグラウンドなどに移る前の処理です。
#pragma mark - バックグラウンド処理

-(void)stopConnect
{
    dispatch_async(dispatch_get_main_queue(),^{
        [SVProgressHUD dismiss];
    });
    started = [NSDate date];
    [sb stopService];
}
-(void)startConnect
{
    [sb startService];
    [SVProgressHUD showWithStatus:@"Reconnection" maskType:SVProgressHUDMaskTypeClear];
    [self performSelector:@selector(delayConnect) withObject:nil afterDelay:2];
    
}


-(void)endTask
{

}

-(void)delayConnect
{
    
    [SVProgressHUD dismiss];
}


//初期の画像



-(void)tapTab:(NSInteger)state
{
    [self.photo endTask];
    closeTabWindow.hidden = YES;
    if(state == 0){
        isOpen = isOpen == YES ? NO : YES;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"KeyboardHide" object:nil];
        
        [self.view removeConstraints:baseViewWidth];
        if(isOpen){
            baseViewWidth =
            [NSLayoutConstraint constraintsWithVisualFormat:@"[view(width)]-side-|"
                                                options:0
                                                metrics:@{@"width":[NSNumber numberWithFloat:self.view.bounds.size.width],@"side":[NSNumber numberWithFloat:-1 * (self.view.bounds.size.width - 40)]}
                                                  views:@{@"view":self.baseView}];
        }else{
            baseViewWidth =
            [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                                    options:0
                                                    metrics:nil
                                                      views:@{@"view":self.baseView}];
        }
        [self.view addConstraints:baseViewWidth];
        
        
        [UIView animateWithDuration:0.4f
                         animations:^{
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                             if(isOpen){
                                 closeTabWindow.hidden = NO;
                             }else{
                             }
                         }];
        
    }else if(state == 1){
        isOpen = NO;
        [self.view removeConstraints:baseViewWidth];
        baseViewWidth =
        [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                                options:0
                                                metrics:nil
                                                  views:@{@"view":self.baseView}];
        [self.view addConstraints:baseViewWidth];
        [UIView animateWithDuration:0.4f
                         animations:^{
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                             
                         }];
    }
}


-(void)closeTab{
    isOpen = NO;
    closeTabWindow.hidden = YES;
    [self.view removeConstraints:baseViewWidth];
    baseViewWidth =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.baseView}];
    [self.view addConstraints:baseViewWidth];
    [UIView animateWithDuration:0.4f
                     animations:^{
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         
                     }];
}

/////////////////////////////////////////////////////////////////////////////////////////////////
#pragma mark END FILE



#pragma mark - VIEW-CONTROLLER DELEGATE METHODS

-(void)createSketch
{
    self.sketch = [[SketchView alloc]initWithFrame:CGRectZero];
    self.sketch.translatesAutoresizingMaskIntoConstraints = NO;
    [self.rootView addSubview:self.sketch];
    [self.rootView sendSubviewToBack:self.sketch];
    [self.sketch setServiceManager:sb];
    self.sketch.delegate = self;
    self.toolbar.hidden = YES;
    
    NSArray *w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.sketch}];
    [self.rootView addConstraints:w];
    
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.sketch}];
    [self.rootView addConstraints:h];
    
    
    
}
     


-(void)createPeerView
{
    self.peerView = [PeerView view];
    self.peerView.delegate = self;
    self.peerView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.rootView addSubview:self.peerView];
    NSArray *w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.peerView}];
    [self.rootView addConstraints:w];
    
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-40-[view]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"view":self.peerView}];
    [self.rootView addConstraints:h];
    
    
}

#pragma mark - ipad iphone 判別
/*
 if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
    //iphone
 }else{
    //ipad
 }
 
 */


//support view

-(void)createAccountDescription
{
    
}
#pragma mark -manifesto マニフェストを設定する画面の作成



#pragma mark - 
#pragma mark - Cloud Database methods
/*
-(void)createCloudView
{
    self.cloud = [[CloudView alloc]initWithFrame:CGRectMake(0,0,self.rootView.bounds.size.width, self.rootView.bounds.size.height)];
    self.cloud.delegate = self;
    self.cloud.hidden = YES;
    [self.rootView addSubview:self.cloud];
    [self.cloud setController:self];
}
*/


#pragma mark -receivDataObject 接続時のデータの管理クラス


//使用しません


#pragma mark - SETLAYER methods セットレイヤー

/*
 
 追加した画像に書き込み等等を制御する仕組み
 
 まずどのメソッドを利用して画像を追加するという事です。
 uiview,CALayer,UIScrollViewなどが有ります。
 
 画像に追加する動作
 
 move
 pinch
 drawing
 
 この３点の動作が共存出来るようにします。
 もしUIPinchGestureRecognizerなどが共存出来ないならすべてをtouchで実装する必要が有ります。
 
 */


-(void)restartService
{
    [self.delegate consoleViewControllerClosed:self];
}


#pragma mark - delegate methods


#pragma mark - ToolbarMenuView
/*
 変更

 操作 - ファイル - 接続状況 - 終了
 操作 -- paint - flip - layer
 file -- album


 */



-(void)receiveTableRow:(NSIndexPath *)indexPath
{
    [NSThread sleepForTimeInterval:0.5];

    switch (indexPath.row) {
        case 0:
        {//sketch mode
            isSketch = YES;
            [self allMenuHidden];
            self.sketch.hidden = NO;
            self.toolbar.hidden = YES;
            self.layerView.hidden = YES;

            [self.sketch setEditTab:0];
            [self tapTab:1];
        }
            break;
        case 1:
        {//layer
            isSketch = NO;
            if(self.layerView.hidden == YES){
                [self allMenuHidden];
                self.layerView.hidden = NO;
                self.toolbar.hidden = NO;
                [self.toolbar menuEditable:2 title:NSLocalizedString(@"Theme",@"")];
            }
            [self tapTab:1];
        }
            break;
        case 2:
        {//album
            isSketch = NO;
            if(self.photo.hidden == YES){
                [self allMenuHidden];
                self.photo.hidden = NO;
                self.toolbar.hidden = NO;
                [self.toolbar menuEditable:1 title:NSLocalizedString(@"Album",@"")];
            }
            [self tapTab:1];
        }
            break;
        case 3:
        {//peer list
            isSketch = NO;
            if(self.peerView.hidden == YES){
                [self allMenuHidden];
                [self.peerView peerSet];
                self.peerView.hidden = NO;
                self.toolbar.hidden = NO;
                [self.toolbar menuEditable:1 title:NSLocalizedString(@"Link",@"")];
            }
            [self tapTab:1];
        }
            break;
        case 4:
        {
            isSketch = NO;
            if(self.log.hidden == YES){
                [self allMenuHidden];
                [self.peerView peerSet];
                self.log.hidden = NO;
                self.toolbar.hidden = NO;
                [self.toolbar menuEditable:1 title:NSLocalizedString(@"Log",@"")];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"removeLogBadge" object:self userInfo:nil];
            }
            [self tapTab:1];
            
            
        }
            break;
        case 5:
        {// exit
            [sb endService];
            [self.delegate consoleViewControllerClosed:self];
        }
            break;
        case 6:
        {//Help
            isSketch = NO;
            if(self.desc.hidden == YES){
                [self allMenuHidden];
                self.desc.hidden = NO;
                self.toolbar.hidden = NO;
                [self.toolbar menuEditable:1 title:NSLocalizedString(@"Help",@"")];
            }
            [self tapTab:1];
        }
            break;
        case 8:
        {//test
            //[self.sessionController test];
        }
            break;
        case 20://配列にはない処理です
        {//スライドせずに移行
            [self allMenuHidden];
            self.toolbar.hidden = YES;
            self.layerView.hidden = YES;
            [self.sketch setEditTab:0];
            [self.menuView stepPaint];
        }
            break;
        default:
            break;
    }
}


-(void)allMenuHidden
{
    self.desc.hidden = YES;
    self.log.hidden = YES;
    self.photo.hidden = YES;
    self.desc.hidden = YES;
    [self.photo endTask];
    self.layerView.hidden = YES;
    self.peerView.hidden = YES;
    self.sketch.hidden = YES;
    [self.sketch setEditTab:55];
}


//////////////////////////////////////////////////////////////////////////
#pragma mark - CloudView

//////////////////////////////////////////////////////////////////////////
#pragma mark - Sketch Path


-(void)touchBut:(NSInteger)sender
{
    switch (sender) {
        case 0:
        {
            self.layerView.hidden = YES;
            self.toolbar.hidden = YES;
        }
            break;
        case 1:
        {
            self.layerView.hidden = self.layerView.hidden == YES ? NO : YES;
        }
            break;
        case 2:
        {
            isSketch = NO;
            [self allMenuHidden];
            self.layerView.hidden = NO;
            self.toolbar.hidden = NO;
            [self.toolbar menuEditable:2 title:NSLocalizedString(@"Theme",@"")];
            [self.menuView stepLayer];

        }
            break;
        case 3:
        {
            isSketch = YES;
            [self allMenuHidden];
            self.sketch.hidden = NO;
            self.toolbar.hidden = YES;
            [self.sketch setEditTab:0];
            [self.menuView stepPaint];
        }
            break;
            
        //4 ~ 9 は編集モード
        case 4:
        {//sketchtoolview の　表示／非表示
            [self.sketch setEditTab:0];
        }
            break;
        case 5:
        {//animationtoolview
            [self.sketch setEditTab:10];
        }
            break;
        case 6:
        {
            isSketch = NO;
            [self allMenuHidden];
            self.photo.hidden = NO;
            self.toolbar.hidden = NO;
            [self.toolbar menuEditable:1 title:NSLocalizedString(@"Album",@"")];
            [self.menuView stepAlbum];
            
        }
            break;
        default:
            break;
    }
}



#pragma mark -NotificationCenter and popup
-(void)setPopup{
    pop = [[PopupView alloc]initWithFrame:self.view.bounds];
    pop.delegate = self;
    [self.view addSubview:pop];
}

-(void)popupview:(event)enventState{
    switch (enventState) {
        case OK:
        {
            UIImage *img = [[UIImage alloc]initWithData:[FileManager loadFile:[sb getService] name:@"Image.PNG"]];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:self userInfo:@{@"type":@30,@"image":img,@"name":@"Image"}];
        }
            break;
        case Cancel:
        {
            
        }
        default:
            break;
    }
}

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //削除依頼で削除された画像の使用における警告
        {
            [pop.popSub setTitle:[NSString stringWithFormat:@"%@ : %@",NSLocalizedString(@"POPSUB-CAUTION", @""),NSLocalizedString(@"LAYER-IMAGE-TITLE", @"")] text:NSLocalizedString(@"LAYER-IMAGE-TEXT",@"")];
            [self.view bringSubviewToFront:pop];
            [pop.popSub type:Caution];
            dispatch_async(dispatch_get_main_queue(), ^{
                [pop show];
            });
            
        }
            break;
        case 30: //chat
        {

        }
            break;
        case 31: //chat close
        {

        }
            break;
        default:
            break;
    }
}

-(void)setFile{
    UIImage *img = [[UIImage alloc]initWithData:[FileManager loadFile:[sb getService] name:@"Image.PNG"]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:self userInfo:@{@"type":@30,@"image":img,@"name":@"Image"}];
}



#pragma mark -終了処理




@end

//MEMO
/*
 スクラップ用のクラスはまだ未実装です。
 infoDataの@"class"から取得して実装して下さい。
 
 
 
 各viewの決まりごと
 [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(endTask) name:@"endService" object:nil];
 
 
 endTaskでobserverを削除してください。Observerを利用する場合必ずendTaskを実装しないとサービスへのリログインで落ちま
 
 
 
*/
