//
//  HighlightedButton.m
//  ICOI
//
//  Created by mokako on 2016/04/10.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "HighlightedButton.h"

@implementation HighlightedButton

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

-(void)setHighlighted:(BOOL)highlighted{
    [super setHighlighted:highlighted];
    if(highlighted){
        self.backgroundColor = [self.backgroundColor colorWithAlphaComponent:0.5];
    }else{
        self.backgroundColor = [self.backgroundColor colorWithAlphaComponent:0.0];
    }
}

@end
