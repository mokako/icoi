//
//  ImageSIzeTable.h
//  ICOI
//
//  Created by mokako on 2015/01/08.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"


@protocol ImageSIzeTableDelegate <NSObject>
-(void)resizeScale:(CGFloat)scale;
@end


@interface ImageSIzeTable : UIView<UITableViewDataSource, UITableViewDelegate>
{
    NSMutableArray *list;
    NSArray *name;
    id selectButton;
    NSInteger selectIndex;
}

@property (nonatomic, weak) id<ImageSIzeTableDelegate>delegate;

-(void)displayToggle;
-(void)hide;
-(void)defaultWidthAndHeight:(CGFloat)width height:(CGFloat)height;
@end
