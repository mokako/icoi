//
//  NormalHeaderView.m
//  ICOI
//
//  Created by mokako on 2016/03/17.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "NormalHeaderView.h"

@implementation NormalHeaderView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.undo.layer.cornerRadius = 5;
    self.titleButton.layer.cornerRadius = 5;
    
    UILabel *butl = [[UILabel alloc]initWithFrame:CGRectMake(0,0,20,20)];
    butl.font = [UIFont fontWithName:@"font-icon-00" size:20];
    butl.textColor = [UIColor colorWithHex:@"FFFFFF"];
    butl.center = CGPointMake(self.titleButton.frame.size.width - 20,self.titleButton.frame.size.height / 2);
    butl.text = @"e";
    butl.layer.cornerRadius = 5;
    [self.titleButton addSubview:butl];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


-(void)setButtonColor:(BOOL)state{
    self.titleButton.backgroundColor = state == YES ? [UIColor colorWithHex:@"389BA6"] : [UIColor colorWithHex:@"DB00A5"];
}
@end
