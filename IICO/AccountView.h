//
//  AccountView.h
//  ICOI
//
//  Created by mokako on 2016/03/23.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AccountView : UIView


@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *icon;
@property (weak, nonatomic) IBOutlet UILabel *desc;
@property (weak, nonatomic) IBOutlet UITextField *accountText;
@property (weak, nonatomic) IBOutlet UIButton *undo;
@property (weak, nonatomic) IBOutlet UIButton *done;

-(IBAction)touchbut:(UIButton *)sender;

+(instancetype)view;
@end
