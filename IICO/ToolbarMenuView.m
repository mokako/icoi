//
//  ToolbarMenuView.m
//  ICOI
//
//  Created by mokako on 2014/05/17.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "ToolbarMenuView.h"

@implementation ToolbarMenuView
- (id)init
{
    self = [super init];
    if (self) {
        CGRect r = [DeviceData deviceMainScreen];
        
        self.frame = CGRectMake(0,0,r.size.width,40);
        self.backgroundColor = RGBA(255,255,255,1.0);
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(addBadge) name:@"addLogBadge" object:nil];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(removeBadge) name:@"removeLogBadge" object:nil];
        
        
        count = 0;
        [self initializeView];
        
        
        defaultColor = RGBA(20,20,30,1.0);
        selectColor = RGBA(60,163,156,1.0);
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)initializeView
{
    UIColor *fontColor = RGBA(80,80,80,1.0);
    
    gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(0,39.5,self.frame.size.width,0.5);
    gradient.colors = @[
                        (id)[UIColor colorWithHex:@"#70e1f5"].CGColor,
                        (id)[UIColor colorWithHex:@"#ffd194"].CGColor
                        ];
    gradient.startPoint = CGPointMake(0, 0);
    gradient.endPoint = CGPointMake(1,0);
    [self.layer addSublayer:gradient];
    
    
    bar = [[ClearToolBar alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    [self addSubview:bar];
    
    menu = [ICOIImage setIcon:[UIFont fontWithName:@"font-icon-l-01" size:20] icon:@"k" title:NSLocalizedString(@"Menu",@"") color:fontColor];
    menu.tag = 2;
    [menu addTarget:self action:@selector(tapTab) forControlEvents:UIControlEventTouchUpInside];
    label = [[UIFlexibleLabel alloc]init];
    label.font = [UIFont fontWithName:@"AvenirNext-Medium" size:10.0];
    label.center = CGPointMake(30,10);
    label.textColor = RGBA(255,255,255,1.0);
    label.backgroundColor = RGBA(59,192,195,1.0);
    label.textAlignment = NSTextAlignmentCenter;
    label.trigger = @"0";
    label.radius = 0.5;
    label.padding = 1;
    [label setValue:[NSString stringWithFormat:@"%ld",(long)count]];
    [menu addSubview:label];
    a = [[UIBarButtonItem alloc]initWithCustomView:menu];
    
    
    title = [[UILabel alloc]initWithFrame:CGRectMake(0,0,120,40)];
    title.text = @"";
    title.font = [UIFont fontWithName:@"AppleSDGothicNeo-Light" size:14.0];
    title.textColor = defaultColor;
    title.textAlignment = NSTextAlignmentCenter;
    b = [[UIBarButtonItem alloc]initWithCustomView:title];
    
    UIButton *layer = [ICOIImage setIcon:[UIFont fontWithName:@"font-icon-l-00" size:20] icon:@"o" title:NSLocalizedString(@"Theme",@"") color:fontColor];
    layer.tag = 2;
    [layer addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    d = [[UIBarButtonItem alloc]initWithCustomView:layer];
    
    UIButton *paint = [ICOIImage setIcon:[UIFont fontWithName:@"font-icon-l-02" size:20] icon:@"7" title:NSLocalizedString(@"Board",@"") color:fontColor];
    paint.tag = 3;
    [paint addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    c = [[UIBarButtonItem alloc]initWithCustomView:paint];
    
    
    UIButton *al = [ICOIImage setIcon:[UIFont fontWithName:@"font-icon-l-02" size:20] icon:@"g" title:NSLocalizedString(@"Album",@"") color:fontColor];
    al.tag = 6;
    [al addTarget:self action:@selector(touchBut:) forControlEvents:UIControlEventTouchUpInside];
    e = [[UIBarButtonItem alloc]initWithCustomView:al];
}

-(void)menuEditable:(NSInteger)type title:(NSString *)titleText
{    //self.tool
    
    
    
    UIBarButtonItem *gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    
    UIBarButtonItem *fixedSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    fixedSpace.width = -15;
    
    UIBarButtonItem *blankSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    blankSpace.width = 0;
    switch (type) {
        case 0:
        {//sketch mode default
            self.hidden = YES;
        }
            break;
        case 1:
        {// 各編集画面
            self.hidden = NO;
            title.text = titleText;
            bar.items = [NSArray arrayWithObjects:fixedSpace,a,gap,b,gap,d,blankSpace,c,blankSpace,nil];
        }
            break;
        case 2:
        {
            self.hidden = NO;
            title.text = titleText;
            bar.items = [NSArray arrayWithObjects:fixedSpace,a,gap,b,gap,e,blankSpace,c,blankSpace,nil];
        }
            break;
        default:
            break;
    }
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void)update
{
    CGRect r = [DeviceData deviceMainScreen];
    self.frame = CGRectMake(0,0,r.size.width,40);
    bar.frame = CGRectMake(0,0,self.frame.size.width,40);
    gradient.frame = CGRectMake(0,39.5,bar.frame.size.width,0.5);
}



-(void)tapTab
{
    [self.delegate tapTab:0];
}


-(void)touchBut:(UIButton *)sender
{
    [self.delegate touchBut:sender.tag];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
}

-(void)addBadge{
    count++;
    [label setValue:[NSString stringWithFormat:@"%ld",(long)count]];
}

-(void)removeBadge{
    count = 0;
    [label setValue:[NSString stringWithFormat:@"%ld",(long)count]];
}











@end
