//
//  SketchListView.m
//  ICOI
//
//  Created by mokako on 2015/10/17.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "SketchListView.h"

@implementation SketchListView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        slide = NO;
        animation = NO;
        self.clipsToBounds = YES;
        st0 = RGBA(105,105,105,0.6);
        st1 = RGBA(255,255,255,1.0);
        st2 = RGBA(95,222,221,1.0);
        st3 = RGBA(155,155,155,1.0);
        tap = NO;
        colorHex = @"FFDCDC";
        sketchListX = 0;
        self.layer.borderColor = [UIColor colorWithHex:@"cccccc"].CGColor;
        self.layer.borderWidth = 0.5;
         [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendSketchList" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    nlist = [NormalList view];
    nlist.translatesAutoresizingMaskIntoConstraints = NO;
    [self.base addSubview:nlist];
    NSArray *nw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[tv]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"tv":nlist}];
    [self.base addConstraints:nw];
    NSArray *nh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tv]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"tv":nlist}];
    [self.base addConstraints:nh];
    // 初期化
    //self.label.text = NSStringFromClass([self class]);
    ///[self.close addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    //[self.tool addTarget:self action:@selector(toolToggle) forControlEvents:UIControlEventTouchUpInside];
}

-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className;
    if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
        className = @"SketchListForiPhone";
    }
    else{
        className = @"SketchListViewForiPad";
    }
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}



-(void)layoutSubviews{
    //gradient.frame = CGRectMake(0,0,3,self.frame.size.height);
}




-(void)setConstraint:(NSArray *)constraints{
    horizon = constraints;
}


-(void)reConstraints{
    UIView *superView = [self superview];
    [superView removeConstraints:horizon];
    horizon =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view]-width-|"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:-self.bounds.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:horizon];
}


-(void)setNaviView{
    //comment window fade in
    [self layoutIfNeeded];
    
}

-(void)displayToggle{
    if(sketchListX > -100){
        [self fadeIn];
    }else{
        [self fadeOut];
    }
}


-(void)fadeIn{
    sketchListX = -self.bounds.size.width;
    animation = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"KeyboardHide" object:nil];
    UIView *superView = [self superview];
    [superView removeConstraints:horizon];
    horizon =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view]-width-|"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:0]}
                                              views:@{@"view":self}];
    [superView addConstraints:horizon];
    [self sketchTab:YES];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        slide = YES;
        animation = NO;
    }];
}



-(void)fadeOut{
    sketchListX = 0;
    animation = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"KeyboardHide" object:nil];
    UIView *superView = [self superview];
    [superView removeConstraints:horizon];
    horizon =
    [NSLayoutConstraint constraintsWithVisualFormat:@"[view]-width-|"
                                            options:0
                                            metrics:@{@"width":[NSNumber numberWithFloat:-self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:horizon];
    [self sketchTab:NO];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
        
    } completion:^(BOOL comp){
        slide = NO;
        animation = NO;
        //[cw close];
    }];
}

//tab 開閉
-(void)sketchTab:(BOOL)tabState{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketchListTab" object:nil userInfo:@{@"type":tabState == YES ? @0 : @1}];
}

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0:
        {
            [self fadeOut];
        }
            break;
        case 10:
        {
            if(sketchListX > -100){
                [self fadeIn];
            }else{
                [self fadeOut];
            }
            
        }
            break;
        case 20:
        {
            if(!animation){

                
                sketchListX = sketchListX + [[center userInfo][@"x"] floatValue];
                if(sketchListX > -100 && [[center userInfo][@"x"] floatValue] > 0){
                    [self fadeOut];
                }else if (sketchListX < (-self.frame.size.width + 100) && [[center userInfo][@"x"] floatValue] < 0){
                    [self fadeIn];
                }else{
                
                    UIView *superView = [self superview];
                    
                    [superView removeConstraints: horizon];
                    horizon =
                    [NSLayoutConstraint constraintsWithVisualFormat:@"[view]-width-|"
                                                            options:0
                                                            metrics:@{@"width":[NSNumber numberWithFloat:-self.frame.size.width - sketchListX]}
                                                              views:@{@"view":self}];
                    [superView addConstraints:horizon];
                    [superView layoutIfNeeded];
                }
                
                /*
                }else if((self.bounds.size.height + [[center userInfo][@"x"] floatValue]) > PickupMinHeight){
                    
                    UIView *superView = [self superview];
                    [superView removeConstraints: horizon];
                    horizon =
                    [NSLayoutConstraint constraintsWithVisualFormat:@"[view]-width-|"
                                                            options:0
                                                            metrics:@{@"width":[NSNumber numberWithFloat:self.bounds.origin.x + [[center userInfo][@"x"] floatValue]]}
                                                              views:@{@"view":self}];
                    [superView addConstraints:horizon];
                    [superView layoutIfNeeded];
                }else if((self.bounds.size.height + [[center userInfo][@"x"] floatValue]) < PickupMinHeight){
                    [self fadeOut];
                }
                 */
            }
        }
            break;
        default:
            break;
    }
}

            

#pragma mark - touch event
/*
- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    tap = YES;
    beginPoint = [[touches anyObject] locationInView:self];
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    tap = NO;
    CGPoint ipoint = [[touches anyObject] locationInView:self];
    CGPoint point = CGpointSum(beginPoint,ipoint);
    CGFloat x = self.frame.origin.x + point.x;
    CGRect rec = [DeviceData deviceMainScreen];
    if(x > rec.size.width - self.frame.size.width){
        self.frame = CGRectMake(self.frame.origin.x + point.x,self.frame.origin.y,self.frame.size.width,self.frame.size.height);
    }
}
- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    CGRect rec = [DeviceData deviceMainScreen];
    if((rec.size.width - (self.frame.size.width / 2)) > self.frame.origin.x){
        [self fadeIn];
    }else{
        [self fadeOut];
    }
    if(tap){
        [self fadeOut];
    }
}

-(void)touchesCancelled:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    CGRect rec = [DeviceData deviceMainScreen];
    if((rec.size.width - (self.frame.size.width / 2)) > self.frame.origin.x){
        [self fadeIn];
    }else{
        [self fadeOut];
    }
    if(tap){
        [self fadeOut];
    }
}
*/


@end
