//
//  PickUpTab.h
//  ICOI
//
//  Created by mokako on 2016/03/20.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface PickUpTab : UIView
{
    CGPoint oldPoint;
    NSString *selectedColor,*normalColor,*tabBack;
    BOOL isSlide;
}
@property (weak, nonatomic) IBOutlet UILabel *tab;
+ (instancetype)view;
@end
