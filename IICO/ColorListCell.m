//
//  ColorListCell.m
//  ICOI
//
//  Created by mokako on 2015/10/20.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "ColorListCell.h"

@implementation ColorListCell

- (void)awakeFromNib {
    // Initialization code
    
    self.tapCell.layer.cornerRadius = 15;
    self.tapCell.layer.borderColor = [UIColor clearColor].CGColor;
    self.tapCell.layer.borderWidth = 2;
    
    
}

-(void)setSelectedState:(BOOL)state{
    self.backgroundColor = [UIColor clearColor];
    if(state){
        self.tapCell.layer.cornerRadius = 5;
        self.tapCell.layer.borderColor = [UIColor blackColor].CGColor;
    }else{
        self.tapCell.layer.cornerRadius = 15;
        self.tapCell.layer.borderColor = [UIColor clearColor].CGColor;
    }
    
}

@end
