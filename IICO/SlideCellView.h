//
//  SlideCellView.h
//  ICOI
//
//  Created by mokako on 2014/05/24.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>


#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]

@protocol SlideCellViewDelegate <NSObject>
-(void)toucheBegan;
-(void)toucheEnd;
-(void)tapCell:(NSIndexPath *)path tap:(BOOL)tap;
-(void)setPath:(id)view path:(NSIndexPath *)indexpath stat:(NSInteger)stat;
@end

@interface SlideCellView : UIView
{
    UIView *forGround;
    UIView *backGround;
    CGPoint oldPoint;
    BOOL isSlide;
    BOOL isMove;
}
@property (nonatomic) UIImageView *top;
@property (nonatomic) UILabel *title;
@property (nonatomic) UILabel *backgroundTitle;
@property (nonatomic) UILabel *stat;
@property (nonatomic) UILabel *date;
@property (nonatomic) UILabel *saved;
@property (nonatomic) NSIndexPath *indexPath;
@property (nonatomic) UIColor *backgroundColor;
@property (nonatomic) UIColor *selectedBackgroundColor;
@property (nonatomic) BOOL selected;
@property (nonatomic, weak)id<SlideCellViewDelegate>delegate;
@end
