//
//  SlideCellView.m
//  ICOI
//
//  Created by mokako on 2014/05/24.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "SlideCellView.h"

@implementation SlideCellView



-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        isSlide = NO;
        //default color
        self.backgroundColor = RGBA(255,255,255,1.0);
        self.selectedBackgroundColor = RGBA(245,245,245,1.0);
        
        self.selected = NO;
        [self initializeView];
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



-(void)initializeView
{
    forGround = [[UIView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    [[forGround layer]setBorderColor:[RGBA(205,205,205,1.0) CGColor]];
    [[forGround layer]setBorderWidth:0.5];
    if(self.selected){
        forGround.backgroundColor = self.selectedBackgroundColor;
    }else{
        forGround.backgroundColor = self.backgroundColor;
    }
    
    
    self.top = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,36,36)];
    self.top.center = CGPointMake(24,self.frame.size.height / 2);
    self.top.contentMode = UIViewContentModeCenter;
    self.top.layer.cornerRadius = 18;
    
    
    [forGround addSubview:self.top];
    
    
    self.title = [[UILabel alloc]initWithFrame:CGRectMake(0,0,110,15)];
    self.title.center = CGPointMake(self.frame.size.width / 2 + 5,self.frame.size.height / 2);
    self.title.textColor = RGBA(55,55,55,0.7);
    self.title.textAlignment = NSTextAlignmentLeft;
    [forGround addSubview:self.title];
    
    self.date = [[UILabel alloc]initWithFrame:CGRectMake(45,55,self.frame.size.width - 75,8)];
    self.date.textColor = RGBA(75,75,75,0.7);
    self.date.textAlignment = NSTextAlignmentLeft;
    [forGround addSubview:self.date];
    
    
    self.saved = [[UILabel alloc]initWithFrame:CGRectMake(0,0,30,30)];
    self.saved.center = CGPointMake(self.frame.size.width - 25,self.frame.size.height / 2);
    self.saved.textColor = RGBA(218,80,14,0.5);
    self.saved.textAlignment = NSTextAlignmentCenter;
    [forGround addSubview:self.saved];
    
    
    backGround = [[UIView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    backGround.backgroundColor = RGBA(255,255,255,1.0);
    
    self.backgroundTitle = [[UILabel alloc]initWithFrame:CGRectMake(0,0,36,36)];
    self.backgroundTitle.clipsToBounds = YES;
    self.backgroundTitle.layer.cornerRadius = 18.0;
    self.backgroundTitle.textColor = RGBA(255,255,255,1.0);
    self.backgroundTitle.text = @"";
    self.backgroundTitle.textAlignment = NSTextAlignmentCenter;
    self.backgroundTitle.font = [UIFont fontWithName:@"Avenir Next" size:12.0];
    [backGround addSubview:self.backgroundTitle];
    
    
    [self addSubview:backGround];
    [self addSubview:forGround];
}


-(void)slideView
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         forGround.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         isSlide = NO;
                     }];
}

-(void)slideViewLeft
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         forGround.frame = CGRectMake(self.frame.size.width * -1,0,self.frame.size.width,self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         
                         [self.delegate setPath:self path:self.indexPath stat:0];
                         forGround.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
                         isSlide = NO;
                     }];
}
-(void)slideViewRight
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         forGround.frame = CGRectMake(self.frame.size.width,0,self.frame.size.width,self.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         
                         [self.delegate setPath:self path:self.indexPath stat:1];
                         forGround.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
                         isSlide = NO;
                     }];
}


-(void)scrollStop
{
    if(isMove){
        isMove = NO;
        [self.delegate toucheBegan];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesBegan:touches withEvent:event];
    //UITouch *touch = [touches anyObject];
    //[self.delegate toucheBegan];
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    oldPoint = currentPoint;
    isMove = YES;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(isSlide == NO){
        //[super touchesMoved:touches withEvent:event];
        CGPoint currentPoint = [[touches anyObject] locationInView:self];
        CGFloat dif = currentPoint.x - oldPoint.x;
        //しきい値を設定
        if(dif < 1 && dif > -1){
            //低い値は無視してください
        }else{
            [self scrollStop];
            if(dif > 0){
                self.backgroundTitle.text = @"SET";
                self.backgroundTitle.center = CGPointMake(24,self.frame.size.height / 2);
                self.backgroundTitle.backgroundColor = RGBA(255,90,0,1.0);
            }else{
                self.backgroundTitle.text = @"SAVE";
                self.backgroundTitle.center = CGPointMake(self.frame.size.width - 24,self.frame.size.height / 2);
                self.backgroundTitle.backgroundColor = RGBA(0,90,255,1.0);
            }
            forGround.frame = CGRectMake(dif,0,self.frame.size.width,self.frame.size.height);
        }
    }

}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    
    CGFloat dif = currentPoint.x - oldPoint.x;
    isSlide = YES;

    if(dif > 70){
        [self slideViewRight];
    }else if(dif < -70){
        [self slideViewLeft];
    }else if(dif == 0 && isMove){
        //tap処理は下で一括して行っています
    }else{
        [self slideView];
    }
    
    if(isMove){
        if(self.selected){
            self.selected = NO;
            forGround.backgroundColor = self.backgroundColor;
        }else{
            self.selected = YES;
            forGround.backgroundColor = self.selectedBackgroundColor;
        }
        [self.delegate tapCell:self.indexPath tap:self.selected];
    }
    isMove = YES;
    [self.delegate toucheEnd];
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    isMove = YES;
    [self slideView];
    [self.delegate toucheEnd];
}
















@end
