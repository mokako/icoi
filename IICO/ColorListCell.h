//
//  ColorListCell.h
//  ICOI
//
//  Created by mokako on 2015/10/20.
//  Copyright © 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ColorListCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *tapCell;

-(void)setSelectedState:(BOOL)state;
@end
