//
//  AppDelegate.h
//  IICO
//
//  Created by moca on 2013/12/02.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{

}
@property (strong, nonatomic) UIWindow *window;
@property (nonatomic)NSURL *launchURL;
@end
