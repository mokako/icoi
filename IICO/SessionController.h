//
//
//  SessionController.h
//  ICOI
//
//  Created by moca on 2013/12/09.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ICOI.h"





#define receiveMaxCount 2


@class SessionController;

@protocol SessionControllerDelegate;





@interface SessionController : NSObject
{
    //MCBrowserViewController *browser;

    NSMutableData *bufData;
    NSMutableData *setData;
    uint8_t byteIndex;
    
    NSUInteger size;
    NSUInteger sendSize;
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    NSMutableDictionary *setOutputData;
    NSMutableArray *noneExistList;
    NSInteger noneExistListCurrentCount;//未保存データ配列
    NSMutableArray *requestArray;//リクエストデータ配列
    NSString *loopRequest;//リクエストのループ防止用
    BOOL requestFlag;
    BOOL inputFlag;
    BOOL outputFlag;
    //NSRunLoop *_roop;
    //dispatch_queue_t stream_queue;
    dispatch_queue_t recieve_queue;
    NSString *uuid;
    BOOL isContacto;
    BOOL front;
    NSMutableArray *myPeers;
}
@property (nonatomic) NSString *serviceType;
@property (nonatomic) NSUInteger connectedPeersCount;

@property (nonatomic, strong) MCSession *session;
@property (nonatomic, strong) MCNearbyServiceBrowser *nearbyBrowser;
@property (nonatomic, strong) MCNearbyServiceAdvertiser *nearbyAdvertiser;
@property (nonatomic) MCPeerID *peerID;
@property (retain, nonatomic) MCAdvertiserAssistant *advertiserAssistant;
@property (nonatomic) NSMutableDictionary *peerDisplayName;


@property (nonatomic) NSMutableDictionary *hashID;




@property (nonatomic) NSString *displayUUID;
@property (nonatomic) NSString *displayName;
@property (nonatomic) NSString *serviceName;
@property (nonatomic) NSMutableData *contextData;
@property (nonatomic) NSInteger requestCount;
@property (nonatomic) NSInteger receiveCount;

@property (nonatomic, weak) id <SessionControllerDelegate> delegate;



- (instancetype)initWithDisplayName:(NSString *)displayUUID serviceType:(NSString *)serviceType service:(NSString *)serviceName displayName:(NSString *)displayName;
-(void)checkLostData:(NSString *)service dataInfo:(NSMutableDictionary *)dic otherInfo:(NSDictionary *)odic;
-(BOOL)requestLostData:(NSDictionary *)dic;
-(void)dataSender:(NSArray *)peers data:(NSArray *)dataArray;
-(void)connectStart:(MCPeerID *)peerID service:(NSString *)serviceName;

+(NSArray *)sendDataEncodeToArray:(NSInteger)ident data:(NSDictionary *)dic;
//-(void)setStream:(MCPeerID *)peer data:(NSDictionary *)dataDictionary;

-(void)setURL:(NSURL *)path peer:(MCPeerID *)peer;
-(void)cancelRequest:(NSString *)request;
-(void)setFileSize:(NSUInteger)setSize;
-(void)setLog;


-(void)selectPeer:(MCPeerID *)peer;
-(void)cancelPeer:(MCPeerID *)peer;
-(void)stopConnect;
-(void)startConnect;

-(void)endSession;


-(void)test;//test
@end


@protocol SessionControllerDelegate <NSObject>

-(BOOL)receiveDataObject:(MCPeerID *)peer state:(NSInteger)state context:(NSDictionary *)dic;
-(void)delayRequestProc;
-(void)connectPeers:(NSDictionary *)peers;
-(void)cancelPeer:(MCPeerID *)peer;


//-(void)getBufferData:(NSDictionary *)dic;
-(void)sendPeersData:(NSArray *)foundPeers connects:(NSArray *)connectPeers;

-(void)setForReceiveData:(NSString *)fileName state:(NSInteger)state;
-(void)setMessageToLog:(NSDictionary *)dic;
-(void)setErroMessageLog:(NSError *)error;

@end
