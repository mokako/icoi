//
//  PageListTable.m
//  ICOI
//
//  Created by mokako on 2014/09/05.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "PageListTable.h"

@implementation PageListTable

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        [self initializeView];
        
        
        
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/



-(void)initializeView
{
    strObjects = [[NSMutableArray alloc]init];
    viewObjects = [[NSMutableArray alloc]init];
    
    mview = [[UIView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height)];
    baseRect = mview.frame;
    mview.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
    mview.transform = CGAffineTransformMakeRotation(-M_PI/2);
    [self addSubview:mview];
    
    
    
    table = [self createTable:CGRectMake(0,0,self.frame.size.height,self.frame.size.width) tag:1];
    table.center = CGPointMake(mview.frame.size.height / 2,mview.frame.size.width / 2);
    table.backgroundColor = RGBA(35,30,25,1.0);
    table.pagingEnabled = YES;
    table.frame = baseRect;
    [mview addSubview:table];

    
}

-(void)setLog
{
    NSLog(@"table frame %@",NSStringFromCGRect(table.frame));
    NSLog(@"table bounds %@",NSStringFromCGRect(table.bounds));
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    NSLog(@"cell %@",NSStringFromCGRect([table rectForRowAtIndexPath:indexPath]));
}



-(void)review
{
    [self initLabelState];
    mview.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height);
    table.frame = CGRectMake(0,0,self.frame.size.height,self.frame.size.width);
    //[table performSelectorOnMainThread:@selector(reloadData) withObject:nil waitUntilDone:NO];
}

-(void)setTableState:(NSArray *)strArray
{
    strObjects = [NSMutableArray arrayWithArray:strArray];
    [self.delegate setPage:strObjects.count page:0];
    [self initLabelState];
    [table reloadData];
}







#pragma mark -- cell内に使うテキストの表示時の値を事前に算出します。
-(void)initLabelState
{
    CGFloat wScale = 1.0;
    CGFloat hScale = 1.0;
    CGFloat dispWidth = self.frame.size.width * 0.9;
    CGFloat dispHeight = self.frame.size.height * 0.9;
    if(dispWidth < self.width)
    {
        wScale = dispWidth / self.width;
    }
    if(dispHeight < self.height)
    {
        hScale = dispHeight / self.height;
    }
    scale = wScale < hScale ? wScale : hScale;
    

    [viewObjects removeAllObjects];
    for (NSInteger i = 0;i < strObjects.count;i++) {
        UILabel *label = [[CustomUILabel alloc]initWithFrame:CGRectMake(0, 0, self.width * scale, self.height * scale)];
        label.transform = CGAffineTransformMakeRotation(M_PI/2);
        [viewObjects addObject:label];
    }
}





-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return self.frame.size.width;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return strObjects.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CustomCell *cell;
    cell = [tableView dequeueReusableCellWithIdentifier:@"identifier"];
    NSString *ident = [NSString stringWithFormat:@"ident-%ld-%ld",(long)indexPath.section,(long)indexPath.row];
    if (cell == nil) {
        cell = [[CustomCell alloc] initWithStyleAndColor:UITableViewCellStyleSubtitle reuseIdentifier:ident defaultColor:RGBA(82,188,174,1.0) selectColor:RGBA(255,255,255,1.0) defaultBack:RGBA(255,255,255,1.0) selectBack:RGBA(245,245,245,0.0)];
    }
    for (UIView *subview in [cell.contentView subviews]) {
        [subview removeFromSuperview];
    }

    
    [self updateCell:cell atIndexPath:indexPath];
    //[self updateTextContaner:cell indexPath:indexPath.row];
    
    return cell;
}


-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //UITableViewCell *cell = [table cellForRowAtIndexPath:indexPath];
}


-(void)updateCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    ((CustomUILabel *)viewObjects[indexPath.row]).text = strObjects[indexPath.row];
    
    ((CustomUILabel *)viewObjects[indexPath.row]).backgroundColor = RGBA(255,255,255,0.0);
    ((CustomUILabel *)viewObjects[indexPath.row]).numberOfLines = 0;//self.lineCount;
    ((CustomUILabel *)viewObjects[indexPath.row]).verticalAlignment = VerticalAlignmentTop;
    //text indent

    ((CustomUILabel *)viewObjects[indexPath.row]).insets = UIEdgeInsetsMake(10,10,10,10);
    
    ((CustomUILabel *)viewObjects[indexPath.row]).lineBreakMode = NSLineBreakByWordWrapping;
  
    ((CustomUILabel *)viewObjects[indexPath.row]).font = [UIFont fontWithName:self.font.fontName size:self.font.pointSize * scale];
    ((CustomUILabel *)viewObjects[indexPath.row]).center = CGPointMake(self.frame.size.height / 2, self.frame.size.width / 2);
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell.contentView addSubview:viewObjects[indexPath.row]];
        [cell layoutSubviews];
    });
    
}


-(void)updateTextContaner:(UITableViewCell *)cell indexPath:(NSInteger)num
{
    NSTextContainer *cont = [[NSTextContainer alloc]init];
    cont = strObjects[num];
    NSLayoutManager *manager = [[NSLayoutManager alloc]init];
    [manager addTextContainer:cont];
    
    NSTextStorage *storage = [[NSTextStorage alloc]init];
    [storage addLayoutManager:manager];

    UITextView *view = [[UITextView alloc]initWithFrame:CGRectMake(0,0,self.frame.size.width,self.frame.size.height) textContainer:cont];
    NSLog(@"str objects %@",strObjects[num]);
    view.backgroundColor = RGB(5,255,55);
    view.font = [UIFont fontWithName:@"Avenir Next" size:15.0];
    view.textColor = RGBA(0,0,0,1.0);
    dispatch_async(dispatch_get_main_queue(), ^{
        [cell.contentView addSubview:view];
        [cell layoutSubviews];
    });
}


#pragma mark - UITableViewDelegate
// tableViewがスクロールした際に呼び出される
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    if([table visibleCells].count == 1)
    {
        NSIndexPath *indexPath = [table indexPathForCell:[table visibleCells][0]];
        [self.delegate setPage:strObjects.count page:indexPath.row];
    }
}



-(UITableView *)createTable:(CGRect)rect tag:(NSInteger)tag
{
    UITableView *atable = [[UITableView alloc] initWithFrame:rect  style:UITableViewStylePlain];
    //atable.backgroundColor = RGBA(45,45,45,0.6);
    atable.separatorStyle = UITableViewCellSeparatorStyleNone;
    atable.delegate = self;
    atable.dataSource = self;
    atable.tag = tag;
    atable.rowHeight = self.frame.size.height;
    atable.showsVerticalScrollIndicator = NO;
    return atable;
}



@end
