//
//  ToolWindow.h
//  ICOI
//
//  Created by mokako on 2016/01/05.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
#import "ToolWindowCell.h"


typedef NS_ENUM(NSInteger, toolMode) {
    tool_mode_title     = 0,
    tool_mode_subtitle  = 1,
    tool_mode_other     = 2,
    tool_mode_external  = 3
};


@interface ToolWindow : UIView<UITableViewDataSource,UITableViewDelegate>
{
    NSArray *item;
    NSArray *icon;
    NSArray *list;
    toolMode t_mode;
    NSString *order,*exchange,*display,*restore,*del;
}

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *close;
@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet UIView *overView;
@property (weak, nonatomic) IBOutlet UIButton *ok;

@property (weak, nonatomic) IBOutlet UILabel *desc_mess;

+ (instancetype)view;


-(void)deleteMode:(BOOL)state;
-(void)displayToggle;
-(void)closeView;
-(void)setToolMode:(NSDictionary *)selectUID root:(BOOL)root;//0 title 1 other
@end
