//
//  PathListTable.h
//  ICOI
//
//  Created by mokako on 2014/05/01.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomCell.h"

@interface PathListTable : UIView <UITableViewDataSource, UITableViewDelegate>
{
    UITableView *table;
    NSMutableArray *pathList;
}
@end
