//
//  serviceListTable.m
//  ICOI
//
//  Created by mokako on 2014/07/05.
//  Copyright (c) 2014年 moca. All rights reserved.
//
/*
 
 
 tableと編集用のボタンの配置
 
 */
#import "ServiceListTable.h"



@implementation ServiceListTable


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    }
    return self;
}



- (void)awakeFromNib {
    // Initialization code
     [super awakeFromNib];
    cedit = NO;
    list = [[NSMutableArray alloc]init];
    startEdit = NO;
    keyboardRect = CGRectZero;
    duration = 0.0;
    
    
    
    [edit setTitle:NSLocalizedString(@"Edit",@"") forState:UIControlStateNormal];
    [edit addTarget:self action:@selector(sendButStat:) forControlEvents:UIControlEventTouchUpInside];
    edit.layer.cornerRadius = 15.0;
    
    [re setTitle:NSLocalizedString(@"Back",@"") forState:UIControlStateNormal];
    [re addTarget:self action:@selector(sendButStat:) forControlEvents:UIControlEventTouchUpInside];
    
    
    UINib *nib = [UINib nibWithNibName:@"ServiceListTableCell" bundle:nil];

    service.separatorStyle = UITableViewCellSeparatorStyleNone;
    service.delegate = self;
    service.dataSource = self;
    service.estimatedRowHeight = 120.0;
    service.rowHeight = 120.0;
    [service registerNib:nib forCellReuseIdentifier:@"Cell"];
    [service setTableHeaderView:[[UIView alloc] initWithFrame:CGRectMake(0, 0, 0.1, 0.1)]];
    
    
    [self setView];

}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/


-(void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

// キーボードが表示される時に呼び出されますー

-(void)keyboardWillChange:(NSNotification *)notification {
    keyboardRect = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    duration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] doubleValue];
    UIView *superView = [self superview];
    [self removeConstraints:commentHeightContains];
    commentHeightContains =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[comment(height)]-bottom-|"
                                            options:0
                                            metrics:@{@"height":@"50",@"bottom":[NSNumber numberWithFloat:superView.frame.size.height - keyboardRect.origin.y]}
                                              views:@{@"comment":comment}];
    [self addConstraints:commentHeightContains];
    [UIView animateWithDuration:duration animations:^{
        [self layoutIfNeeded];
    } completion:^(BOOL comp){
    }];
}


-(void)openComment{
    [commentText endEditing:YES];
    UIView *superView = [self superview];
    [superView removeConstraints:commentHeightContains];
    commentHeightContains =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:0]}
                                              views:@{@"view":self}];
    [superView addConstraints:commentHeightContains];
    [UIView animateWithDuration:duration animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        
    }];
}


-(void)endComment{
    UIView *superView = [self superview];
    [superView removeConstraints:commentHeightContains];
    commentHeightContains =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view]-bottom-|"
                                            options:0
                                            metrics:@{@"bottom":[NSNumber numberWithFloat:-self.frame.size.height]}
                                              views:@{@"view":self}];
    [superView addConstraints:commentHeightContains];
    [UIView animateWithDuration:duration animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
    }];
}



-(void)setView
{

    
    //コメント用のuiview(テキストフィールドと決定ボタン)
    comment = [[UIView alloc] initWithFrame:CGRectZero];
    comment.translatesAutoresizingMaskIntoConstraints = NO;
    comment.hidden = YES;
    [self addSubview:comment];
    NSArray *h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[comment]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"comment":comment}];
    [self addConstraints:h];
    
    commentHeightContains =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[comment(height)]-bottom-|"
                                            options:0
                                            metrics:@{@"height":@50,@"bottom":[NSNumber numberWithFloat:0]}
                                              views:@{@"comment":comment}];
    [self addConstraints:commentHeightContains];
    [self layoutIfNeeded];
    
    
    

    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleDark];
    //スタイルは（UIBlurEffectStyleLight/UIBlurEffectStyleExtraLight/UIBlurEffectStyleDark）
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.translatesAutoresizingMaskIntoConstraints = NO;
    [comment addSubview:visualEffectView];
    NSArray *vw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[visualEffectView]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"visualEffectView":visualEffectView}];
    [comment addConstraints:vw];
    NSArray *vh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[visualEffectView(height)]|"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:comment.frame.size.height]}
                                              views:@{@"visualEffectView":visualEffectView}];
    [comment addConstraints:vh];
    
    
    commentText = [[UITextField alloc]initWithFrame:CGRectZero];
    [[commentText layer]setBorderColor:[RGBA(125,125,125,1.0) CGColor]];
    [[commentText layer]setBorderWidth:0.5];
    commentText.attributedPlaceholder = [[NSAttributedString alloc] initWithString:NSLocalizedString(@"setServiceComment",@"") attributes:@{NSForegroundColorAttributeName: RGBA(155,150,145,1.0) , NSFontAttributeName :  [UIFont fontWithName:@"Avenir Next" size:10.0]}];
    commentText.textAlignment = NSTextAlignmentCenter;
    commentText.backgroundColor = RGBA(255,255,255,1.0);
    commentText.font = [UIFont fontWithName:@"Avenir Next" size:14.0];
    commentText.textColor = RGBA(35,30,25,1.0);
    commentText.layer.cornerRadius = 5;
    commentText.delegate = self;
    commentText.translatesAutoresizingMaskIntoConstraints = NO;
    [commentText setClearButtonMode:UITextFieldViewModeWhileEditing];
    [comment addSubview:commentText];
    
    
    
    UIButton *sender = [[UIButton alloc]initWithFrame:CGRectZero];
    sender.tag = 10;
    [[sender layer]setBorderColor:[RGBA(125,125,125,1.0) CGColor]];
    [[sender layer]setBorderWidth:0.5];
    sender.layer.cornerRadius = 15;
    [sender setBackgroundColor:RGBA(59,192,195,1.0)];
    [sender setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    sender.titleLabel.font = [UIFont fontWithName:@"font-icon-00" size:24.0];
    sender.titleEdgeInsets = UIEdgeInsetsMake(5, 0, 0, 0);
    [sender setTitle:@"3" forState:UIControlStateNormal];
    sender.translatesAutoresizingMaskIntoConstraints = NO;
    [sender addTarget:self action:@selector(sendButStat:) forControlEvents:UIControlEventTouchUpInside];
    
    [comment addSubview:sender];
    
    /*
    
    */
    
    UIButton *can = [[UIButton alloc]initWithFrame:CGRectZero];
    can.tag = 11;
    [can setBackgroundColor:RGBA(255,255,255,0.0)];
    can.titleEdgeInsets = UIEdgeInsetsMake(-10, 0, 0, 0);
    [can setTitleColor:RGBA(255,255,255,1.0) forState:UIControlStateNormal];
    can.titleLabel.font = [UIFont fontWithName:@"font-icon-00" size:17.0];
    [can setTitle:@"t" forState:UIControlStateNormal];
    can.translatesAutoresizingMaskIntoConstraints = NO;
    [can addTarget:self action:@selector(sendButStat:) forControlEvents:UIControlEventTouchUpInside];
    [comment addSubview:can];
    
    
    NSArray *cw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-5-[back(40)]-20-[commentText]-20-[set(40)]-5-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"back":can,@"commentText":commentText,@"set":sender}];
    [comment addConstraints:cw];
    NSArray *ch =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[commentText(30)]-10-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"commentText":commentText}];
    [comment addConstraints:ch];
    NSArray *bh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[back(30)]-10-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"back":can}];
    [comment addConstraints:bh];
    NSArray *sh =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[set(30)]-10-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"set":sender}];
    [comment addConstraints:sh];
    
    UILabel *ct = [[UILabel alloc]initWithFrame:CGRectMake(5,30,30,10)];
    ct.text = NSLocalizedString(@"Back",@"");
    ct.font = [UIFont fontWithName:@"Avenir Next" size:10.0];
    ct.textColor = RGBA(255,255,255,1.0);
    ct.textAlignment = NSTextAlignmentCenter;
    ct.translatesAutoresizingMaskIntoConstraints = NO;
    [can addSubview:ct];
    NSArray *ctw =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|[ct]|"
                                            options:0
                                            metrics:nil
                                              views:@{@"ct":ct}];
    [can addConstraints:ctw];
    NSArray *cth =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[ct]-2-|"
                                            options:0
                                            metrics:nil
                                              views:@{@"ct":ct}];
    [can addConstraints:cth];
    
    keyboardHeight = 0.0;
}


-(void)sendButStat:(UIButton *)but
{
    if(but.tag == 9){
        if(cedit){
            [commentText endEditing:YES];
            [self endComment];
            
            [edit setBackgroundColor:RGBA(255,255,255,0.0)];
            cedit = NO;
            [service reloadData];
        }else{
            [edit setBackgroundColor:RGBA(255,255,255,0.2)];
            cedit = YES;
            [service reloadData];
        }
    }else if(but.tag == 8){
        [commentText endEditing:YES];
        [self endComment];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"appFront" object:self userInfo:@{@"type":@0,@"state":[NSNumber numberWithInteger:but.tag]}];
    }else if(but.tag == 10){
        //コメントをデータベースに送る
        [self.delegate setComment:list[editNumber][@"serviceName"] comment:commentText.text];
        [commentText endEditing:YES];
        [self endComment];
    }else if (but.tag == 11){
        [commentText endEditing:YES];
         [self endComment];
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self.delegate deleteService:list[indexPath.row][@"serviceName"]];
    [list removeObjectAtIndex:indexPath.row];
	[tableView deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationTop];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [self.delegate setComment:list[editNumber][@"serviceName"] comment:commentText.text];
    [commentText endEditing:YES];
    [self endComment];
    return YES;
}





-(void)reloadServiceList:(NSArray *)ary
{
    list = [NSMutableArray arrayWithArray:ary];
    [service reloadData];
}



-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return list.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.1;
}
- (UITableViewCellEditingStyle) tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return UITableViewCellEditingStyleDelete;
}

-(BOOL)tableView:(UITableView *)tableView shouldIndentWhileEditingRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}




//cell tap
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(!cedit){
        [commentText endEditing:YES];
         [self endComment];
        [self.delegate startICOI:list[indexPath.row][@"serviceName"] name:list[indexPath.row][@"displayName"] uuid:list[indexPath.row][@"displayUUID"]];
    }else{
        //コメント編集
        editNumber = indexPath.row;
        [self openComment];
        [commentText becomeFirstResponder];
        [commentText setText:list[indexPath.row][@"comment"]];
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ServiceListTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    cell.serviceName.text = list[indexPath.row][@"serviceName"];
    cell.date.text = [NSString stringWithFormat:@"%@ : %@   , %@ : %@",NSLocalizedString(@"Create",@""),[DateFormatter dateToString:list[indexPath.row][@"since"] state:0],NSLocalizedString(@"Update",@""),[DateFormatter dateToString:list[indexPath.row][@"date"] state:0]];
    cell.comment.text = list[indexPath.row][@"comment"];
    cell.edit.hidden = YES;
    if(cedit){
        cell.edit.hidden = NO;
    }
    return cell;
}

-(void)touchCell:(UIButton *)but{
    
}





















@end
