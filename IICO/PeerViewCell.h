//
//  PeerViewCell.h
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface PeerViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *name;
@property (weak, nonatomic) IBOutlet UILabel *file;
@property (weak, nonatomic) IBOutlet UILabel *state;
@property (weak, nonatomic) IBOutlet UILabel *stateIcon;
@property (weak, nonatomic) IBOutlet UILabel *ban;

@property (nonatomic) NSString *uid;
-(void)setNetworkState:(NSInteger)state;
@end
