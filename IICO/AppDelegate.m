//
//  AppDelegate.m
//  IICO
//
//  Created by moca on 2013/12/02.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import "AppDelegate.h"
#import "ConsoleViewController.h"

@implementation AppDelegate
@synthesize launchURL;
- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [self isFirstRun];
    NSURL *launch = [launchOptions objectForKey:UIApplicationLaunchOptionsURLKey];

    
	NSInteger majorVersion =
    [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] integerValue];
	if (launch && majorVersion < 4) {
		[self application:application handleOpenURL:launch];
		return NO;
	}
    return YES;
}


- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    launchURL = url;
	return NO;
}



- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{

    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

//初回判定
- (BOOL)isFirstRun
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    BOOL state;
    if ([userDefaults objectForKey:@"firstRun"]) {
        // 日時が設定済みなら初回起動でない
        state = NO;
    }else{
        state = YES;
    }
    [userDefaults setBool:state forKey:@"firstRun"];
    // 初回起動日時を設定
    
    
    // 保存
    [userDefaults synchronize];
    
    // 初回起動
    return YES;
}


@end
