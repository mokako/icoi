//
//  VisitView.h
//  ICOI
//
//  Created by mokako on 2016/03/23.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"
@interface VisitView : UIView


@property (weak, nonatomic) IBOutlet UIButton *signin;
@property (weak, nonatomic) IBOutlet UIButton *login;
@property (weak, nonatomic) IBOutlet UILabel *signIcon;
@property (weak, nonatomic) IBOutlet UILabel *loginIcon;

+(instancetype)view;

@end
