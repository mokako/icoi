//
//  LogViewCell.h
//  ICOI
//
//  Created by mokako on 2016/03/18.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LogViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *icon;
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *article;
@property (weak, nonatomic) IBOutlet UILabel *re;

@end
