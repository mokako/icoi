//
//  PeerController.m
//  ICOI
//
//  Created by mokako on 2015/06/02.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "PeerController.h"
#import "FileManager.h"

@implementation PeerController

-(instancetype)init:(NSString *)serviceName{
    self = [super init];
    if (self) {
        service = serviceName;
        list = [[NSMutableDictionary alloc]init];
        list[@"peer"] = [[NSMutableArray alloc]init];
        list[@"file"] = [[NSMutableArray alloc]init];
        [self initPeerPlist];
    }
    return self;
}

-(void)initPeerPlist{
    if(![FileManager checkFile:service fileName:@"peerController.plist"]){
        //無いので作成します。
        NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@[],@"peer",@[],@"file",nil];
        [FileManager savePlist:service fileName:@"peerController.plist" file:dic];
    }
    NSDictionary *dic = [[NSDictionary alloc]initWithDictionary:[FileManager loadPlist:service name:@"peerController.plist"]];
    list[@"peer"] = [dic[@"peer"] mutableCopy];
    list[@"file"] = [dic[@"file"] mutableCopy];
}

-(void)loadPeer{
    NSDictionary *dic = [[NSDictionary alloc]initWithDictionary:[FileManager loadPlist:service name:@"peerController.plist"]];
    list[@"peer"] = [dic[@"peer"] mutableCopy];
    list[@"file"] = [dic[@"file"] mutableCopy];
}
-(void)addPeer:(NSString *)peerName{
    [list[@"peer"] addObject:peerName];
    [FileManager savePlist:service fileName:@"peerController.plist" file:list];
}

-(void)removePeer:(NSString *)peerName{
    [list[@"peer"] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:peerName]) {
            [list[@"peer"] removeObjectAtIndex:idx];
            *stop = YES;
        }   
    }];
    [FileManager savePlist:service fileName:@"peerController.plist" file:list];
}

-(void)addFile:(NSString *)fileName{
    [list[@"file"] addObject:fileName];
    [FileManager savePlist:service fileName:@"peerController.plist" file:list];
}

-(void)removeFile:(NSString *)fileName{
    [list[@"file"] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:fileName]) {
            [list[@"file"] removeObjectAtIndex:idx];
            *stop = YES;
        }
    }];
    [FileManager savePlist:service fileName:@"peerController.plist" file:list];
}

-(BOOL)checkPeer:(NSString *)peerName{
    __block BOOL hit = NO;
    [list[@"peer"] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:peerName]) {
            hit = YES;
            *stop = YES;
        }
    }];
    return hit;
}

-(BOOL)checkFile:(NSString *)fileName{
    __block BOOL hit = NO;
    [list[@"file"] enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        if ([obj isEqualToString:fileName]) {
            hit = YES;
            *stop = YES;
        }
    }];
    return hit;
}


@end
