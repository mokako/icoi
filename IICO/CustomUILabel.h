//
//  CustomUILabel.h
//  ICOI
//
//  Created by mokako on 2014/09/20.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>

typedef enum VerticalAlignment {
    VerticalAlignmentTop,
    VerticalAlignmentMiddle,
    VerticalAlignmentBottom,
} VerticalAlignment;



@interface CustomUILabel : UILabel
{
    UIEdgeInsets insets;
@private
    VerticalAlignment verticalAlignment_;
}

@property (nonatomic) UIEdgeInsets insets;

@property (nonatomic, assign) VerticalAlignment verticalAlignment;

@end
