//
//  SessionController.m
//  ICOI
//
//  Created by moca on 2013/12/09.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import "SessionController.h"



@interface SessionController () <MCSessionDelegate ,NSStreamDelegate,MCNearbyServiceAdvertiserDelegate,MCNearbyServiceBrowserDelegate>


@end

@implementation SessionController

#pragma mark - Accessor methods


#pragma mark - Lifecycle methods

- (instancetype)initWithDisplayName:(NSString *)displayUUID serviceType:(NSString *)serviceType service:(NSString *)serviceName displayName:(NSString *)displayName
{
    self = [super init];
    if (self) {
        self.peerDisplayName = [[NSMutableDictionary alloc]init];

        self.receiveCount = 0;
        self.requestCount = 0;
        loopRequest = @"";
        self.serviceName = serviceName;
        self.serviceType = serviceType;
        self.displayName = displayName;
        self.displayUUID = displayUUID;
        self.peerID = [[MCPeerID alloc] initWithDisplayName:displayUUID];
        self.session = [[MCSession alloc]initWithPeer:self.peerID securityIdentity:nil encryptionPreference:MCEncryptionNone];
        self.session.delegate = self;
        self.contextData = [NSMutableData data];
        
        requestArray = [[NSMutableArray alloc]init];
        noneExistList = [[NSMutableArray alloc]init];
        noneExistListCurrentCount = 0;
        
        [self startAdvertiser];
        [self setBrowser];
        
        

        
        
        
        myPeers = [[NSMutableArray alloc]init];
        
        //_roop = [NSRunLoop currentRunLoop];
        recieve_queue = dispatch_queue_create("com.senpu-ki.recieve", 0);
        
    }
    return self;
}

- (void)dealloc
{
    [self.nearbyAdvertiser stopAdvertisingPeer];
    [self.nearbyBrowser stopBrowsingForPeers];
    [self.session disconnect];
    
}

-(void)startSession{
    self.session = [[MCSession alloc]initWithPeer:self.peerID securityIdentity:nil encryptionPreference:MCEncryptionNone];
    self.session.delegate = self;
    [self startAdvertiser];
    [self setBrowser];
}

-(void)setBrowser
{
    self.nearbyBrowser = [[MCNearbyServiceBrowser alloc]initWithPeer:self.peerID serviceType:self.serviceType];
    self.nearbyBrowser.delegate = self;
    [self.nearbyBrowser startBrowsingForPeers];
}
-(void)startAdvertiser
{

    NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
    NSString *stringToTime = [NSString stringWithFormat:@"%f",time];

    
    self.hashID = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.displayName,@"name",stringToTime,@"date",nil];

    //まず自peerを登録及び更新します。

    [self.contextData setData:[NSKeyedArchiver archivedDataWithRootObject:self.hashID]];
    

    
    self.nearbyAdvertiser = [[MCNearbyServiceAdvertiser alloc]
                        initWithPeer:self.peerID
                        discoveryInfo:self.hashID
                        serviceType:self.serviceType];
    
    self.nearbyAdvertiser.delegate = self;
    
    [self.nearbyAdvertiser startAdvertisingPeer];
}

-(void)startConnect
{
    
    NSTimeInterval time = [NSDate timeIntervalSinceReferenceDate];
    NSString *stringToTime = [NSString stringWithFormat:@"%f",time];
    
    
    self.hashID = [[NSMutableDictionary alloc]initWithObjectsAndKeys:self.displayName,@"name",stringToTime,@"date",nil];
    
    //まず自peerを登録及び更新します。
    
    [self.contextData setData:[NSKeyedArchiver archivedDataWithRootObject:self.hashID]];
    

    self.session = [[MCSession alloc]initWithPeer:self.peerID];
    self.session.delegate = self;
    self.nearbyBrowser = [[MCNearbyServiceBrowser alloc]initWithPeer:self.peerID serviceType:self.serviceType];
    self.nearbyBrowser.delegate = self;
    
    self.nearbyAdvertiser = [[MCNearbyServiceAdvertiser alloc]
                             initWithPeer:self.peerID
                             discoveryInfo:self.hashID
                             serviceType:self.serviceType];
    
    self.nearbyAdvertiser.delegate = self;
    
    
    [self.nearbyAdvertiser startAdvertisingPeer];
    [self.nearbyBrowser startBrowsingForPeers];
}


-(void)stopConnect
{
    for(NSString *name in [self.peerDisplayName allKeys]){
        [self cancelPeer:self.peerDisplayName[name][@"peer"]];
    }
    [self.session disconnect];
    [self.nearbyBrowser stopBrowsingForPeers];
    [self.nearbyAdvertiser stopAdvertisingPeer];
    
}


-(void)endSession
{
    [self stopConnect];
}


-(void)selectPeer:(MCPeerID *)peer
{
    [self.session cancelConnectPeer:peer];
    /*
    if(self.session){
        [self.nearbyBrowser invitePeer:peer toSession:self.session withContext:nil timeout:4];
    }
     */
}

-(void)cancelPeer:(MCPeerID *)peer
{
    [self.session cancelConnectPeer:peer];
    
}
-(void)browser:(MCNearbyServiceBrowser *)browser foundPeer:(MCPeerID *)peerID withDiscoveryInfo:(NSDictionary *)info
{
    self.peerDisplayName[peerID.displayName] = [[NSMutableDictionary alloc]initWithObjectsAndKeys:peerID,@"peer",info[@"name"],@"name",info[@"date"],@"date",@"1",@"state", nil];
    [self.nearbyBrowser invitePeer:peerID toSession:self.session withContext:self.contextData timeout:30];
}


-(void)test{
    [self.session disconnect];
    self.session.delegate = nil;
    [self startSession];
}

-(BOOL)checkPeer:(NSDictionary *)dic peer:(MCPeerID *)peer id:(NSString *)identifier
{
    for(MCPeerID *peer in [dic allKeys])
    {
        if([dic[peer][@"id"] isEqualToString:identifier]){
            return  YES;
        }
    }
    return NO;
}


- (void)browser:(MCNearbyServiceBrowser *)mybrowser lostPeer:(MCPeerID *)peerID
{
}

-(void)browser:(MCNearbyServiceBrowser *)browser didNotStartBrowsingForPeers:(NSError *)error
{
}


-(void)advertiser:(MCNearbyServiceAdvertiser *)advertiser didReceiveInvitationFromPeer:(MCPeerID *)peerID withContext:(NSData *)context invitationHandler:(void (^)(BOOL, MCSession *))invitationHandler
{
    NSDictionary *info = [[NSDictionary alloc]initWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:context]];
    self.peerDisplayName[peerID.displayName] = [[NSMutableDictionary alloc]initWithObjectsAndKeys:peerID,@"peer",info[@"name"],@"name",info[@"date"],@"date",@"1",@"state", nil];
    invitationHandler(YES,self.session);
}


- (void)session:(MCSession *)session didReceiveCertificate:(NSArray *)certificate fromPeer:(MCPeerID *)peerID certificateHandler:(void(^)(BOOL accept))certificateHandler
{
    certificateHandler(YES);
}

-(void)startFound{
    [self.nearbyBrowser startBrowsingForPeers];
    front = YES;
}

-(void)setLog
{

}
#pragma mark - MCSessionDelegate methods

- (void)session:(MCSession *)session peer:(MCPeerID *)peerID didChangeState:(MCSessionState)state
{
    if (state == MCSessionStateConnected) {
        self.peerDisplayName[peerID.displayName][@"state"] = @"2";
        [self connectStart:peerID service:self.serviceName];
        NSDictionary *setDic = @{@"id":@(4),@"text":[NSString stringWithFormat:@"%@ state change : connected",self.peerDisplayName[peerID.displayName][@"name"]],@"state":@(1)};
        [self.delegate setMessageToLog:setDic];
    } else if(state == MCSessionStateNotConnected){
        self.peerDisplayName[peerID.displayName][@"state"] = @"0";
        NSDictionary *setDic = @{@"id":@(4),@"text":[NSString stringWithFormat:@"%@ state change : not connected",self.peerDisplayName[peerID.displayName][@"name"]],@"state":@(0)};
        
        [self.delegate setMessageToLog:setDic];
    }else if (state == MCSessionStateConnecting){
        self.peerDisplayName[peerID.displayName][@"state"] = @"1";
    }
    [self.delegate connectPeers:self.peerDisplayName];
}

-(void)connectStart:(MCPeerID *)peerID service:(NSString *)serviceName
{
    NSError *error;
    NSDictionary *dic = [FileManager loadDataPlist:serviceName];
    NSArray *a = [[NSArray alloc]initWithObjects:@200,dic,nil];
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:a];
    [self.session sendData:data toPeers:@[peerID] withMode:MCSessionSendDataReliable error:&error];
}

-(void)refoundPeer
{
    [self.nearbyBrowser startBrowsingForPeers];
    if(front == NO){
        [self performSelector:@selector(delayProc) withObject:nil afterDelay:4];
    }
}

-(void)delayProc
{
    [self.nearbyBrowser stopBrowsingForPeers];
}

 - (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
 {
     dispatch_async(recieve_queue, ^{
         NSArray *dataArray = [[NSArray alloc]initWithArray:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
         NSInteger c = [dataArray[0] integerValue];
         NSDictionary *dic = [[NSDictionary alloc]initWithDictionary:dataArray[1]];
         [self.delegate receiveDataObject:peerID state:c context:dic];
     });
     
 }


/*
- (void)session:(MCSession *)session didReceiveData:(NSData *)data fromPeer:(MCPeerID *)peerID
{
    [self.delegate receiveDataObject:data peer:peerID];
}
*/
- (void)session:(MCSession *)session
didStartReceivingResourceWithName:(NSString *)resourceName
       fromPeer:(MCPeerID *)peerID
   withProgress:(NSProgress *)progress
{
    //requestした画像の受信を開始
    [requestArray removeObject:[resourceName stringByDeletingPathExtension]];//一旦requestArrayから指定の要素を削除
}

- (void)session:(MCSession *)session
didFinishReceivingResourceWithName:(NSString *)resourceName
       fromPeer:(MCPeerID *)peerID
          atURL:(NSURL *)localURL
      withError:(NSError *)error
{
    if(error){
        [self.delegate setForReceiveData:[resourceName stringByDeletingPathExtension] state:0];
        [noneExistList addObject:[resourceName stringByDeletingPathExtension]];//途中で失敗してるのでfile要請に追加
    }else{
        
        if (![[NSFileManager defaultManager] copyItemAtPath:[localURL path] toPath:[FileManager setFilePath:self.serviceName name:resourceName] error:nil])
        {
            //保存に失敗時
            [noneExistList addObject:[resourceName stringByDeletingPathExtension]];//失敗してるのでfile要請に追加
            [self.delegate setForReceiveData:[resourceName stringByDeletingPathExtension] state:0];
            
        }else{
            //success時はrequestArrayとnonExistListから共に要素を削除された状態になっています。
            [self.delegate setForReceiveData:[resourceName stringByDeletingPathExtension] state:2];
        }
    }
    //layerTableの情報を更新します。
}


#pragma mark - stream
///////////////////////////////////////////  STREAM  //////////////////////////////////////////////////////


- (void)session:(MCSession *)session didReceiveStream:(NSInputStream *)stream withName:(NSString *)streamName fromPeer:(MCPeerID *)peerID
{
    //inputStream = stream;
    //inputStream.delegate = self;
    //[inputStream scheduleInRunLoop:_roop forMode:NSDefaultRunLoopMode];
    //[inputStream open];
}
/*
- (void)stream:(NSStream *)stream handleEvent:(NSStreamEvent)eventCode {
    switch(eventCode) {
        case NSStreamEventNone:
        {
        }
            break;
        case NSStreamEventOpenCompleted:
        {
            if(stream == inputStream){

            }

        }
            break;
        case NSStreamEventHasBytesAvailable:
        {
            if(stream == inputStream){
                if (bufData == nil) {
                    bufData = [[NSMutableData alloc] init];
                }
                uint8_t buf[1024];
                unsigned long int len = 0;
                len = [(NSInputStream *)stream read:buf maxLength:1024];
                if(len) {
                    [bufData appendBytes:(const void *)buf length:len];
                    [self.delegate replaceDataLog:[NSString stringWithFormat:@"recive %ld / %ld",(long)bufData.length,(long)size]];
                }
            }
        }
            break;
        case NSStreamEventHasSpaceAvailable:
        {
            if(stream == outputStream){
                uint8_t *readBytes = (uint8_t *)[setData mutableBytes];
                readBytes += byteIndex; // instance variable to move pointer
                NSUInteger data_len = [setData length];
                NSUInteger len = ((data_len - byteIndex >= 1024) ?
                                1024 : (data_len - byteIndex));
                uint8_t buf[len];
                (void)memcpy(buf, readBytes, len);
                len = [outputStream write:(const uint8_t *)buf maxLength:len];
                byteIndex += len;
            }
        }
            break;
        case NSStreamEventErrorOccurred:
        {
            //network error
            [inputStream close];
            [inputStream removeFromRunLoop:_roop
                                   forMode:NSDefaultRunLoopMode];
            inputStream = nil;
            bufData = nil;
            //openStream = NO;
            [self checkData];
        }
            break;
        case NSStreamEventEndEncountered:
        {
            if(stream == inputStream){
                //data complete
                [inputStream close];
                [inputStream removeFromRunLoop:_roop
                              forMode:NSDefaultRunLoopMode];
                inputStream = nil;
                if(size == bufData.length){
                    
                    [self.delegate getBufferData:[[NSDictionary alloc]initWithDictionary:[NSKeyedUnarchiver unarchiveObjectWithData:bufData]]];
                }else{
                    //recieve miss data
                    [self checkData];
                    
                }
                bufData = nil;
                //openStream = NO;
            }
        }
            break;
            default:
            break;
    }
}
*/
#pragma mark - Public methods


-(void)dataSender:(NSArray *)peers data:(NSArray *)dataArray
{
    if(peers.count != 0){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
            @autoreleasepool {
                NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dataArray];
                NSError *error;
                if([self.session sendData:data  toPeers:peers withMode:MCSessionSendDataReliable error:&error]){
                    
                }else{
                    [self.delegate setErroMessageLog:error];
                }
                data = nil;
            }
        });
    }
}
/*
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{

 });
 
 */


#pragma mark - url送信
-(void)setURL:(NSURL *)path peer:(MCPeerID *)peer
{
    //
    
    [self.session sendResourceAtURL:path withName:[path lastPathComponent] toPeer:peer withCompletionHandler:^(NSError *error) {
        if (error) {
            [self.delegate setErroMessageLog:error];
        }
        self.requestCount--;
        NSDictionary *dic = [[NSDictionary alloc]init];
        [self dataSender:self.session.connectedPeers data:[SessionController sendDataEncodeToArray:304 data:dic]];

    }];
}


//streamは使用していません。ただし動画などの用途があるため開発の痕跡は維持を
/*
-(void)setStream:(MCPeerID *)peer data:(NSDictionary *)dataDictionary
{
    dispatch_async(stream_queue, ^{
        NSError *error;
        outputStream = [self.session startStreamWithName:[NSString stringWithFormat:@"%@",peer] toPeer:peer error:&error];
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:dataDictionary];
        [outputStream setDelegate:self];
        [outputStream open];
        const uint8_t *buf = [data bytes];
        NSUInteger length = [data length];
        [outputStream write:buf maxLength:length];
        [outputStream close];
    });
}

*/

#pragma mark -check directory
////
#pragma mark - 他のデータリストと比較して自分に無いデータが相手が持っているか比較します。
//-(void)checkSaveFileData:(NSString *)service dataInfo:(NSDictionary *)dicはこれが完成次第廃止しこちらをメイン処理として採用
-(void)checkLostData:(NSString *)service dataInfo:(NSMutableDictionary *)dic otherInfo:(NSDictionary *)odic{
    for(NSString *key in [odic allKeys])
    {
        if([odic[key][@"fileState"] integerValue] == 4 || [dic[key][@"fileState"] integerValue] == 4){
            //throw
        }else if([odic[key][@"fileState"] integerValue] == 2 && [dic[key][@"fileState"] integerValue] == 0){
            //こちらに無く相手側にある場合
            [noneExistList addObject:key];
        }
    }
    //重複を削除
    noneExistList = [NSMutableArray arrayWithArray:[[NSSet setWithArray:noneExistList] allObjects]];
    [self requestLostData:dic];
}


-(NSArray *)checkInfoData:(NSDictionary *)dic{
    NSMutableArray *n = [[NSMutableArray alloc]init];
    for(NSString *key in [dic allKeys]){
        if([dic[key][@"fileState"] integerValue] != 2){
            [n addObject:key];
        }
    }
    return n;
}

//自分も他人も持っていないデータはその情報を持っている人が居ないのでリストにあげても開始されないので他人と比較が妥当と思います。
-(BOOL)requestLostData:(NSDictionary *)dic{
    /*
     ここで一旦dataInfoとの齟齬を解消します。
    */
    NSArray *ary = [[NSArray alloc]initWithArray:[self checkInfoData:dic]];//残りのファイルをinfoDataより調査
    if(ary.count == 0){
        [noneExistList removeAllObjects];
        noneExistListCurrentCount = 0;
        return NO;
    }
    
    
    
    
    
    noneExistListCurrentCount = noneExistListCurrentCount >= dic.count ? 0 : noneExistListCurrentCount;
    
    
    
    
    if(noneExistList.count == 0){noneExistListCurrentCount = 0;return NO;}//無いなら処理を中止
    if(requestArray.count < receiveMaxCount){
        NSDictionary *sendData = [[NSDictionary alloc]initWithObjectsAndKeys:noneExistList[noneExistListCurrentCount],@"file", nil];
        [self dataSender:self.session.connectedPeers data:[SessionController sendDataEncodeToArray:300 data:sendData]];
        [requestArray addObject:noneExistList[noneExistListCurrentCount]];//requestを送った要素をrequestArrayで保管
        [noneExistList removeObjectAtIndex:noneExistListCurrentCount];//noneExistListからruquestを送った要素を削除
        double delayInSeconds = 10.0;
        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
        dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
            //停滞を防ぐためある一定の時間毎にデータ取得用の処理を起動させます
            [self delayRequestDataCheck];
        });
        
    }
    noneExistListCurrentCount = noneExistListCurrentCount + 1 >= dic.count ? 0 : noneExistListCurrentCount++;
    return YES;
}



-(void)cancelRequest:(NSString *)request{
    [requestArray removeObject:request];
    [noneExistList addObject:request];
    [self.delegate delayRequestProc];
}

-(void)delayRequestDataCheck{

    if(requestArray.count == 0){return;}
    
    NSString *d = [requestArray firstObject];
    [requestArray removeObjectAtIndex:0];
    if([loopRequest isEqualToString:d]){
        //処理が停滞しています。これは同じデータがループして要求されてる為なので一旦要素を削除したまま放置します。
        //noneExistListにも返しません。
        return;
    }else{
        [noneExistList addObject:d];
        [self.delegate delayRequestProc];
    }
    loopRequest = d;
}
/////////////////////////////////////////////////////////////////////////////


+(NSArray *)sendDataEncodeToArray:(NSInteger)ident data:(NSDictionary *)dic
{
    return [[NSArray alloc]initWithObjects:[NSNumber numberWithInteger:ident],dic,nil];
}


-(void)setFileSize:(NSUInteger)setSize
{
    size = setSize;
}



@end