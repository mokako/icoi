//
//  ServiceManager.m
//  ICOI
//
//  Created by mokako on 2015/10/27.
//  Copyright © 2015年 moca. All rights reserved.
//

#import "ServiceManager.h"

@implementation ServiceManager
-(id)init{
    self = [super init];
    if(self){
        self.selectFile = @"Image";//default image name
        tempList = [@{} mutableCopy];
        self.titleID = @"";
        self.mode = 0;
        self.edit_mode = edit_normal;
        DM = [[DataListManager alloc]init];
    }
    return self;
}



/*
 self.displayUUID = displayUUID;
 //"icoi-"と"icois"の２種類を作成できるようにする。
 serviceOfICOI = [NSString stringWithFormat:@"%@%@",@"icoi-",serviceType];
 service = serviceType;
 self.displayName = displayName;
 
 */

-(void)dealloc{
    session.delegate = nil;
    session = nil;
}


#pragma mark - initialize service

-(void)initService:(NSString *)serviceName display:(NSString *)displayName uuid:(NSString *)uuid{
    service = serviceName;
    display = displayName;
    dspUUID = uuid;
    serviceAccount = [NSString stringWithFormat:@"%@%@",@"icoi-",service];
    pcon = [[PeerController alloc]init:service];
}

-(BOOL)setFileManager
{
    //ここを改良してください。
    if(![FileManager checkServiceDir:service]){
        if([FileManager createServiceDir:service]){
            BOOL a = [FileManager createSysDir:service];
            BOOL b = [FileManager createFileDir:service];
            BOOL c = [FileManager initDataPlist:service];
            if(a == YES && b == YES && c == YES){
                //データの初期化が全て成功したので次にデータベースのファイルと必要なデータのセットを行います。
                [self setDictionaryData:service];
                [self initDatabase];
                [self setFirstImage];
                return YES;
            }else{
                [DataManager alert:@"UNSUCCESS" text:@"error initialize."];
                [FileManager deleteServiceDir:service];
                return NO;
            }
        }else{
            return NO;
        }
    }else{
        //既にサービスフォルダが存在します。
        
        //必要なデータを取得
        [self setDictionaryData:service];
        [self initDatabase];
        
        return YES;
    }
    return NO;
}


-(void)initDatabase
{//データベースファイルは一律でicoi_data.dbとし、sysフォルダ内に作成します。
    if([FileManager checkDatabaseFile:service]){
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:service] stringByAppendingPathComponent:@"sys"] stringByAppendingPathComponent:@"icoi_data.db"];
        db = [FMDatabaseQueue databaseQueueWithPath:path];
        //database と app version　との整合性を確認
        [self checkDatabaseScheme];
    }else{
        //新規作成なので整合性チェックはしません。
        NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
        NSString *path = [[[[[paths objectAtIndex:0] stringByAppendingPathComponent:@"service"] stringByAppendingPathComponent:service] stringByAppendingPathComponent:@"sys"] stringByAppendingPathComponent:@"icoi_data.db"];
        db = [FMDatabaseQueue databaseQueueWithPath:path];
        [DataManager initializeTable:db];
    }
}


-(void)checkDatabaseScheme{
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    if ([DM.version compare:version options:NSNumericSearch] == NSOrderedAscending) {
        [DataManager updateDataTable:db];
        DM.version = version;
        if([DM save]){
            [self setMessageform:message_type_system message:@"Save the success of data info." state:message_state_positive];
        }else{
            [self setMessageform:message_type_system message:@"save failure of data info." state:message_state_positive];
        }
    }
}




-(void)setDictionaryData:(NSString *)serviceName
{//仕様変更　まずinfo.plistを読み込みそれを元に配列を読み込みます。
    [DM setServiceName:serviceName];
    [DM initDataList];
    if(![DM.dataInfo isEqual:[NSNull null]]){
        if ([DM.dataInfo count] != 0){
            DM.dataInfo = [[DataManager checkFileToDataList:service dataList:DM.dataInfo] mutableCopy];
        }
    }
}

-(void)setFirstImage
{
    
    NSString *fileName = [NSString stringWithFormat:@"Image.PNG"];
    NSData *dat = [[NSData alloc] initWithData:UIImagePNGRepresentation([ICOIImage createImage:CGRectMake(0,0,768,1024) color:RGB(255,255,255)])];
    
    //実際の画像データをfileディレクトリに作成
    [FileManager saveFile:service fileName:fileName file:dat];
    
    //画像の名前をデータベースに作成
    [DataManager insertFileName:db name:@"Image"];
    DM.dataInfo = [@{@"Image" : @{
                             @"title"   : @"Image",
                             @"exte"    : @"PNG",
                             @"doner"   : @"system",
                             @"donorID" : @"",
                             @"mime"    : @"image",
                             @"lastUpdate" : [NSDate date],
                             @"fileState" : @2,
                             @"class"   : @1
                             }} mutableCopy];
    
    [DM save];
}





//サービス開始への着火です。
-(void)ignition{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@0,@"list":DM.dataInfo}];
}


#pragma mark - session controller


-(void)connectPeers:(NSDictionary *)peers{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendPeer" object:self userInfo:@{@"type":@0,@"list":peers}];
    //[self.peerView setData:nil connects:peers];
}


-(void)cancelPeer:(MCPeerID *)peer
{
    [session cancelPeer:peer];
}

-(void)sendPeersData:(NSArray *)foundPeers connects:(NSArray *)connectPeers
{
    //[self.peerView setData:foundPeers connects:connectPeers];
}

-(void)setMessageform:(message_type)type message:(NSString *)string state:(message_state)state{
    [self setMessageToLog:@{@"id" : [NSNumber numberWithInteger:type],@"text":string,@"state":[NSNumber numberWithInteger:state]}];
}

-(void)setMessageToLog:(NSDictionary *)dic{
    //sketchにも伝達させる
    [[NSNotificationCenter defaultCenter] postNotificationName:@"addLogBadge" object:self userInfo:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLog" object:self userInfo:@{@"type":@0,@"message":dic}];
}


-(void)setErroMessageLog:(NSError *)error{
    NSString *mess;
    NSString *title;
    bool r;
    if(error.localizedFailureReason == nil){
        mess = error.localizedDescription;
        r = NO;
    }else{
        mess = error.localizedFailureReason;
        r = YES;
    }
    if(error.localizedRecoverySuggestion.length != 0){
        title = error.localizedRecoverySuggestion;
    }else{
        title = @"error";
    }
    [self setMessageToLog:@{@"id":@(4),@"text":[NSString stringWithFormat:@"%@ : %@",title,mess],@"state":@(r == YES ? 0 : 1)}];
    
}


#pragma mark - sketch data
-(void)setSendData:(NSDictionary *)dic
{
    //自身パスデータをデータベースに保管します。
    [DataManager addPathData:db data:dic];
    [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:601 data:dic]];
}


-(void)setRequestForList:(NSString *)uid{
    NSDictionary *dic = @{@"uid": uid.length == 0 ? [NSNull null] : uid,@"fname":self.selectFile};
    [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:602 data:dic]];
}

#pragma mark -- リスト要求を解析し返信します
-(void)replyRequestForList:(NSDictionary *)_data peer:(MCPeerID *)peer{
    NSArray *ary = [self getListManager:_data[@"uid"] root:YES];
    if(ary){
        NSDictionary *newdata = @{@"uid":_data[@"uid"],@"fname":_data[@"fname"],@"list":ary};
        [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:603 data:newdata]];
    }
    
    /*
    if(![_data[@"uid"] isEqual:[NSNull null]]){
        //リスト(2 - 3)を返します
        NSDictionary *dic = [DataManager getItem:db fileID:_data[@"fname"] uid:_data[@"uid"]];
        
        
        
        NSLog(@"dic %@",dic);
        if(dic){
            NSArray *ary;
            
            
            
            switch ([dic[@"state"] integerValue]) {
                case 2:
                {// -> 3
                    ary = [DataManager getBranch:db fileID:_data[@"fname"] uid:_data[@"uid"] mode:GetDataLevel];
                }
                    break;
                case 3:
                {// -> 0, 1, 4
                    ary = [DataManager getBranch:db fileID:_data[@"fname"] uid:_data[@"uid"] mode:GetDataOther];
                }
                    break;
                default:
                    break;
            }
            NSLog(@"ary %@",ary);
            if(ary){
                NSDictionary *newdata = @{@"uid":_data[@"uid"],@"fname":_data[@"fname"],@"list":ary};
                [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:603 data:newdata]];
            }
        }
    }else{
        //uidからstateを解析し適切なリストを返します
        NSArray *ary = [DataManager getTreeList:db fileID:_data[@"fname"]];
        if(ary){
            NSDictionary *newdata = @{@"uid":_data[@"uid"],@"fname":_data[@"fname"],@"list":ary};
            [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:603 data:newdata]];
        }
    }
     */

}

/*
受信したリストを一時保存し受信がなくなり次第反映させます。
 
*/
-(void)receiveRequestForList:(NSDictionary *)_data{
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(reflectTheChangeList) object:nil];
    for(NSDictionary *dic in _data[@"list"]){
        if([tempList objectForKey:dic[@"uid"]] != nil){
            //既にキーが存在する場合は時間で比較して新しい物を採用します
            if([tempList[dic[@"uid"]][@"date"] doubleValue] < [dic[@"date"] doubleValue]){
                //更新
                NSNumber *count = tempList[dic[@"uid"]][@"count"];
                tempList[dic[@"uid"]] = [dic mutableCopy];
                tempList[dic[@"uid"]][@"count"] = count;
            }
        }else{
            //まだ無いキーはuidでキーを作成します
            tempList[dic[@"uid"]] = [dic mutableCopy];
            tempList[dic[@"uid"]][@"count"] = [NSNumber numberWithInteger:tempList.count];
        }
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        [self performSelector:@selector(reflectTheChangeList) withObject:nil afterDelay:5];
    });
}

-(void)reflectTheChangeList{
    //databaseに反映
    NSSortDescriptor *sortDescString;
    sortDescString = [[NSSortDescriptor alloc] initWithKey:@"count" ascending:YES];
    // NSSortDescriptorは配列に入れてNSArrayに渡す
    NSArray *sortDescArray;
    sortDescArray = [NSArray arrayWithObjects:sortDescString, nil];

    // ソートの実行
    NSArray *sortArray;
    sortArray = [[tempList allValues] sortedArrayUsingDescriptors:sortDescArray];
    [DataManager insertOrUpdateList:db data:sortArray];
    
    
     [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:self userInfo:@{@"type":@99}];
    
    [tempList removeAllObjects];
}

/*
-(NSArray *)getSubjectList{
    return [DataManager getPathWithRange:db fid:self.selectFile limit:limit offset:offset];
}
*/




#pragma mark - data proccess
-(BOOL)receiveDataObject:(MCPeerID *)peer state:(NSInteger)state context:(NSDictionary *)dic
{
    
    // 時間を測りたい処理
    
    //0x178013dc0
    
    
    
    /*
     host flag
     これはhostと繋がっているpeerだけが持っています。
     このpeerと繋がっているpeerの中にhostが居る事になります。
     誰かが切断された時普通のpeerは対象のpeerIDを削除するだけですが、
     hostと繋がっているpeerは自身と繋がっているpeerにmanifestoの提出を要求します。
     
     
     
     peerIDが紐付けられている情報
     この処理の最大の問題点でdisplayNameとの紐付けは行われているが内部的なものなのか
     出力されて見えるものなのかということころです。
     
     実際に出力される値を見る限りでは値は変動的なのでこちらでの値の操作は難しい所だと思われます。
     
     検証の要求
     
     どの程度紐付けられているのかという事と紐付けは必要なことなのかということです。
     
     想定される方法
     peerIDは受信側で固定されているという事実です。
     その値を利用する方法
     ただし一意なのかどうかということです。
     特定の人にメッセージを送る方法を採用する場合に必要ですが、その処理を採用しないなら必要ではありません。
     
     その場合はhostかどうかの値だけが必要です。
     
     
     検証課題
     
     peerへの全体送信はどのようにして伝達されるのか？
     たとえば近接しているpeerにしか送信出来ないのか？
     バックグラウンドで伝達を補助する処理が有り、バイパスとなって情報を伝達しているのか？
     
     
     
     */
    
    
    
    switch (state) {
            //100~ manifesto plist
            
            
        case 100:
        {
            
        }
            break;
        case 101:
        {
            
        }
            break;
        case 102:
        {
            
        }
            break;
        case 103:
        {
            
        }
            break;
        case 104:
        {
            
        }
            break;
        case 105:
        {
            
        }
            break;
            //200~ data plist8
            
        case 200:
        {
            //結合しチェックします。
            //[DM.dataInfo addEntriesFromDictionary:dic[@"dataInfo"]];
            DM.dataInfo = [[DataManager checkFileToDataList:service dataList:[DataManager checkDictionaryCompare:DM.dataInfo new:dic[@"dataInfo"]]] mutableCopy];
            [DM save];
            [session checkLostData:service dataInfo:DM.dataInfo otherInfo:dic[@"dataInfo"]];
            
            //リストの提出
            /*
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:210 data:[self getMessageList]]];
            });
            */
             
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@0,@"list":DM.dataInfo}];
        }
            break;
        case 201: //data.plist receive
        {
        }
            break;
        case 202:
        {//受信したデータリストを検証
            //[DataManager addFileData:db data:DM.dataInfo];
        }
            break;
        case 203:
        {//スタックデータの受信
            
            DM.dataInfo = [[DataManager checkFileToDataList:service dataList:[DataManager checkDictionaryCompare:DM.dataInfo new:dic]] mutableCopy];
            [DM save];
            [session checkLostData:service dataInfo:DM.dataInfo otherInfo:dic];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@1,@"list":dic,@"item":[dic allKeys][0]}];
        }
            break;
        case 204:
        {//削除依頼
            [self deleteRequest:dic[@"fileID"]];
        }
            break;
            
            
        //messageのtitle及びsub titleのリストの提出要請
        case 210:
        {
            
            
        }
            break;
            //300~からはデータの送受信部分に成ります。
            /*
             送受信のルール
             
             データは必ずdictionaryデータで送信します。
             
             
             NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
             service   ,   @"serviceName"  ,
             exte               ,   @"exte"         ,
             mime               ,   @"mime"         ,
             fileName           ,   @"fileName"     ,
             update             ,   @"lastUpdate"   ,
             , nil];
             
             
             -付きがdata.plistの通信保証部分です。
             *付きはデータの振り分けとデータ部分です。
             -*が付いていない部分はdataInfoに格納される部分です。
             
             typeは取り扱い方を規定します。
             何に対してサーブおよびレシーブされたのを設定されています。
             mimeはデータの型を指定します。
             hostIDは自由モードでは空ですがhostモードでは指定されます。
             fileNameはdataを受信後にファイル化させる時に使います。
             dataは実際のデータがバイナリ形式で格納されています。
             
             
             
             
             
             
             ビーコンを送信します。
             内容は送信した時刻、file名、fileの有無BOOL値
             
             
             ビーコンを使用する場合peerは全員になってしまう。それではもの凄い数が送信される恐れが有る。
             
             
             データの流れの検証
             全peerにデータを送信したとしてそれは途中の端末もデータが通る事を意味する。
             そのデータは端末によって拾われるのかという事です。フレームワーク内で処理されてスルーされるなら良いですがもし
             
             
             */
        case 300:
        {
            
            //他のpeerから該当のファイルデータが有るかの通信が来ます。
            //無いなら無視で有るなら301に返信してください。
            
            if(session.requestCount < requestMaxCount){
                if([DM.dataInfo[dic[@"file"]][@"fileState"] integerValue] == 2){
                    [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:301 data:dic]];
                }
            }
            
        }
            break;
        case 301:
        {
            //該当のデータのダウンロードの受諾通知です。
            //この時に最初に届いた相手に302申請を出してください。
            if([DM.dataInfo[dic[@"file"]][@"fileState"] integerValue] == 0){
                DM.dataInfo[dic[@"file"]][@"fileState"] = @"1";
                [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:302 data:dic]];
            }
        }
            break;
        case 302:
        {//データ送信要請を受信
            /*
             
             データの送受信時の取り決め
             
             まず自分に該当のファイルデータが有るか無いか調べ、無いならPEERに無い事を送信します。
             この時fileデータをdectionaryデータとして保持し、ダウンロード要求をしているかどうかを調べます。
             送信内容は欲しいfile情報と送信時の時間です。
             データを要求して一番最初に返信してくれた人にデータの要求をします。
             
             
             
             */
            if(session.requestCount < requestMaxCount){
                NSString *file = [NSString stringWithFormat:@"%@.%@",dic[@"file"],DM.dataInfo[dic[@"file"]][@"exte"]];
                session.requestCount++;
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                    [session setURL:[NSURL fileURLWithPath:[FileManager setFilePath:service name:file]] peer:peer];
                });
            }else{
                //先にリクエストを許可申請を出したので拒否内容を通知
                [session dataSender:@[peer] data:[SessionController sendDataEncodeToArray:303 data:dic]];
            }
        }
            break;
        case 303:
        {//送信拒否 新しい受信元を探す
            
            DM.dataInfo[dic[@"file"]][@"fileState"] = @"0";
            [session cancelRequest:dic[@"file"]];
            [self delayRequestProc];
        }
            break;
        case 304:
            
        {//データが受信終了した後に回りに必要かの通知を出します。これはその通知の受信部分です。
            [self delayRequestProc];
        }
            break;
        case 401: //manifesto error 立候補の失敗時等に送られてきます。
        {
            //NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:@"type",@"message",@"title",@"UNSUCCESS",@"resulte",@"The request has been rejected.",nil];
            
        }
            break;
#pragma mark - Chatdata用
        case 500:
        {
            //[chat setMessageFromUser:dic];
        }
            break;
        case 600:
        {//sketch用
            
            
        }
            break;
        case 601:
        {//sketchデータの受信部分
            //除外リストに名前が存在するかどうか
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                if([pcon checkPeer:peer.displayName]){
                    //throw
                }else{
                    //受信したパス情報をデータベースに格納します。
                    NSDictionary *new = [ServiceManagerAssist adjustDictionary:dic];
                    [DataManager addPathData:db data:new];
                    [self pathDivision:new];
                }
            });
            
        }
            break;
        case 602:
        {//listの提出要請
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                [self replyRequestForList:dic peer:peer];
            });
        }
            break;
        case 603:
        {//listの受信
            //変更要素も投げられます
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
                [self receiveRequestForList:dic];
            });
        }
            break;
        case 604:
        {//sketchデータのコメント部分の変更を承認します
            
        }
            break;
        //700番台はユーザー情報を送信
        case 700:
        {//現在選択している画像 NSDictionary *dic = @{@"user":dspUUID,@"image":DM.dataInfo[fileName]};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"peerViewCell" object:self userInfo:@{@"type":@0,@"user":dic[@"user"],@"item":dic[@"image"]}];
        }
            break;
        case 900:
        {
            //例外処理用
            //[self setDataLog:@"例外処理が発生しました。"];
        }
            break;
        default:
        {
            [DataManager alert:@"ERROR" text:@"Invalid parameter."];
        }
            break;
    }
    return YES;
}



-(void)pathDivision:(NSDictionary *)dic{
    if([dic[@"fid"] isEqualToString:self.selectFile]){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"addPath" object:self userInfo:@{@"path":dic}];
    }else{
        
    }
    
    
}

/*
 -(void)getBufferData:(NSDictionary *)dic
 {
 if([DM.dataInfo[[dic[@"listNum"]integerValue]][@"fileState"] integerValue] == 2){
 DM.dataInfo[[dic[@"listNum"]integerValue]][@"fileState"] = @3;
 dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT ,0 ), ^{
 [FileManager saveFile:service fileName:[NSString stringWithFormat:@"%@.%@",dic[@"fileName"],dic[@"exte"]] file:dic[@"data"]];
 
 });
 }
 }
 
 */

#pragma mark - setLayer url送信が完了すれば呼ばれます。
-(void)setForReceiveData:(NSString *)fileName state:(NSInteger)state
{
    if(state == 0){
        DM.dataInfo[fileName][@"fileState"] = @"0";
        
    }else{
        
        [DataManager insertFileName:db name:fileName];
         DM.dataInfo[fileName][@"fileState"] = @"2";
        [DM save];
        
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@2,@"list":DM.dataInfo,@"item":fileName}];
        
        
        
        NSDictionary *setDic = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInteger:2],@"id",@"New theme has been added.",@"text",[NSNumber numberWithInteger:0],@"state",nil];
        [self setMessageToLog:setDic];
    }
    //自分のfileデータに抜けが無いかチェック
    [self delayReceiveProc];
}

-(void)delayReceiveProc{
    [session requestLostData:DM.dataInfo];
    NSDictionary *dic = [[NSDictionary alloc]init];
    //fileデータのノードとして周りに拡散を促す
    [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:304 data:dic]];
}

-(void)delayRequestProc{
    [session requestLostData:DM.dataInfo];
    
}

#pragma mark - get message title list
-(NSDictionary *)getMessageList{
    return  @{@"list":[DataManager getMessageList:db]};
}



#pragma mark - file manager

-(BOOL)checkImageFile:(NSString *)fileName {
    NSString *path = [NSString stringWithFormat:@"%@.%@",fileName,DM.dataInfo[fileName][@"exte"]];
    return [FileManager checkFile:service fileName:path];
}

-(UIImage *)displaythumbnailImage:(NSString *)fileName size:(CGFloat)size{
    NSString *path = [NSString stringWithFormat:@"%@.%@",fileName,DM.dataInfo[fileName][@"exte"]];
    UIImage *img = [[UIImage alloc]initWithData:[FileManager loadFile:service name:path]];
    return [ICOIImage thumbnail:img size:size];
}

-(NSString *)getFilePath:(NSString *)fileName{
    return [NSString stringWithFormat:@"%@.%@",fileName,DM.dataInfo[fileName][@"exte"]];
}

-(void)setDeleteRequest:(NSString *)imageID{
    DM.dataInfo[imageID][@"fileState"] = @"4";
    NSString *path = [NSString stringWithFormat:@"%@.%@",imageID,DM.dataInfo[imageID][@"exte"]];
    [FileManager removeFile:service fileName:path];
    [DM save];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@0,@"list":DM.dataInfo}];
    NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:imageID,@"fileID", nil];
    [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:204 data:dic]];
    NSDictionary *setDic = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInteger:2],@"id",@"Theme has been removed.",@"text",[NSNumber numberWithInteger:1],@"state",nil];
    [self setMessageToLog:setDic];
}
-(void)deleteRequest:(NSString *)imageID{
    DM.dataInfo[imageID][@"fileState"] = @"4";
    NSString *path = [NSString stringWithFormat:@"%@.%@",imageID,DM.dataInfo[imageID][@"exte"]];
    [FileManager removeFile:service fileName:path];
    
    [DM save];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@0,@"list":DM.dataInfo}];
    NSDictionary *setDic = [[NSDictionary alloc]initWithObjectsAndKeys:[NSNumber numberWithInteger:2],@"id",@"Theme has been removed.",@"text",[NSNumber numberWithInteger:1],@"state",nil];
    [self setMessageToLog:setDic];
}

-(void)setNewFileData:(NSDictionary *)fileData
{
    //新規Fileデータをデータベースに追加
    NSString *fileName = [fileData allKeys][0];
    [DataManager insertFileName:db name:fileName];
    //dataListに追加
    [DM.dataInfo addEntriesFromDictionary:fileData];
    //dataListを保存
    
    [DM save];
    
    
    [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:203 data:fileData]];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendLayer" object:self userInfo:@{@"type":@3,@"list":DM.dataInfo,@"item":fileName}];
}


#pragma mark - peer controller
-(void)appealPeerToConsole:(NSString *)peerName{
    [pcon addPeer:peerName];
}

-(void)loadPeerController{
    [pcon loadPeer];
}


#pragma mark - set pick up item


#pragma mark - chage image to sketch and path list
-(void)selectImage:(NSString *)fileName
{
    NSString *path = [NSString stringWithFormat:@"%@.%@",fileName,DM.dataInfo[fileName][@"exte"]];
    self.selectFile = fileName;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"setItem" object:self userInfo:@{@"image":[UIImage imageWithData:[FileManager loadFile:service name:path] scale:[[UIScreen mainScreen] scale]],@"name":fileName}];
    NSDictionary *dic = @{@"user":dspUUID,@"image":DM.dataInfo[fileName]};
    [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:700 data:dic]];
}


#pragma mark - get database data

-(NSArray *)getListManager:(NSString *)uid root:(BOOL)root{
    if(![uid isEqual:[NSNull null]]){
        if(uid.length != 0){
            NSDictionary *dic = [self getItemToList:uid];
            NSInteger sta = [dic[@"state"] integerValue];
            if(sta == 2){
                NSArray *ary = root == YES ? [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataLevel] : [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataLevelOther];
                if(root){self.pathOrder = [ServiceManagerAssist initIndexPath:ary];}
                
                return ary;
            }else if(sta == 3){
                NSArray *ary = [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataLevelOther];
                self.pathOrder = [ServiceManagerAssist initIndexPath:ary];
                return ary;
            }
        }else{
            NSArray *ary = root == YES ? [DataManager getTreeList:db fileID:self.selectFile] : [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataOther];
            if(root){self.pathOrder = [ServiceManagerAssist initIndexPath:ary];}
            return ary;
        }
        
        
    }else{
        NSArray *ary = root == YES ? [DataManager getTreeList:db fileID:self.selectFile] : [DataManager getBranch:db fileID:self.selectFile uid:@"" mode:GetDataOther];
        if(root){self.pathOrder = [ServiceManagerAssist initIndexPath:ary];}
        return ary;
    }
    return @[];
}



-(NSArray *)getTreeList{
    NSArray *ary = [DataManager getTreeList:db fileID:self.selectFile];
    self.pathOrder = [ServiceManagerAssist initIndexPath:ary];
    return ary;
}

-(NSArray *)getTreeBranch:(NSString *)uid{
    NSArray *ary = [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataLevel];
    self.pathOrder = [ServiceManagerAssist initIndexPath:ary];
    return ary;
}

-(NSArray *)getTreeAll:(NSString *)uid{
    NSArray *ary = [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataAll];
    self.pathOrder = [ServiceManagerAssist initIndexPath:ary];
    return ary;
}

-(NSInteger)getTreeNumbar:(NSString *)uid{
    return [[DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataAll] count];
}

-(NSArray *)getTreeMessage:(NSString *)uid{
    NSArray *ary = [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataOther];
    self.pathOrder = [ServiceManagerAssist initIndexPath:ary];
    return ary;
}

-(NSInteger)getTreeMessageNumbar:(NSString *)uid{
    return [[DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataOther] count];
}

-(NSArray *)getTreeBranchMessage:(NSString *)uid{
    NSArray *ary = [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataLevelOther];
    self.pathOrder = [ServiceManagerAssist initIndexPath:ary];
    return ary;
}

-(NSInteger)getTreeBranchMessageNumbar:(NSString *)uid{
    return [[DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataLevelOther] count];
}


-(NSDictionary *)getItemToList:(NSString *)uid{
    return [DataManager getItem:db fileID:self.selectFile uid:uid];
}

-(BOOL)setUpdateNumber:(NSArray *)list{
    return [DataManager updateWithNumber:db list:list];
}

-(BOOL)setUpdateList:(NSArray *)list{
    return [DataManager updateWhithList:db list:list];
}

-(BOOL)setUpdateComment:(NSDictionary *)_dic{
    NSMutableDictionary *dic = [[self getItemToList:_dic[@"uid"]] mutableCopy];
    dic[@"date"] = [NSNumber numberWithDouble:[DateFormatter julianDate:[NSDate new]]];
    dic[@"comment"] = _dic[@"text"];
    BOOL re = NO;
    if([DataManager insertOrUpdateList:db data:@[dic]]){
        re = YES;
        NSDictionary *newdata = @{@"uid":dic[@"uid"],@"fname":dic[@"fid"],@"list":@[dic]};
        [session dataSender:session.session.connectedPeers data:[SessionController sendDataEncodeToArray:603 data:newdata]];
    }
    return re;
    //
}


-(BOOL)deleteUidFromList:(NSArray *)list{
    return [DataManager deleteSelectedPaths:db fileID:self.selectFile list:list];
}

-(NSArray *)getListWithoutException:(NSString *)uid{
    return [DataManager getBranch:db fileID:self.selectFile uid:uid mode:GetDataALL_2];
}

-(NSArray *)getRestoreList{
    return [DataManager getBranch:db fileID:self.selectFile uid:@"" mode:GetRestoreData];
}
#pragma mark - service state

-(void)startService{
    session = [[SessionController alloc] initWithDisplayName:dspUUID serviceType:serviceAccount service:service displayName:display];
    session.delegate = self;
    [session startConnect];
    
}

-(void)stopService{
    [DM save];
    [session stopConnect];
}

-(void)deleteService{
    [FileManager deleteServiceDir:service];
}

-(void)endService
{//終了処理
    //自作NSNotificationCenterをremoveしていきます。
    [[NSNotificationCenter defaultCenter] postNotificationName:@"endService" object:self userInfo:nil];
    
    
    [session endSession];
    session = nil;
    [DM save];
}


#pragma mark - get methods
-(NSString *)getService{
    return service;
}
-(NSString *)getUUID{
    return dspUUID;
}
-(NSString *)getDisplay{
    return display;
}

-(double)getJulianDay:(NSDate *)date{
    return [DateFormatter julianDate:date];
}








@end
