//
//  CommentTool.h
//  ICOI
//
//  Created by mokako on 2014/12/27.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#define RGB(r, g, b) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1]
#define RGBA(r, g, b, a) [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:a]


@protocol CommentToolDelegate<NSObject>
-(void)setEditTab:(NSInteger)state;
-(void)tapSpotPointWithCGpoint:(CGPoint)point;
-(void)commentBut:(UIButton *)but;
@end


@interface CommentTool : UIView
{
    UIButton *lock;
    BOOL isLock;
}


@property (nonatomic, weak)id<CommentToolDelegate>delegate;
-(void)update;
@end
