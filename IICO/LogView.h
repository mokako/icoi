//
//  LogView.h
//  ICOI
//
//  Created by mokako on 2015/07/16.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "CustomCell.h"
#import "UIColor+Hex.h"
#import "ICOI.h"
#import "LogViewCell.h"
#import <iAd/iAd.h>

@interface LogView : UIView<UITableViewDataSource,UITableViewDelegate,ADBannerViewDelegate>
{
    BOOL isVisible;
    NSMutableArray *logList;
    NSArray *titleID,*colorList,*typeColor,*titleIcon;
}

@property (weak, nonatomic) IBOutlet UITableView *table;
@property (weak, nonatomic) IBOutlet ADBannerView *ad;
@property (nonatomic)NSInteger maxLog;
-(void)addMessage:(NSDictionary *)dic;
+ (instancetype)view;
@end
