//
//  FileManager.h
//  ICOI
//
//  Created by moca on 2013/12/26.
//  Copyright (c) 2013年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DataManager.h"
@class FileManager;

@protocol FileManagerDelegate <NSObject>
@required
-(void)FileManagerClosed:(FileManager *)FileManager;
@end


@interface FileManager : NSObject

@property (nonatomic, weak) id <FileManagerDelegate> delegate;


-(NSString *)DocumentRoot;


//temporary
+(NSArray *)loadTempList;
+(NSDictionary *)getAttributesOfFile:(NSString *)file;
+(NSDictionary *)getAttributesOfFileSystem:(NSString *)file;
+(BOOL)removeTmpFile:(NSString *)file;
+(UIImage *)readImageOfTmp:(NSString *)file;
+(NSString *)readTextFileOfTmp:(NSString *)file;


//base
+(BOOL)checkServiceDir:(NSString *)serviceName;
+(BOOL)createServiceDir:(NSString *)serviceName;
+(BOOL)createSysDir:(NSString *)serviceName;
+(BOOL)deleteServiceDir:(NSString *)serviceName;
+(BOOL)checkDatabaseFile:(NSString *)serviceName;
+(BOOL)createFileDir:(NSString *)serviceName;


//test
//+(void)getDir:(NSString *)serviceName;
//+(void)readFileDir:(NSString *)serviceName;

//sys/manifesto.plist
+(BOOL)initManifestoPlist:(NSString *)serviceName;
+(NSDictionary *)loadManifestoPlist:(NSString *)serviceName;
+(BOOL)saveManifestoPlist:(NSDictionary *)dic service:(NSString *)serviceName;

//sys/data.plist
+(BOOL)initDataPlist:(NSString *)serviceName;
+(BOOL)saveDataPlist:(NSString *)serviceName data:(NSDictionary *)dic;
+(NSDictionary *)loadDataPlist:(NSString *)serviceName;


//file/
+(BOOL)saveFile:(NSString *)serviceName fileName:(NSString *)fileName file:(id)file;
+(id)loadFile:(NSString *)serviceName name:(NSString *)fileName;
+(id)getData:(NSString *)path;
+(BOOL)checkFile:(NSString *)serviceName fileName:(NSString *)fileName;
+(BOOL)removeFile:(NSString *)serviceName fileName:(NSString *)fileName;
+(NSString *)setFilePath:(NSString *)serviceName name:(NSString *)fileName;


//sys/peerController.plist
+(BOOL)savePlist:(NSString *)serviceName fileName:(NSString *)fileName file:(NSDictionary *)dic;
+(NSDictionary *)loadPlist:(NSString *)serviceName name:(NSString *)fileName;

@end
