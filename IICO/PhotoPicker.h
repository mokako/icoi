//
//  PhotoPicker.h
//  ICOI
//
//  Created by mokako on 2014/02/13.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import "ICOI.h"
#import "ServiceManager.h"
#import "PickerView.h"


@interface PhotoPicker : UIView<UITextFieldDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,PickerViewDelegate>
{
    UICollectionView *collectview;
    UICollectionViewFlowLayout *layout;
    NSMutableArray *photos;
    UIImagePickerController *imagePicker;
    UIImage *tmpImage;
    UIView *imageBoard;
    CGFloat layoutWidth;
    UITextField *text;
    PickerView *picker;
    NSInteger selectIndex;
    ServiceManager *sb;

}


-(void)reloadCollection;
+ (ALAssetsLibrary *)defaultAssetsLibrary;
-(void)endTask;


@end
