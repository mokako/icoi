//
//  HeaderCurveView.h
//  ICOI
//
//  Created by mokako on 2016/03/26.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface HeaderCurveView : UIView
{

}

@property (nonatomic) IBInspectable CGFloat angle;
@property (nonatomic) IBInspectable UIColor *color;


@end
