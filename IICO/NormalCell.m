//
//  RegularCell.m
//  ICOI
//
//  Created by mokako on 2016/02/05.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "NormalCell.h"

@implementation NormalCell

- (void)awakeFromNib {
    [super awakeFromNib];
    fontSize = 15.0;
    colorList = @[
                  RGBA(145,145,145,0.5),
                  RGBA(88,179,49,0.5),
                  RGBA(0,159,232,0.5),
                  RGBA(236,111,0,0.5),
                  RGBA(240,96,96,0.5)
                  ];
    
    //exchage
    a = [[CustomBarButton alloc]initWithTitle:@"C" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    a.tag = 1;
    //rename
    b = [[CustomBarButton alloc]initWithTitle:@"d" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    b.tag = 2;
    //exclusion
    c = [[CustomBarButton alloc]initWithTitle:@"J" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    c.tag = 3;
    //delete
    d = [[CustomBarButton alloc]initWithTitle:@"r" style:UIBarButtonItemStylePlain target:self action:@selector(setItem:)];
    d.tag = 4;

    
    gap = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    flex2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    flex2.width = 5.0;
    flex = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    flex.width = 20.0;
    
    self.ct.items = @[flex2,a,gap,b,gap,d,flex];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

-(void)drawCorner{
    //[self.baseView setAngleWithColor:10.0 color:[UIColor colorWithHex:@"E6E5EB"]];
}

-(void)setButton:(NSInteger)state holder:(BOOL)holder{
    self.stateIcon.textColor = colorList[state];
    switch (state) {
        case 0://path
        {
            self.ct.items = holder == YES ? @[flex2,a,gap,b,gap,d,flex] : @[flex2,a,gap,d,flex];
            self.stateIcon.text = @"4";
            self.dateType.text = @"P";
        }
            break;
        case 1://path
        {
            self.ct.items = holder == YES ? @[flex2,a,gap,b,gap,d,flex] : @[flex2,a,gap,d,flex];
            self.stateIcon.text = @"4";
            self.dateType.text = @"P";
        }
            break;
        case 2://title
        {
            self.ct.items = holder == YES ? @[flex2,b,gap,c,gap,d,flex] : @[flex2,c,gap,d,flex];
            self.stateIcon.text = @"n";
            self.dateType.text = @"j";
        }
            break;
        case 3://subtitle
        {
            self.ct.items = holder == YES ? @[flex2,a,gap,b,gap,c,gap,d,flex] : @[flex2,a,gap,c,gap,d,flex];
            self.stateIcon.text = @"d";
            self.dateType.text = @"j";
        }
            break;
        case 4://message
        {
            self.ct.items = holder == YES ? @[flex2,a,gap,b,gap,d,flex] : @[flex2,a,gap,d,flex];
            self.stateIcon.text = @"j";
            self.dateType.text = @"P";
        }
            break;
        default:
            self.ct.items = @[flex2,a,gap,d,flex];
            break;
    }
}


-(void)setArticleText:(NSString *)str{
    self.article.text = str;
}

/*
- (CGFloat)layoutManager:(NSLayoutManager *)layoutManager lineSpacingAfterGlyphAtIndex:(NSUInteger)glyphIndex withProposedLineFragmentRect:(CGRect)rect
{
    // ここで高さを設定する
    return 5;
}
*/
 
//押されたspotのステータス及びcellの値を取得し送信
-(IBAction)touch:(UIButton *)but{

}

-(void)setItem:(CustomBarButton *)sta{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@20,@"state":[NSNumber numberWithInteger:sta.tag],@"indexPath":self.indexPath}];
}

#pragma mark - cell slide proc

-(void)slideView
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.forView.frame = CGRectMake(0,0,self.frame.size.width,self.forView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         isOpen = NO;
                     }];
}

-(void)slideViewLeft
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.forView.frame = CGRectMake(self.frame.size.width * -1,0,self.frame.size.width,self.forView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         self.forView.frame = CGRectMake(0,0,self.frame.size.width,self.frame.size.height - 4);
                     }];
}
-(void)slideViewRight
{
    [UIView animateWithDuration:0.2f
                     animations:^{
                         self.forView.frame = CGRectMake(self.frame.size.width - 20,0,self.frame.size.width,self.forView.frame.size.height);
                     }
                     completion:^(BOOL finished){
                         isOpen = YES;
                     }];
}

-(void)scrollStop
{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@10}];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesBegan:touches withEvent:event];
    //UITouch *touch = [touches anyObject];
    //[self.delegate toucheBegan];
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    oldPoint = currentPoint;
    isSlide = NO;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    //[super touchesMoved:touches withEvent:event];
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    CGFloat dif = currentPoint.x - oldPoint.x;
    //しきい値を設定
    if(dif < slideMinWidth && dif > -slideMinWidth){
        //低い値は無視してください
    }else{
        isSlide = YES;
        [self scrollStop];
        if(dif > 0){
            self.forView.frame = CGRectMake(dif,0,self.frame.size.width,self.forView.frame.size.height);
        }else{
            //負の値は設定していません
        }
    }
    
    
}
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:self];
    
    CGFloat dif = currentPoint.x - oldPoint.x;
    if(dif > 70){
        [self slideViewRight];
    }else{
        [self slideView];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@11}];
    if(!isSlide){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@12,@"indexPath":self.indexPath}];
    }
    
}


- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    [self slideView];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendNormalList" object:nil userInfo:@{@"type":@11}];
}





@end
