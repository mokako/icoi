//
//  ColorToolView.m
//  ICOI
//
//  Created by mokako on 2015/10/10.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "ColorToolView.h"

@implementation ColorToolView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.hidden = YES;
        self.translatesAutoresizingMaskIntoConstraints = NO;
        self.layer.borderColor = [UIColor colorWithHex:@"cccccc"].CGColor;
        self.layer.borderWidth = 0.5;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(fadeOut) name:@"fadeOut" object:nil];
        
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    mode = 0;
    selectColor = [NSIndexPath indexPathForRow:0 inSection:0];
    color1 = 0;
    
    self.glasses.layer.shadowRadius = 1.0;
    
    NSBundle* bundle = [NSBundle mainBundle];
    NSString* path = [bundle pathForResource:@"color" ofType:@"plist"];
    NSDictionary* dic = [NSDictionary dictionaryWithContentsOfFile:path];
    list =[NSArray arrayWithArray:[dic objectForKey:@"Color"]];

    
    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = self.bounds;
    [self insertSubview:visualEffectView atIndex:0];

    [self.fade addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
    
    
    UISlider *slider = [[UISlider alloc]initWithFrame:CGRectMake(0,0,240,30)];
    slider.center = CGPointMake(15,120);
    slider.minimumValue = 0.1;
    slider.maximumValue = 1.0;
    slider.minimumTrackTintColor = RGBA(96,96,85,1.0);
    slider.maximumTrackTintColor = RGBA(96,96,85,1.0);
    slider.value = 1.0;
    
    slider.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    [slider addTarget:self action:@selector(state:)
     forControlEvents:UIControlEventValueChanged];
    [self.opacityBase addSubview:slider];
    [slider setThumbImage:[UIImage imageNamed:@"ind1.png"] forState:UIControlStateNormal];
    
    
    
    UINib *nib = [UINib nibWithNibName:@"ColorListCell" bundle:nil];
    // UICollectionViewに項目表示に使うセルとして登録
    [self.collection registerNib:nib forCellWithReuseIdentifier:@"Cell"];
    self.collection.dataSource = self;
    self.collection.delegate = self;
    
}

-(void)state:(UISlider*)slider{
    CGFloat s = ceilf(slider.value * 10);
    CGFloat c = s / 10;
    self.glasses.textColor = [self.glasses.textColor colorWithAlphaComponent:c];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@5,@"state":[NSNumber numberWithFloat:c]}];
}


-(void)set{
    UIView *superView = [self superview];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:-self.frame.size.width],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    h =
    [NSLayoutConstraint constraintsWithVisualFormat:@"V:[view(height)]"
                                            options:0
                                            metrics:@{@"height":[NSNumber numberWithFloat:self.frame.size.height]}
                                              views:@{@"view":self}];
    [superView addConstraints:h];
    [superView addConstraint:[NSLayoutConstraint constraintWithItem:self  attribute:NSLayoutAttributeCenterY  relatedBy:NSLayoutRelationEqual  toItem:superView  attribute:NSLayoutAttributeCenterY  multiplier:1  constant:0]];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


#pragma mark - collection view





-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return list.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ColorListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    cell.tapCell.backgroundColor = [UIColor colorWithHex:list[indexPath.row]];
    [cell setSelectedState:selectColor.row == indexPath.row ? YES : NO];
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
     [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketch" object:nil userInfo:@{@"type":@4,@"state":[UIColor colorWithHex:list[indexPath.row]]}];
    
    ColorListCell *oldCell = (ColorListCell *)[collectionView cellForItemAtIndexPath:selectColor];
    [oldCell setSelectedState:NO];
    
    ColorListCell *cell = (ColorListCell *)[collectionView cellForItemAtIndexPath:indexPath];
    selectColor = indexPath;
    [cell setSelectedState:YES];
}



-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{

}








#pragma mark - display

-(void)toggle{
    if(isToggle){
        [self fadeOut];
    }else{
        isToggle = YES;
        [self fadeIn];
    }
}

-(void)fadeIn{
    self.hidden = NO;
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:80],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    }];
}


-(void)fadeOut{
    isToggle = NO;
    UIView *superView = [self superview];
    [superView removeConstraints:w];
    w =
    [NSLayoutConstraint constraintsWithVisualFormat:@"|-padding-[view(width)]"
                                            options:0
                                            metrics:@{@"padding":[NSNumber numberWithFloat:-self.frame.size.width],@"width":[NSNumber numberWithFloat:self.frame.size.width]}
                                              views:@{@"view":self}];
    [superView addConstraints:w];
    [UIView animateWithDuration:0.5 animations:^{
        [superView layoutIfNeeded];
    } completion:^(BOOL comp){
        self.hidden = YES;
    }];
}



@end
