//
//  SketchListTabView.m
//  ICOI
//
//  Created by mokako on 2016/03/22.
//  Copyright © 2016年 moca. All rights reserved.
//

#import "SketchListTabView.h"

@implementation SketchListTabView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.hidden = YES;
        isSlide = NO;
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendSketchListTab" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    self.tab.layer.cornerRadius = 12;
    self.tab.transform = CGAffineTransformMakeRotation(M_PI / 2);
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


-(void)fadeIn{
    self.hidden = NO;
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 1.0;
    } completion:^(BOOL comp){
    }];
}


-(void)fadeOut{
    [UIView animateWithDuration:0.5 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL comp){
        self.hidden = YES;
    }];
}



- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    CGPoint currentPoint = [[touches anyObject] locationInView:[self superview]];
    oldPoint = currentPoint;
    self.tab.textColor = [UIColor colorWithHex:@"ffffff"];
    self.tab.backgroundColor = [UIColor colorWithHex:@"2E97D8"];
    isSlide = NO;
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    //[super touchesMoved:touches withEvent:event];
    CGPoint currentPoint = [[touches anyObject] locationInView:[self superview]];
    CGFloat dif = oldPoint.x - currentPoint.x;
    //しきい値を設定
    isSlide = YES;
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketchList" object:nil userInfo:@{@"type":@20,@"x":[NSNumber numberWithFloat:-dif]}];
    oldPoint = currentPoint;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if(!isSlide){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"sendSketchList" object:nil userInfo:@{@"type":@10}];
    }
    self.tab.textColor = [UIColor colorWithHex:@"677176"];
    self.tab.backgroundColor = [UIColor colorWithHex:@"2E97D8" alpha:0.0];

}



- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event{
    self.tab.textColor = [UIColor colorWithHex:@"677176"];
    self.tab.backgroundColor = [UIColor colorWithHex:@"2E97D8" alpha:0.0];
}

-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0:
        {
            [self fadeIn];
        }
            break;
        case 1:
        {
            [self fadeOut];
        }
            break;
        default:
            break;
    }
}

@end
