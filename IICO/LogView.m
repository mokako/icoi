//
//  LogView.m
//  ICOI
//
//  Created by mokako on 2015/07/16.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "LogView.h"
#import "ClearToolBar.h"

@implementation LogView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        // Initialization code
        logList = [@[] mutableCopy];
        self.maxLog = 200;
        
        titleID = @[
                    NSLocalizedString(@"Board",@""),
                    NSLocalizedString(@"Flip",@""),
                    NSLocalizedString(@"Theme",@""),
                    NSLocalizedString(@"Album",@""),
                    NSLocalizedString(@"Network",@""),
                    @"System"
                    ];
        
        colorList = @[
                      RGBA(59,192,195,1.0),
                      RGBA(0,116,188,1.0),
                      RGBA(218,80,14,1.0),
                      RGBA(242,56,39,1.0),
                      RGBA(54,47,60,1.0),
                      RGBA(101,108,126,1.0)
                      ];
        
        typeColor = @[
                     [UIColor colorWithHex:@"F23827"],//否定
                     [UIColor colorWithHex:@"0074BC"],//追認
                     [UIColor colorWithHex:@"99C630"]//肯定
                     ];
        
        
        titleIcon = @[
                      @"O",
                      @"c",
                      @"N",
                      @"G",
                      @"5",
                      @"9"
                      ];
        
        
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(catchEvent:) name:@"sendLog" object:nil];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    
    
    
    UINib *nib = [UINib nibWithNibName:@"LogViewCell" bundle:nil];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    self.table.estimatedRowHeight = 76.0;
   [self.table registerNib:nib forCellReuseIdentifier:@"Cell"];

    self.ad.delegate = self;
    self.ad.hidden = YES;
    self.ad.autoresizesSubviews = YES;
    self.ad.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    
    
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

+ (instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}


-(void)update:(BOOL)rotate{

}
-(void)bannerViewDidLoadAd:(ADBannerView *)banner{
    self.ad.hidden = NO;
}

//バナー表示でエラーが発生した場合
-(void)bannerView:(ADBannerView *)banner didFailToReceiveAdWithError:(NSError *)error{
    self.ad.hidden = YES;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return logList.count;
}


-(void)addMessage:(NSDictionary *)dic{
    dispatch_async(dispatch_get_main_queue(),^{
        [self.table beginUpdates];
        [logList insertObject:dic atIndex:0];
        NSIndexPath *new = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.table insertRowsAtIndexPaths:@[new] withRowAnimation:UITableViewRowAnimationNone];
        if(logList.count >= self.maxLog){
            [logList removeLastObject];
            NSIndexPath *path = [NSIndexPath indexPathForRow:logList.count - 1 inSection:0];
            [self.table deleteRowsAtIndexPaths:@[path] withRowAnimation:UITableViewRowAnimationNone];
        }
        [self.table endUpdates];
    });
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    LogViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    NSInteger i = [logList[indexPath.row][@"id"] integerValue];
    cell.title.text = titleID[i];
    cell.icon.textColor = colorList[i];
    cell.icon.text = titleIcon[i];
    cell.article.text = logList[indexPath.row][@"text"];
    cell.re.textColor = typeColor[[logList[indexPath.row][@"state"] integerValue]];

    return cell;
}



-(void)catchEvent:(NSNotification *)center{
    switch ([[center userInfo][@"type"] integerValue]) {
        case 0: //
        {
            [self addMessage:[center userInfo][@"message"]];
        }
            break;
        default:
            break;
    }
}


@end
