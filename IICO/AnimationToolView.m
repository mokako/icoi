//
//  AnimationToolView.m
//  ICOI
//
//  Created by mokako on 2015/10/08.
//  Copyright (c) 2015年 moca. All rights reserved.
//

#import "AnimationToolView.h"

@implementation AnimationToolView
- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        self.clipsToBounds = YES;
        view = [[UIView alloc]initWithFrame:CGRectMake(-20,0,40,40)];
        view.hidden = YES;
        [self insertSubview:view atIndex:0];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        visualEffectView.frame = view.bounds;
        [view addSubview:visualEffectView];
        
        
        UIButton *but = [[UIButton alloc]initWithFrame:CGRectMake(0,0,40,40)];
        but.titleLabel.font = [UIFont fontWithName:@"select" size:20];
        but.layer.cornerRadius = 5;
        but.titleEdgeInsets = UIEdgeInsetsMake(0,20, 0, 0);
        [but setTitle:@"w" forState:UIControlStateNormal];
        but.titleLabel.textAlignment = NSTextAlignmentCenter;
        [but setTitleColor:RGBA(103,113,118,1.0) forState:UIControlStateNormal];
        [but addTarget:self action:@selector(fadeIn) forControlEvents:UIControlEventTouchUpInside];
        [view addSubview:but];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    

    UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
    UIVisualEffectView *visualEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
    visualEffectView.frame = self.base.frame;
    [self.base insertSubview:visualEffectView atIndex:0];
    [self.list addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    
    [self.chat addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.play addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.paste addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.del addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.paste addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.lock addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.setting addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.menu addTarget:self action:@selector(setBut:) forControlEvents:UIControlEventTouchUpInside];
    [self.fade addTarget:self action:@selector(fadeOut) forControlEvents:UIControlEventTouchUpInside];
}


+(instancetype)view
{
    NSString *className = NSStringFromClass([self class]);
    return [[[NSBundle mainBundle] loadNibNamed:className owner:nil options:0] firstObject];
}

- (IBAction)buttonTapped:(id)sender
{
}

-(void)fadeOut{
    UIView *superView = [self superview];
    [UIView animateWithDuration:0.25 animations:^{
        self.base.frame = CGRectMake(-self.base.frame.size.width,self.base.frame.origin.y,self.base.frame.size.width,self.base.frame.size.height);
    } completion:^(BOOL comp){
        superView.frame = CGRectMake(superView.frame.origin.x,superView.frame.origin.y + 40,40,40);
        view.hidden = NO;
    }];
}

-(void)fadeIn{
    view.hidden = YES;
    UIView *superView = [self superview];
    superView.frame = CGRectMake(superView.frame.origin.x,superView.frame.origin.y - 40,self.base.frame.size.width,self.base.frame.size.height);
    [UIView animateWithDuration:0.25 animations:^{
        self.base.frame = CGRectMake(0,self.base.frame.origin.y,self.base.frame.size.width,self.base.frame.size.height);
    }];
}

-(void)chat:(UIButton *)but{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"sendConsole" object:self userInfo:@{@"type":@30}];
}
-(void)setBut:(UIButton *)but{
    [self.delegate setEditTab:but.tag];
}
@end
