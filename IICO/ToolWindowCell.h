//
//  ToolWindowCell.h
//  ICOI
//
//  Created by mokako on 2016/02/03.
//  Copyright © 2016年 moca. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ICOI.h"

@interface ToolWindowCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UILabel *icon;
@end
