//
//  DataManager.m
//  ICOI
//
//  Created by moca on 2014/01/02.
//  Copyright (c) 2014年 moca. All rights reserved.
//

#import "DataManager.h"

@implementation DataManager


-(void)dealloc
{
    
}



+(BOOL)textFiledCheck:(NSString *)str maxLength:(NSInteger)max minLength:(NSInteger)min
{
    if(str.length > max || str.length < min)
    {
        return NO;
    }
    NSError *error = nil;
    NSRegularExpression *regexp = [NSRegularExpression regularExpressionWithPattern:@"([^a-zA-Z0-9-_])" options:0 error:&error];
    __block NSUInteger count = 0;
    [regexp enumerateMatchesInString:str
                             options:0
                               range:NSMakeRange(0, [str length])
                          usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         if (++count >= 100) *stop = YES;
     }];
    return count == 0 ? YES : NO;
}

+(BOOL)serviceTypeCheck:(NSString *)str maxLength:(NSInteger)max minLength:(NSInteger)min
{
    if(str.length > max || str.length < min)
    {
        return NO;
    }
    NSError *error = nil;
    NSRegularExpression *regexp = [NSRegularExpression regularExpressionWithPattern:@"([^a-z0-9-])" options:0 error:&error];
    __block NSUInteger count = 0;
    [regexp enumerateMatchesInString:str
                             options:0
                               range:NSMakeRange(0, [str length])
                          usingBlock:^(NSTextCheckingResult *match, NSMatchingFlags flags, BOOL *stop)
     {
         if (++count >= 100) *stop = YES;
     }];
    return count == 0 ? YES : NO;
}



+(void)alert:(NSString *)title text:(NSString *)text
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:text
                                                   delegate:self
                                          cancelButtonTitle:nil
                                          otherButtonTitles:@"OK", nil];
    [alert show];
}


+(NSArray *)exceptFromArray:(NSMutableArray *)array object:(id)object
{
    [array removeObject:object];
    return array;
}


//data.plistのdataInfo配列をソートします。


+(NSArray *)sortDataInfo:(NSDictionary *)so array:(NSDictionary *)data
{
    //重複チェック
    return [[[NSSet alloc]initWithArray:[[so allKeys] arrayByAddingObjectsFromArray:[data allKeys]]] allObjects];
}
//
+(BOOL)checkDataInfo:(NSMutableArray *)so data:(NSDictionary *)dic
{
    BOOL flag = NO;
    NSInteger h = [so count];
    for(int i = 0; i < h; ++i)
    {
        NSDictionary *fo = [so objectAtIndex:i];
        if([fo[@"fileName"]isEqualToString:dic[@"fileName"]] == YES)
        {
            if([dic[@"fileName"] isEqualToString:@"Image"]){
                flag = YES;
            }
            if([fo[@"lastUpdate"] isEqualToDate:dic[@"lastUpdate"]] == YES){
                flag = YES;
            }
        }
    }
    return flag;
}

+(NSDictionary *)checkFileToDataList:(NSString *)serviceName dataList:(NSDictionary *)dic
{
    /*
    指定されたサービスのフォルダを取得して指定されたデータが有るかどうかを確認します。
    なければ要求を。あるなら放置させます。
    */
    NSMutableDictionary *newDic = [[NSMutableDictionary alloc]init];

    for(NSString *key in [dic allKeys])
    {
        NSMutableDictionary *al = [[NSMutableDictionary alloc]initWithDictionary:dic[key]];
        if([al[@"fileState"] integerValue] == 4){
            [FileManager removeFile:serviceName fileName:[NSString stringWithFormat:@"%@.%@",key,al[@"exte"]]];
            newDic[key] = al;
        }else{
            if([FileManager checkFile:serviceName fileName:[NSString stringWithFormat:@"%@.%@",key,al[@"exte"]]])
            {
                al[@"fileState"] = @"2";
            }else{
                al[@"fileState"] = @"0";
            }
            newDic[key] = al;
        }
    }
    return newDic;
}

//2つのデータを比較して振り分けます。
+(NSMutableDictionary *)checkDictionaryCompare:(NSMutableDictionary *)current new:(NSDictionary *)sub{
    [sub enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        if([current objectForKey:key] != nil){
            if([current[key][@"fileState"] integerValue] == 4 || [sub[key][@"fileState"] integerValue] == 4){
                current[key] = sub[key];
                current[key][@"fileState"] = [NSNumber numberWithInteger:4];
            }
        }else{
            current[key] = sub[key];
        }
    }];
    
    return current;
}


+(NSArray *)changeState:(NSArray *)array name:(NSString *)name state:(NSInteger)state
{
    NSMutableArray *a = [[NSMutableArray alloc]init];
    for(NSMutableDictionary *dic in array){
        if([dic[@"fileName"] isEqualToString:name]){
            dic[@"fileState"] = [NSNumber numberWithInteger:state];
        }
        [a addObject:dic];
    }
    return a;
}

+(NSString *)getHash
{
    CFUUIDRef uuidRef = CFUUIDCreate(kCFAllocatorDefault);
    CFStringRef uuidStr = CFUUIDCreateString(kCFAllocatorDefault,uuidRef);
    CFRelease(uuidRef);
    NSString *hash = [NSString stringWithFormat:@"%@", uuidStr];
    CFRelease(uuidStr);
    return hash;
}






#pragma mark - DATABASE
///////////////////// Database /////////////////////
//
/*
 id(-)
    fid(fileName) - ファイルの名前。表示名ではなくユニーク名で表示されます。
    pid(MCPeer ID) - 自分のユニークID
    uid(Unique ID) - パスのユニークID
    path(path) - パス情報
    paid(paret MCPeer ID)  - 親のユニークID
    paname(parent name) - 親のdisplayName
    pauid(path unique ID) - 親のパスのユニークID
    date - 日付
 
 
 
ステータス
 
 0 -  コメントなしのパス
 1 - コメントありのパス
 2 - 題目
 3 - 区切り
 4 - チャット
 
 */
 
 //NSString *sqlB = @"create table if not exists filename(id integer primary key autoincrement,filename text);";

// id fid pid uid path paid pauid date

+(void)initServiceTable:(FMDatabaseQueue *)queue
{
    NSString *sql = @"create table if not exists `service`(id integer primary key autoincrement, serviceName text, displayName text, displayUUID text, comment text, since real, date real);";
    [queue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:sql];
    }];
}


//定義変更及び絡むの追加はここで行ってください。
//ここを変更する場合必ずinitializeTableも同時に変更してください
+(void)updateDataTable:(FMDatabaseQueue *)queue{
    //fid, pid, pname, uid, path, paid, paname, pauid, comment, state, date, retain
    
    NSArray *update = @[
                        @"ALTER TABLE 'data' rename to 'temp'",
                        @"CREATE TABLE 'data'(`id` integer primary key AUTOINCREMENT,`fid` integer,`pid` text,`pname` text,`uid` text,`path` blob,`paid` text,`paname` text,`pauid` text,`comment` text, `state` integer,`date` real,`retain` integer NOT NULL)",
                        @"INSERT INTO 'data' SELECT temp.id, temp.fid, temp.pid, temp.pname, temp.uid, temp.path, temp.paid, temp.paname, temp.pauid, temp.comment, temp.state, temp.date, temp.retain FROM 'temp'",
                        @"DROP TABLE 'temp'"
                        ];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for(int i = 0; i < 4; i++)
        {
            if (![db executeUpdate:update[i]]) {
                *rollback = YES;
                return;
            }
        }
    }];
}

#pragma mark - create path table
+(void)initializeTable:(FMDatabaseQueue *)queue
{
    NSString *sqlA = @"create table if not exists `data`(id integer primary key autoincrement, fid integer, pid text, pname text, uid text, path blob, paid text, paname text, pauid text, comment text, state integer, date real,retain integer not null);";
    NSString *sqlB = @"create table if not exists fname(id integer primary key autoincrement,fid text);";
    [queue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:sqlA];
        [db executeUpdate:sqlB];
    }];
}

+(void)insertService:(FMDatabaseQueue *)queue serviceName:(NSString *)serviceName displayName:(NSString *)displayName displayUUID:(NSString *)uuid date:(NSDate *)date
{
    NSString *st = @"No comments yet.";
    [queue inDatabase:^(FMDatabase *db) {
        [db executeUpdate:@"insert into service(id,serviceName,displayName,displayUUID,comment,since,date) values(NULL,?,?,?,?,?,?)",
         serviceName,displayName,uuid,st,[NSNumber numberWithDouble:[DateFormatter julianDate:date]],[NSNumber numberWithDouble:[DateFormatter julianDate:date]]];
        
    }];
}
//コメントを編集
+(void)updateComment:(FMDatabaseQueue *)queue serviceName:(NSString *)serviceName commnet:(NSString *)comment
{
    NSString *sql = @"UPDATE service SET comment = ?, date = ? WHERE serviceName = ?";
    NSDate *date = [NSDate new];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,comment,[NSNumber numberWithDouble:[DateFormatter julianDate:date]],serviceName]) {
            *rollback = YES;
            return;
        }

    }];
}
//サービスログインの日付更新
+(void)updateServiceDate:(FMDatabaseQueue *)queue serviceName:(NSString *)serviceName
{
    NSString *sql = @"UPDATE service SET date = ? WHERE serviceName = ?";
    NSDate *date = [NSDate new];
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,[NSNumber numberWithDouble:[DateFormatter julianDate:date]],serviceName]) {
            *rollback = YES;
            return;
        }
    }];
}
//サービスを削除
+(void)deleteService:(FMDatabaseQueue *)queue serviceName:(NSString *)serviceName
{
    NSString *sql = @"DELETE FROM service WHERE serviceName = ?";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,serviceName]) {
            *rollback = YES;
            return;
        }
    }];
}
+(NSArray *)serviceList:(FMDatabaseQueue *)queue
{
    NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSString *sql = @"select id, serviceName, displayName, displayUUID, comment, datetime(since) as since, datetime(date) as date from service ORDER BY date ASC";
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            NSDictionary *dic = [[NSDictionary alloc]initWithObjectsAndKeys:
                                 [NSNumber numberWithInteger:[rs intForColumn:@"id"]] , @"id",
                                 [rs stringForColumn:@"serviceName"] , @"serviceName",
                                 [rs stringForColumn:@"displayName"] , @"displayName",
                                 [rs stringForColumn:@"displayUUID"] , @"displayUUID",
                                 [rs stringForColumn:@"comment"] , @"comment",
                                 [rs stringForColumn:@"since"] , @"since",
                                 [rs stringForColumn:@"date"] , @"date",
                                 nil];
            [ary addObject:dic];
        }
    }];
    return ary;
}

+(NSDictionary *)existService:(FMDatabaseQueue *)queue service:(NSString *)service
{
    __block NSDictionary *dic;
    NSString *sql = @"select id, serviceName, displayUUID, displayName, datetime(since) as since, datetime(date) as date from service where serviceName = ?";
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:sql];
        while ([rs next]) {
            dic =   [[NSDictionary alloc]initWithObjectsAndKeys:
                   [NSNumber numberWithInteger:[rs intForColumn:@"id"]] , @"id",
                   [rs stringForColumn:@"serviceName"] , @"serviceName",
                   [rs stringForColumn:@"displayUUID"] , @"displayUUID",
                   [rs stringForColumn:@"displayName"] , @"displayName",
                   [rs stringForColumn:@"since"] , @"since",
                   [rs stringForColumn:@"date"] , @"date",
                   nil];
        }
    }];
    return dic;
}

/*
+(void)readData:(FMDatabaseQueue *)queue name:(NSString *)name
{
    NSString *sql = @"select * from data where uid = ?";
    [queue open];
    
    FMResultSet *rs = [queue executeQuery:sql,name];
    while( [rs next] ){
        //@"uid %@",[rs stringForColumn:@"uid"];
        //@"retain %@",[NSNumber numberWithInteger:[rs intForColumn:@"retain"]];
    }
    [rs close];
    [queue close];
}
*/

+(NSInteger)getFileID:(FMDatabaseQueue *)queue name:(NSString *)fileid
{
    __block NSNumber *i;
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:@"select count(id) as count from fname where fid = ?",fileid];
        while ([rs next]) {
            while( [rs next] ){
                i = [NSNumber numberWithInteger:[rs intForColumn:@"count"]];
            }
        }
    }];
    return [i integerValue];
}

+(void)insertFileName:(FMDatabaseQueue *)queue name:(NSString *)name
{
    NSInteger i = [self getFileID:queue name:name];
    if(i == 0){
        [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
            if (![db executeUpdate:@"insert into fname(id,fid) values(NULL,?)",name]) {
                *rollback = YES;
                return;
            }
        }];
    }
}


+(void)addFileData:(FMDatabaseQueue *)queue data:(NSArray *)ary
{
    for(NSDictionary *dic in ary){
        [self insertFileName:queue name:dic[@"fileName"]];
    }
}

+(NSArray *)getAllFileName:(FMDatabaseQueue *)queue{
    __block NSMutableArray *ary = [[NSMutableArray alloc]init];
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:@"select fid from fname"];
        while([rs next]){
            [ary addObject:[rs stringForColumn:@"fid"]];
        }
    }];
    return ary;
}

#pragma mark - Path data

+(BOOL)insertOrUpdateList:(FMDatabaseQueue *)queue data:(NSArray *)list{
    __block int count = 0;
        NSString *sql1 = @"INSERT INTO data( fid, pid, pname, uid, path, paid, paname, pauid, comment, state, date, retain ) SELECT ( SELECT fname.id FROM fname WHERE fname.fid = ? ) ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,0 WHERE EXISTS ( SELECT fname.id FROM fname WHERE fname.fid = ? ) AND NOT EXISTS ( SELECT * FROM data WHERE data.uid = ? ) AND CASE WHEN EXISTS (SELECT * FROM data WHERE data.uid = ?) THEN 1 WHEN ? IS NULL THEN 1 WHEN EXISTS (SELECT * FROM data WHERE data.uid = ? AND data.state IN (5,6)) THEN 0 ELSE 0 END";
    
    NSString *sql2 = @"UPDATE data SET comment = ? ,date = ? WHERE data.uid = ? AND data.date < ?;";
    
    
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for(int i = 0; i < list.count; i++){
            BOOL re = [db executeUpdate:sql1 withArgumentsInArray:@[
                                                         list[i][@"fid"],
                                                         list[i][@"pid"],
                                                         list[i][@"pname"],
                                                         list[i][@"uid"],
                                                         [list[i][@"state"] integerValue] <= 1 ? [NSKeyedArchiver archivedDataWithRootObject:list[i][@"path"]] : list[i][@"path"],
                                                         list[i][@"paid"],
                                                         list[i][@"paname"],
                                                         list[i][@"pauid"],
                                                         list[i][@"comment"],
                                                         list[i][@"state"],
                                                         list[i][@"date"],
                                                         //settings
                                                         list[i][@"fid"],
                                                         list[i][@"uid"],
                                                         list[i][@"pauid"],
                                                         list[i][@"pauid"],
                                                         list[i][@"pauid"]
                                                         ]];
            count += re == NO ? 1 : 0;
            BOOL le = [db executeUpdate:sql2 withArgumentsInArray:@[
                                                                       list[i][@"comment"],
                                                                       list[i][@"date"],
                                                                       list[i][@"uid"],
                                                                       list[i][@"date"]
                                                                       ]];
            count += le == NO ? 1 : 0;
            if (re != YES || le != YES) {
                *rollback = YES;
                return;
            }
        }
        
    }];
    return count == 0 ? YES : NO;
}
+(void)addPathData:(FMDatabaseQueue *)queue data:(NSDictionary *)dic
{
    //WHEN EXISTS(SELECT * FROM 'data' WHERE data.uid = ? AND data.state NOT IN(2,3)) THEN 0
    NSString *sql = @"INSERT INTO data( fid, pid, pname, uid, path, paid, paname, pauid, comment, state, date, retain ) SELECT ( SELECT fname.id FROM fname WHERE fname.fid = ? ) ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,0 WHERE EXISTS ( SELECT fname.id FROM fname WHERE fname.fid = ? ) AND NOT EXISTS ( SELECT * FROM data WHERE data.uid = ? ) AND CASE WHEN EXISTS (SELECT * FROM data WHERE data.uid = ?) THEN 1 WHEN ? IS NULL THEN 1 WHEN EXISTS (SELECT * FROM data WHERE data.uid = ? AND data.state IN (5,6)) THEN 0 ELSE 0 END";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,
              dic[@"fid"],
              dic[@"pid"],
              dic[@"pname"],
              dic[@"uid"],
              [dic[@"state"] integerValue] <= 1 ? [NSKeyedArchiver archivedDataWithRootObject:dic[@"path"]] : dic[@"path"],
              dic[@"paid"],
              dic[@"paname"],
              dic[@"pauid"],
              dic[@"comment"],
              dic[@"state"],
              dic[@"date"],
              //条件
              dic[@"fid"],
              dic[@"uid"],
              dic[@"pauid"],
              dic[@"pauid"],
              dic[@"pauid"]
              
              ]) {
            *rollback = YES;
            return;
        }
    }];
}

#pragma mark -- パスの保持を変更
+(void)updatePath:(FMDatabaseQueue *)queue fid:(NSString *)fid uid:(NSString *)uid retain:(BOOL)save
{
    NSString *sql = @"UPDATE data SET retain = ? WHERE fid = (select fname.id from fname where fname.fid = ?) AND uid = ?";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,[NSNumber numberWithBool:save],fid,uid]) {
            *rollback = YES;
            return;
        }
    }];
}

//条件付きアップデート
+(BOOL)updateWhithDate:(FMDatabaseQueue *)queue list:(NSArray *)list{
    __block BOOL isSuccess = YES;
    NSString *updateSql = @"UPDATE 'data' SET pid = ?, pname = ?, 'path' = ?, 'paid' = ?, 'paname' = ?, 'pauid' = ?, 'comment' = ?, 'state' = ?, 'date' = ? WHERE data.uid = ? AND data.date < ?;";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for(int i = 0; i < list.count; i++){
            if (![db executeUpdate:updateSql,
                  list[i][@"pid"],
                  list[i][@"pname"],
                  [list[i][@"state"] integerValue] <= 1 ? [NSKeyedArchiver archivedDataWithRootObject:list[i][@"path"]] : list[i][@"path"],
                  list[i][@"paid"],
                  list[i][@"paname"],
                  list[i][@"pauid"],
                  list[i][@"comment"],
                  list[i][@"state"],
                  list[i][@"date"],
                  list[i][@"uid"],
                  list[i][@"date"]]) {
                *rollback = YES;
                isSuccess = NO;
                return;
            }
        }
    }];
    return isSuccess;
}
//内部的な更新時に使用します
+(BOOL)updateWhithList:(FMDatabaseQueue *)queue list:(NSArray *)list{
    __block BOOL isSuccess = YES;
    NSString *updateSql = @"UPDATE 'data' SET pid = ?, pname = ?, 'path' = ?, 'paid' = ?, 'paname' = ?, 'pauid' = ?, 'comment' = ?, 'state' = ?, 'date' = ? WHERE data.uid = ?;";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for(int i = 0; i < list.count; i++){
            if (![db executeUpdate:updateSql,
                  list[i][@"pid"],
                  list[i][@"pname"],
                  [list[i][@"state"] integerValue] <= 1 ? [NSKeyedArchiver archivedDataWithRootObject:list[i][@"path"]] : list[i][@"path"],
                  list[i][@"paid"],
                  list[i][@"paname"],
                  list[i][@"pauid"],
                  list[i][@"comment"],
                  list[i][@"state"],
                  list[i][@"date"],
                  list[i][@"uid"],
                  list[i][@"date"]]) {
                *rollback = YES;
                isSuccess = NO;
                return;
            }
        }
    }];
    return isSuccess;
}


+(BOOL)updateWithNumber:(FMDatabaseQueue *)queue list:(NSArray *)list{
    __block BOOL isSuccess = YES;
    NSString *updateSql = @"UPDATE data SET fid = (select fname.id from fname where fname.fid = ?),pid = ?,pname = ?,uid = ?,path = ?,paid = ?,paname = ?,pauid = ?,comment = ?,state = ?,date = ?,retain = ? WHERE id = ?";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for(int i = 0; i < list.count; i++){
            if (![db executeUpdate:updateSql,
                  list[i][@"fid"],
                  list[i][@"pid"],
                  list[i][@"pname"],
                  list[i][@"uid"],
                  [list[i][@"state"] integerValue] <= 1 ? [NSKeyedArchiver archivedDataWithRootObject:list[i][@"path"]] : list[i][@"path"],
                  list[i][@"paid"],
                  list[i][@"paname"],
                  list[i][@"pauid"],
                  list[i][@"comment"],
                  list[i][@"state"],
                  list[i][@"date"],
                  list[i][@"retain"],
                  list[i][@"nid"]]) {
                *rollback = YES;
                isSuccess = NO;
                return;
            }
        }
    }];
    return isSuccess;
}



+(NSDictionary *)getItemForUID:(FMDatabaseQueue *)queue uid:(NSString *)uid{
    __block NSMutableDictionary *dic;
    NSString *sql = @"select *,d.id as nid FROM data as 'd' LEFT JOIN fname as 'f' ON f.id = d.fid where 'data'.uid = ?" ;
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:sql,uid];
        while([rs next]){
            dic = [[rs resultDictionary] mutableCopy];
            dic[@"path"] = [dic[@"state"] integerValue] <= 1 ? [NSKeyedUnarchiver unarchiveObjectWithData:dic[@"path"]] : dic[@"path"];
        }
    }];
    return dic;
}

+(NSDictionary *)getItem:(FMDatabaseQueue *)queue fileID:(NSString *)fileID uid:(NSString *)uid{
    __block NSMutableDictionary *dic;
    NSString *sql = @"select * ,d.id as nid FROM data as d LEFT JOIN fname as f ON f.id = d.fid where d.fid = ( SELECT fname.id FROM fname WHERE fname.fid = ? ) and d.uid = ?";
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:sql,fileID,uid];
        while([rs next]){
            dic = [[rs resultDictionary] mutableCopy];
            dic[@"path"] = [dic[@"state"] integerValue] <= 1 ? [NSKeyedUnarchiver unarchiveObjectWithData:dic[@"path"]] : dic[@"path"];
        }
    }];
    return dic;
}


+(NSArray *)getTreeList:(FMDatabaseQueue *)queue fileID:(NSString *)fileID{
    __block NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSString *sql = @"SELECT * ,d.id as nid FROM data as 'd' LEFT JOIN fname as 'f' ON f.id = d.fid WHERE d.fid = ( SELECT f.id FROM fname as 'f' WHERE f.fid = ? ) AND d.state = 2 order by d.id ";
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:sql,fileID];
        while([rs next]){
            NSMutableDictionary *dic = [[rs resultDictionary] mutableCopy];
            dic[@"path"] = [dic[@"state"] integerValue] <= 1 ? [NSKeyedUnarchiver unarchiveObjectWithData:dic[@"path"]] : dic[@"path"];
            [ary addObject:dic];
        }
    }];
    return ary;
}


+(NSArray *)getBranch:(FMDatabaseQueue *)queue fileID:(NSString *)fileID uid:(NSString *)uid mode:(GetDataMode)mode{
    __block NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSString *sql;
    if(mode == GetDataAll){//主キーを含む主キーに連なるデータを全取得
        sql = @"WITH RECURSIVE 'new' as (SELECT * FROM 'data' WHERE 'data'.uid = ? UNION ALL SELECT 'data'.* FROM 'data', 'new' WHERE  'data'.pauid = 'new'.uid) SELECT * ,new.id as nid FROM 'new' LEFT JOIN fname as 'f' ON f.id = new.fid WHERE 'new'.fid = ( SELECT fname.id FROM fname WHERE fname.fid = ? ) and state in (0,1,2,3,4) order by 'new'.id;";
    }else if(mode == GetDataLevel){//主キーに連なる副キーのみを取得
        sql = @"WITH RECURSIVE 'new' as (SELECT * FROM 'data' WHERE 'data'.uid = ? UNION ALL SELECT 'data'.* FROM 'data', 'new' WHERE  'data'.pauid = 'new'.uid) SELECT * ,new.id as nid FROM 'new' LEFT JOIN fname as 'f' ON f.id = new.fid   WHERE 'new'.fid = ( SELECT fname.id FROM fname WHERE fname.fid = ? ) AND 'new'.state = 3  order by 'new'.id;";
        
    }else if (mode == GetDataOther){//主キーの無いメッセージ
        sql = @"select * ,data.id as nid from 'data' LEFT JOIN fname as 'f' ON f.id = data.fid where 'data'.fid = (SELECT fname.id FROM fname WHERE fname.fid = ?) and 'data'.pauid is null and state in (0,1,4) order by 'data'.id;";
        
    }else if(mode == GetDataLevelOther){//主キーのあるメッセージ
        sql = @"SELECT * ,data.id as nid FROM 'data' LEFT JOIN fname as 'f' ON f.id = data.fid WHERE 'data'.fid = ( SELECT fname.id FROM fname WHERE fname.fid = ? ) AND 'data'.pauid = ? AND 'data'.state IN (0,1,4)  order by 'data'.id;";
        
    }else if (mode == GetDataALL_2){
        sql = @"WITH RECURSIVE 'new' as (SELECT * FROM 'data' WHERE 'data'.uid = ? UNION ALL SELECT 'data'.* FROM 'data', 'new' WHERE  'data'.pauid = 'new'.uid) SELECT * ,new.id as nid FROM 'new' LEFT JOIN fname as 'f' ON f.id = new.fid   WHERE 'new'.fid = ( SELECT fname.id FROM fname WHERE fname.fid = ?) AND CASE WHEN 2 = (SELECT state FROM new WHERE new.uid = ?) THEN new.state NOT IN (2) ELSE new.state NOT IN (3) END order by 'new'.id;";
        
    }else if (mode == GetRestoreData){
        sql = @"select * ,data.id as nid from 'data' LEFT JOIN fname as 'f' ON f.id = data.fid where 'data'.fid = (SELECT fname.id FROM fname WHERE fname.fid = ?) and state in (5,6) order by 'data'.id;";
        
    }
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs;
        switch (mode) {
            case GetDataAll:
                rs = [db executeQuery:sql,uid,fileID];
                break;
            case GetDataLevel:
                rs = [db executeQuery:sql,uid,fileID];
                break;
            case GetDataOther:
                rs = [db executeQuery:sql,fileID];
                break;
            case GetDataLevelOther:
                rs = [db executeQuery:sql,fileID,uid];
                break;
            case GetDataALL_2:
                rs = [db executeQuery:sql,uid,fileID,uid];
                break;
            case GetRestoreData:
                rs = [db executeQuery:sql,fileID];
                break;
            default:
                break;
        }
        while([rs next]){
            NSMutableDictionary *dic = [[rs resultDictionary] mutableCopy];
            dic[@"path"] = [dic[@"state"] integerValue] <= 1 ? [NSKeyedUnarchiver unarchiveObjectWithData:dic[@"path"]] : dic[@"path"];
            [ary addObject:dic];
        }
    }];
    return ary;
}

+(NSArray *)getMessageList:(FMDatabaseQueue *)queue{
    __block NSMutableArray *ary = [[NSMutableArray alloc]init];
    NSString *sql =  @"select *, data.id as nid from 'data' left join 'fname' on 'fname'.id = 'data'.fid where 'data'.state IN (2,3) order by 'data'.id";
    [queue inDatabase:^(FMDatabase *db) {
        FMResultSet *rs = [db executeQuery:sql];
        while([rs next]){
            NSMutableDictionary *dic = [[rs resultDictionary] mutableCopy];
            dic[@"path"] = [dic[@"state"] integerValue] <= 1 ? [NSKeyedUnarchiver unarchiveObjectWithData:dic[@"path"]] : dic[@"path"];
            [ary addObject:dic];
        }
    }];
    return ary;
}


//メッセージを除外処理を施し特定の操作をしなければ復活不可能でかつ親としての機能を停止させます
-(BOOL)omitMessageList:(FMDatabaseQueue *)queue fileID:(NSString *)fileID list:(NSArray *)list{
    
    
    
    
    
    
    return YES;
}

+(BOOL)deleteSelectedPaths:(FMDatabaseQueue *)queue fileID:(NSString *)fileID list:(NSArray *)list{
    __block BOOL isSuccess = YES;
    NSString *sql = @"DELETE FROM data WHERE data.fid = (SELECT fname.id FROM fname WHERE fname.fid = ?) AND data.uid = ? AND data.retain = 0";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        for(NSDictionary *dic in list){
            if (![db executeUpdate:sql,fileID,dic[@"uid"]]){
                *rollback = YES;
                isSuccess = NO;
                return;
            }
        }
    }];
    return isSuccess;;
}

+(BOOL)deleteSelectedPath:(FMDatabaseQueue *)queue fileID:(NSString *)fileID uid:(NSString *)uid
{
    __block BOOL isSuccess = YES;
    NSString *sql = @"DELETE FROM data WHERE  data.fid = (SELECT fname.id FROM fname WHERE fname.fid = ?) AND data.uid = ? AND data.retain = 0";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,fileID,uid]) {
            *rollback = YES;
            isSuccess = NO;
            return;
        }
    }];
    return isSuccess;
}


//ファイルに属するデータを一括で削除します
+(BOOL)deletePath:(FMDatabaseQueue *)queue fileID:(NSString *)fileID
{
    __block BOOL isSuccess = YES;
    NSString *sql = @"DELETE FROM data WHERE  data.fid = (SELECT fname.id FROM fname WHERE fname.fid = ?) AND data.retain = 0";
    [queue inTransaction:^(FMDatabase *db, BOOL *rollback) {
        if (![db executeUpdate:sql,fileID]) {
            *rollback = YES;
            isSuccess = NO;
            return;
        }
    }];
    return isSuccess;
}



@end
